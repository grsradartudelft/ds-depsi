function save_to_shp(dsStmList,options)

%% Read original shapefile data
if isfield(options,'shapefilePath')
    shpPath     = options.shapefilePath;
else
    shpPath     = dsStmList(1).techniqueAttrib.shp_path;
    if strcmp(shpPath(1:18),'/project/caroline/')
        idx     = strfind(shpPath,'contextual_data');
        shpPath = ['/Users/pconroy/phd/projects/' shpPath(idx:end)];
    end
end
S  = shaperead(shpPath);

%% Get parcel IDs from shapefile
if isfield(S,'int_id')
    ids         = [S.int_id];
else
    ids         = [S.id];
end

%% Loop over shapefile and add model params as new field
for j = 1:length(S)
    pointIdx = find(ismember(dsStmList(1).pntAttrib.parcelId,ids(j)));
    if isempty(pointIdx)
        S(j).x_P = nan;
        S(j).x_E = nan;
        S(j).x_I = nan;
        S(j).tau = nan;
    else
        if isnan(dsStmList(1).pntAttrib.finalModelParams(pointIdx,:))
            S(j).x_P = nan;
            S(j).x_E = nan;
            S(j).x_I = nan;
            S(j).tau = nan;
        else
            x_P = dsStmList(1).pntAttrib.finalModelParams(pointIdx,1)*dsStmList(1).pntAttrib.finalModelParams(pointIdx,2);
            x_E = dsStmList(1).pntAttrib.finalModelParams(pointIdx,1);
            x_I = dsStmList(1).pntAttrib.finalModelParams(pointIdx,3);
            tau = round(dsStmList(1).pntAttrib.finalModelParams(pointIdx,4));
            S(j).x_P = x_P;
            S(j).x_E = x_E;
            S(j).x_I = x_I;
            S(j).tau = tau;
        end
    end
end

%% Save new shapefile
[filepath,name,ext] = fileparts(shpPath);
outFile = fullfile(filepath,[name '_spams' ext]);
shapewrite(S,outFile)


