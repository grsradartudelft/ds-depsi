function stm_linear_plot(dsStmList,options)
warning('off','MATLAB:polyshape:repairedBySimplify')

dsStmList(1).pntAttrib.linRate = nan(dsStmList(1).numPoints,1);

if isa(dsStmList(1).pntAttrib.groupId,'string')
    dsStmList(1).pntAttrib.groupId   = str2double(dsStmList(1).pntAttrib.groupId);
    dsStmList(1).auxData.groupIdList = str2double(dsStmList(1).auxData.groupIdList);
end

if isfield(options,'shapefilePath')
    shpPath     = options.shapefilePath;
else
    shpPath     = dsStmList(1).techniqueAttrib.shp_path;
    if strcmp(shpPath(1:18),'/project/caroline/')
        idx     = strfind(shpPath,'contextual_data');
        shpPath = ['/Users/pconroy/phd/projects/' shpPath(idx:end)];
    end
end
S           = shaperead(shpPath);
if isfield(S,'int_id')
    ids         = [S.int_id];
else
    ids         = [S.id];
end

pointIdx    = find(dsStmList(1).pntAttrib.groupId ~= 0);
%parcelIds   = dsStmList(1).pntAttrib.parcelId(pointIdx);
nPoints     = length(pointIdx);

for i = 1:length(dsStmList)
    dsStmList(i).pntAttrib.linDefo = nan(size(dsStmList(1).pntAttrib.parcelId));
end

shapeIdx = [];
for j = 1:length(S)
    
    pointIdx = find(ismember(dsStmList(1).pntAttrib.parcelId,ids(j)));
    
    if ~isempty(pointIdx)
        groupIdx    = find(dsStmList(1).auxData.groupIdList == dsStmList(1).pntAttrib.groupId(pointIdx));
    
        if isempty(groupIdx)
            linRate(j)  = nan;
        else
            if options.method == 1 || options.method == 2
                % Seasonal plus linear model
                linRate(j)  = dsStmList(1).auxData.groupModelParams(groupIdx,3);
                shapeIdx    = [shapeIdx; j];
            else
                
                % Hydrological model
                if size(dsStmList(1).auxData.precipitation,1) > 1
                    stationIdx  = dsStmList(1).auxData.knmiStations == dsStmList(1).pntAttrib.knmi_id(pointIdx);
                    precip      = dsStmList(1).auxData.precipitation(stationIdx,:);
                    evap        = dsStmList(1).auxData.evapotrans(stationIdx,:);
                else
                    precip      = dsStmList(1).auxData.precipitation;
                    evap        = dsStmList(1).auxData.evapotrans;
                end
                
                if iscolumn(precip)
                    precip = precip';
                end
                if iscolumn(evap)
                    evap = evap';
                end
                
                params      = dsStmList(1).pntAttrib.finalModelParams(pointIdx,:);
                modelFit    = p_model_lin(precip,evap,params)';
                t           = years(dsStmList(1).auxData.knmiDates-dsStmList(1).auxData.knmiDates(1));
                
                % Remove nans
                t(isnan(modelFit))          = [];
                modelFit(isnan(modelFit))   = [];
                
                p           = polyfit(t,modelFit,1);
                linRate(j)  = p(1);
                shapeIdx    = [shapeIdx; j];
                
                dsStmList(1).pntAttrib.linRate(pointIdx) = linRate(j);
            end
        end
    else
        linRate(j)  = nan;
    end
    
    X{j}        = S(j).X;
    Y{j}        = S(j).Y;
end

normRate = (linRate-options.lin.cbarLim(1))/(options.lin.cbarLim(2)-options.lin.cbarLim(1));
normRate(normRate > 1) = 1;
normRate(normRate < 0) = 0;

% Plot colour from linear deformation rate
bgFlag      = 0;
cmap        = turbo(512);
cmap        = flipud(cmap(257:end,:));
colorRow    = round(normRate * 255)+1;

%% Background Image
if options.lin.showBackground == 1 && isfield(options,'background') 
    shpInfo     = shapeinfo(shpPath);
    boundingBox = shpInfo.BoundingBox;
    latLim      = [boundingBox(1,2), boundingBox(2,2)];
    lonLim      = [boundingBox(1,1), boundingBox(2,1)];
    im          = flipud(imread(options.background));
    bgFlag      = 1;
elseif options.lin.showBackground == 1 && ~isfield(options,'background')
    warning('No background image supplied')
end
%% Plot shapefile

figure('Position',[409 117 1045 860],'color','w');
% title('Estimated Linear Subsidence Rate','fontsize',14)
mapshow(S,'FaceColor','none','LineWidth',0.05,'EdgeAlpha',0.2);
if bgFlag == 1
    hold on
    b=image(lonLim,latLim,im);
    uistack(b,'bottom')
end
axis square
set(gcf,'color','w')
k=colorbar('south');
set(get(k,'title'),'string','mm/y','fontsize',14,'color','w');
set(k,'color','w');
% k.Position = [.79 .15 .025 .1];
k.Position = [.73 .15 .1 .025];
colormap(cmap)
set(gca, 'fontsize',14)
caxis(options.lin.cbarLim*1000)

a=gca;
% Inner loop: plot each polygon
for j = 1:nPoints
    if ~isnan(normRate(j))
        color_row = round(normRate(j) * 255)+1;
        cc = cmap(color_row,:);
        set(a.Children(1).Children(j),'FaceColor',cc);
    end
end
axis tight
end

