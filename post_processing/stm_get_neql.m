function [L,dates] = stm_get_neql(dsStmList,options)
% Get the number of equlivalent looks for every epoch in a group time
% series

dates = [];
L = [];
for i = 1:length(dsStmList)
    pntIdx      = find(ismember(dsStmList(i).pntAttrib.groupId,options.group.groupSel));
    OSR         = dsStmList(i).techniqueAttrib.prf/dsStmList(i).techniqueAttrib.az_bw * dsStmList(i).techniqueAttrib.r_fs/dsStmList(i).techniqueAttrib.r_bw;
    numPixels   = dsStmList(i).pntAttrib.nPixels(pntIdx);
    avail       = sum(~isnan(dsStmList(i).obsData.vertUnwPhase(pntIdx,:)).*numPixels,1);
    L           = [L, avail / OSR];  
    dates       = [dates, dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day')];
end

[dates,order]   = sort(dates,'ascend');
L               = L(order);

end