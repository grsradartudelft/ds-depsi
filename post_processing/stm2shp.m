function stm2shp(stm,outputpath)

lat     = stm.pntCrd(:,1);
lon     = stm.pntCrd(:,2);
type    = stm.pntAttrib.type;

S = geoshape(lat,lon,'Geometry','point');
S.Type = type;


shapewrite(S,outputpath)

end

