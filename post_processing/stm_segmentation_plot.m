function stm_segmentation_plot(dsStmList,options)

%% Detect the coherent segments

pntIdx      = find(ismember(dsStmList(1).pntAttrib.groupId,options.seg.groupSel));

if isempty(pntIdx)
    error('Invalid group selection')
end

i = options.seg.track;
epochIdx    = find(dsStmList(i).epochAttrib.timestamp >= options.seg.startDate & dsStmList(i).epochAttrib.timestamp <= options.seg.endDate);
coherence   = dsStmList(i).obsData.dcCoherence(pntIdx,epochIdx);
nParcels    = size(coherence,1);
segVec      = [];

for j = 1:nParcels
    [nSegments(j),segIdx{j},segLength{j},segVec(j,:)] = segmentation(coherence(j,:),options.seg.minCoherence,options.seg.minEpochs);
end

segIdx{j}           = segIdx{j}';
segLength{j}        = segLength{j}';
tempCoverage        = sum(~isnan(segVec),2)/size(segVec,2);
[~,coverageRank]    = sort(tempCoverage,'descend');
rankedPoints        = pntIdx(coverageRank);
segVecRanked        = segVec(coverageRank,:);

if options.seg.background == 1
    density = sum(~isnan(segVec),1)/nParcels;
end

%% Plot
figure
hold on
if options.seg.background == 1
    cmap = gray(100);
%     xvals = [dsStmList(i).epochAttrib.timestamp(epochIdx(1)),dsStmList(i).epochAttrib.timestamp(epochIdx(1)),dsStmList(i).epochAttrib.timestamp(epochIdx(end)),dsStmList(i).epochAttrib.timestamp(epochIdx(end))];
%     yvals = [0,size(segVecRanked,1),size(segVecRanked,1),0];
%     fill(xvals,yvals,cmap(round(density*100),:))
    
    for j = 1:length(density)-1
        xvals = [dsStmList(i).epochAttrib.timestamp(epochIdx(j)),dsStmList(i).epochAttrib.timestamp(epochIdx(j)),dsStmList(i).epochAttrib.timestamp(epochIdx(j+1)),dsStmList(i).epochAttrib.timestamp(epochIdx(j+1))];
        yvals = [0,size(segVecRanked,1),size(segVecRanked,1),0];
        fill(xvals,yvals,cmap(round(density(j)*100),:),'FaceAlpha',.5,'linestyle','none')
    end
    
end

for j = 1:size(segVecRanked,1)
    plot(dsStmList(i).epochAttrib.timestamp(epochIdx),j*segVecRanked(j,:),'LineWidth',3)
    ticklabels{j} = num2str(dsStmList(i).pntAttrib.parcelId(rankedPoints(j)));
end

yticks(1:size(segVecRanked,1));
yticklabels(ticklabels);
ylabel('Parcel ID')
title(['Pointwise Segmentation Diagram (' dsStmList(i).datasetId ')'],'Interpreter', 'none')
set(gca,'FontSize',14)
set(gcf,'color','w')
grid on



end