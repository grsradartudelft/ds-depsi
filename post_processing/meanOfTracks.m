function [meanDefoAll, dates] = meanOfTracks(dsStmList,groupIdx)

combOutputTT = timetable();
for i = 1:length(dsStmList)
    ph2v            = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    medianDefo      = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
    
    dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
    
    trackOutputTT   = timetable(dates,medianDefo);
    combOutputTT    = synchronize(combOutputTT,trackOutputTT);
end

[meanDefoAll, dates] = combine_tracks(combOutputTT,7);
end

