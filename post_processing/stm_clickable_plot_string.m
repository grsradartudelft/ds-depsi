function stm_clickable_plot_string(dsStmList,options)
groups = dsStmList(1).auxData.groupIdList;

% Old results have group IDs as a double
if isa(groups,'double')
    groups = string(num2str(groups));
    for i = 1:length(dsStmList)
        dsStmList(i).auxData.groupIdList = string(num2str(dsStmList(i).auxData.groupIdList));
        dsStmList(i).pntAttrib.groupId = string(num2str(dsStmList(i).pntAttrib.groupId));
        dsStmList(i).pntAttrib.groupId(strcmp(dsStmList(i).pntAttrib.groupId,"    0")) = "";
    end
end

usrIn=[];
while isempty(usrIn)
    
    
    % Base plot of all points coloured by peilgebied
    close all
    figure
    hold on
    plotColours = jet(length(groups));
    for p = 1:length(groups)
        pntIdx = strcmp(dsStmList(1).pntAttrib.groupId,groups(p));
        plot(dsStmList(1).pntCrd(:,2),     dsStmList(1).pntCrd(:,1),'k.','HandleVisibility','off')
        plot(dsStmList(1).pntCrd(pntIdx,2),dsStmList(1).pntCrd(pntIdx,1),'*','color',plotColours(p,:));
        legendstr{p} = groups(p);
    end
    legend(legendstr,'location','northeastoutside')
    set(gcf,'color','w')
    
    % Get user input to select a peilgebied
    [x,y]=ginput(1);
    
    % Need to define a list of "used points" here, then get group ID of
    % nearest point
    
    usedPoints      = find(~strcmp(dsStmList(1).pntAttrib.groupId,""));
    
    [~,nearestPnt]  = min(hypot(dsStmList(1).pntCrd(usedPoints,2)-x,dsStmList(1).pntCrd(usedPoints,1)-y));
    groupSel        = dsStmList(1).pntAttrib.groupId(usedPoints(nearestPnt));
    
    groupIdx        = find(strcmp(groups,groupSel));
    pointIdx        = find(strcmp(dsStmList(1).pntAttrib.groupId,groupSel))';
    soilCode        = string(mode(categorical(dsStmList(1).pntAttrib.soilcode(pointIdx))));
    
    % Plot mean deformation of selected peilgebied
    if options.plotMeanDefo == 1
        figure
        hold on
        legendtext = [];
        combOutputTT = timetable();
        for i = 1:length(dsStmList)
            ph2v            = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
            medianDefo      = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
            
            % Calculate shift w.r.t. ext data for comparison
            dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
            
            % Plot time series
            plot(dsStmList(i).epochAttrib.timestamp,medianDefo,'LineWidth',1)
            legendtext      = [legendtext string(dsStmList(i).datasetId)];
            
            trackOutputTT   = timetable(dates,medianDefo);
            combOutputTT    = synchronize(combOutputTT,trackOutputTT);
        end
        [meanDefoAll, dates] = combine_tracks(combOutputTT,7);
        shift                = mean(meanDefoAll-table2array(combOutputTT(:,1)),'omitnan');
        plot(dates,meanDefoAll-shift,'b','LineWidth',2)
        legendtext = [legendtext "Combined InSAR"];
%             if peilgbdSel == 7185
%                 plot(extTT.Time_CET,extTT.Displacement_mm,'k','LineWidth',2)
%                 legendtext = [legendtext "Extensometer"];
%             end
        legend(legendtext,'interpreter','none')
        title(['Group ' char(groupSel) ',  Soil code: ' char(soilCode)])
        ylabel('Relative Vertical Position [mm]')
        set(gcf,'color','w')
        set(gca,'fontsize',14)
        grid on
    end
    
    % Plot "spaghetti" of every segment in selected group
    if options.plotSpaghetti == 1
        legendtext = [];
        figure
        hold on
        for i = 1:length(dsStmList)
            ph2v         = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
            medianDefo   = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
            pointIdx     = find(dsStmList(i).pntAttrib.groupId == groupSel)';
            vertSegments = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)*ph2v*1000;
            
            plot(dsStmList(i).epochAttrib.timestamp,vertSegments,'handlevisibility','off')
        end
        
        % Plot fitted model used to reconnect segments
        if options.plotFittedModel == 1
            
            dates = [];
            for i = 1:length(dsStmList)
                dates   = [dates dsStmList(i).epochAttrib.timestamp];
            end
            dates       = sort(dates,'ascend');
            doy         = day(dates,'dayofyear')/365.25;
            t           = unwrap(doy*2*pi)/(2*pi);
            params      = dsStmList(1).auxData.groupModelParams(groupIdx,:); % Same for all tracks
            modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
            
            plot(dates,modelFit*1000,'k','LineWidth',3)
            legendtext = [legendtext "Fitted Model"];
        end
        
        if options.plotMeanDefo == 1
            plot(dates,meanDefoAll,'b','LineWidth',3)
            legendtext = [legendtext "Combined InSAR"];
        end
        
        title(['Group ' char(groupSel) ',  Soil code: ' char(soilCode)])
        ylabel('Relative Vertical Position [mm]')
        ylim([-100 100])
        set(gcf,'color','w')
        set(gca,'fontsize',14)
        grid on
        
        if ~isempty(legendtext)
            legend(legendtext)
        end        
        
    end
    usrIn = input('Click a peilgebied (enter to reset, any key+enter to quit): ',"s");
end

