function [diffModel, time] = meteo_regression(dsStmList, options)

for i = 1:length(dsStmList)
    
    % Rerun this to make sure all the correct data is included
    knmiFile            = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/oct2022/etmgeg_348_cabauw.txt';
    dsStmList(i)        = stm_add_knmi(dsStmList(i),knmiFile);
    
    
    pntIdx              = find(ismember(dsStmList(i).pntAttrib.groupId,options.group.groupSel));
    nPoints             = length(pntIdx);
    
    ph2v                = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    vertSegments        = dsStmList(i).obsData.vertUnwPhase(pntIdx,:)*ph2v; % Convert to m
    
    unshiftedSegs{i}    = nan(size(vertSegments));
    diffSegs{i}         = nan(size(vertSegments));
    
    for j = 1:nPoints
        
        % Find starting and ending indices of each segment
        segVec      = ~isnan(vertSegments(j,:));
        aa          = [0,segVec,0];
        segStart    = strfind(aa, [0 1]);
        segEnd      = strfind(aa, [1 0]) - 1;
        
        % Remove estimated shift
        for k = 1:length(segStart)
            unshiftedSegs{i}(j,segStart(k):segEnd(k)) = vertSegments(j,segStart(k):segEnd(k)) - vertSegments(j,segStart(k));
            diffSegs{i}(j,segStart(k)+1:segEnd(k))  = diff(unshiftedSegs{i}(j,segStart(k):segEnd(k)));
        end
    end
    
    % Find corresponding meteo data
    epochIdx    = find(ismember(dsStmList(i).auxData.knmiDates,dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day')));
    
    temperature = dsStmList(i).auxData.temperature';
    normTemp    = temperature/max(temperature);
    
    precip      = dsStmList(i).auxData.precipitation';
    normPrecip  = precip/(2*std(precip));
    normPrecip(normPrecip > 1) = 1;
    
    evapotrans  = dsStmList(i).auxData.evapotrans';
    normEvap    = evapotrans/max(evapotrans);
    
    % Linear regression
    y = median(diffSegs{i},1,'omitnan');
%     y = median(unshiftedSegs{i},1,'omitnan');
    nanIdx = ~isnan(y);
    usedIdx = epochIdx(nanIdx);
    
    Pt(i,:) = polyfit(normTemp(usedIdx)  ,y(nanIdx),1);
    tFit = Pt(i,1)*normTemp   + Pt(i,2);
    
    Pp(i,:) = polyfit(normPrecip(usedIdx),y(nanIdx),1);
    pFit = Pp(i,1)*normPrecip + Pp(i,2);
    
    Pe(i,:) = polyfit(normEvap(usedIdx),  y(nanIdx),1);
    eFit = Pe(i,1)*normEvap   + Pe(i,2);
    
%     Plot relationships
    figure
    hold on
    plot(normPrecip(epochIdx),median(diffSegs{i},1,'omitnan'),'.')
    plot(normPrecip,pFit)
    
    figure
    hold on
    plot(normEvap(epochIdx),median(diffSegs{i},1,'omitnan'),'.')
    plot(normEvap,eFit)
    
end

groupIdx = find(ismember(dsStmList(1).auxData.groupIdList,options.group.groupSel));
scale = dsStmList(1).auxData.groupModelParams(groupIdx)*2;

% model = normTemp*mean(Pt(:,1)) + normPrecip*mean(Pp(:,1)) + normEvap*mean(Pe(:,1));
diffModel = normPrecip*mean(Pp(:,1)) + normEvap*mean(Pe(:,1));
% diffModel = smoothdata(diffModel);
diffModel = diffModel - mean(diffModel);
% diffModel = diffModel * scale/max(abs(diffModel));

model     = cumsum(diffModel);

time  = dsStmList(i).auxData.knmiDates;

figure
plot(time,diffModel)
grid on

figure
plot(time,model)
grid on


end

