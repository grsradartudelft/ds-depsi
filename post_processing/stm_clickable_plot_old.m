function stm_clickable_plot_old(dsStmList,options)
close all
groups = dsStmList(1).auxData.groupIdList;

usrIn=[];
while isempty(usrIn)
    
    % Base plot of all points coloured by peilgebied
    %close all
    figure(1)
    hold on
    plotColours = jet(length(groups));
    for p = 1:length(groups)
        pntIdx = ismember(dsStmList(1).pntAttrib.groupId,groups(p));
        plot(dsStmList(1).pntCrd(:,2),     dsStmList(1).pntCrd(:,1),'k.','HandleVisibility','off')
        plot(dsStmList(1).pntCrd(pntIdx,2),dsStmList(1).pntCrd(pntIdx,1),'*','color',plotColours(p,:));
        legendstr{p} = num2str(groups(p));
    end
    legend(legendstr,'location','northeastoutside')
    set(gcf,'color','w')
    
    % Get user input to select a group
    [x,y]=ginput(1);
    
    % Need to define a list of "used points" here, then get group ID of
    % nearest point
    
    usedPoints      = find(~isnan(dsStmList(1).pntAttrib.groupId));
    
    [~,nearestPnt]  = min(hypot(dsStmList(1).pntCrd(usedPoints,2)-x,dsStmList(1).pntCrd(usedPoints,1)-y));
    groupSel        = dsStmList(1).pntAttrib.groupId(usedPoints(nearestPnt));
    
    groupIdx        = find(ismember(groups,groupSel));
    pointIdx        = find(ismember(dsStmList(1).pntAttrib.groupId,groupSel))';
    soilCode        = dsStmList(1).pntAttrib.soilcode(pointIdx(1));
    cropCode        = dsStmList(1).pntAttrib.cropcode(pointIdx(1));
    ghg             = dsStmList(1).pntAttrib.ghg(pointIdx(1))*-10;
    glg             = dsStmList(1).pntAttrib.glg(pointIdx(1))*-10;
    params          = dsStmList(1).auxData.groupModelParams(groupIdx,:); % Same for all tracks
    
    figure('position',[300 300 1100 600])
    hold on
    legendtext = [];
    % Plot mean deformation of selected peilgebied
    if options.click.plotMeanDefo == 1
        combOutputTT = timetable();
        for i = 1:length(dsStmList)
            ph2v            = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
            medianDefo      = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
            
            % Calculate shift w.r.t. ext data for comparison
            dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
            
            % Plot time series
            plot(dsStmList(i).epochAttrib.timestamp,medianDefo,'LineWidth',1)
            legendtext      = [legendtext string(dsStmList(i).datasetId)];
            
            trackOutputTT   = timetable(dates,medianDefo);
            combOutputTT    = synchronize(combOutputTT,trackOutputTT);
        end
       
        [meanDefoAll, datesComb] = combine_tracks(combOutputTT,7);
        combOutputTT         = synchronize(combOutputTT,timetable(datesComb,meanDefoAll));
        shift                = mean(combOutputTT.(length(dsStmList)+1)-combOutputTT.(1),'omitnan');
%         plot(dates,meanDefoAll-shift,'b','LineWidth',2)
%         legendtext = [legendtext "Combined InSAR"];
        legend(legendtext,'interpreter','none')
        
        
    end
    
    % Plot "spaghetti" of every segment in selected group
    if options.click.plotSpaghetti == 1
        
        for i = 1:length(dsStmList)
            ph2v         = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
            medianDefo   = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
            pointIdx     = find(dsStmList(i).pntAttrib.groupId == groupSel)';
            vertSegments = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)*ph2v*1000 - shift;
            
            plot(dsStmList(i).epochAttrib.timestamp,vertSegments,'handlevisibility','off')
        end
        
        % Plot fitted model used to reconnect segments
        if options.click.plotFittedModel == 1
            if options.method == 3 || options.method == 4
                precip      = dsStmList(1).auxData.precipitation;
                evap        = dsStmList(1).auxData.evapotrans;
                modelFit    = p_model_lin(precip,evap,params);
                knmiDates   = dsStmList(1).auxData.knmiDates;
                
                plot(knmiDates,modelFit*1000 - shift,':k','LineWidth',3)
                legendtext = [legendtext "Fitted Model"];
            else
                dates = [];
                for i = 1:length(dsStmList)
                    dates   = [dates dsStmList(i).epochAttrib.timestamp];
                end
                dates       = sort(dates,'ascend');
                doy         = day(dates,'dayofyear')/365.25;
                t           = unwrap(doy*2*pi)/(2*pi);
                modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
                
                plot(dates,modelFit*1000 - shift,':k','LineWidth',3)
                legendtext = [legendtext "Fitted Model"];
            end
        end
        
        if options.click.plotMeanDefo == 1
            plot(datesComb,meanDefoAll - shift,'b','LineWidth',3)
            legendtext = [legendtext "Combined InSAR"];
        end
        
        pointIdx = find(dsStmList(1).pntAttrib.groupId == groupSel)';
        nMembers = length(pointIdx);
        
        title(['Group ' num2str(groupSel)])
        ylabel('Relative Vertical Position [mm]')
%         ylim([-150 150])
        set(gcf,'color','w')
        set(gca,'fontsize',14)
        grid on
        
        if ~isempty(legendtext)
            legend(legendtext,'location','nw')
        end        
        
        if options.method == 3 || options.method == 4
            annotationText =   {['Group Id: ' num2str(options.group.groupSel)], ...
                ['Soil code: ' char(soilCode)], ...
                ['Crop code: ' num2str(cropCode)], ...
                ['GHG: ' num2str(ghg,3) ' cm (MV)'], ...
                ['GLG: ' num2str(glg,3) ' cm (MV)'], ...
                ['A: ' num2str(params(1),2) ], ...
                ['B: ' num2str(params(2),2) ], ...
                ['C: ' num2str(params(3),2) ]};
        else
            annotationText =   {['Group Id: ' num2str(options.group.groupSel)], ...
                ['Soil code: ' char(soilCode)], ...
                ['Crop code: ' num2str(cropCode)], ...
                ['GHG: ' num2str(ghg,3) ' cm (MV)'], ...
                ['GLG: ' num2str(glg,3) ' cm (MV)'], ...
                ['Lin. Rate: ' num2str(params(3)*1000,2) ' mm/y'], ...
                ['Amplitude: ' num2str(params(1)*1000,2) ' mm']};
        end
                
        annotation('textbox',[0.85 .825 .1 .1],'String',annotationText,'fontsize',14,'EdgeColor','k')

        
    end
    usrIn = input('Select a group (enter to reset, any key+enter to quit): ',"s");
end

