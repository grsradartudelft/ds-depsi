function fit = get_model_fit_group(dsStmList,groupSel,options)

% This function determines the differences between the shifted unwrapped
% segemnts and the group model and evaluates the goodness of fit. 

groupIdx = find(dsStmList(1).auxData.groupIdList == groupSel);
params = dsStmList(1).auxData.groupModelParams(groupIdx,:);

if isempty(groupIdx)
    % In this case, no estimation has been made
    SSR = nan;
    SST = nan;
    residuals = nan;
    fit  = nan;
    return
end

if options.method == 3 || options.method == 4
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params)';
else
    knmiDates   = sort(dsStmList(1).auxData.knmiDates,'ascend');
    doy         = day(knmiDates,'dayofyear')/365.25;
    t           = unwrap(doy*2*pi)/(2*pi);
    modelFit    = (params(1)*cos( 2*pi*t + params(2) ) + params(3)*t)';
end

if strcmp(options.fit.type,'R2')
    SSR = 0;
    SST = 0;
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        
        residuals   = segments - (modelFit(epochIdx)-shift);
        dispersion  = segments - dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v;
        
        SSR         = SSR + sum(residuals(:).^2,'omitnan');
        SST         = SST + sum(dispersion(:).^2,'omitnan');
        
        modelTrack  = modelFit(epochIdx);
    end
    fit = 1 - SSR/SST;
end

if strcmp(options.fit.type,'R2')
    SSD = 0;
    SSR = 0;
    SST = 0;
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        
%         residuals   = segments - (modelFit(epochIdx)-shift);
        deviation   = segments - (modelFit(epochIdx)-shift);
        residuals   = modelFit(epochIdx)-shift - dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v;
        dispersion  = segments - dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v;
        
        SSD         = SSD + sum(deviation(:).^2,'omitnan');
        SSR         = SSR + sum(residuals(:).^2,'omitnan');
        SST         = SST + sum(dispersion(:).^2,'omitnan');
        
        % for debugging
%         figure
%         hold on
%         plot(dates,segments,'color',[0.7,0.7,0.7])
%         plot(dates,modelFit(epochIdx)-shift)
%         plot(dates,dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v)
        
%         figure
%         hold on
%         plot(dates,dispersion,'color','r')
%         plot(dates,error,'color',[0.7,0.7,0.7])
%         modelTrack  = modelFit(epochIdx);
    end
     fit = SSD/SST;
%     fit = 1 - SSR/SST;
%     fit = 1 - SSD/SST; %old
end

if strcmp(options.fit.type,'F')
    SSD = 0;
    SST = 0;
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        
        deviation   = segments - (modelFit(epochIdx)-shift);
        dispersion  = segments - dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v;
        
        SSD         = SSD + sum(deviation(:).^2,'omitnan');
        SST         = SST + sum(dispersion(:).^2,'omitnan');
        
    end
     fit = SSD/SST;
end

if strcmp(options.fit.type,'chi2')
    chi2 = [];
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        
%         residuals   = segments - (modelFit(epochIdx)-shift);
        residuals   = segments - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v);
        dispersion  = var(segments,0,1,'omitnan');
        
        residuals(:,dispersion == 0) = [];
        dispersion(dispersion == 0) = [];
        
        chi2        = [chi2,mean(residuals.^2,'omitnan')./dispersion];
%         chi2        = [chi2, dispersion];
    end
    fit = mean(chi2,'omitnan');
end

if strcmp(options.fit.type,'confidence')
    confAll = [];
    for i = 1:length(dsStmList)
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        confidence  = dsStmList(i).obsData.confidence(pointIdx,:);
        confAll     = [confAll; confidence(:)];
    end
    variance    = mean(confAll,'omitnan');
    fit         = variance;
end

if strcmp(options.fit.type,'corr')
    meanCorrTrack = [];
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = dsStmList(i).pntAttrib.groupId == groupSel;
        modelTrack  = modelFit(epochIdx);
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        for j = 1:size(segments,1)
            goodIdx = ~isnan(segments(j,:));
            C       = corrcoef(segments(j,goodIdx),modelTrack(goodIdx));
            c(j)    = C(1,2);
        end
        
        meanCorrTrack = [meanCorrTrack,mean(c,'omitnan')];
        
    end
    fit = mean(meanCorrTrack,'omitnan');
%     if isnan(fit)
%         fit
%     end
end