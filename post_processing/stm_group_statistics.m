function dsStmList = stm_group_statistics(dsStmList,options)

if options.stats.paramsBySoiltype
    
    % Get list of soil categories
    soilcodes = unique(dsStmList(1).pntAttrib.soilcode);
    mainSoilType = strings(size(soilcodes));
    for m = 1:length(dsStmList(1).pntAttrib.soilcode)
        soilcodeLong    = char(dsStmList(1).pntAttrib.soilcode(m));
        idx             = isstrprop(soilcodeLong,'upper');
        mainSoilType(m) = string(soilcodeLong(find(idx,1)));
    end
    maincodes = unique(mainSoilType);
    
    for i = 1:length(maincodes)
        parcelIdxs = find(ismember(mainSoilType,maincodes(i)));
        x_P = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,1).*dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,2);
        x_E = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,1);
        x_I = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,3);
        A = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,1);
        B = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,2);
        C = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,3);
        tau = round(dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,4));
        
        nBins = round(sqrt(length(parcelIdxs)));
        
        figure(1)
        subplot(3,3,i)
        hold on
        histogram(x_P,nBins)
        title(maincodes(i))
        if ~all(isnan(x_P))
            xline(median(x_P,'omitnan'),'--','linewidth',4)
        end
        
        figure(2)
        subplot(3,3,i)
        hold on
        histogram(x_E,nBins)
        title(maincodes(i))
        if ~all(isnan(x_E))
            xline(median(x_E,'omitnan'),'--','linewidth',4)
        end
        
        figure(3)
        subplot(3,3,i)
        hold on
        histogram(x_I,nBins)
        title(maincodes(i))
        if ~all(isnan(x_I))
            xline(median(x_I,'omitnan'),'--','linewidth',4)
        end
        
        figure(4)
        subplot(3,3,i)
        hold on
        histogram(tau,nBins)
        title(maincodes(i))
        if ~all(isnan(tau))
            xline(median(tau,'omitnan'),'--','linewidth',4)
        end
        
        disp(['Median parameter values type ' char(maincodes(i))])
%         disp(['x_P: ' num2str(median(x_P,'omitnan'))])
%         disp(['x_E: ' num2str(median(x_E,'omitnan'))])
%         disp(['x_I: ' num2str(median(x_I,'omitnan'))])
%         disp(['tau: ' num2str(median(tau,'omitnan'))])
        
        disp(['A: ' num2str(median(A,'omitnan'))])
        disp(['B: ' num2str(median(B,'omitnan'))])
        disp(['C: ' num2str(median(C,'omitnan'))])
        disp(['tau: ' num2str(median(tau,'omitnan'))])
        
        disp('______________________________________')
    end
    figure(1)
    sgtitle('x_P distribution by primary soil type')
    annotation('textbox',[0.68 0.04 0.25 0.27],'String', ... 
    {'A=defined associations (?)','E=thick moorland soils (?)','H=podzolic soils','M=marine clay soils','R=river clay soils','V=peat soils','W=marshy soils','Z=calcareous sands'})
    figure(2)
    sgtitle('x_E distribution by primary soil type')
    annotation('textbox',[0.68 0.04 0.25 0.27],'String', ... 
    {'A=defined associations (?)','E=thick moorland soils (?)','H=podzolic soils','M=marine clay soils','R=river clay soils','V=peat soils','W=marshy soils','Z=calcareous sands'})
    figure(3)
    sgtitle('x_I distribution by primary soil type')
    annotation('textbox',[0.68 0.04 0.25 0.27],'String', ... 
    {'A=defined associations (?)','E=thick moorland soils (?)','H=podzolic soils','M=marine clay soils','R=river clay soils','V=peat soils','W=marshy soils','Z=calcareous sands'})
    figure(4)
    sgtitle('tau distribution by primary soil type')
    annotation('textbox',[0.68 0.04 0.25 0.27],'String', ... 
    {'A=defined associations (?)','E=thick moorland soils (?)','H=podzolic soils','M=marine clay soils','R=river clay soils','V=peat soils','W=marshy soils','Z=calcareous sands'})

end
if options.stats.paramsByGroup
    for i = 1:length(options.stats.groupSel)
        parcelIdxs = find(ismember(dsStmList(1).pntAttrib.groupId,options.stats.groupSel(i)));
        
        x_P = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,1).*dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,2);
        x_E = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,1);
        x_I = dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,3);
        tau = round(dsStmList(1).pntAttrib.finalModelParams(parcelIdxs,4));
        
        nBins = 20; %round(sqrt(length(parcelIdxs)));
        
        figure
        sgtitle(['Parameter distribution group ' num2str(options.stats.groupSel(i))])
        
        subplot(2,2,1)
        histogram(x_P,nBins)
        title('x_P')
        
        subplot(2,2,2)
        histogram(x_E,nBins)
        title('x_E')
        
        subplot(2,2,3)
        histogram(x_I,nBins)
        title('x_I')
        
        subplot(2,2,4)
        histogram(tau,nBins)
        title('tau')
        
    end
end