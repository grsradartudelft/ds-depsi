clear
clc
close all

addpath(genpath('/Users/pconroy/phd/code/ds-depsi'))
addpath(genpath('/Users/pconroy/phd/code/stmutil'))
addpath(genpath('/Users/pconroy/phd/code/LAMBDA'))

location = 'green heart';
% location = 'zegveld';
% location = 'rouveen';
% location = 'delfland';
% location = 'assendelft';
% location = 'vlist';

%% Data folders
% Processing output folder (saved runs)
if strcmp(location,'green heart')
    procOutputFolder        = '/Users/pconroy/phd/projects/green_heart_full/insar/test_4_sequential/output/';
    options.shapefilePath   = '/Users/pconroy/phd/projects/green_heart_full/contextual_data/groene_hart_EUPs_wgs84_clean.shp';
    options.background      = '/Users/pconroy/phd/projects/green_heart_full/aoi/greenheart_aoi.png';
    
    % Zegveld
%     extTT                   = readtable('/Users/pconroy/phd/projects/extensometer/sept2023/ZEG_RF_sept2023_combined.csv');
%     extTT.Extensometer      = extTT.Extensometer - extTT.Extensometer(1);
%     extTT.Extensometer      = extTT.Extensometer*1000;
%     extTT.Temp              = [];
%     extTT.Rainfall          = [];
%     extTT                   = table2timetable(extTT,'RowTimes','Time');
%     extTT                   = retime(extTT,'Daily','mean');
%     options.extParcel       = 72959;
    
    %Vlist
    extTT                   = readtable('/Users/pconroy/phd/projects/extensometer/sept2023/VLI_RF_sept2023_combined.csv');
    extTT.Extensometer      = extTT.Extensometer - extTT.Extensometer(1);
    extTT.Extensometer      = extTT.Extensometer;
    extTT.Temp              = [];
    extTT.Rainfall          = [];
    extTT                   = table2timetable(extTT,'RowTimes','Time');
    extTT                   = retime(extTT,'Daily','mean');
    options.extParcel       = 3720;
end

if strcmp(location,'zegveld')
    procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/insar/debug_full/output/';
%       procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/insar/depsi_aoi_test/output/';
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/insar/test_spams_unw/output/';
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/insar/hydro_diff_8/'; % paper result
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/insar/hydro_diff_4_seg/'; % First good result, no constraints!!!!
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/zegveld/output/run_15/'; % Good result with forced lin. rate 


    % Extensometer data
    extTT               = readtable('/Users/pconroy/phd/projects/extensometer/feb2022/zegveld_simple.csv');
    extTT               = table2timetable(extTT,'RowTimes','Time_CET');
    extTT               = retime(extTT,'Daily','mean');
    
    options.extParcel   = 591099;
%     options.extParcel   = 1646282; %old
   
    options.background          = '/Users/pconroy/phd/projects/contextual_data/locations/zegveld/aoi/zegveld_AOI.png';
   
    
elseif strcmp(location,'rouveen')
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/rouveen/output/hydro_diff_3_segmentation/';
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/rouveen/output/blockwise_emi_1-1/';
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/rouveen/output/hydro_diff_3/'; % paper result
    procOutputFolder    = '/Users/pconroy/phd/projects/spider/rouveen/output/spams_unwrap_test_2/';
    % Extensometer data
    extTT               = readtable('/Users/pconroy/phd/projects/extensometer/feb2022/rouveen_simple.csv');
    extTT               = table2timetable(extTT,'RowTimes','Time_CET');
    options.extParcel   = 391704;
    
%     extTT               = readtable('/Users/pconroy/phd/projects/extensometer/aug2022/Rouveen_Visscher_sept2022_combined.csv');
%     extTT               = table2timetable(extTT,'RowTimes','Time');
%     options.extParcel           = 126094;

    extTT               = retime(extTT,'Daily','mean');
    extTT.(1)           = extTT.(1) - extTT.(1)(1);
    extTT.(1)           = extTT.(1) * 1000;
    
    options.background  = '/Users/pconroy/phd/projects/contextual_data/locations/rouveen/aoi/rouveen_aoi.png';
    
elseif strcmp(location,'delfland')
    procOutputFolder    = '/Users/pconroy/phd/projects/spider/delfland/hydro_diff_3/';
    options.background  = '/Users/pconroy/phd/projects/contextual_data/locations/delfland/delfland_AOI_image.png';
    extTT               = [];
    options.extParcel   = [];
    
elseif strcmp(location,'vlist')
    procOutputFolder    = '/Users/pconroy/phd/projects/spider/vlist/hydro_diff_1/';
%     procOutputFolder    = '/Users/pconroy/phd/projects/spider/vlist/run_1_segmentation';
    extTT               = readtable('/Users/pconroy/phd/projects/extensometer/aug2022/LangeWeide_combined.csv');
    extTT               = table2timetable(extTT,'RowTimes','Time');
    extTT               = retime(extTT,'Daily','mean');
    extTT.(1)           = extTT.(1) - extTT.(1)(1);
    
    options.extParcel   = 300846;
    
 elseif strcmp(location,'assendelft')
    procOutputFolder    = '/Users/pconroy/phd/projects/spider/assendelft/run_5_shptest/';
    
    extTT               = readtable('/Users/pconroy/phd/projects/extensometer/aug2022/ASD_RF_aug2022_combined.csv');
    extTT               = table2timetable(extTT,'RowTimes','Time');
    extTT               = retime(extTT,'Daily','mean');
    extTT.(1)           = extTT.(1) - extTT.(1)(1);
    extTT.(1)           = extTT.(1) * 1000;
    
    options.extParcel   = 464176;
end

loadedSettings  = load([procOutputFolder 'settings.mat']);
settings        = loadedSettings.settings;
options.method  = settings.seg.method;

%% Load processed data
ds_files = dir([procOutputFolder '/*ds_stm.mat']);
ps_files = dir([procOutputFolder '/*ps_stm.mat']);

for i = 1:length(ds_files)
    dsStmNameList(i)        = string([ds_files(i).folder '/' ds_files(i).name]);
    dsStmList(i)            = stmread(dsStmNameList(i));
%     psStmNameList(i)        = string([ps_files(i).folder '/' ps_files(i).name]);
%     psStmList(i)            = stmread(psStmNameList(i));
    if ~isfield(dsStmList(i).pntAttrib,'knmi_id')
        if strcmp(location,'zegveld')
            dsStmList(i).pntAttrib.knmi_id = repmat(348,dsStmList(i).numPoints);
            dsStmList(i).auxData.knmiStations = 348;
        end
    end
end

settings.seg.Amin               = 0;                % Minimum model amplitude (m/m)
settings.seg.Amax               = 1e-3;             % Maximum model amplitude (m/m)
settings.seg.Bmin               = 0.2;              % Minimum precip/evap balance (m/m)
settings.seg.Bmax               = 1.7;              % Maximum precip/evap balance (m/m)
settings.seg.Cmin               = -2e-4;            % Minimum active subsidence rate (m/day)
settings.seg.Cmax               = 0;                % Maximum active subsidence rate (m/day)
settings.seg.Tmin               = 50;               % Minimum integration time (days)
settings.seg.Tmax               = 150;              % Maximum integration time (days)

%% Find group containing extensometer if applicable
if ~isempty(options.extParcel)
    options.extGroup = dsStmList(1).pntAttrib.groupId(dsStmList(1).pntAttrib.parcelId == options.extParcel);
else
    options.extGroup = [];
end

%% Options
% Clickable map
options.clickableMap            = 0;
options.click.plotMeanDefo      = 1;
options.click.plotSpaghetti     = 1;
options.click.plotFittedModel   = 0;
options.click.annotations       = 1;

% Group segmentation diagram
options.plotSeg                 = 0;
options.seg.groupSel            = options.extGroup; 
options.seg.minEpochs           = 5;
options.seg.minCoherence        = 0.11;
options.seg.track               = 1;                    
options.seg.background          = 1;
options.seg.startDate           = datetime(2015,1,1);
options.seg.endDate             = datetime(2023,10,1);

% Group plot (can be multiple)
options.plotGroup               = 0;
options.group.groupSel          = options.extGroup;
options.group.idSel             = options.extParcel;
options.group.plotExt           = 1;
options.group.plotPoint         = 0;
options.group.plotSpaghetti     = 1;
options.group.plotPrecip        = 0;
options.group.plotTemp          = 0;
options.group.plotFittedModel   = 1;
options.group.plotAvailability  = 0;
options.group.plotMeteo         = 0;
options.group.annotations       = 1;
options.group.ylim              = [-100 100];
options.group.startDate         = datetime(2015,1,1);
options.group.endDate           = datetime(2023,10,1);
options.group.plotMode          = 'normal'; %'paper'; %'normal';

% Point plot
options.plotPoint               = 0;
options.point.idSel             = options.extParcel;
options.point.plotGroupMean     = 0;
options.point.plotFittedModel   = 0;
options.point.plotPrecip        = 0;
options.point.plotExt           = 1;

% Plot group parameter statistics
options.plotStatistics          = 1;
options.stats.paramsBySoiltype  = 1;
options.stats.paramsByGroup     = 0;
options.stats.groupSel          = options.extGroup;

% Shaded map of parcels (linear)
options.plotLinearMap           = 0;
options.lin.cbarLim             = [-0.015 0];
options.lin.showBackground      = 1;

% Shaded map of parcels (amplitude)
options.plotAmplitudeMap        = 0;
options.amp.cbarLim             = [0 0.15];
options.amp.showBackground      = 0;

% Shaded map of parcels (model fit)
options.plotFitMap              = 0;
options.fit.type                = 'OMT';
options.fit.cbarLim             = [0 10];
options.fit.showBackground      = 1;

%% Plots
if options.clickableMap     == 1
    stm_clickable_plot(dsStmList,options);
end

if options.plotGroup        == 1
    stm_group_plot(dsStmList,extTT,options);
end

if options.plotSeg          == 1
    stm_segmentation_plot(dsStmList,options);
end

if options.plotPoint        == 1
    stm_point_plot(dsStmList,extTT,options);
end

if options.plotStatistics   == 1
    stm_group_statistics(dsStmList,options);
end

if options.plotLinearMap    == 1
    stm_linear_plot(dsStmList,options);
end

if options.plotAmplitudeMap == 1
    stm_amplitude_plot(dsStmList,options);
end

if options.plotFitMap == 1  
    stm_fit_plot(dsStmList,options,settings);
end