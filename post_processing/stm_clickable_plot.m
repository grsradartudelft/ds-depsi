function stm_clickable_plot(dsStmList,options)
% close all
groups = dsStmList(1).auxData.groupIdList;

usrIn=[];
while isempty(usrIn)
    
    % Base plot of all points coloured by peilgebied
    %close all
    figure(999)
    hold on
    plotColours = jet(length(groups));
    for p = 1:length(groups)
        pntIdx = ismember(dsStmList(1).pntAttrib.groupId,groups(p));
        plot(dsStmList(1).pntCrd(:,2),     dsStmList(1).pntCrd(:,1),'k.','HandleVisibility','off')
        plot(dsStmList(1).pntCrd(pntIdx,2),dsStmList(1).pntCrd(pntIdx,1),'*','color',plotColours(p,:));
        legendstr{p} = num2str(groups(p));
    end
    legend(legendstr,'location','northeastoutside')
    set(gcf,'color','w')
    
    % Get user input to select a group
    [x,y]=ginput(1);
    
    % Need to define a list of "used points" here, then get group ID of
    % nearest point
    
    usedPoints      = find(~isnan(dsStmList(1).pntAttrib.groupId));
    
    [~,nearestPnt]  = min(hypot(dsStmList(1).pntCrd(usedPoints,2)-x,dsStmList(1).pntCrd(usedPoints,1)-y));
    groupSel        = dsStmList(1).pntAttrib.groupId(usedPoints(nearestPnt));
    
    options.group.groupSel          = groupSel;
    options.group.plotPoint         = 0;
    options.group.plotSpaghetti     = options.click.plotSpaghetti;
    options.group.plotFittedModel   = options.click.plotFittedModel;
    options.group.annotations       = options.click.annotations;
    stm_group_plot(dsStmList,[],options);
    
    usrIn = input('Select a group (enter to reset, any key+enter to quit): ',"s");
end

