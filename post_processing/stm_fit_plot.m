function stm_fit_plot(dsStmList,options,settings)
warning('off','MATLAB:polyshape:repairedBySimplify')

if isa(dsStmList(1).pntAttrib.groupId,'string')
    dsStmList(1).pntAttrib.groupId   = str2double(dsStmList(1).pntAttrib.groupId);
    dsStmList(1).auxData.groupIdList = str2double(dsStmList(1).auxData.groupIdList);
end
    
shpPath     = dsStmList(1).techniqueAttrib.shp_path;
if strcmp(shpPath(1:18),'/project/caroline/')
    idx     = strfind(shpPath,'contextual_data');
    shpPath = ['/Users/pconroy/phd/projects/' shpPath(idx:end)];
end

S           = shaperead(shpPath);
ids         = [S.id];

% pointIdx    = find(dsStmList(1).pntAttrib.groupId ~= 0);
% parcelIds   = dsStmList(1).pntAttrib.parcelId(pointIdx);
% nPoints     = length(pointIdx);

parcelIds   = dsStmList(1).pntAttrib.parcelId;
nPoints     = length(parcelIds);

for i = 1:length(dsStmList)
    dsStmList(i).pntAttrib.linDefo = nan(size(dsStmList(1).pntAttrib.parcelId));
end

shapeIdx = [];
for j = 1:length(S)
    
    pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,ids(j)));
    if ~isempty(pointIdx)
        fit(j)      = dsStmList(1).pntAttrib.T(pointIdx);
%         [~,fit(j)]  = omt_parcel_final(dsStmList,ids(j),0.1,settings);
    else
        fit(j)      = nan;
    end
%     if ~isempty(pointIdx)
%         groupSel = dsStmList(1).pntAttrib.groupId(pointIdx);
%         groupIdx = find(dsStmList(1).auxData.groupIdList == groupSel);
% 
% %         if isempty(groupSel) || isempty(groupIdx)
% %             fit(j)  = nan;
% %         else
% %             [SSR(j),SST(j),R2(j)] = get_model_fit_point(dsStmList,ids(j),options);
% %             fit(j) = get_model_fit_point(dsStmList,ids(j),options);
% %             fit(j) = get_model_fit_group(dsStmList,groupSel,options);
% %             if isfield(
% %             [accept,fit(j)] = omt_parcel(dsStmList,ids(j),settings);
% %             R2(j) = dsStmList(1).auxData.groupModelCost(groupIdx);
%         end
%     else
%         fit(j)  = nan;
%     end
    
    X{j}        = S(j).X;
    Y{j}        = S(j).Y;
    
%     if groupSel == 1004
%         disp('hi')
%     end
    
end

if isempty(options.fit.cbarLim)
    options.fit.cbarLim(1) = round(min(fit)*100)/100;
    options.fit.cbarLim(2) = round(max(fit)*100)/100;
end

normRate = (fit-options.fit.cbarLim(1))/(options.fit.cbarLim(2)-options.fit.cbarLim(1));
% normRate = (residuals-options.fit.cbarLim(1))/(options.fit.cbarLim(2)-options.fit.cbarLim(1));
normRate(normRate > 1) = 1;
normRate(normRate < 0) = 0;

% Plot colour from linear deformation rate
bgFlag      = 0;
cmap        = parula(512);
colorRow    = round(normRate * 511)+1;

%% Background Image
if options.fit.showBackground == 1 && isfield(options,'background') 
    shpInfo     = shapeinfo(shpPath);
    boundingBox = shpInfo.BoundingBox;
    latLim      = [boundingBox(1,2), boundingBox(2,2)];
    lonLim      = [boundingBox(1,1), boundingBox(2,1)];
    im          = flipud(imread(options.background));
    bgFlag      = 1;
elseif options.fit.showBackground == 1 && ~isfield(options,'background')
    warning('No background image supplied')
end
%% Plot shapefile

figure('Position',[409 117 1045 860],'color','w');
% title('Estimated Goodness of Fit','fontsize',14)
mapshow(S,'FaceColor','none','LineWidth',0.25,'EdgeAlpha',0.5);
if bgFlag == 1
    hold on
    b=image(lonLim,latLim,im);
    uistack(b,'bottom')
end
axis square
set(gcf,'color','w')
k=colorbar('south');
set(get(k,'title'),'string','T','fontsize',14,'color','w');
set(k,'color','w');
k.Position = [.73 .15 .1 .025];
% set(k,'xtick',[-1,-0.8,-0.6,-0.4],'xticklabel',{'-1','-0.8','-0.6','-0.4'})
set(k,'xtick',[0, 5, 10],'xticklabel',{'0','5','10'})
colormap(cmap)
set(gca, 'fontsize',14)
caxis(options.fit.cbarLim)

a=gca;
% Inner loop: plot each polygon
for j = 1:nPoints
    if ~isnan(normRate(j))
        color_row = round(normRate(j) * 511)+1;
        cc = cmap(color_row,:);
        set(a.Children(1).Children(j),'FaceColor',cc);
    end
end
axis tight
end

