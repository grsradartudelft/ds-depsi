function stm_amplitude_plot(dsStmList,options)
warning('off','MATLAB:polyshape:repairedBySimplify')

if isa(dsStmList(1).pntAttrib.groupId,'string')
    dsStmList(1).pntAttrib.groupId   = str2double(dsStmList(1).pntAttrib.groupId);
    dsStmList(1).auxData.groupIdList = str2double(dsStmList(1).auxData.groupIdList);
end
    
shpPath     = dsStmList(1).techniqueAttrib.shp_path;
if strcmp(shpPath(1:18),'/project/caroline/')
    idx     = strfind(shpPath,'contextual_data');
    shpPath = ['/Users/pconroy/phd/projects/' shpPath(idx:end)];
end

S           = shaperead(shpPath);
ids         = [S.id];

pointIdx    = find(dsStmList(1).pntAttrib.groupId ~= 0);
parcelIds   = dsStmList(1).pntAttrib.parcelId(pointIdx);
nPoints     = length(pointIdx);

for i = 1:length(dsStmList)
    doy     = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
    t{i}    = unwrap(doy*2*pi)/(2*pi);
end

shapeIdx = [];
ph2v = dsStmList(1).techniqueAttrib.wavelength/(4*pi);
for j = 1:length(S)
    
    pointIdx = find(ismember(dsStmList(1).pntAttrib.parcelId,ids(j)));
    
    if ~isempty(pointIdx)
        groupId     = dsStmList(1).pntAttrib.groupId(pointIdx);
        groupIdx    = find(dsStmList(1).auxData.groupIdList == groupId);
    
        if isempty(groupIdx)
            amp(j)  = nan;
        else
%             tsVecAll = [];
%             for i = 1:length(dsStmList)
%                 % Get the 90th percentile of all segments to estimate the
%                 % peak-to-peak fluctuations
%                 % First remove linear rate to avoid biasing the result
%                 pointIdx    = find(dsStmList(i).pntAttrib.groupId == groupId);
%                 ts          = dsStmList(i).obsData.vertUnwPhase(pointIdx,:);
%                 tsVec       = reshape(ts*ph2v - t{i}*dsStmList(1).auxData.groupModelParams(groupIdx,3),1,[]);
%                 tsVecAll    = [tsVecAll tsVec(~isnan(tsVec))];
%             end   
% %             figure
% %             histogram(tsVecAll)
%             amp(j)      = prctile(tsVecAll,95); % Two-sided 90th

            amp(j)      = 2*dsStmList(1).auxData.groupModelParams(groupIdx,1);
            shapeIdx    = [shapeIdx; j];
            
        end
    else
        amp(j)  = nan;
    end
    
    X{j}        = S(j).X;
    Y{j}        = S(j).Y;
end

normAmp = (amp-options.amp.cbarLim(1))/(options.amp.cbarLim(2)-options.amp.cbarLim(1));
normAmp(normAmp > 1)  = 1;
normAmp(normAmp < 0)  = 0;

% Plot colour from linear deformation rate
cmap        = flipud(jet(256));
colorRow    = round(normAmp * 255)+1;
bgFlag      = 0;
%% Background Image
if options.amp.showBackground == 1 && isfield(options,'background') 
    shpInfo     = shapeinfo(shpPath);
    boundingBox = shpInfo.BoundingBox;
    latLim      = [boundingBox(1,2), boundingBox(2,2)];
    lonLim      = [boundingBox(1,1), boundingBox(2,1)];
    im          = flipud(imread(options.background));
    bgFlag      = 1;
elseif options.amp.showBackground == 1 && ~isfield(options,'background')
    warning('No background image supplied')
end

%% Plot shapefile

figure('Position',[409 117 1045 860],'color','w');
title('Estimated Seasonal Amplitude (Peak-to-Peak)','fontsize',14)
mapshow(S,'FaceColor','none','LineWidth',0.25,'EdgeAlpha',0.5);
if bgFlag == 1
    hold on
    b=image(lonLim,latLim,im);
    uistack(b,'bottom')
end
axis square
set(gcf,'color','w')
k=colorbar;
set(get(k,'title'),'string','mm','fontsize',14);
colormap(flipud(jet))
caxis(options.amp.cbarLim*1000)

a=gca;
% Inner loop: plot each polygon
for j = 1:nPoints
    if ~isnan(normAmp(j))
        color_row = round(normAmp(j) * 255)+1;
        cc = cmap(color_row,:);
        set(a.Children(1).Children(j),'FaceColor',cc);
    end
end

end

