function [accept,T] = omt_parcel_final(dsStmList,pointSel,alpha,settings)

% This function determines the differences between the unwrapped
% segemnts and the group model and evaluates the goodness of fit.

pointIdx    = find(dsStmList(1).pntAttrib.parcelId == pointSel);
groupSel    = dsStmList(1).pntAttrib.groupId(pointIdx);
groupIdx    = find(dsStmList(1).auxData.groupIdList == groupSel);
params      = dsStmList(1).auxData.groupModelParams(groupIdx,:);

if isempty(groupIdx)
    % In this case, no estimation has been made
    T = nan;
    accept = 0;
    return
end

if settings.seg.method == 3 || settings.seg.method == 4
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params)';
else
    knmiDates   = sort(dsStmList(1).auxData.knmiDates,'ascend');
    doy         = day(knmiDates,'dayofyear')/365.25;
    t           = unwrap(doy*2*pi)/(2*pi);
    modelFit    = (params(1)*cos( 2*pi*t + params(2) ) + params(3)*t)';
end

for i = 1:length(dsStmList)
    pointIdx    = find(dsStmList(i).pntAttrib.parcelId == pointSel);
    dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
    epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);

    ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    segments    = diff([0 dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v],1,2);
    modelDiff   = diff([0 modelFit(epochIdx)]);
    
    residual    = segments - modelDiff;
    CRB         = diag(estimate_Qy(dsStmList(i),pointSel))';
    Ttrack(i)   = sum(residual.^2./CRB,'omitnan'); 
end

% Overall model test
T           = sum(Ttrack);
dof         = length(params);
alpha       = alpha;
crit        = chi2inv(1-alpha,dof);

if T > crit
    accept = false;
else
    accept = true;
end




