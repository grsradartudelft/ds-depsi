function seasonal_plot(dsStmList)


for p = 1:length(dsStmList(1).auxData.groupIdList)
    combOutputTT = timetable();
    for i = 1:length(dsStmList)
        trackOutputTT   = timetable(dsStmList(i).epochAttrib.timestamp',dsStmList(i).auxData.groupMedian(p,:)');
        combOutputTT    = synchronize(combOutputTT,trackOutputTT);
    end
    [groupMedianAll(p,:), dates] = combine_tracks(combOutputTT,5);
    
    doy                     = day(dates','dayofyear')/365.25;
    fracYear                = unwrap(doy*2*pi)/(2*pi);
    ph2v                    = dsStmList(1).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    seasonalTrend(p,:)      = groupMedianAll(p,:)*ph2v - dsStmList(1).auxData.groupModelParams(p,3)*fracYear;
end

figure('color','w')
plot(dates,seasonalTrend)
grid on
title('Seasonal Trend (All Groups)')
ylabel('Vertical position (m)')
set(gca, 'fontsize',14)

figure('color','w')
imagesc(seasonalTrend-mean(seasonalTrend,1))
colorbar
[~,ticks] = unique(dateshift(dates,'start','year'));
ticks(1) = [];
labels = datestr(dateshift(dates(ticks), 'start', 'day'),'yyyymmdd');
set(gca, 'XTick', ticks, 'XTickLabel', labels)
set(gca, 'fontsize',14)
ylabel('Group number')

figure
hold on
for i = 1:size(test,1)
    histogram(test(i,:),50)
end

figure
hold on
plot(seasonalTrend(33,:))
plot(mean(seasonalTrend,1))

figure
plot(dates,(seasonalTrend(33,:)-mean(seasonalTrend,1))*100)
grid minor
end

