function stm_group_plot(dsStmList,extTT,options)

% First read attributes which are the same for all tracks
combOutputTT = timetable();
pntIdx       = find(ismember(dsStmList(1).pntAttrib.groupId,options.group.groupSel));
nParcels     = length(pntIdx);
if isempty(pntIdx)
    error('Invalid group selection.')
end
soilCode     = string(mode(categorical(dsStmList(1).pntAttrib.soilcode(pntIdx))));
cropCode     = string(mode(categorical(dsStmList(1).pntAttrib.cropcode(pntIdx))));
ghg          = mean(dsStmList(1).pntAttrib.ghg(pntIdx)) * -1/10;
glg          = mean(dsStmList(1).pntAttrib.glg(pntIdx)) * -1/10;
groupIdx     = find(ismember(dsStmList(1).auxData.groupIdList,options.group.groupSel));

for i = 1:length(dsStmList)
    
    ph2v            = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    medianDefo{i}   = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
    
    if size(medianDefo{i},2) > 1
        medianDefo{i} = median(medianDefo{i},2);
    end
    
    % Combine tracks into one timetable
    dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
    trackOutputTT   = timetable(dates,medianDefo{i});
    combOutputTT    = synchronize(combOutputTT,trackOutputTT);
end

% Combine median otput of all tracks
[meanDefoAll, combDates] = combine_tracks(combOutputTT,7);

% Synchronize mean InSAR with extensometer data
if ~isempty(extTT)   
    extTT                = synchronize(extTT,timetable(combDates,meanDefoAll,'VariableNames',{'mean_group'}));
    shift                = mean(extTT.mean_group-extTT.(1),'omitnan');
    extTT.mean_group     = extTT.mean_group - shift;
else
    shift = 0;
end

%% Plot time series
% [left bottom width height]

if strcmp(options.group.plotMode,'paper')
    figure('position',[300 300 800 600]) % Paper mode
else
    figure('position',[300 300 1100 600]) % Normal mode
end
hold on
legendtext = [];
if isfield(options.group,'ylim')
    ylim(options.group.ylim)
end

% Plot "spaghetti" of every segment in selected group
if options.group.plotSpaghetti == 1
    for i = 1:length(dsStmList)
        ph2v         = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        pointIdx     = find(dsStmList(i).pntAttrib.groupId == options.group.groupSel)';
        if isfield(dsStmList(i).obsData,'finalUnwPhase')
            vertSegments = dsStmList(i).obsData.finalUnwPhase(pointIdx,:)*ph2v*1000;
        else
            vertSegments = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)*ph2v*1000;
        end
        if i == 1
            plot(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day'),vertSegments(1,:)-shift,'color',[0.7,0.7,0.7])
            legendtext = [legendtext "Unwrapped Segments (All Parcels, All Tracks)"];
            plot(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day'),vertSegments(2:end,:)-shift,'color',[0.7,0.7,0.7],'handlevisibility','off')
        else
            plot(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day'),vertSegments-shift,'color',[0.7,0.7,0.7],'handlevisibility','off')
        end
%         plot(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day'),vertSegments-shift)
%         plot(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day'),wrappedDC,'k.','handlevisibility','off')
    end
end

if options.group.plotSpaghetti ~= 1
    for i = 1:length(dsStmList)
    plot(dsStmList(i).epochAttrib.timestamp,medianDefo{i}-shift,'LineWidth',1)
    legendtext = [legendtext string(dsStmList(i).datasetId)];
    end
end

plot(combDates,meanDefoAll-shift,'b','LineWidth',3);
% legendtext = [legendtext "Group Median (All Tracks)"];
legendtext = [legendtext "Median InSAR"];

% Show the plot of a certain parcel of interest in the group
if options.group.plotPoint == 1
    pointTT = timetable();
    for i = 1:length(dsStmList)
        pointIdx     = find(dsStmList(i).pntAttrib.parcelId == options.group.idSel);
        ph2v         = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        if isfield(dsStmList(i).obsData,'finalUnwPhase')
            vertSegments = dsStmList(i).obsData.finalUnwPhase(pointIdx,:)*ph2v*1000;
        else
            vertSegments = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)*ph2v*1000;
        end
        
        pointTT = synchronize(pointTT,timetable(dateshift(dsStmList(i).epochAttrib.timestamp,'start','day')',vertSegments'));
    end
    [pointTs,pointDates] = combine_tracks_nan(pointTT,7); 
    plot(pointDates,pointTs-shift,'r','LineWidth',2)
    legendtext = [legendtext "Parcel Mean (All Tracks)"];
end

if ~isempty(extTT) && options.group.plotExt == 1
    plot(extTT.Properties.RowTimes,extTT.(1),'k','LineWidth',3)
    legendtext = [legendtext "In-Situ Measurement"];
end

yLims = get(gca,'ylim');

% Estimated Model Parameters    
dates = [];
for i = 1:length(dsStmList)
    dates   = [dates dsStmList(i).epochAttrib.timestamp];
end
dates       = sort(dates,'ascend');
doy         = day(dates,'dayofyear')/365.25;
t           = unwrap(doy*2*pi)/(2*pi);
params      = dsStmList(1).auxData.groupModelParams(groupIdx,:); % Same for all tracks
    
if options.group.plotFittedModel == 1
    if options.method == 3 || options.method == 4 
        % Hydrological model
        if size(dsStmList(1).auxData.precipitation,1) > 1
            stationIdx  = dsStmList(1).auxData.knmiStations == dsStmList(1).pntAttrib.knmi_id(pointIdx(1));
            precip      = dsStmList(1).auxData.precipitation(stationIdx,:);
            evap        = dsStmList(1).auxData.evapotrans(stationIdx,:);
        else
            precip      = dsStmList(1).auxData.precipitation;
            evap        = dsStmList(1).auxData.evapotrans;
        end
        
        if iscolumn(precip)
            precip = precip';
        end
        if iscolumn(evap)
            evap = evap';
        end
        modelFit    = p_model_lin(precip,evap,params)';
        knmiDates   = dsStmList(1).auxData.knmiDates;
        
        modelIdx    = ismember(knmiDates,combDates);
        shift       = mean(modelFit(modelIdx)*1000 - (meanDefoAll-shift),'omitnan');
        
        plot(knmiDates,modelFit*1000 - shift,':r','LineWidth',3)
        
%         diffModel   = stm_get_diff_model(dsStmList,options.group.groupSel)/ph2v;
%         plot(knmiDates,diffModel,'r','LineWidth',3)
        legendtext = [legendtext "Fitted Model"];
    else
        modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
        
        plot(dates,modelFit*1000 - shift,':b','LineWidth',3)
        legendtext = [legendtext "Fitted Model"];
    end
end

if options.group.plotPrecip == 1
    yyaxis right
    epochIdx = dsStmList(1).auxData.knmiDates >= dsStmList(1).epochAttrib.timestamp(1) & dsStmList(1).auxData.knmiDates <= dsStmList(1).epochAttrib.timestamp(end);
    bar(dsStmList(1).auxData.knmiDates(epochIdx),dsStmList(1).auxData.precipitation(epochIdx),'b')
    ylabel('Precipitation [mm]')
    legendtext = [legendtext "Precipitation"];
end

if options.group.plotTemp == 1
    yyaxis right
    epochIdx = dsStmList(1).auxData.knmiDates >= dsStmList(1).epochAttrib.timestamp(1) & dsStmList(1).auxData.knmiDates <= dsStmList(1).epochAttrib.timestamp(end);
    smoothedTemp = smooth(dsStmList(1).auxData.temperature(epochIdx),7);    
    plot(dsStmList(1).auxData.knmiDates(epochIdx),smoothedTemp,'Color',[1, 0, 0, 0.6])
    ylabel('Temperature (smoothed) [C]')
    legendtext = [legendtext "Temperature"];
end

if options.group.plotMeteo == 1
    [model, time] = meteo_regression(dsStmList, options);
    
    yyaxis right
    plot(time,model*1000,'r','linewidth',3)
    ylabel('Linear regression model')
    legendtext = [legendtext "Model"];
    ylim(yLims)
end

if options.group.plotPrecip == 1 || options.group.plotTemp == 1 || options.group.plotMeteo == 1
    yyaxis left
end

legend(legendtext,'interpreter','none','location','se')
ylabel('Relative Vertical Position [mm]')
% set(gcf,'color','w')
set(gca,'fontsize',14)
grid on

if ~isempty(extTT) && options.group.plotExt == 1
    RMSE = sqrt(mean((extTT.mean_group-extTT.(1)).^2,'omitnan'));
    disp(['Group RMSE = ' num2str(RMSE,2) ' mm'])
    
    if options.group.plotPoint == 1
        extTT = synchronize(extTT,timetable(pointDates,pointTs-shift,'VariableNames',{'mean_point'}));
        pointRMSE = sqrt(mean((extTT.mean_point-extTT.(1)).^2,'omitnan'));
        disp(['Point RMSE = ' num2str(pointRMSE,2) ' mm'])
    end
    
end

if options.group.annotations == 1
    pos = get(gca, 'Position');
    xoffset = -0.077;
    pos(1) = pos(1) + xoffset;
    set(gca, 'Position', pos)
end

if options.group.plotAvailability == 1
    [availability,dates] = stm_get_availability(dsStmList,options);
    [neql,dates] = stm_get_neql(dsStmList,options);
    
    
    ax1 = gca;
    subplot(2,1,1,ax1)
    
    if options.group.annotations == 1
        pos = get(ax1, 'Position');
        xoffset = -0.077;
        pos(1) = pos(1) + xoffset;
        set(ax1, 'Position', pos)
    end
    
    subplot(2,1,2)
%     bar(dates,availability)
%     ylabel('Num. coherent observations')
    bar(dates,neql)
    ylabel('Num. Equivalent Looks')
    set(gca,'fontsize',14)
    grid on
    
    if options.group.annotations == 1
        ax2 = gca;
        pos = get(ax2, 'Position');
        xoffset = -0.077;
        pos(1) = pos(1) + xoffset;
        set(ax2, 'Position', pos)
    end
end


if isempty(options.group.startDate) || isempty(options.group.endDate)
    startDate   = dateshift(combDates(1), 'start', 'year') - days(1);
    endDate     = dateshift(combDates(end), 'end', 'year') + days(1);
else
    startDate   = options.group.startDate;
    endDate     = options.group.endDate;
end
xlim([startDate endDate])
if isfield(options.group,'ylim')
    ylim(options.group.ylim)
end

%% Annotation next to plot
if options.group.annotations == 1
    if options.method == 3 || options.method == 4
        annotationText =   {['Group Id: ' num2str(options.group.groupSel)], ...
            ['Num. Parcels: ' num2str(nParcels)], ...
            ['Soil code: ' char(soilCode)], ...
            ['Crop code: ' char(cropCode)], ...
            ['GHG: ' num2str(ghg,3) ' cm (MV)'], ...
            ['GLG: ' num2str(glg,3) ' cm (MV)'], ...
            ['X1: ' num2str(params(1),2) ], ...
            ['X2: ' num2str(params(2),2) ], ...
            ['X3: ' num2str(params(3),2) ], ...
            ['tau: ' num2str(params(4),2) ]};
    else
        annotationText =   {['Group Id: ' num2str(options.group.groupSel)], ...
            ['Soil code: ' char(soilCode)], ...
            ['Crop code: ' char(cropCode)], ...
            ['GHG: ' num2str(ghg,3) ' cm (MV)'], ...
            ['GLG: ' num2str(glg,3) ' cm (MV)'], ...
            ['Lin. Rate: ' num2str(params(3)*1000,2) ' mm/y'], ...
            ['Amplitude: ' num2str(params(1)*1000,2) ' mm']};
    end
    
    annotation('textbox',[0.85 .825 .1 .1],'String',annotationText,'fontsize',14,'EdgeColor','k')
end
end

