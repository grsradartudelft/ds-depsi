function [dsStmList,meanLinDefo] = stm_estimate_parcel_lin_defo(dsStmList,parcelIdSel)

for i = 1:length(dsStmList)
    pntIdx          = find(dsStmList(i).pntAttrib.parcelId == parcelIdSel);    
    ph2v            = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    vertDefo{i}     = dsStmList(i).obsData.vertUnwPhase(pntIdx,:)'*ph2v*1000;
    epochIdx        = ~isnan(vertDefo{i});
    
    dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
    doy             = day(dates,'dayofyear')/365.25;
    t               = unwrap(doy*2*pi)/(2*pi);
    
    p               = polyfit(t(epochIdx),vertDefo{i}(epochIdx),1);
    linDefo(i)      = p(1);
    
    dsStmList(i).pointAttrib.linDefo(pntIdx) = linDefo;
end

meanLinDefo = mean(linDefo);

end

