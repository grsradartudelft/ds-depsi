function [availability,dates] = stm_get_availability(dsStmList,options)

dates = [];
availability = [];
for i = 1:length(dsStmList)
    pntIdx              = find(ismember(dsStmList(i).pntAttrib.groupId,options.group.groupSel));
    nPoints             = length(pntIdx);
    trackAvail{i}       = sum(~isnan(dsStmList(i).obsData.vertUnwPhase(pntIdx,:)),1);
    trackAvailFrac{i}   = trackAvail{i}/nPoints;
    
    availability        = [availability, trackAvail{i}];
    dates               = [dates, dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day')];
end

[dates,order] = sort(dates,'ascend');
availability  = availability(order);

end