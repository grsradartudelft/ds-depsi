function stm_point_plot(dsStmList,extTT,options)

insarTT = timetable();
combOutputTT = timetable();
for i = 1:length(dsStmList)
    pntIdx          = find(dsStmList(i).pntAttrib.parcelId == options.point.idSel);
    groupIdx        = find(ismember(dsStmList(i).auxData.groupIdList,dsStmList(i).pntAttrib.groupId(pntIdx)));
    
    soilCode        = string(mode(categorical(dsStmList(i).pntAttrib.soilcode(pntIdx))));
    
    ph2v            = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    vertDefo{i}     = dsStmList(i).obsData.vertUnwPhase(pntIdx,:)'*ph2v*1000;
    medianDefo{i}   = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v*1000;
    
    % Combine tracks into one timetable
    dates{i}        = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
    trackOutputTT   = timetable(dates{i},vertDefo{i},'VariableNames',{dsStmList(i).datasetId});
    insarTT         = synchronize(insarTT,trackOutputTT);
    combOutputTT    = synchronize(combOutputTT,timetable(dates{i},medianDefo{i}));
end

[meanDefoAll, datesMean] = combine_tracks(combOutputTT,7);

% Synchronize mean InSAR with extensometer data
if ~isempty(extTT)
%     meanTT  = synchronize(extTT,timetable(datesMean,meanDefoAll,'VariableNames',{'mean_insar'}));
    combTT      = synchronize(extTT,insarTT);
    combArr     = table2array(combTT);
    shift       = mean(mean(combArr(:,2:end)-combTT.(1),'omitnan'),'omitnan');
    shiftedArr  = combArr(:,2:end) - shift;
    combTT(:,2:end) = array2table(shiftedArr);
    RMSE    = sqrt(mean(mean((shiftedArr-combTT.(1)).^2,'omitnan')));
    disp(['Parcel RMSE = ' num2str(RMSE,3) ' mm'])
else
    shift = 0;
end

%% Plot time series
figure
hold on
legendtext = [];

% Plot fitted model used to reconnect segments
if options.point.plotFittedModel == 1
    if options.method == 3 || options.method == 4
        params      = dsStmList(1).auxData.groupModelParams(groupIdx,:); % Same for all tracks
        precip      = dsStmList(1).auxData.precipitation;
        evap        = dsStmList(1).auxData.evapotrans;
        modelFit    = p_model_lin(precip,evap,params);
        knmiDates   = dsStmList(1).auxData.knmiDates;
        
        plot(knmiDates,modelFit*1000 - shift,':b','LineWidth',3)
        legendtext = [legendtext "Fitted Model"];
    else
        datesComb = [];
        for i = 1:length(dsStmList)
            datesComb   = [datesComb; dates{i}];
        end
        datesComb   = sort(datesComb,'ascend');
        doy         = day(datesComb,'dayofyear')/365.25;
        t           = unwrap(doy*2*pi)/(2*pi);
        params      = dsStmList(1).auxData.groupModelParams(groupIdx,:); % Same for all tracks
        modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
    
        plot(datesComb,modelFit*1000 - shift,':b','LineWidth',2)
        legendtext = [legendtext "Fitted Model"];
    end
end

if options.point.plotGroupMean == 1
    plot(datesMean,meanDefoAll - shift,':b','LineWidth',2)
    legendtext = [legendtext "Group Median"];
end

for i = 1:length(dsStmList)
%     epochIdx = dsStmList(i).epochAttrib.timestamp >=datetime(2020,3,1) & dsStmList(i).epochAttrib.timestamp < datetime(2022,1,1)
    plot(dsStmList(i).epochAttrib.timestamp,vertDefo{i} - shift,'LineWidth',1)
    legendtext      = [legendtext string(dsStmList(i).datasetId)];
end

% if options.point.plotMean == 1
%     plot(datesMean,meanDefoAll-shift,'r','linewidth',2)
%     legendtext = [legendtext "Mean"];
% end

if ~isempty(extTT) && options.point.plotExt == 1
    plot(extTT.Properties.RowTimes,extTT.(1),'k','LineWidth',2)
    legendtext = [legendtext "Extensometer"];
end

if options.point.plotPrecip == 1
    yyaxis right
    epochIdx = dsStmList(1).auxData.knmiDates >= dsStmList(1).epochAttrib.timestamp(1) & dsStmList(1).auxData.knmiDates <= dsStmList(1).epochAttrib.timestamp(end);
    bar(dsStmList(1).auxData.knmiDates(epochIdx),dsStmList(1).auxData.precipitation(epochIdx))
    ylabel('Precipitation [mm]')
    yyaxis left
end

legend(legendtext,'interpreter','none')
title(['Parcel ID ' num2str(options.point.idSel) ',  Soil code: ' char(soilCode)])
ylabel('Relative Vertical Position [mm]')
set(gcf,'color','w')
set(gca,'fontsize',14)
grid on

end

