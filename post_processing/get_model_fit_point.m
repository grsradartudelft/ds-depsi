function fit = get_model_fit_point(dsStmList,pointSel,options)

% This function determines the differences between the shifted unwrapped
% segemnts and the group model and evaluates the goodness of fit. 
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,pointSel));
groupIdx = find(dsStmList(1).auxData.groupIdList == dsStmList(1).pntAttrib.groupId(pointIdx));


if isempty(groupIdx)
    % In this case, no estimation has been made
    SSR = nan;
    SST = nan;
    residuals = nan;
    R2  = nan;
    return
end

params = dsStmList(1).auxData.groupModelParams(groupIdx,:);
if options.method == 3 || options.method == 4
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params)';
else
    modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
end

if strcmp(options.fit.type,'r2')
    SSR = 0;
    SST = 0;
    MSR = 0;
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = find(ismember(dsStmList(i).pntAttrib.parcelId,pointSel));
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        dispersion  = segments - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v);
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        residuals   = segments - (modelFit(epochIdx)-shift);
        
        SSR         = SSR + sum(residuals(:).^2,'omitnan');
        MSR         = MSR + mean(residuals(:).^2,'omitnan');
        SST         = SST + sum(dispersion(:).^2,'omitnan');
    end
    
    residuals = sqrt(MSR);
    fit = 1 - SSR/SST;
end


if strcmp(options.fit.type,'sst')
       SSR = 0;
    SST = 0;
    MSR = 0;
    for i = 1:length(dsStmList)
        dates       = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        epochIdx    = ismember(dsStmList(1).auxData.knmiDates',dates);
        pointIdx    = find(ismember(dsStmList(i).pntAttrib.parcelId,pointSel));
        
        ph2v        = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        segments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:) * ph2v;
        
        dispersion  = segments - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v);
        
        shift       = mean(modelFit(epochIdx) - (dsStmList(i).auxData.groupMean(groupIdx,:)*ph2v),'omitnan');
        residuals   = segments - (modelFit(epochIdx)-shift);
        
        SSR         = SSR + sum(residuals(:).^2,'omitnan');
        MSR         = MSR + mean(residuals(:).^2,'omitnan');
        SST         = SST + sum(dispersion(:).^2,'omitnan');
    end
    fit = SST;
end

if strcmp(options.fit.type,'coh')
    coherence = [];
    for i = 1:length(dsStmList)
        pointIdx    = find(ismember(dsStmList(i).pntAttrib.parcelId,pointSel));
        coherence   = [coherence dsStmList(i).obsData.dcCoherence(pointIdx)];
    end
    fit = mean(coherence,'omitnan');
end