function stm_linear_plot_old(dsStmList,options)
warning('off','MATLAB:polyshape:repairedBySimplify')

parcelIds   = dsStmList(1).pntAttrib.parcelId;
S           = shaperead(dsStmList(1).techniqueAttrib.parcel_shp_path);
objectids   = [S.objectid];

for i = 1:length(parcelIds)
    shapeIdxFull(i) = find(objectids == parcelIds(i));
end
shapeIdxFull = shapeIdxFull';

% Linear deformation rates
% linRate     = abs(dsStmList(1).auxData.groupModelParams(:,3));


for p = 1:length(dsStmList(1).auxData.groupIdList)
    
    [meanDefoAll, dates]    = meanOfTracks(dsStmList,p);
    doy                     = day(dates,'dayofyear')/365.25;
    goodIdx                 = ~isnan(meanDefoAll);
    coeffs                  = polyfit(doy(goodIdx),meanDefoAll(goodIdx),1);
    linRate(p)              = coeffs(1);
end

absRate     = abs(linRate);
normRate    = (absRate-min(absRate))/(max(absRate)-min(absRate));

% Plot colour from linear deformation rate
cmap = jet(256);
colorRow = round(normRate * 255)+1;

figure('Position',[409 117 1045 860],'color','w');
hold on
for p = 1:length(dsStmList(1).auxData.groupIdList)

    % Find all parcels in this group
    parcelIdx   = dsStmList(1).pntAttrib.peilgebied == dsStmList(1).auxData.groupIdList(p);
    parcelIds   = dsStmList(1).pntAttrib.parcelId(parcelIdx);
    shapeIdx    = shapeIdxFull(parcelIdx);
    
    for i = 1:length(shapeIdx)
        polyvec(i) = polyshape(S(shapeIdx(i)).X,S(shapeIdx(i)).Y);
    end
    peilPolygon(p) = union(polyvec);
    
    plot(peilPolygon(p),'FaceColor',cmap(colorRow(p),:))
end
colorbar
caxis([min(linRate) max(linRate)])

end

