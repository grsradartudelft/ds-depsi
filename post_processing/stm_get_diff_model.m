function diffModel = stm_get_diff_model(dsStmList,groupSel)
% Plot the daily 6-day difference of the fitted model of a selected group

groupIdx    = find(ismember(dsStmList(1).auxData.groupIdList,groupSel));

precip      = dsStmList(1).auxData.precipitation;
evap        = dsStmList(1).auxData.evapotrans;
params      = dsStmList(1).auxData.groupModelParams(groupIdx,:);
modelFit    = p_model_lin(precip,evap,params);

diffModel           = nan(size(modelFit));
diffModel(7:6:end)  = (modelFit(7:6:end)  - modelFit(1:6:end-6));
diffModel(8:6:end)  = (modelFit(8:6:end)  - modelFit(2:6:end-6));
diffModel(9:6:end)  = (modelFit(9:6:end)  - modelFit(3:6:end-6));
diffModel(10:6:end) = (modelFit(10:6:end) - modelFit(4:6:end-6));
diffModel(11:6:end) = (modelFit(11:6:end) - modelFit(5:6:end-6));
diffModel(12:6:end) = (modelFit(12:6:end) - modelFit(6:6:end-6));

end

