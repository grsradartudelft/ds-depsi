function [accept,T] = omt_simple(dsStm,unwrappedSegments,pointSel,settings)

% This function determines the differences between the unwrapped
% segemnts and the *parcel* model and evaluates the goodness of fit.

pointIdx    = find(dsStm.pntAttrib.parcelId == pointSel);
params      = dsStm.pntAttrib.initParams(pointIdx,:);
dates       = dateshift(dsStm.epochAttrib.timestamp, 'start', 'day');
epochIdx    = ismember(dsStm.auxData.knmiDates',dates);

if settings.seg.method == 3 || settings.seg.method == 4
    precip      = dsStm.auxData.precipitation;
    evap        = dsStm.auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params)';
else
    knmiDates   = sort(dsStm.auxData.knmiDates,'ascend');
    doy         = day(knmiDates,'dayofyear')/365.25;
    t           = unwrap(doy*2*pi)/(2*pi);
    modelFit    = (params(1)*cos( 2*pi*t + params(2) ) + params(3)*t)';
end

ph2v        = -1*dsStm.techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
segments    = diff([0 unwrappedSegments * ph2v]);
modelDiff   = diff([0 modelFit(epochIdx)]);

residual    = segments - modelDiff;
CRB         = diag(estimate_Qy(dsStm,pointSel))';
T           = sum(residual.^2./CRB,'omitnan');

% Overall model test
dof         = length(params);
alpha       = 0.2;
crit        = chi2inv(1-alpha,dof);

if T > crit
    accept = 0;
else
    accept = 1;
end




