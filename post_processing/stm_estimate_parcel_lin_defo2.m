function [dsStmList,linDefo] = stm_estimate_parcel_lin_defo2(dsStmList,parcelIdSel)

trackTT = timetable();
for i = 1:length(dsStmList)
    pntIdx          = find(dsStmList(i).pntAttrib.parcelId == parcelIdSel);    
    ph2v            = dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
    vertDefo        = dsStmList(i).obsData.vertUnwPhase(pntIdx,:)'*ph2v*1000;
    dates           = dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day');
    trackTT         = synchronize(trackTT,timetable(dates,vertDefo,'VariableNames',{dsStmList(i).datasetId}));
end

combTT = mergevars(trackTT,string(trackTT.Properties.VariableNames));
combTs = nan(height(combTT),1);

for l=1:length(combTs)
    if any(~isnan(combTT.(1)(l,:)))
        combTs(l) = combTT.(1)(l,~isnan(combTT.(1)(l,:)));
    end
end

% for debugging
% figure
% plot(combTT.Time,combTs)

epochIdx = ~isnan(combTs);
doy      = day(combTT.Time,'dayofyear')/365.25;
t        = unwrap(doy*2*pi)/(2*pi);
p        = polyfit(t(epochIdx),combTs(epochIdx),1);
linDefo  = p(1);

for i = 1:length(dsStmList)
    pntIdx = find(dsStmList(i).pntAttrib.parcelId == parcelIdSel);    
    dsStmList(i).pntAttrib.linDefo(pntIdx) = linDefo;
end


end