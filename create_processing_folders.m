function settings = create_processing_folders(settings)

%% Create root insar dir if not already there
if ~isfolder(settings.runDir)
    mkdir(settings.runDir)
end

%% Create depsi dir if not already there
if ~isfolder(settings.depsiDir)
    mkdir(settings.depsiDir)
end

%% Create phase estimation dir if not already there
if ~isfolder(settings.phaseEstDir)
    mkdir(settings.phaseEstDir)
end

%% Create stm dir if not already there
if ~isfolder(settings.stmDir)
    mkdir(settings.stmDir)
end

%% Create segmentation dir if not already there
if ~isfolder(settings.segDir)
    mkdir(settings.segDir)
end

%% Create output dir if not already there
if ~isfolder(settings.outputDir)
    mkdir(settings.outputDir)
end

%% Create parcel phase estimation directories
for i = 1:settings.numTracks
    
    % Make directory
    settings.phaseEstStackDir(i) = strcat(settings.phaseEstDir,settings.stackId(i),"/");
    if ~isfolder(settings.phaseEstStackDir(i))
        mkdir(settings.phaseEstStackDir(i))
    end
end

%% Create depsi processing directories and fill them the junk depsi needs
for i = 1:settings.numTracks
    
    % Make directory
    settings.depsiStackdir(i) = strcat(settings.depsiDir,settings.stackId(i),"/");
    if ~isfolder(settings.depsiStackdir(i))
        mkdir(settings.depsiStackdir(i))
    end
    
    if ~isfolder(strcat(settings.depsiStackdir(i),"plots"))
        mkdir(strcat(settings.depsiStackdir(i),"plots"));
    end
    
    % Detect files in master directory
    filelist    = dir(fullfile(char(settings.masterDir(i)), 'dem_radar*.raw'));
    demFilename = filelist.name;
    
    filelist    = dir(fullfile(char(settings.masterDir(i)), 'lam*.raw'));
    lamFilename = filelist.name;
    
    filelist    = dir(fullfile(char(settings.masterDir(i)), 'phi*.raw'));
    phiFilename = filelist.name;
    
    resFilename = 'master.res'; 
    
    % Copy dem, lam, phi and res files
    copyfile([char(settings.masterDir(i)) demFilename],[char(settings.depsiStackdir(i)) demFilename])
    copyfile([char(settings.masterDir(i)) lamFilename],[char(settings.depsiStackdir(i)) lamFilename])
    copyfile([char(settings.masterDir(i)) phiFilename],[char(settings.depsiStackdir(i)) phiFilename])
    copyfile([char(settings.masterDir(i)) resFilename],[char(settings.depsiStackdir(i)) resFilename])
    
    % Save filenames for making the depsi param file and STMs
    settings.demFilename(i)         = string(demFilename);
    settings.dsExportFilename(i)    = strcat(settings.stackId(i), "_", settings.pe.depsiExportFile);
    settings.dsStmFilename(i)       = strcat(settings.stackId(i), "_", settings.dsStmFile);
    settings.psStmFilename(i)       = strcat(settings.stackId(i), "_", settings.psStmFile);
end

end

