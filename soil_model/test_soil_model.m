clear
clc
close all
addpath('/Users/pconroy/phd/code/ds-depsi/segmentation/model_based')

% location = 'aldeboarn'
% location = 'assendelft'
% location = 'rouveen'
% location = 'vlist'
location = 'zegveld'

%% File paths

if strcmp(location,'zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ZEG_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_348_cabauw.txt';
%     
%     extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ZEG_RF_jan2023_combined.csv';
%     knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
    
elseif strcmp(location,'rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ROV_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_279_hoogveen.txt';
    
elseif strcmp(location,'aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ALB_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ASD_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_257_wijkaanzee.txt';
    
elseif strcmp(location,'vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/VLI_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);
% extdata.(1) = extdata.(1)/1000;
%% Load precip data
% startDate       = datetime(2020,5,20);
startDate       = extdata.Time(1) - days(80);
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

%% Best model params
    if strcmp(location,'zegveld')
%         A           = 2.8e-4;
%         B           = 0.4;
%         C           = -2.8e-5;
%         tau         = 70;
        
        A           = 2.7e-4;
        B           = 0.36;
        C           = -2.3e-5;
        tau         = 69;
        
    elseif strcmp(location,'rouveen')
%         A           = 9e-5;
%         B           = 0.52;
%         C           = -2.3e-5;
%         tau         = 50;
        A           = 8.2e-5;
        B           = 0.768;
        C           = -2.9e-5;
        tau         = 54;

        extdata.(1) = extdata.(1)/1000;
        
    elseif strcmp(location,'aldeboarn')
        A           = 1.3e-4;
        B           = 1.3;
        C           = -1e-4;
        tau         = 80;
        
    elseif strcmp(location,'assendelft')
        A           = 9.2e-5;
        B           = 1.6;
        C           = -1.4e-4;
        tau         = 80;

    elseif strcmp(location,'vlist')
%         A           = 9e-5;
%         B           = 1.4;
%         C           = -2.2e-5;
%         tau         = 60;
        A   = 6.38e-05;
        B   = 1.26;
        C   = -2.01e-05;
        tau = 86;
        extdata.(1) = extdata.(1)/1000;
    end

%% Combine datasets
combTT          = synchronize(extdata,knmiTT);
ext             = combTT.(1);
precip          = combTT.(3);
evap            = combTT.(4);
dates           = combTT.Time;

modelParams = [A,B,C,tau];
[bestModel, bestRev, bestIrr]   = p_model_lin(precip,evap,modelParams);

xP = modelParams(1)*modelParams(2);
xE = modelParams(1);
xI = modelParams(3);

disp('Final values:')
disp(['x_P  = ' num2str(xP,3)])
disp(['x_E  = ' num2str(xE,3)])
disp(['x_I  = ' num2str(xI,3)])
disp(['tau  = ' num2str(round(modelParams(4)),3)])

%% Error calculations

% Overall RMSE
shift       = mean(bestModel - ext,'omitnan');
residuals   = bestModel-shift - ext;
rmse        = sqrt(mean((bestModel-shift - ext).^2,'omitnan'));
rmseNorm    = rmse/std(ext,'omitnan');

disp('___________________________________')
disp(['RMSE                = ' num2str(rmse,3) ' m'])
disp(['RMSE/sigma          = ' num2str(rmseNorm,3)])


% Training RMSE
splitDate       = datetime(2022,10,1);
trainingEpochs  = dates < splitDate;

rmseTrain       = sqrt(mean((bestModel(trainingEpochs)-shift - ext(trainingEpochs)).^2,'omitnan'));
rmseTrainNorm   = rmseTrain/std(ext,'omitnan');

disp(' ')
disp(['Training RMSE       = ' num2str(rmseTrain,3) ' m'])
disp(['Training RMSE/sigma = ' num2str(rmseTrainNorm,3)])

% Testing RMSE
testingEpochs   = dates >= splitDate;

rmseTest        = sqrt(mean((bestModel(testingEpochs)-shift - ext(testingEpochs)).^2,'omitnan'));
rmseTestNorm    = rmseTest/std(ext,'omitnan');

disp(' ')
disp(['Testing RMSE        = ' num2str(rmseTest,3) ' m'])
disp(['Testing RMSE/sigma  = ' num2str(rmseTestNorm,3)])

%% Plot model
figure('color','w')
hold on
plot(dates,ext*1000,'k','linewidth',3)
plot(dates,(bestModel-shift)*1000,'b','linewidth',3)
ylabel('Relative Surface Elevation (mm)')
xlim([startDate endDate])
% ylim([-0.06 0.06])
ylim([-60 60])


set(gca,'fontsize',16)
grid on
% legend('Extensometer','Model')
% title([location ' (RMSE = ' num2str(rmse*1000,2) ' mm)'])
yyaxis right
plot(combTT.Time,bestIrr*1000,'r:','linewidth',2)
ylabel('Estimated Irreversible Subsidence (mm)')
legend('Extensometer','Model (Total)','Model (Irreversible)','location','sw')

ax=gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'r';
ax.YAxis(2).Limits = [-35 0];

xline(splitDate,'k--','linewidth',2,'handlevisibility','off')

%% Overall model test

varExt      = 1e-8;                                 % From supplied precision of the raw data
T           = sum(residuals.^2/varExt,'omitnan');   % T value of model vs extensometer
alpha       = 0.05;                                 % Significance level
crit        = chi2inv(1-alpha,4);                   % Critical value (4 degrees of freedom)

% Model variance factor
% Amount by which the T must be reduced to accept the model per observation
varFac      = T/crit; 
Tnew        = sum(residuals.^2/(varExt*varFac),'omitnan');

% "Model precision"
varTot      = 1/crit * sum(residuals.^2,'omitnan');
varModel    = varTot - varExt;
sigmaModel  = sqrt(varModel)

%% Regression plot (?)

R2   = 1 - sum(residuals.^2,'omitnan')/sum((ext-mean(ext,'omitnan')).^2,'omitnan')
lims = 0.05;

figure('color','w')
hold on
plot(ext(trainingEpochs),bestModel(trainingEpochs)-shift,'r.','markersize',6)
plot(ext(testingEpochs),bestModel(testingEpochs)-shift,'b.','markersize',6)
plot(linspace(-lims,lims,2),linspace(-lims,lims,2),'k','handlevisibility','off')
legend('Training','Testing','location','nw')
xlabel('Measurement values (m)')
ylabel('Model values (m)')
xlim([-lims,lims])
ylim([-lims,lims])
xticks(-lims:0.01:lims)
yticks(-lims:0.01:lims)
set(gca,'fontsize',14)