function hDiff_ds = p_model_diff_fitting_exp(precip,evap,epochIdx,params,settings)

A       = params(1);
B       = params(2);
C       = params(3);
tau     = round(params(4));

% Add "constraints" to the model (for fminsearch)
if A < settings.seg.Amin || A > settings.seg.Amax
    hDiff_ds = ones(size(epochIdx))*9999999;  
    return
end
if B < settings.seg.Bmin || B > settings.seg.Bmax
    hDiff_ds = ones(size(epochIdx))*9999999;  
    return
end
if C < settings.seg.Cmin || C > settings.seg.Cmax
    hDiff_ds = ones(size(epochIdx))*9999999;  
    return
end
if tau < settings.seg.Tmin || tau > settings.seg.Tmax
    hDiff_ds = ones(size(epochIdx))*9999999;  
    return
end

% The first intTime number of values will be nans, that is okay because this
% keeps the output height the same length as the inputs, which is then
% sampled to match the insar dates, removing the nans in the process
reversible  = nan(size(precip));
for i = 1:length(precip)
    reversible(i) = sum( A*(B*precip(1:i) - evap(1:i)) .*exp(-1*(1:i)'/tau) );
    dryFlag(i) = reversible(i) < 0;
end

% Position time series spanning full range of meteo dates
irreversible = cumsum(C*dryFlag');
height = reversible + irreversible;

% Downsample to find the daily, 6-day differences
hDiff   = [];
idx     = (1:length(height))';
idxDs   = [];

for i = 1:6
    hDiffLoop = [nan; diff(height(i:6:end))];
    hDiff = [hDiff; hDiffLoop];
    idxDs = [idxDs; idx(i:6:end)];
end

[~,sortIdx] = sort(idxDs);
hDiff       = hDiff(sortIdx);
    
% Downsample to match insar dates
hDiff_ds = hDiff(epochIdx);
end

