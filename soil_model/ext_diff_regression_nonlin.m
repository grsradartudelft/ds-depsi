clear
clc
close all

location = 'Zegveld'
% location = 'Rouveen'
% location = 'Aldeboarn'
% location = 'Assendelft'
% location = 'Vlist'

%% File paths

if strcmp(location,'Zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ZEG_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
    
elseif strcmp(location,'Rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ROV_RF_feb2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_279_hoogveen.txt';
    
elseif strcmp(location,'Aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ALB_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'Assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ASD_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_257_wijkannzee.txt';
    
elseif strcmp(location,'Vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/VLI_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);

%% Load precip data
startDate       = extdata.Time(1);
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

%% Initial model params
% if strcmp(location,'Zegveld')
%     A           = 2.8e-4;
%     B           = 0.52;
%     C           = -1e-5;
%     tau         = 70;
% 
% elseif strcmp(location,'Rouveen')
%     A           = 9e-5;
%     B           = 0.52;
%     C           = -2.3e-5;
%     tau         = 50;
%     extdata.(1) = extdata.(1)/1000;
%     
% elseif strcmp(location,'Aldeboarn')
%     A           = 7e-5;
%     B           = 1.45;
%     C           = -1e-4;
%     tau         = 80;
%     
% elseif strcmp(location,'Assendelft')
%     A           = 9.2e-5;
%     B           = 1.6;
%     C           = -1.4e-4;
%     tau         = 80;
%     
% elseif strcmp(location,'Vlist')
%     A           = 9e-5;
%     B           = 1.4;
%     C           = -2.2e-5;
%     tau         = 60;
%     
% end

A           = 1e-5;
B           = 1;
C           = 5e-5;
tau         = 80;

if strcmp(location,'Rouveen')
    extdata.(1) = extdata.(1)/1000;
end

%% Combine datasets
combTT          = synchronize(extdata,knmiTT);
ext             = combTT.(1);
precip          = combTT.(3);
evap            = combTT.(4);
dates           = combTT.Time;

% Downsample to find the daily, 6-day differences
extDiff = [];
datesDs = [];
for i = [1:2,5:6]
    extDiffLoop = [nan; diff(ext(i:6:end))];
    extDiff = [extDiff; extDiffLoop];
    datesDs = [datesDs; dates(i:6:end)];
end

[~,sortIdx] = sort(datesDs);
extDiff     = extDiff(sortIdx);

epochIdx    = find(ismember(dates,datesDs));

%% Add noise 
extDiffNoisy = extDiff + normrnd(0,1e-2,size(extDiff));

% figure
% yyaxis left
% plot(dates,ext)
% yyaxis right
% hold on
% plot(datesDs,extDiff)
% % plot(datesDs,extDiffNoisy)

%% fminunc
A_fmin      = [];
b_fmin      = [];
Aeq_fmin    = [];
beq_fmin    = [];
lb          = [0,       0.3,  1e-5,  50];
ub          = [1e-3,    1.7,  1e-2,  80];

%% Solve
% Options
maxFunEvals = 1e6;
funTol      = 1e-7;
optTol      = 1e-7;
usePar      = false;
opts        = optimoptions('fmincon','MaxFunEvals',maxFunEvals,'OptimalityTolerance',optTol,'FunctionTolerance',funTol,'UseParallel',usePar);

% Solver
cost                            = @(theta) sqrt(mean(mean((extDiffNoisy - p_model_diff_fitting_nonlin(precip,evap,epochIdx,theta)).^2,'omitnan'),'omitnan'));
[modelParams,costVal]           = fmincon(cost,[A, B, C, tau],A_fmin,b_fmin,Aeq_fmin,beq_fmin,lb,ub,[],opts);
% [modelParams,costVal]           = fminsearch(cost,[A, B, C, tau]);
[bestModel, bestRev, bestIrr]   = p_model_nonlin(precip,evap,modelParams);

disp('Final values:')
disp(['A   = ' num2str(modelParams(1),3)])
disp(['B   = ' num2str(modelParams(2),3)])
disp(['C   = ' num2str(modelParams(3),3)])
disp(['tau = ' num2str(round(modelParams(4)),3)])

%% Plot model

shift = mean(bestModel - ext,'omitnan');
rmse = sqrt(mean((bestModel-shift - ext).^2,'omitnan'));

figure('color','w')
hold on
plot(combTT.Time,ext+shift,'linewidth',2)
plot(combTT.Time,bestModel,'linewidth',2)
ylabel('Surface position (m)')
legend('Extensometer','Model')
set(gca,'fontsize',14)
grid on
title([location ' (RMSE = ' num2str(rmse*1000,2) ' mm)'])

% figure('color','w')
% hold on
% plot(combTT.Time,bestRev,'linewidth',2)
% ylabel('Reversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on

figure('color','w')
hold on
plot(combTT.Time,bestIrr,'linewidth',2)
ylabel('Irreversible subsidence (m)')
set(gca,'fontsize',14)
grid on