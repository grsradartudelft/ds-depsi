function [height,reversible,irreversible] = p_model_exp(precip,evap,params)

A       = params(1);
B       = params(2);
C       = params(3);
tau     = round(params(4));

reversible  = nan(size(precip));
for i = 1:length(precip)
    reversible(i) = sum( A*(B*precip(1:i) - evap(1:i)) .*exp(-1*(1:i)'/tau) );
    dryFlag(i) = reversible(i) < 0;
end

% Position time series spanning full range of meteo dates
irreversible = cumsum(C*dryFlag');
height = reversible + irreversible;

end

