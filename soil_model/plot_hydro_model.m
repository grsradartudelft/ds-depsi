function plot_hydro_model(dsStmList,params)

precip  = dsStmList(1).auxData.precipitation;
evap    = dsStmList(1).auxData.evapotrans;
dates   = dsStmList(1).auxData.knmiDates;

[height, reversible, irreversible] = p_model_lin(precip,evap,params);

figure
plot(dates,height,'linewidth',2)
ylabel('H (mm)')

end

