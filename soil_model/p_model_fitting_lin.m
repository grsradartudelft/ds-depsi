function height_ds = p_model_fitting_lin(precip,evap,epochIdx,params,settings)

A       = params(1);
B       = params(2);
C       = params(3);
intTime = round(params(4));

% Add "constraints" to the model (for fminsearch)
if A < settings.seg.Amin || A > settings.seg.Amax
    height_ds = ones(size(epochIdx))*9999999;  
    return
end
if B < settings.seg.Bmin || B > settings.seg.Bmax
    height_ds = ones(size(epochIdx))*9999999;  
    return
end
if C < settings.seg.Cmin || C > settings.seg.Cmax
    height_ds = ones(size(epochIdx))*9999999;  
    return
end
if intTime < settings.seg.Tmin || intTime > settings.seg.Tmax
    height_ds = ones(size(epochIdx))*9999999;  
    return
end

% The first intTime number of values will be nans, that is okay because this
% keeps the output height the same length as the inputs, which is then
% sampled to match the insar dates, removing the nans in the process
reversible  = nan(size(precip));
for i = intTime+1:length(precip)
    reversible(i) = A*sum(B*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i) = reversible(i) < 0;
end

% Position time series spanning full range of meteo dates
irreversible = cumsum(C*dryFlag)';
height = reversible + irreversible;

% Downsample to match insar dates
height_ds = height(epochIdx);
end

