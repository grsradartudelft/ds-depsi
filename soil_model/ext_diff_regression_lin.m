clear
clc
close all
addpath('/Users/pconroy/phd/code/ds-depsi/segmentation/model_based')

saveOutput = 0
doOpt = 0

% location = 'zegveld'
location = 'rouveen'
% location = 'aldeboarn'
% location = 'assendelft'
% location = 'vlist'

%% File paths

if strcmp(location,'zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ZEG_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
    
elseif strcmp(location,'rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ROV_RF_feb2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_279_hoogveen.txt';
    
elseif strcmp(location,'aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ALB_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ASD_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_257_wijkannzee.txt';
    
elseif strcmp(location,'vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/VLI_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);
extdata.(1) = extdata.(1)/1000;
%% Load precip data
% startDate       = datetime(2020,5,20);
startDate       = extdata.Time(1) - days(80);
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

%% Best model params
if doOpt == 0
    if strcmp(location,'zegveld')
        A           = 2.8e-4;
        B           = 0.4;
        C           = -2.8e-5;
        tau         = 70;
        
    elseif strcmp(location,'rouveen')
        A           = 9e-5;
        B           = 0.52;
        C           = -2.3e-5;
        tau         = 50;
        
    elseif strcmp(location,'aldeboarn')
        A           = 1.3e-4;
        B           = 1.4;
        C           = -1e-4;
        tau         = 80;
        
    elseif strcmp(location,'assendelft')
        A           = 9.2e-5;
        B           = 1.6;
        C           = -1.4e-4;
        tau         = 80;

    elseif strcmp(location,'vlist')
%         A           = 9e-5;
%         B           = 1.4;
%         C           = -2.2e-5;
%         tau         = 60;
        A   = 6.38e-05
        B   = 1.26
        C   = -2.01e-05
        tau = 86
    end
else
    % Generic initial guess for fminsearch
    A           = 8e-5;
    B           = 1;
    C           = -5e-5;
    tau         = 70;
end

if strcmp(location,'vlist')
    extdata.(1) = extdata.(1)*1000;
end

%% Combine datasets
combTT          = synchronize(extdata,knmiTT);
ext             = combTT.(1);
precip          = combTT.(3);
evap            = combTT.(4);
dates           = combTT.Time;

% Downsample to find the daily, 6-day differences
extDiff = [];
datesDs = [];
for i = 1:6 %[1:2,5:6]
    extDiffLoop = [nan; diff(ext(i:6:end))];
    extDiff = [extDiff; extDiffLoop];
    datesDs = [datesDs; dates(i:6:end)];
end

[~,sortIdx] = sort(datesDs);
extDiff     = extDiff(sortIdx);

epochIdx    = find(ismember(dates,datesDs));

%% Add noise
extDiffNoisy = extDiff; % + normrnd(0,0,size(extDiff));

% figure
% yyaxis left
% plot(dates,ext)
% yyaxis right
% hold on
% plot(datesDs,extDiff)
% % plot(datesDs,extDiffNoisy)

%% fminunc

settings.seg.Amin               = 0;                % Minimum model amplitude (m/m)
settings.seg.Amax               = 1e-3;             % Maximum model amplitude (m/m)
settings.seg.Bmin               = 0.2;              % Minimum precip/evap balance (m/m)
settings.seg.Bmax               = 1.7;              % Maximum precip/evap balance (m/m)
settings.seg.Cmin               = -1e-4;            % Minimum active subsidence rate (m/day)
settings.seg.Cmax               = -1e-5;            % Maximum active subsidence rate (m/day)
settings.seg.Tmin               = 50;               % Minimum integration time (days)
settings.seg.Tmax               = 150;              % Maximum integration time (days)

A_fmin      = [];
b_fmin      = [];
Aeq_fmin    = [];
beq_fmin    = [];
lb          = [0,       0.4,  -5e-5,  50];
ub          = [1e-3,    1.7,  -1e-5,  160];



%% Solve

if doOpt == 1
    % Options
    maxFunEvals = 1e6;
    funTol      = 1e-7;
    optTol      = 1e-7;
    usePar      = false;
    opts        = optimoptions('fmincon','MaxFunEvals',maxFunEvals,'OptimalityTolerance',optTol,'FunctionTolerance',funTol,'UseParallel',usePar);
    
    % Solver
    cost                            = @(theta) sqrt(mean(mean((extDiffNoisy - p_model_diff_fitting_exp(precip,evap,epochIdx,theta,settings)).^2,'omitnan'),'omitnan'));
    % [modelParams,costVal]           = fmincon(cost,[A, B, C, tau],A_fmin,b_fmin,Aeq_fmin,beq_fmin,lb,ub,[],opts);
    [modelParams,costVal]           = fminsearch(cost,[A, B, C, tau]);
    
else
    modelParams = [A,B,C,tau];
end
[bestModel, bestRev, bestIrr]   = p_model_lin(precip,evap,modelParams);

x1 = modelParams(1)*modelParams(2);
x2 = modelParams(1);
x3 = modelParams(3);

disp('Final values:')
% disp(['A   = ' num2str(modelParams(1),3)])
% disp(['B   = ' num2str(modelParams(2),3)])
% disp(['C   = ' num2str(modelParams(3),3)])
disp(['x1  = ' num2str(x1,3)])
disp(['x2  = ' num2str(x2,3)])
disp(['x3  = ' num2str(x3,3)])
disp(['tau = ' num2str(round(modelParams(4)),3)])

%% Plot model

shift       = mean(bestModel - ext,'omitnan');
rmse        = sqrt(mean((bestModel-shift - ext).^2,'omitnan'));
rmseNorm    = rmse/std(ext,'omitnan');
% rmseNorm    = rmse/(max(ext)-min(ext))

figure('color','w')
hold on
plot(dates,ext*1000,'k','linewidth',3)
plot(dates,(bestModel-shift)*1000,'b','linewidth',3)
ylabel('Relative Surface Elevation (mm)')
xlim([startDate endDate])
% ylim([-0.06 0.06])
ylim([-60 60])


set(gca,'fontsize',16)
grid on
% legend('Extensometer','Model')
% title([location ' (RMSE = ' num2str(rmse*1000,2) ' mm)'])
yyaxis right
plot(combTT.Time,bestIrr*1000,'r:','linewidth',2)
ylabel('Estimated Irreversible Subsidence (mm)')
legend('Extensometer','Model (Total)','Model (Irreversible)','location','sw')

ax=gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'r';
ax.YAxis(2).Limits = [-35 0];

% figure('color','w')
% hold on
% plot(combTT.Time,bestRev,'linewidth',2)
% ylabel('Reversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on

% figure('color','w')
% hold on
% plot(combTT.Time,bestIrr,'linewidth',2)
% ylabel('Irreversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on

%% Save output to csv
if saveOutput == 1
    outputTT = timetable(dates,ext,bestModel-shift,'variablenames',{'Extensometer','Model'});
    outputDir = '/Users/pconroy/phd/projects/param_model/';
    writetimetable(outputTT,[outputDir location '_param_model.csv']);
end