function height = p_model_montecarlo_lin(precip,evap,space,params)

A       = space(:,1);
B       = space(:,2);
C       = space(:,3);
% C       = params(3);
intTime = round(params(4));

% The first intTime number of values will be nans, that is okay because this
% keeps the output height the same length as the inputs, which is then
% sampled to match the insar dates, removing the nans in the process
reversible  = nan(length(precip),length(A),length(B));
irreversible  = nan(length(precip),length(A),length(B),length(C));
for i = intTime+1:length(precip)
    reversible(i,:,:) = A.*sum(B'.*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i,:,:)    = reversible(i,:,:) < 0;
    for j = 1:length(C)
        irreversible(i,:,:,j) = sum(C(j)*dryFlag(i-intTime:i,:,:),1);
    end
end

% Position time series spanning full range of meteo dates
% irreversible = cumsum(C.*dryFlag);
height = repmat(reversible,1,1,1,length(C)) + irreversible;


end

