clear
clc
close all

splitDate = datetime(2022,10,1);

% location = 'Zegveld'
location = 'Rouveen'
% location = 'Aldeboarn'
% location = 'Assendelft'
% location = 'Vlist'

%% File paths

if strcmp(location,'Zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ZEG_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_348_cabauw.txt';
    
elseif strcmp(location,'Rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/sept2023/ROV_RF_sept2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/sept2023/etmgeg_279_hoogveen.txt';
    
elseif strcmp(location,'Aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ALB_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'Assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ASD_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_257_wijkannzee.txt';
    
elseif strcmp(location,'Vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/VLI_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);

%% Load precip data
startDate       = extdata.Time(1)-64;
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

%% Initial model params
if strcmp(location,'Zegveld')
    A           = 2.8e-4;
    B           = 0.52;
    C           = -1e-5;
    tau         = 70;
    shift       = 0.036; %0.02;

elseif strcmp(location,'Rouveen')
    A           = 9e-5;
    B           = 0.52;
    C           = -2.3e-5;
    tau         = 50;
    shift       = 0.01;
    extdata.(1) = extdata.(1)/1000;
    
elseif strcmp(location,'Aldeboarn')
    A           = 1.3e-4;
    B           = 0.8;
    C           = -1e-4;
    tau         = 80;
    shift       = 0.005; 
    extdata.(1) = extdata.(1)/1000;
    
elseif strcmp(location,'Assendelft')
%     A           = 9.2e-5;
%     B           = 1.6;
%     C           = -1.4e-4;
%     tau         = 80;
    shift       = 0.005;
    
elseif strcmp(location,'Vlist')
%     A           = 9e-5;
%     B           = 1.4;
%     C           = -2.2e-5;
%     tau         = 60;
    shift       = 0.012;
    
end

% A           = 8e-5;
% B           = 1;
% C           = -3e-5;
% tau         = 70;


%% Combine datasets
combTT          = synchronize(extdata,knmiTT);
ext             = combTT.(1) - shift;
% ext             = combTT.(1)/1000 - shift;
precip          = combTT.(3);
evap            = combTT.(4);
dates           = combTT.Time;
epochIdx        = find(dates < splitDate);

%% fmincon
A_fmin      = [];
b_fmin      = [];
Aeq_fmin    = [];
beq_fmin    = [];
lb          = [0,       0.3,  -1.5e-4,  50];
ub          = [1e-3,    1.7,  -1e-5,    80];

settings.seg.Amin = lb(1);
settings.seg.Amax = ub(1);
settings.seg.Bmin = lb(2);
settings.seg.Bmax = ub(2);
settings.seg.Cmin = lb(3);
settings.seg.Cmax = ub(3);
settings.seg.Tmin = lb(4);
settings.seg.Tmax = ub(4);


%% Solve
% Options
maxFunEvals = 1e6;
funTol      = 1e-5;
usePar      = false;
opts        = optimoptions('fmincon','MaxFunEvals',maxFunEvals,'FunctionTolerance',funTol,'UseParallel',usePar);

% Solver
cost                            = @(theta) sqrt(mean(mean((ext(epochIdx) - p_model_fitting_lin(precip,evap,epochIdx,theta,settings)).^2,'omitnan'),'omitnan'));
% [modelParams,costVal]           = fmincon(cost,[A, B, C, tau],A_fmin,b_fmin,Aeq_fmin,beq_fmin,lb,ub,[],opts);
[modelParams,costVal]           = fminsearch(cost,[A, B, C, tau]);
[bestModel, bestRev, bestIrr]   = p_model_lin(precip,evap,modelParams);

xP = modelParams(1)*modelParams(2);
xE = modelParams(1);
xI = modelParams(3);

disp('Final values:')
disp(['x_P  = ' num2str(xP,3)])
disp(['x_E  = ' num2str(xE,3)])
disp(['x_I  = ' num2str(xI,3)])
disp(['tau  = ' num2str(round(modelParams(4)),3)])

disp(' ')
disp(['A   = ' num2str(modelParams(1),3)])
disp(['B   = ' num2str(modelParams(2),3)])

%% Plot model
figure('color','w')
hold on
plot(combTT.Time,ext,'linewidth',2)
plot(combTT.Time,bestModel,'linewidth',2)
ylabel('Surface position (m)')
legend('Extensometer','Model')
set(gca,'fontsize',14)
grid on
title([location ' (RMSE = ' num2str(costVal*1000,2) ' mm)'])
xline(splitDate,'k--','linewidth',2,'handlevisibility','off')

% figure('color','w')
% hold on
% plot(combTT.Time,bestRev,'linewidth',2)
% ylabel('Reversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on

% figure('color','w')
% hold on
% plot(combTT.Time,bestIrr,'linewidth',2)
% ylabel('Irreversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on