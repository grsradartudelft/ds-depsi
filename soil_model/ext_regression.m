clear
clc
close all

location = 'Zegveld'
% location = 'Rouveen'
% location = 'Aldeboarn'
% location = 'Assendelft'
% location = 'Vlist'

%% File paths

if strcmp(location,'Zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ZEG_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
    
elseif strcmp(location,'Rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ROV_RF_feb2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_279_hoogveen.txt';
    
elseif strcmp(location,'Aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ALB_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'Assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ASD_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_257_wijkannzee.txt';
    
elseif strcmp(location,'Vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/VLI_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);

%% Load precip data
startDate       = extdata.Time(1);
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

%% Initial model params
if strcmp(location,'Zegveld')
    A           = 2.8e-4;
    B           = 0.52;
    C           = -1e-5;
    tau         = 70;
    shift       = 0.02;

elseif strcmp(location,'Rouveen')
    A           = 9e-5;
    B           = 0.52;
    C           = -2.3e-5;
    tau         = 50;
    shift       = 0.01;
    extdata.(1) = extdata.(1)/1000;
    
elseif strcmp(location,'Aldeboarn')
    A           = 7e-5;
    B           = 1.45;
    C           = -1e-4;
    tau         = 80;
    shift       = 0.005;
    
elseif strcmp(location,'Assendelft')
    A           = 9.2e-5;
    B           = 1.6;
    C           = -1.4e-4;
    tau         = 80;
    shift       = 0.005;
    
elseif strcmp(location,'Vlist')
    A           = 9e-5;
    B           = 1.4;
    C           = -2.2e-5;
    tau         = 60;
    shift       = 0.012;
end

%% Combine datasets
combTT          = synchronize(extdata,knmiTT);

%% Parametric sweep
sweep = 1e-5:1e-5:1e-3; % A
% sweep = 0:1e-2:1.5; % B
% sweep = -1e-4:1e-6:0; % C
% sweep=1;

bestErr = 1;
bestModel = nan(size(combTT.(1)));
ext       = combTT.(1) - shift;

for i = 1:length(sweep)
    A           = sweep(i);

    params      = [A, B, C, tau];
    
    [modelOut, reversible, irreversible]  = p_model_lin(combTT.(3),combTT.(4),params);
    err         = modelOut - ext;
    rmse        = sqrt(mean(err.^2,'omitnan'));
    
    if rmse < bestErr
        bestModel   = modelOut;
        bestRev     = reversible;
        bestIrr     = irreversible;
        bestErr     = rmse;
        best        = sweep(i);
    end
end

disp(['RMSE = ' num2str(bestErr*1000,2) ' mm'])

%% Plot model
figure('color','w')
hold on
plot(combTT.Time,ext,'linewidth',2)
plot(combTT.Time,bestModel,'linewidth',2)
% plot(combTT.Time,modelOut,'linewidth',2)
ylabel('Surface position (m)')
ylim([-60 60])
legend('Extensometer','Model')
set(gca,'fontsize',16)
grid on
title([location ' (RMSE = ' num2str(bestErr*1000,2) ' mm)'])

% figure('color','w')
% hold on
% plot(combTT.Time,bestRev,'linewidth',2)
% ylabel('Reversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on

figure('color','w')
hold on
plot(combTT.Time,bestIrr,'linewidth',2)
ylabel('Irreversible subsidence (m)')
set(gca,'fontsize',14)
grid on