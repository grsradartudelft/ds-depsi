clear
clc
close all

% location = 'Zegveld'
location = 'Rouveen'
% location = 'Aldeboarn'
% location = 'Assendelft'
% location = 'Vlist'

%% File paths

if strcmp(location,'Zegveld')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ZEG_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
    shift       = 0.015;
    
elseif strcmp(location,'Rouveen')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ROV_RF_feb2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_279_hoogveen.txt';
    shift       = -0.01;
    
elseif strcmp(location,'Aldeboarn')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ALB_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_270_leeuwarden.txt';
    
elseif strcmp(location,'Assendelft')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/ASD_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_257_wijkannzee.txt';
    
elseif strcmp(location,'Vlist')
    extFile     = '/Users/pconroy/phd/projects/extensometer/feb2023/VLI_RF_jan2023_combined.csv';
    knmiFile    = '/Users/pconroy/phd/projects/contextual_data/datasets/knmi/feb2023/etmgeg_348_cabauw.txt';
end

%% Load extensometer data
extdata = readtable(extFile);
extdata.(1) = datetime(extdata.(1),'inputformat','yyyy-mm-dd');
extdata = table2timetable(extdata(:,1:2));
extdata.(1) = extdata.(1) - extdata.(1)(1);

%% Load precip data
startDate       = extdata.Time(1);
endDate         = extdata.Time(end);

opts            = delimitedTextImportOptions;
opts.DataLines  = 53;
knmiData        = readtable(knmiFile,opts);
knmiData        = knmiData(:,[2,12,23,41]);

knmiDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
temperature     = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,2))); % 0.1 deg C
precipitation   = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,3))); % 0.1 mm
precipitation(precipitation==-1) = 0;
evapotrans      = str2double(table2array(knmiData(knmiDates>=startDate & knmiDates<=endDate,4))); % 0.1 mm
knmiDates       = knmiDates(knmiDates>=startDate & knmiDates<=endDate);
knmiTT          = timetable(knmiDates,temperature/10,precipitation/10,evapotrans/10,'variablenames',{'Temp','Precip','Evap'});

if strcmp(location,'Rouveen')
    extdata.(1) = extdata.(1)/1000;
end

%% Combine datasets
combTT          = synchronize(extdata,knmiTT);
ext             = combTT.(1);
ext             = ext-shift;
precip          = combTT.(3);
evap            = combTT.(4);
dates           = combTT.Time;


%% Monte carlo space
nPoints  = 50;

Arange   = [0,      1e-3];
Brange   = [0.4,    1.7];
Crange   = [-5e-5,  -1e-5];
tauRange = [50,     150];

Aspace   = sort(rand(nPoints,1) * (Arange(2) - Arange(1)) + Arange(1));
Bspace   = sort(rand(nPoints,1) * (Brange(2) - Brange(1)) + Brange(1));
Cspace   = sort(rand(nPoints,1) * (Crange(2) - Crange(1)) + Crange(1));
% tauSpace = round(linspace(tauRange(1),tauRange(2),nPoints));

%% Solve

A = 1e-4;
B = 0.5;
C = -1e-5;
tau = 70;

params      = [A,B,C,tau];
modelOut    = p_model_montecarlo_lin(precip,evap,[Aspace,Bspace,Cspace],params);
cost        = squeeze(sqrt(mean( ( repmat(ext,1,nPoints,nPoints) - modelOut ).^2,1,'omitnan')));

%% Evalue best model
[val,idx]       = min(cost(:));
[aIdx,bIdx,cIdx]     = ind2sub(size(cost),idx);

bestParams      = params;
bestParams(1)   = Aspace(aIdx);
bestParams(2)   = Bspace(bIdx);
bestParams(3)   = Cspace(cIdx);

[bestModel, bestRev, bestIrr]   = p_model_lin(precip,evap,bestParams);

disp('Final values:')
disp(['A   = ' num2str(bestParams(1),3)])
disp(['B   = ' num2str(bestParams(2),3)])
disp(['C   = ' num2str(bestParams(3),3)])
disp(['tau = ' num2str(round(bestParams(4)),3)])

%% Plot model

% shift = mean(bestModel - ext,'omitnan');
rmse = sqrt(mean((bestModel - ext).^2,'omitnan'));

figure('color','w')
hold on
plot(combTT.Time,ext,'linewidth',2)
plot(combTT.Time,bestModel,'linewidth',2)
ylabel('Surface position (m)')
legend('Extensometer','Model')
set(gca,'fontsize',14)
grid on
title([location ' (RMSE = ' num2str(rmse*1000,2) ' mm)'])

% figure('color','w')
% hold on
% plot(combTT.Time,bestRev,'linewidth',2)
% ylabel('Reversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on
% 
% figure('color','w')
% hold on
% plot(combTT.Time,bestIrr,'linewidth',2)
% ylabel('Irreversible subsidence (m)')
% set(gca,'fontsize',14)
% grid on