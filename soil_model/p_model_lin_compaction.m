function [height, reversible, irreversible] = p_model_lin_compaction(precip,evap,params)

A       = params(1);
B       = params(2);
C       = params(3);
D       = params(4);
intTime = round(params(5));

reversible  = nan(size(precip));
for i = intTime+1:length(precip)
    reversible(i) = A*sum(B*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i) = reversible(i) < 0;
end

irreversible = cumsum(C*dryFlag)' + cumsum(D*~dryFlag)';

height = reversible + irreversible;

end

