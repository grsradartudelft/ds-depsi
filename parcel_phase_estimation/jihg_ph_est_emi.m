function [cpxcoh_opt] = jihg_ph_est_emi(cpxcoh)
%JIHG_PH_EST_EMI
% Based on Ansari et al. (2018)

% Author: FMG HEUFF - 02/2020.

% PC: disabled this test because large coherence matrices fail it every time. 
% The SVD can still function despite this

%     cpxcoh = cpxcoh_in;
    % check if matrix is positive definite, otherwise regularize
%     [~,p] = chol(abs(cpxcoh));
%     e=1e-6;
%     nslcs = length(cpxcoh);
%     iter = 1;
%     while p > 0
%         cpxcoh = cpxcoh+e*eye(nslcs);
%         [~,p] = chol(abs(cpxcoh));
%         e = e*2;
%         iter=iter+1;
%         if iter > 10
%             cpxcoh_opt  = nan(nslcs);
%             return
%         end
%     end        
    
    % EMI
    % After testing, I believe the SVD or EIG give the same results. SVD is
    % computationally more efficient.
    
    % PC: added method from Ansari et al. to check result
%     [V,D] = eig(inv(abs(cpxcoh)).*cpxcoh);
%     [lambda,idx] = min(diag(D));
%     zeta = V(:,idx);
%     n = zeta'*zeta;
%     zeta = sqrt(n)/norm(zeta) * zeta;
%     sigma = lambda * abs(cpxcoh) .* (zeta*zeta');
    
    [U,S,~] = svd(inv(abs(cpxcoh)).*cpxcoh); % weighted with coh.
    
    cpxcoh_opt = U(:,end)*S(end,end)*U(:,end)';      
    
%     if strncmpi(lastwarn,'Matrix is close to singular or badly scaled',length('Matrix is close to singular or badly scaled'))
%         
%         lastwarn('');
%     end
    
    % plot for debugging
%     figure
%     hold on
%     plot(angle(cpxcoh_opt(:,11)))
%     plot(angle(sigma(:,11)))
%     plot(angle(zeta.^-1))
    
%     
%     disp('hi')
    
end