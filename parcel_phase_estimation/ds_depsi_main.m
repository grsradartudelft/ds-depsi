function ds_depsi_main(stackData)

t = tic;
                
%%
fprintf('#### Reading stack information. \n');
[stackData]             = ds_depsi_read_stackinfo(stackData); 
                        fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));                
               
%%
% fprintf('#### Calibrating amplitude.  \n');
% [stackData]             = ds_depsi_amplitude_cal(stackData);
%                         save([stackData.outputFolder,'stackData'],'-struct','stackData');
%                         fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
                
%%
fprintf('#### Labeling pixels. \n');
[stackData]             = ds_depsi_id_pixels_parfor(stackData);
                        fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));
                       
%%                
fprintf('#### Multilooking over parcels. \n');
% [stackData,outputGrid]  = ds_depsi_coherence_matrix_bigdata(stackData);
[stackData,outputGrid]  = ds_depsi_coherence_matrix_parfor(stackData);
%                         save([char(stackData.outputFolder),'stackData'],'-struct','stackData');
%                         save([char(stackData.outputFolder),'outputGrid'],'-struct','stackData');
                        fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));

%%                
fprintf('#### Exporting data to space-time matrix (STM) and DePSI formats. \n');
[stackData]             = ds_depsi_export(stackData,outputGrid);
%                         save([char(stackData.outputFolder),'stackData'],'-struct','stackData');
                        fprintf('## DONE, elapsed time: %0.2f s. \n',toc(t));

end

