function [cpxcoh_opt] = jihg_ph_est_evd(cpxcoh)
        
    % EVD
    [U,S,~] = svd(cpxcoh); % weighted with coh.
    
    cpxcoh_opt = U(:,1)*S(1,1)*U(:,1)';   
    
end