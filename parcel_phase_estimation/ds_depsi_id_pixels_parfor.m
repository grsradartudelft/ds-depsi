function [stackData] = ds_depsi_id_pixels_parfor(stackData)
% Modified version of id_pixels which uses a parfor loop
% Reads a polygon shapefile of land parcels and assigns every pixel to a
% polygon. Pixels not contained within a polygon are labeled nan.

% Authors: Philip Conroy and Marc Bruna
% October 2023
% TU Delft Department of Geoscience and remote sensing

outputFilepath = strcat(stackData.outputFolder,"id_pixels_out.mat");
if ~isfile(outputFilepath)
    
    %%Parcel-based multilooking components
    nlines          = stackData.stackinfo.nlines;
    path_coords     = stackData.stackinfo.path_coords;
    parcelShapefile = stackData.parcelShapefile;
    minShps         = stackData.minShps;
    lon             = freadbk_quiet(char(path_coords(1)),nlines);
    lat             = freadbk_quiet(char(path_coords(2)),nlines);
    lon             = lon(:);
    lat             = lat(:);
    
    % Read brp shapefile to get all parcel polygons
    disp(['Reading ' parcelShapefile])
    polygons        = shaperead(parcelShapefile);
    nPolygons       = length(polygons);
    disp(['Found ' num2str(nPolygons) ' polygons'])

    % Assign polygon ids to each pixel
    centroids       = nan(nPolygons,2);
    polygonId       = nan(nPolygons,1);
    pixelIdx        = cell(nPolygons,1);
    
    
    %% Run parallel loop to id pixels
    parfor j=1:nPolygons
        
        warning('off','MATLAB:polyshape:repairedBySimplify')
        
        % Find pixels contained within the polygon
        [in_poly,~]     = inpolygon(lon,lat,polygons(j).X,polygons(j).Y);
        selectedPixels  = find(in_poly); 
 
        if length(selectedPixels) >= minShps            
            pixelIdx{j}    = selectedPixels;
            [x,y]           = centroid(polyshape(polygons(j).X,polygons(j).Y));
            centroids(j,:)  = [x,y];
            
            if isfield(polygons(j),'id')
                polygonId(j) = polygons(j).id;
            elseif isfield(polygons(j),'int_id')
                polygonId(j) = polygons(j).int_id;
            else
                error('Couldnt read polygon IDs')
            end
        end
        
%         if mod(j,1000) == 0
%             disp(['Done ' num2str(j) ' polygons (' num2str(j/nPolygons*100,2) '%)'])
%         end
        
    end
    
    %% Recombine outputs after parfor loop
   
    % For debugging
%     centroids
%     polygonId
    
    % Remove centroid entries with no correponding pixels
    centroids(isnan(polygonId),:)   = [];
    pixelIdx(isnan(polygonId))      = [];
    polygonId(isnan(polygonId))     = [];
    
%     disp(['Size centroids after nan removal: ' num2str(size(centroids))])
%     disp(['Size poygonId after nan removal: '  num2str(size(polygonId))])
    
    if size(centroids,1) == 0
        error('No pixels corresponding with the provided shapefile! Check if file is in WGS84')
    end
    
    nDs = size(centroids,1);
    disp(['Using ' num2str(nDs) ' polygons'])
    
    %% Get bounding box of full area for output STAC json file
    shapeData                   = shapeinfo(parcelShapefile);
    bbox                        = shapeData.BoundingBox;
    geometry.type               = shapeData.ShapeType;
    geometry.coordinates(1,:)   = [bbox(1,1),bbox(1,2)];
    geometry.coordinates(2,:)   = [bbox(2,1),bbox(1,2)];
    geometry.coordinates(3,:)   = [bbox(2,1),bbox(2,2)];
    geometry.coordinates(4,:)   = [bbox(1,1),bbox(2,2)];
    geometry.coordinates(5,:)   = [bbox(1,1),bbox(1,2)];
    
    %% Save result in intermediate file
    idPixelsOut.pixelIdx        = pixelIdx;
    idPixelsOut.centroids       = centroids;
    idPixelsOut.polygonId       = polygonId;
    idPixelsOut.bbox            = bbox;
    idPixelsOut.geometry        = geometry;
    idPixelsOut.nDs             = nDs;
    
    save(outputFilepath,'idPixelsOut','-v7.3');
    
else
    disp('Found pixel id data')
    load(outputFilepath,'idPixelsOut');
    pixelIdx                    = idPixelsOut.pixelIdx;
    centroids                   = idPixelsOut.centroids;
    polygonId                   = idPixelsOut.polygonId;
    bbox                        = idPixelsOut.bbox;
    geometry                    = idPixelsOut.geometry;
    if isfield(idPixelsOut,'nDs')
        nDs                     = idPixelsOut.nDs;
    else
        nDs                     = length(polygonId);
    end
end

%% Store ids to output struct
stackData.pixelIdx              = pixelIdx;
stackData.centroids             = centroids;
stackData.nDs                   = nDs;
stackData.polygonId             = polygonId;
stackData.processingStep        = 2;
stackData.aoiData.bbox          = bbox;
stackData.aoiData.geometry      = geometry;

end

