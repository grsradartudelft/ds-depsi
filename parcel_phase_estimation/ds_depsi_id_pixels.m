function [stackData] = ds_depsi_id_pixels(stackData)
% Reads a polygon shapefile of land parcels and assigns every pixel to a
% polygon. Pixels not contained within a polygon are labeled nan.

% Authors: Philip Conroy and Marc Bruna
% September 2021
% TU Delft Department of Geoscience and remote sensing

outputFilepath = strcat(stackData.outputFolder,"id_pixels_out.mat");
if ~isfile(outputFilepath)
    
    %Parcel-based multilooking components
    nlines          = stackData.stackinfo.nlines;
    path_coords     = stackData.stackinfo.path_coords;
    parcelShapefile = stackData.parcelShapefile;
    minShps         = stackData.minShps;
    
    % Read shapefile to get all parcel polygons
    polygons        = shaperead(parcelShapefile);
    
    % Read pixel geolocation data
    lon             = freadbk_quiet(char(path_coords(1)),nlines);
    lat             = freadbk_quiet(char(path_coords(2)),nlines);
    lon             = lon(:);
    lat             = lat(:);
    
    % This vector contains the id of the parcel it is in, or or
    % nan if not in a parcel
    pixelId = nan(size(lat(:)));
    
    % Assign polygon ids to each pixel
    centroids = [];
    polygonId = [];
    for j=1:length(polygons)
        
        [in_poly,~] = inpolygon(lon,lat,polygons(j).X,polygons(j).Y); % Do not include pixels on the boundary
        
        if mod(j,1000) == 0
            disp(['Done ' num2str(j) ' polygons (' num2str(j/length(polygons)*100,2) '%)'])
        end
        
        if sum(in_poly & isnan(pixelId)) >= minShps
            if isfield(polygons(j),'id')
                pixelId(in_poly & isnan(pixelId)) = polygons(j).id;
            elseif isfield(polygons(j),'int_id')
                pixelId(in_poly & isnan(pixelId)) = polygons(j).int_id;
            else
                error('Couldnt read polygon IDs')
            end
            [x,y] = centroid(polyshape(polygons(j).X,polygons(j).Y));
            centroids = [centroids; [x,y]];
            
            if isfield(polygons(j),'id')
                polygonId = [polygonId; polygons(j).id];
            elseif isfield(polygons(j),'int_id')
                polygonId = [polygonId; polygons(j).int_id];
            else
                error('Couldnt read polygon IDs')
            end
            
            check = unique(pixelId(~isnan(pixelId)));
            if length(polygonId) ~= length(check)
                error('Num. unique polygon IDs ~= unique pixel IDS. Check shapefile.')
            end
        end
    end
    
    if size(centroids,1) == 0
        error('No pixels corresponding with the provided shapefile! Check if file is in WGS84')
    end
    
    %% Get bounding box of full area for output STAC json file
    shapeData                   = shapeinfo(parcelShapefile);
    bbox                        = shapeData.BoundingBox;
    geometry.type               = shapeData.ShapeType;
    geometry.coordinates(1,:)   = [bbox(1,1),bbox(1,2)];
    geometry.coordinates(2,:)   = [bbox(2,1),bbox(1,2)];
    geometry.coordinates(3,:)   = [bbox(2,1),bbox(2,2)];
    geometry.coordinates(4,:)   = [bbox(1,1),bbox(2,2)];
    geometry.coordinates(5,:)   = [bbox(1,1),bbox(1,2)];
    
    %% Stare result in intermediate file
    idPixelsOut.pixelId         = single(pixelId);
    idPixelsOut.centroids       = centroids;
    idPixelsOut.polygonId       = polygonId;
    idPixelsOut.bbox            = bbox;
    idPixelsOut.geometry        = geometry;
    
    save(outputFilepath,'idPixelsOut','-v7.3');
    
else
    disp('Found pixel id data')
    load(outputFilepath,'idPixelsOut');
    pixelId                     = idPixelsOut.pixelId;
    centroids                   = idPixelsOut.centroids;
    polygonId                   = idPixelsOut.polygonId;
    bbox                        = idPixelsOut.bbox;
    geometry                    = idPixelsOut.geometry;
end

%% Store ids to output struct
stackData.pixelId           = pixelId;
stackData.centroids         = centroids;
stackData.nDs               = size(centroids,1);
stackData.polygonId         = polygonId;
stackData.processingStep    = 2;
stackData.aoiData.bbox      = bbox;
stackData.aoiData.geometry  = geometry;

end

