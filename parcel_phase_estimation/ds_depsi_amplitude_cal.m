function [stackData,output] = ds_depsi_amplitude_cal(stackData)
%JIHG_AMP_FUN Calibration of SLCS based on amplitude. Mean amplitude of
%pixels in time.
%   Calibrates SLCS based on the mean amplitude per SLC. Amplitude average 
%   per pixel is determined. The mean amplitude is used to:
%       - Select PS pixels based on amplitude dispersion.
%       - Used for to select SHPs based on the Mean test.
%       - Also checks SLCs for unexpected values

%   Input:

%     nlines          = S.stackinfo.nlines
%     npixels         = S.stackinfo.npixels
%     nslcs           = S.stackinfo.nslcs
%     path_slcs       = S.stackinfo.path_slcs  

%   Output:

%     amp_mean:     mean amplitude per pixel
%     D_sq:         amplitude dispersion squared                 
%     cal_fac:      calibration factor based on average amplitude of SLC   

% Author: FMG HEUFF - Feb 2020.

%% If output exists, load it instead.
    output_path     = stackData.outputFolder;    
    s               = dbstack;
    fun_name        = s(1).name;
    
% if ~(exist(fullfile(output_path,[fun_name,'_output.mat']),'file')==2)
    %% Otherwise run function.
    tic
    
    nlines          = stackData.stackinfo.nlines;
    npixels         = stackData.stackinfo.npixels;
    nslcs           = stackData.stackinfo.nslcs;
    path_slcs       = stackData.stackinfo.path_slcs;  
    
    % Preallocate arrays
    cal_fac         = zeros(nslcs,1);
    amp_sum         = zeros(nlines,npixels); % For calculation of mean
    amp_sq_sum      = zeros(nlines,npixels); % For calculation of variance
    bad_dates = [];
    for i = 1 : nslcs
        
        amp_slc         = abs(freadbk_quiet(path_slcs{i},nlines,'cpxfloat32'));
        amp_slc(amp_slc < 0 ) = 0; 
        
        if any(isnan(amp_slc(:))) || mean(amp_slc(:)) > 1e10 || sum(amp_slc(:)) == 0
            % skip
            disp(['bad image at :' path_slcs{i}]);
            bad_dates = [bad_dates; stackData.stackinfo.dates(i)];
%             error(['Image ',num2str(i),' (',num2str(S.stackinfo.dates(i)),') seems to be wrong, please check!'])
        end
        
        cal_fac(i)      = mean(amp_slc(:));
        amp_slc         = amp_slc./cal_fac(i); % Amps normalized by mean of this image
       
        amp_sum         = amp_slc + amp_sum; % Sum of the amplitudes
        amp_sq_sum      = amp_slc.^2 + amp_sq_sum; % Sum of the squared amplitudes
        
        if mod(i,round(nslcs/10)) == 0
            fprintf('Finished reading %i of %i slcs. [%i%% - %0.2f s] \n',i,nslcs,round(i/nslcs*100),toc)            
        end       
        

    end   
    
    nslcs           = stackData.stackinfo.nslcs;
    
    amp_mean        = amp_sum/nslcs;
    D_sq            = nslcs.*amp_sq_sum./(amp_sum.^2)-1; % amplitude dispersion squared
    
    
    %% Output
    output.amp_mean = amp_mean;
    output.D_sq     = D_sq;
    output.cal_fac  = cal_fac;

    save([output_path,fun_name,'_output'],'-struct','output');
    
% else
%     output = load([output_path,fun_name,'_output.mat']);
% end

    clearvars -except stackData output
end

