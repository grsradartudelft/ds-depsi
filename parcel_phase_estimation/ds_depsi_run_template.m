clear 
clc
close all
% dbstop if error

warning('off','MATLAB:polyshape:repairedBySimplify')

addpath(genpath('/Users/pconroy/phd/code/DS_Depsi/version_controlled'))
addpath(genpath('/Users/pconroy/phd/code/stmutil'))

%% Folder settings
runId             = 'test_template';
inputFolder       = '/Users/pconroy/phd/projects/nobv/zegveld/input/';
rootOutputFolder  = '/Users/pconroy/phd/projects/nobv/zegveld/insar/flinsar/spider_new/output/'; 

%% Stack parameters
% Can do multiple stacks, separate with newline
stackPath         = {'/Users/pconroy/phd/projects/nobv/zegveld/insar/flinsar/spider_new/s1_dsc_t037' ...
                     '/Users/pconroy/phd/projects/nobv/zegveld/insar/flinsar/spider_new/s1_dsc_t110' ...
                     '/Users/pconroy/phd/projects/nobv/zegveld/insar/flinsar/spider_new/s1_asc_t161' };
% DORIS stack ID
stackId           = 'nl_groenehart';

% Master dates (1 for each stack)
masterDate        = {'20200328', '20200327', '20200330'};

% Start and end dates
startDate         = '20200101';
endDate           = '20220531';
excludeDates      = {''}; % SLC images that give problems can be excluded

%% Identifying pixels
% Shapefile which delineates parcel boundaries in WGS84 coordinates
parcelShapefile   = '/Users/pconroy/phd/projects/contextual_data/zegveld/zegveld_parcel_attributes_full/zegveld_attrs_full.shp';

% Reject parcels which contain too few pixels
minShps           = 50;

% Perform SHP test?
shpTest           = 0;

%% Phase-linking method
plMethod          = 'EMI'; % options: EVD,PTA,EMI;
CRB               = 'var'; % 'var','full', calculate Cramer-Rao Bound Covariance matrix. Full creates the full covariance matrix (nslcs x nslcs) per window.

% Multi ESM based on coherent segements?
doSegmentation    = 0;
minCoherence      = 0.15;
minSegLength      = 10;

%% Additional constants
lambda            = 0.0556; % Sentinel-1 wavelength

%% KNMI data
knmi_file         = 'etmgeg_348.txt'; % For Zegveld
% knmi_file         = 'etmgeg_279.txt'; % For Rouveen

%% RNN output

rnn_model         = '/Users/pconroy/phd/projects/nobv/zegveld/rnn/checkpoints/zegveld_nobv_1660053007.hdf5';
rnn_output_file   = '/Users/pconroy/phd/projects/nobv/zegveld/rnn/output/rnnOutputDF_zegveld_1660053007_spider.csv';
confusion_matrix  = '/Users/pconroy/phd/projects/nobv/zegveld/rnn/output/confusion_matrix.csv';

%% Depsi in/output file
% Data to be imported into Depsi to use DS points as virtual PS
depsiExportFile   = 'ds_export_data';

% Space-time matrix object
stmFile           = 'zegveld_ds_stm.mat';

%% Save outputs
saveCohMatrices   = 0;
saveCohFigs       = 0;
saveParcelIfgs    = 0;
saveDepsiExport   = 1;
saveStmFile       = 1;

%% Create output folders and save settings
runOutputFolder = [rootOutputFolder runId '/'];

% Stack output folders are given the same names as the input stacks
for i = 1:length(stackPath)
    idx = strfind(stackPath{i},'/'); 
    stackName{i} = stackPath{i}(idx(end)+1:end);
    stackOutputFolder{i} = [runOutputFolder stackName{i} '/'];
end
clear idx i

if ~isfolder(runOutputFolder)
    mkdir(runOutputFolder)
end

% Save all these variables as global settings for this run
save([runOutputFolder 'ds_depsi_settings']);

%% Run program
ds_depsi_initialize(runOutputFolder)
