function [stackData,output] = ds_depsi_coherence_matrix_parfor(stackData)
% Implements parcel-based multilooking by calculating the coherence matrix
% for each parcel and determines the equivalent single master (ESM) and
% equivalent daisy chain (EDC) phases for each parcel.

% Authors: Philip Conroy
% September 2021
% TU Delft Department of Geoscience and remote sensing

slcPath         = stackData.stackinfo.path_slcs;
nlines          = stackData.stackinfo.nlines;
npixels         = stackData.stackinfo.npixels;
nslcs           = stackData.stackinfo.nslcs;
pixelIdx        = stackData.pixelIdx;
polygonId       = stackData.polygonId;
nDs             = uint64(stackData.nDs);
nifgs           = nslcs-1;
masterIdx       = stackData.stackinfo.master_idx;
ifgIdx          = stackData.stackinfo.ifg_idx;
stackType       = stackData.stackinfo.stack_type;
doShpTest       = stackData.shpTest;
blockwise       = stackData.blockwise;

% Preallocate output data
esmPhases       = zeros(nDs,nslcs-1);
edcPhases       = zeros(nDs,nslcs-1);
edcCoherence    = zeros(nDs,nslcs-1);
h2ph            = zeros(nDs,nslcs-1);
meanAmp         = zeros(nDs,nslcs);
meanAmpCal      = zeros(nDs,nslcs);
ampDisp         = zeros(nDs,1);
nshp            = zeros(nDs,1);
z               = zeros(nDs,1);
line            = zeros(nDs,1);
pixel           = zeros(nDs,1);

% Loop over each polygon and calculate the coherence matrix
parfor i = 1:length(polygonId)
   
    %disp([num2str(i) ' of ' num2str(length(polygonId))])
    
    %% Find extent of parcel in pixel indices
    [l,p]       = ind2sub([nlines,npixels],pixelIdx{i});
    
    % Rectangular extent, following the notation of freadbk
    r0          = min(l);
    rN          = max(l);
    c0          = min(p);
    cN          = max(p);
    
    selSize     = (rN-r0+1)*(cN-c0+1);
    slc_stack   = nan(selSize,nslcs);
    
    % Load the SLC stack
    %disp('Reading data')
    if strcmp(stackType,'slc')
        for j = 1:nslcs
            slc             = single(freadbk_quiet(slcPath{j},nlines,'cpxfloat32',r0,rN,c0,cN));
            slc_stack(:,j)  = slc(:);
            %         calFac(j)       = mean(abs(slc(:)));
        end
        
    elseif strcmp(stackType,'cint')
        master_slc          = single(freadbk_quiet(slcPath{masterIdx},nlines,'cpxfloat32',r0,rN,c0,cN));
        master_slc          = master_slc(:);
        for j = 1:nslcs
            if ismember(j,ifgIdx)
                ifg             = single(freadbk_quiet(slcPath{j},nlines,'cpxfloat32',r0,rN,c0,cN));
                slc_stack(:,j)  = conj(ifg(:)./master_slc); % Equivalent: S = M.*conj(I)./abs(M).^2
            else
                slc_stack(:,j)  = master_slc;
            end
        end
        
        %clear master_slc ifg
    else
        error('Unrecognized stack data')
    end
    
    %% Select pixel stacks in the current parcel
    subselIdx   = sub2ind([rN-r0+1,cN-c0+1],l-r0+1,p-c0+1);
    selection   = slc_stack(subselIdx,:);
    
    % Look for any nan pixels, ex. water 
    badPix = find(any(isnan(selection),2));
    if ~isempty(badPix)
        selection(badPix,:) = [];
    end
    
    %% SHP test within the parcel
%     if doShpTest == 1
%         ksmat = false(length(selection));
%         for j = 1:size(selection,1)
%             % This makes the lower triangular matrix
%             for k = 1:j
%                 % Zero = they come from the same distribution (null hypothesis)
%                 ksmat(j,k) = ~kstest2(sort(abs(selection(j,:))),sort(abs(selection(k,:))));
%             end
%         end
%         
%         % Fill in the whole thing
%         uptri = tril(ksmat)';
%         ksmat = bitor(ksmat,uptri);
%                 
%         [~,idx]     = max(sum(ksmat,2));
%         selection   = selection(ksmat(:,idx),:);
%         
%     end
    
    % Determine the averaged amplitude time series of each DS (normal and RMS)
    meanAmp(i,:)        = mean(abs(selection),1);
%     rmsAmp(i,:)         = sqrt(mean(abs(selection).^2,1));
    
    % Mean calibrated amplitude
    meanAmpCal(i,:)     = sqrt(mean((abs(selection)./meanAmp(i,:)).^2));
    ampDisp(i)          = sqrt( var(meanAmp(i,:)) / mean(meanAmp(i,:)).^2 ); % Is this correct? Not used anyways...
    
    %% Calculate coherence matrix and do multilooking
    % Number of pixels used in averaging
    %disp('Creating coherence matrix')
    nshp(i) = size(selection,1);
    
    % calculate full complex coherence
    ifgs    = selection.'*conj(selection); % PC: .' is the non-conjugate transpose
    abs_sum = sum(abs(selection).^2);
    denom   = sqrt(abs_sum'*abs_sum);
    cpxcoh  = ifgs./denom;
      
    %% Phase-linking (aka equivalent phase estimation)
    %disp('Phase linking')
    edcCoherence(i,:)   = abs(diag(cpxcoh,1));
    
    if blockwise == 1
        [esmPhases(i,:),edcPhases(i,:)] = emi_mask(cpxcoh,stackData);
        
    elseif blockwise == 2
        [esmPhases(i,:),edcPhases(i,:)] = emi_blockwise(cpxcoh,stackData);
        
    else
        sigma                           = jihg_ph_est_emi(cpxcoh);
        
        if any(isnan(sigma(:)))
            warning(['Matrix could not be regularized, check input data (ID: ' num2str(polygonId(i)) ')'])
        end
        
        esmPhases(i,:)                  = angle(sigma(masterIdx,ifgIdx)); % Gives form: M - S
        edcPhases(i,:)                  = angle(diag(sigma,1)); % Gives form: 1-2, 2-3, 3-4, and wrap(diff(esm)) = edc
    end

    %% Read other data
    % Height-to-phase values
    %disp('Reading h2ph data')
    if ~any(isnan(sigma(:)))
        for j = 1:nifgs
            readData        = freadbk_quiet(char(stackData.stackinfo.path_h2ph(j)),nlines,'float32',r0,rN,c0,cN);
            h2ph(i,j)       = mean(readData(subselIdx));
        end
        
        % DEM height
        dem                 = freadbk_quiet(char(stackData.stackinfo.path_dem),nlines,'float32',r0,rN,c0,cN);
        z(i)                = mean(dem(subselIdx));
        
        % Median line and pixels numbers
        line(i)             = round(median(l));
        pixel(i)            = round(median(p));
    end
end

%% Find and remove nan polygons
badPolys                        = isnan(any(esmPhases,2));
esmPhases(badPolys,:)           = [];
edcPhases(badPolys,:)           = [];
edcCoherence(badPolys,:)        = [];
meanAmp(badPolys,:)             = [];
meanAmpCal(badPolys,:)          = [];
ampDisp(badPolys)               = [];
nshp(badPolys)                  = [];
h2ph(badPolys,:)                = [];
z(badPolys)                     = [];
line(badPolys)                  = [];
pixel(badPolys)                 = [];
stackData.polygonId(badPolys)   = [];

stackData.nDs                   = sum(~badPolys);

pctRemoved                      = sum(badPolys)/length(badPolys);
disp(['Removed ' num2str(sum(badPolys)) ' (' num2str(pctRemoved,2) '%) polygons'])

%% Output
output.edcPhases            = edcPhases;
output.esmPhases            = esmPhases;
output.edcCoherence         = edcCoherence;
output.nifgs                = nifgs;
output.meanAmp              = meanAmp;
output.meanAmpCal           = meanAmpCal;
output.ampDisp              = ampDisp;
output.nshp                 = nshp;
output.h2ph                 = h2ph;
output.z                    = z;
output.line                 = line;
output.pixel                = pixel;
stackData.stackinfo.nifgs   = output.nifgs;


clearvars -except stackData output

end