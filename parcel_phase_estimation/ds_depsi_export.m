function [stackData] = ds_depsi_export(stackData,dataGrid)

nlines          = stackData.stackinfo.nlines;
npixels         = stackData.stackinfo.npixels;
path_coords     = stackData.stackinfo.path_coords;
centroids       = stackData.centroids;
nDs             = stackData.nDs;
dates           = stackData.stackinfo.dates;
masterDate      = stackData.masterDate;
ifgIdx          = stackData.stackinfo.ifg_idx;
nIfgs           = length(dates) - 1;
meteoFile       = stackData.meteoFile;
h2ph            = dataGrid.h2ph;
lineNum         = dataGrid.line;
pixelNum        = dataGrid.pixel;
z               = dataGrid.z;

%% Get incidence angle
[lookAngle,incAngle] = la_inc_angles(lineNum,pixelNum,z,masterDate,dates,stackData.stackinfo.path_resfiles);

%% Get other stack/ image parameters
resData = read_res_data(masterDate,dates,stackData.stackinfo.path_resfiles);

%% Write to for import into Depsi
exportData = nan(nDs,3*nIfgs+6);

exportData(:,1)                     = ones(size(lineNum)); % Set during the depsi import
exportData(:,2)                     = ones(size(lineNum)); % Set during the depsi import
exportData(:,3)                     = lineNum;
exportData(:,4)                     = pixelNum;
exportData(:,5        :  nIfgs+4)   = dataGrid.esmPhases;
exportData(:,nIfgs+5  :2*nIfgs+4)   = h2ph;
exportData(:,2*nIfgs+5          )   = dataGrid.ampDisp'; % Amplitude dispersion
exportData(:,2*nIfgs+6:3*nIfgs+6)   = dataGrid.meanAmp; % Mean amplitude time series
exportData(:,3*nIfgs+7)             = stackData.polygonId; % Parcel ID

save(strcat(stackData.depsiDir, stackData.depsiExportFile),'exportData');

%% Write to STM file

% This operation will create a new STM object and overwrite any current
% file with the same name!

% This will populate the new STM with all relevant radar (meta-) data which
% can then be enriched using the parcelTable or QGIS outputs. In the future
% that step should be replaced by queries to a centralized postGres
% database

stmPath = [char(stackData.stmDir) char(stackData.dsStmFilename)];
dsStm   = stm(char(stackData.projectName),char(stackData.stackId),'insar','DATA',stmPath);

dsStm.numEpochs                         = nIfgs;
dsStm.numPoints                         = nDs;
dsStm.obsData.dcCoherence               = dataGrid.edcCoherence;
dsStm.obsData.rawPhase                  = double(dataGrid.esmPhases);
dsStm.pntCrd                            = [centroids(:,2), centroids(:,1), z];  % z = height above ref. ellipsoid (m)

dsStm.auxData.amplitude                 = double(dataGrid.meanAmp);
dsStm.auxData.amplitudeCal              = double(dataGrid.meanAmpCal);
dsStm.auxData.h2ph                      = h2ph;

dsStm.auxTypes                          = {'amp'};

dsStm.techniqueAttrib.stack_type        = 'ESM';
% dsStm.techniqueAttrib.method            = stackData.plMethod;
dsStm.techniqueAttrib.frequency         = resData.frequency;
dsStm.techniqueAttrib.wavelength        = resData.wavelength;
dsStm.techniqueAttrib.track             = stackData.stackId;
dsStm.techniqueAttrib.polarization      = resData.polarization;
dsStm.techniqueAttrib.swath             = resData.swath;
dsStm.techniqueAttrib.pass              = resData.pass;
dsStm.techniqueAttrib.prf               = resData.prf;
dsStm.techniqueAttrib.az_bw             = resData.az_bw;
dsStm.techniqueAttrib.r_fs              = resData.r_fs;
dsStm.techniqueAttrib.r_bw              = resData.r_bw;
% dsStm.techniqueAttrib.nw_corner         = resData.nw_corner; % These coordianates are garbage...
% dsStm.techniqueAttrib.ne_corner         = resData.ne_corner;
% dsStm.techniqueAttrib.se_corner         = resData.se_corner;
% dsStm.techniqueAttrib.sw_corner         = resData.sw_corner;
dsStm.techniqueAttrib.master_timestamp  = resData.master_timestamp;
dsStm.techniqueAttrib.stack_path        = stackData.stackPath;
% dsStm.techniqueAttrib.stackName         = stackData.stackName; % Not used anymore
dsStm.techniqueAttrib.nlines            = nlines;
dsStm.techniqueAttrib.npixels           = npixels;
dsStm.techniqueAttrib.lon_path          = path_coords(1);
dsStm.techniqueAttrib.lat_path          = path_coords(2);
dsStm.techniqueAttrib.dem_path          = stackData.stackinfo.path_dem;
%dsStm.techniqueAttrib.shape_path        = stackData.parcelShapefile;
dsStm.techniqueAttrib.aoiData           = stackData.aoiData;

dsStm.epochDyear                        = year(dates(2:end)) + day(dates(2:end))/365.25;
dsStm.epochAttrib.timestamp             = resData.timestamps(ifgIdx)';
dsStm.epochAttrib.doy                   = day(dsStm.epochAttrib.timestamp,'dayofyear')/365.25;
dsStm.epochAttrib.fracYear              = unwrap(dsStm.epochAttrib.doy*2*pi)/(2*pi); % time elapsed in fractional years

dsStm.pntAttrib.type                    = repmat('DS',nDs,1);
dsStm.pntAttrib.incAngle                = incAngle;
dsStm.pntAttrib.nPixels                 = dataGrid.nshp;
dsStm.pntAttrib.parcelId                = stackData.polygonId;
dsStm.pntAttrib.lineNum                 = lineNum;
dsStm.pntAttrib.pixelNum                = pixelNum;

% if stackData.skipDepsi == 1
%     dsStm.obsData.refPhase              = dataGrid.refPhase;
% end

% Add paths to coherence matrices
if stackData.saveCohMatrices == 1
    for i = 1:nDs
        dsStm.pntAttrib.coherence_path(i)   = stackData.coherenceMatPath(i);
    end
    dsStm.pntAttrib.coherence_path      = dsStm.pntAttrib.coherence_path';
end

if stackData.saveParcelIfgs == 1
    for i = 1:nDs
        dsStm.pntAttrib.parcel_ifg_path(i)  = stackData.parcelIfgPath(i);
    end
    dsStm.pntAttrib.parcel_ifg_path     = dsStm.pntAttrib.parcel_ifg_path';
end

% Add parcel attributes if available
dsStm                                   = stm_add_parcel_attrs(dsStm,char(stackData.parcelShapefile));

% Add weather data if available
if isfile(meteoFile) || isfolder(meteoFile)
    dsStm                               = stm_add_knmi(dsStm,meteoFile,stackData);
else
    warning('Did not add meteo data to STM: files not found.')
end

% Add RNN data if available
if ~isempty(stackData.rnn_model) && ~isempty(stackData.rnn_output_file) && ~isempty(stackData.confusion_matrix)
    if isfile(stackData.rnn_model) && isfile(stackData.rnn_output_file) && isfile(stackData.confusion_matrix)
        dsStm                               = stm_add_rnn_outputs(dsStm,stackData.rnn_model,stackData.rnn_output_file,stackData.confusion_matrix);
    else
        disp('Did not add RNN data to STM: files not found.')
    end
else
    disp('Did not add RNN data to STM: file not specified.')
end
stmwrite(dsStm,[char(stackData.stmDir) char(stackData.dsStmFilename)])

end

