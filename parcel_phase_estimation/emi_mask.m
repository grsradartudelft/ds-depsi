function [esmPhases,edcPhases] = emi_mask(cpxcoh,stackData)

% Based on Ansari et al. (2018)
% Author: PN Conroy - 08/2023.

% Identify coherent segments based on daisy-chain
masterIdx                       = stackData.stackinfo.master_idx;
ifgIdx                          = stackData.stackinfo.ifg_idx;
dcCoherence                     = abs(diag(cpxcoh,-1))';
minCoherence                    = stackData.minCoherence;
minSegLength                    = stackData.minSegLength;
[nSegments,segIdx,segLength,~]  = segmentation(dcCoherence,minCoherence,minSegLength);

% Generate a mask to index the coherent parts of the coherence matrix
mask = logical(false(size(cpxcoh)));
for i = 1:nSegments
    mask(segIdx(i):segIdx(i)+segLength(i),segIdx(i):segIdx(i)+segLength(i)) = 1;
    
    for j = i:nSegments
        mask(segIdx(j):segIdx(j)+segLength(j),segIdx(i):segIdx(i)+segLength(i)) = 1;
        mask(segIdx(i):segIdx(i)+segLength(i),segIdx(j):segIdx(j)+segLength(j)) = 1;
    end
end

% Include the master index even if it sucks
if mask(masterIdx,masterIdx) == 0
    for i = 1:nSegments
        mask(segIdx(i):segIdx(i)+segLength(i),masterIdx) = 1;
        mask(masterIdx,segIdx(i):segIdx(i)+segLength(i)) = 1;
        mask(masterIdx,masterIdx) = 1;
    end
end

sizeMasked  = sum(mask(segIdx(1),:));
cpxcohMask  = cpxcoh(mask);
cpxcohMask  = reshape(cpxcohMask,sizeMasked,sizeMasked);

sigmaMask   = jihg_ph_est_emi(cpxcohMask);
sigma       = zeros(size(cpxcoh));
sigma(mask) = sigmaMask;

esmPhases   = angle(sigma(masterIdx,ifgIdx));
edcPhases   = angle(diag(sigma,1));


% Plots for debugging
% close all
% figure
% imagesc(mask)
% 
% figure
% imagesc(abs(cpxcoh),[0 0.5])
% 
% figure
% imagesc(abs(cpxcohMask),[0 0.5])
% 
% sigmaTest = jihg_ph_est_emi(cpxcoh);
% esmTest   = angle(sigmaTest(masterIdx,ifgIdx));
% edcTest   = angle(diag(sigmaTest,1));
% 
% esmPlot   = esmPhases; esmPlot(esmPlot==0) = nan;
% edcPlot   = edcPhases; edcPlot(edcPlot==0) = nan;
% 
% figure
% hold on
% plot(esmTest)
% plot(esmPlot)
% 
% figure
% hold on
% plot(edcTest)
% plot(edcPlot)


end