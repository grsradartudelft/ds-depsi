function [esmPhases,edcPhases] = emi_blockwise(cpxcoh,stackData)
% Author: PN Conroy - 08/2023.

phases                          = zeros(1,length(cpxcoh));
segVec                          = zeros(1,length(cpxcoh));
masterIdx                       = stackData.stackinfo.master_idx;
ifgIdx                          = stackData.stackinfo.ifg_idx;
sigmaWhole                      = jihg_ph_est_emi(cpxcoh);
esmPhasesWhole                  = angle(sigmaWhole(masterIdx,ifgIdx));
edcPhasesWhole                  = angle(diag(sigmaWhole,1)); 

% Identify coherent segments based on daisy-chain
dcCoherence                     = abs(diag(cpxcoh,-1))';
minCoherence                    = stackData.minCoherence;
minSegLength                    = stackData.minSegLength;
[nSegments,segIdx,segLength,~]  = segmentation(dcCoherence,minCoherence,minSegLength);

% Perform EMI on each coherent block individually
% Use segLength+1 because we are inexing the full coherence matrix here
for i = 1:nSegments
    cpxcohBlock                 = cpxcoh(segIdx(i):segIdx(i)+segLength(i),segIdx(i):segIdx(i)+segLength(i));
    sigmaBlock                  = jihg_ph_est_emi(cpxcohBlock);
    
    % Interferometric phase wrt 1st coherent epoch
    phases(segIdx(i):segIdx(i)+segLength(i))        = angle(sigmaBlock(1,:));
    if segIdx(i) <= masterIdx
        phases(segIdx(i):segIdx(i)+segLength(i))    = phases(segIdx(i):segIdx(i)+segLength(i)) - phases(segIdx(i)) + esmPhasesWhole(segIdx(i));
    else
        phases(segIdx(i):segIdx(i)+segLength(i))    = phases(segIdx(i):segIdx(i)+segLength(i)) - phases(segIdx(i)) + esmPhasesWhole(segIdx(i)-1);
    end
    segVec(segIdx(i):segIdx(i)+segLength(i))        = 1;
end

% phases = phases - phases(masterIdx);
phases(~segVec) = 0;
esmPhases = phases(ifgIdx);
esmPhases = mod(esmPhases + pi,2*pi) - pi;

edcPhases = diff(phases);
edcPhases = mod(edcPhases + pi,2*pi) - pi;

% % Plot for debugging
% sigmaWhole          = jihg_ph_est_emi(cpxcoh);
% esmPhasesWhole      = angle(sigmaWhole(masterIdx,ifgIdx));
% edcPhasesWhole      = angle(diag(sigmaWhole,1)); 
% dates               = stackData.stackinfo.dates;
% phasesPlot          = phases;
% phasesPlot(~segVec) = nan;
% esmPhasesPlot       = phasesPlot(ifgIdx);
% esmPhasesPlot       = mod(esmPhasesPlot + pi,2*pi) - pi;
% edcPhasesPlot       = diff(phasesPlot);
% edcPhasesPlot       = mod(edcPhasesPlot + pi,2*pi) - pi;
% 
% close all
% figure; plot(dates(ifgIdx),esmPhasesWhole); hold on; plot(dates(ifgIdx),esmPhasesPlot); title('ESM'); ylim([-pi,pi]);
% figure; plot(dates(ifgIdx),edcPhasesWhole); hold on; plot(dates(ifgIdx),edcPhasesPlot); title('EDC'); ylim([-pi,pi]);
% 
% figure; imagesc(abs(cpxcoh),[0 0.5])
% ticks = 1:10:length(esmPhasesPlot);
% labels = datestr(stackData.stackinfo.dates(ticks),'yyyymmdd');
% set(gca, 'XTick', ticks, 'XTickLabel', labels)
% set(gca, 'YTick', ticks, 'YTickLabel', labels)
end