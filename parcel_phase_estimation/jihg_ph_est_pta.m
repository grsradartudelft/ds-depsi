function [cpxcoh_opt] = jihg_ph_est_pta(cpxcoh)
%JIHG_PH_EST_PTA
% Based on Guarnieri and Tebaldini (2008)

% Author: FMG HEUFF - 02/2020.
    error('Needs fixing');

    % check if matrix is positive definite, otherwise regularize
    [~,p] = chol(abs(cpxcoh));
    e=1e-6;
    nslcs = length(cpxcoh);
    while p > 0
        cpxcoh = cpxcoh+e*eye(nslcs);
        [~,p] = chol(abs(cpxcoh));
        e = e*2;
    end        
    
    % PTA
    r_y=exp(1i*angle(cpxcoh));                 % phase observations
    D=ones(nslcs)-eye(nslcs);                % Remove diagonal elements

    [U,~]=svd(cpxcoh);
    r_est=exp(1i*angle(U(:,1)));
    W_pta = inv(abs(cpxcoh));
    W_pta2= abs(cpxcoh).^2 ./ (1-abs(cpxcoh).^2);
    
    rem = 1; j = 1; b_est_cpx = 0;
    while rem > 1e-10 && j < 100
        b_est_cpx_old = b_est_cpx;
        b_est_cpx=(D.*W_pta.*r_y)*r_est;        
        r_est=exp(1i*angle(b_est_cpx));
        rem = abs(sum(angle(b_est_cpx) - angle(b_est_cpx_old)));
        j = j + 1;
    end
      
    est_cpx = b_est_cpx;
    cpxcoh_opt=est_cpx(:,1)*est_cpx(:,1)';   

end