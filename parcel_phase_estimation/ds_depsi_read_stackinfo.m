function [stackData] = ds_depsi_read_stackinfo(stackData)
% ds_depsi_read_stackinfo
% Adapted from JIHG_READ_STACK (FMG HEUFF - Feb 2020)
% Reads the necessary filepaths and stack metadata of a single Doris v5 processed track.
% Philip Conroy Sept 2022

stackPath   = stackData.stackPath;
masterDate  = stackData.masterDate;
startDate   = stackData.startDate;
endDate     = stackData.endDate;

stackDates  = get_stack_dates(stackPath);
dateIdx     = stackDates >= startDate & stackDates <= endDate;
dates       = stackDates(dateIdx);
masterIdx   = stackDates == masterDate;
dropped     = find(~dateIdx);

% These indices only consider included/excluded dates
slcIdx      = dateIdx;
ifgIdx      = slcIdx;
ifgIdx(masterIdx) = 0;
nslcs       = length(dates);

if strcmp(stackData.processor,'flinsar')
    nlines      = load([char(stackPath),'nlines_crp.txt']);
    npixels     = load([char(stackPath),'npixels_crp.txt']);
    fl          = nlines(2);    % First line?
    ll          = nlines(3);    % last line?
    fp          = npixels(2);   % First pixel
    lp          = npixels(3);   % Last pixel?
    nlines      = nlines(1);
    npixels     = npixels(1);
    
elseif strcmp(stackData.processor,'caroline')
    nlines      = stackData.n_lines_res;
    npixels     = stackData.n_pixels_res;
    
else
    error('Unrecognized processing software')
end

disp(['Using ' num2str(length(dates)) ' of ' num2str(length(stackDates)) ' images in stack.'])
if ~isempty(dropped)
    disp('Dropped dates: ')
    disp(datestr(stackDates(dropped)))
end
disp('____________________________________________________________________________')

%% Create list of required files

if strcmp(stackData.processor,'flinsar')
    % Find list of SLC image files
    filelist    = dir(fullfile(char(stackPath), '**/slc_srd*.raw'));
    slcNames    = string({filelist.name})';
    slcFolder   = string({filelist.folder})';
    slcPaths    = sort(strcat(slcFolder, repmat("/",size(slcFolder)), slcNames),'ascend');
    stackType   = 'slc';
    mFolder     = fullfile(stackPath,datestr(masterDate,'yyyymmdd'));

elseif strcmp(stackData.processor,'caroline')
    % Use interferograms if there are no SLCs (apart from master)
    mFilelist   = dir(fullfile(char(stackPath), '**/slave_rsmp_reramped.raw'));
    
    % Filter out any SLCs present in the burst folders
    burstIdx    = contains(string({mFilelist.folder}),'burst');
    mFilelist   = mFilelist(~burstIdx);
    
    mName       = string({mFilelist.name})';
    mFolder     = string({mFilelist.folder})';
    mPath       = fullfile(mFolder,mName);
    
    % Find cint files
    filelist    = dir(fullfile(char(stackPath), '**/cint_srd*.raw'));
    
    % Filter out any SLCs present in the burst folders
    burstIdx    = contains(string({filelist.folder}),'burst');
    filelist    = filelist(~burstIdx);
    
    cintNames   = string({filelist.name})';
    cintFolder  = string({filelist.folder})';
    
    % Append interferograms to list of "SLCs"
    slcPaths    = sort([mPath; strcat(cintFolder, repmat("/",size(cintFolder)), cintNames)],'ascend');
    stackType   = 'cint';
end

% Find list of h2ph image files
filelist    = dir(fullfile(char(stackPath), '**/h2ph*.raw'));
burstIdx    = contains(string({filelist.folder}),'burst');
filelist    = filelist(~burstIdx);
h2phNames   = string({filelist.name})';
h2phFolder  = string({filelist.folder})';
h2phPaths   = fullfile(h2phFolder,h2phNames);
h2phPaths   = [h2phPaths; fullfile(mFolder,'dummy_master.raw')];
h2phPaths   = sort(h2phPaths,'ascend');

% Find list of resfiles
filelist    = dir(fullfile(char(stackPath),'**/slave.res'));
burstIdx    = contains(string({filelist.folder}),'burst');
filelist    = filelist(~burstIdx);
masterIdx   = contains(string({filelist.folder}),datestr(masterDate,'yyyymmdd'));
filelist    = filelist(~masterIdx);
resNames    = string({filelist.name})';
resFolder   = string({filelist.folder})';

resPaths    = fullfile(mFolder, 'master.res');
resPaths    = [resPaths;fullfile(resFolder,resNames)];
resPaths    = sort(resPaths,'ascend');

% Find coordinate and DEM filenames
filelist    = dir(fullfile(char(stackData.masterDir),'lam*.raw'));
lamPath     = string([filelist.folder '/' filelist.name]);

filelist    = dir(fullfile(char(stackData.masterDir),'phi*.raw'));
phiPath     = string([filelist.folder '/' filelist.name]);

filelist    = dir(fullfile(char(stackData.masterDir),'dem_radar*.raw'));
demPath     = string([filelist.folder '/' filelist.name]);

%% Write stack info to struct
stackinfo.path_slcs                 = slcPaths(slcIdx);
stackinfo.path_h2ph                 = h2phPaths(ifgIdx);
stackinfo.path_resfiles             = resPaths(slcIdx);

stackinfo.path_coords               = [lamPath; phiPath];
stackinfo.path_dem                  = demPath;
% stackinfo.path_resfiles             = strcat(slcFolder,repmat("/slave.res",size(slcFolder)));
% stackinfo.path_resfiles(masterIdx)  = strcat(slcFolder(masterIdx),"/master.res");

% Stack parameters
stackinfo.nlines                    = nlines;
stackinfo.npixels                   = npixels;
% stackinfo.scene_corners             = [fl,fp,ll,lp];
stackinfo.dates_stack               = stackDates;
stackinfo.dates                     = dates;
stackinfo.nslcs                     = nslcs;
stackinfo.dropped                   = dropped;
stackinfo.master_idx_full           = find(masterIdx);
stackinfo.master_idx                = find(dates == masterDate);
stackinfo.slc_idx_full              = find(slcIdx);
stackinfo.ifg_idx_full              = find(ifgIdx); 
stackinfo.ifg_idx                   = find(dates ~= masterDate); % Export this as an index of which used dates are ifgs
stackinfo.stack_type                = stackType;

% Write stackinfo to the stack data structure
stackData.stackinfo                 = stackinfo;
stackData.processingStep            = 0;
clearvars -except stackData
end

