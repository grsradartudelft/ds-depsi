function [stackData,output] = ds_depsi_coherence_matrix(stackData)
% Implements parcel-based multilooking by calculating the coherence matrix 
% for each parcel and determines the equivalent single master (ESM) and
% equivalent daisy chain (EDC) phases for each parcel. 

% Authors: Philip Conroy
% September 2021
% TU Delft Department of Geoscience and remote sensing

slcPath         = stackData.stackinfo.path_slcs;
nlines          = stackData.stackinfo.nlines;
nslcs           = stackData.stackinfo.nslcs;
pixelId         = stackData.pixelId;
polygonId       = stackData.polygonId;
nDs             = stackData.nDs;
nifgs           = nslcs-1;
masterIdx       = stackData.stackinfo.master_idx;
ifgIdx          = stackData.stackinfo.ifg_idx;

% Preallocate output data
esmPhases       = zeros(nDs,nslcs-1);
edcPhases       = zeros(nDs,nslcs-1);
edcCoherence    = zeros(nDs,nslcs-1);
meanAmp         = zeros(nDs,nslcs);
rmsAmp          = zeros(nDs,nslcs);
meanAmpCal      = zeros(nDs,nslcs);
ampDisp         = zeros(nDs,1);
nshp            = zeros(nDs,1);

% Load the SLC stack
if strcmp(stackData.stackinfo.stack_type,'slc')
    for j = 1:nslcs
        slc             = single(freadbk_quiet(slcPath{j},nlines,'cpxfloat32'));
        slc_stack(:,j)  = slc(:);
%         calFac(j)       = mean(abs(slc(:)));
    end
    
elseif strcmp(stackData.stackinfo.stack_type,'cint')
    master_slc          = single(freadbk_quiet(slcPath{masterIdx},nlines,'cpxfloat32'));
    master_slc          = master_slc(:);
    for j = 1:length(ifgIdx)
        ifg             = single(freadbk_quiet(slcPath{ifgIdx(j)},nlines,'cpxfloat32'));
        slc_stack(:,j)  = conj(ifg(:)./master_slc); % Equivalent: S = M.*conj(I)./abs(M).^2
    end
    
    clear -master_slc -ifg
else
    error('Unrecognized stack data')
end

% Loop over each polygon and calculate the coherence matrix
for i = 1:nDs
    
    if mod(i,1000) == 0
        disp(['Done ' num2str(i) ' polygons (' num2str(i/nDs*100,2) '%)'])
    end
    
    %% Select pixel stacks in the current parcel
    selection = slc_stack(pixelId == polygonId(i),:);
    
    %% SHP test within the parcel
    if stackData.shpTest == 1
        ksmat = false(length(selection));
        parfor j = 1:size(selection,1)
            % This makes the lower triangular matrix
            for k = 1:j
                % Zero = they come from the same distribution (null hypothesis)
                ksmat(j,k) = ~kstest2(sort(abs(selection(j,:))),sort(abs(selection(k,:))));
            end
        end
        
        % Fill in the whole thing
        uptri = tril(ksmat)';
        ksmat = bitor(ksmat,uptri);
        
        % for debugging
        %figure
        %imagesc(ksmat)
        
        [~,idx]     = max(sum(ksmat,2));
        selection   = selection(ksmat(:,idx),:);
        
    end
    
    %% Calculate coherence matrix and do multilooking
    % Number of pixels used in averaging
    nshp(i) = size(selection,1);
    
    % calculate full complex coherence
    cpx_sum = selection.'*conj(selection); % PC: .' is the non-conjugate transpose
    mlIfg   = cpx_sum / nshp(i); 
    abs_sum = sum(abs(selection).^2);
    denom   = sqrt(abs_sum'*abs_sum);
    cpxcoh  = cpx_sum./denom;
    
    if stackData.saveCohMatrices       
        coherenceMatDir = [char(stackData.outputFolder) 'coherence_matrices/'];
        if ~isfolder(coherenceMatDir)
            mkdir(coherenceMatDir)
        end
        filepath = [coherenceMatDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '.mat'];
        save(filepath,'cpxcoh');
        stackData.coherenceMatPath(i) = string(filepath);
    end
    
    if stackData.saveCohFigs
        figure
        imagesc(abs(cpxcoh), [0 0.6])
        colorbar
        title(['Coherence matrix: Parcel ID ' num2str(polygonId(i))])
        ticks = 1:10:nifgs;
        labels = datestr(stackData.stackinfo.dates(ticks),'yyyymmdd');
        set(gca, 'XTick', ticks, 'XTickLabel', labels)
        set(gca, 'YTick', ticks, 'YTickLabel', labels)
        xtickangle(90)
        set(gcf,'Color','w')
        
        coherenceMatDir = [char(stackData.outputFolder) 'coherence_matrices/'];
        if ~isfolder(coherenceMatDir)
            mkdir(coherenceMatDir)
        end
        filepath = [coherenceMatDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '.fig'];
        saveas(gcf,filepath);
        close(gcf)
    end
    
    if stackData.saveParcelIfgs
        ifgDir = [char(stackData.outputFolder) 'parcel_ifgs/'];
        if ~isfolder(ifgDir)
            mkdir(ifgDir)
        end
        filepath = [ifgDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '_ifg.mat'];
        save(filepath,'mlIfg');
        stackData.parcelIfgPath(i) = string(filepath);
    end
    
    %% Phase-linking (aka equivalent phase estimation)   
    
    edcCoherence(i,:)   = abs(diag(cpxcoh,1));

    if stackData.blockwise == 1
        [esmPhases(i,:),edcPhases(i,:)] = emi_mask(cpxcoh,stackData);
        
    elseif stackData.blockwise == 2
        [esmPhases(i,:),edcPhases(i,:)] = emi_blockwise(cpxcoh,stackData);
        
    else
        sigma                           = jihg_ph_est_emi(cpxcoh);
        esmPhases(i,:)                  = angle(sigma(masterIdx,ifgIdx)); % Gives form: M - S
        edcPhases(i,:)                  = angle(diag(sigma,1)); % Gives form: 1-2, 2-3, 3-4, and wrap(diff(esm)) = edc
    end
    
    % Determine the averaged amplitude time series of each DS (normal and RMS)
    meanAmp(i,:)        = mean(abs(selection),1);
    rmsAmp(i,:)         = sqrt(mean(abs(selection).^2,1));
       
    % Mean calibrated amplitude
    meanAmpCal(i,:)     = sqrt(mean((abs(selection)./calFac).^2));
    ampDisp(i)          = sqrt( var(meanAmp) / mean(meanAmp).^2 ); % Is this correct? Not used anyways...
    
    % Phase statistics
    ifgs                = selection(:,masterIdx).*conj(selection(:,ifgIdx));
    phMean(i,:)         = angle(mean(ifgs,1));
    phStd(i,:)          = std(ifgs,1);
    phSkew(i,:)         = skewness(ifgs,0,1);
    phKurto(i,:)        = kurtosis(ifgs,0,1);
    
end

%% Save a PS reference point phase (ie. IGRS) (for debugging only)

% if stackData.skipDepsi == 1
%     lp_igrs_zegveld = [589,2235;584,2621;311,1833;319,1572];
%     refIdx = sub2ind(size(slc),lp_igrs_zegveld(stackData.stackIdx,1),lp_igrs_zegveld(stackData.stackIdx,2));
%     
%     % Keep the phase in ESM form
%     refPhase    = double(angle(slc_stack(refIdx,masterIdx) .* conj(slc_stack(refIdx,ifgIdx))));
% else
%     refIdx      = [];
%     refPhase    = [];
% end

    
%% Output
output.edcPhases            = edcPhases;
output.esmPhases            = esmPhases;
output.edcCoherence         = edcCoherence;
output.nifgs                = nifgs;
output.meanAmp              = meanAmp;
output.meanAmpCal           = meanAmpCal;
output.ampDisp              = ampDisp;
output.nshp                 = nshp;
% output.refPhase             = refPhase;
% output.refIdx               = refIdx;

stackData.stackinfo.nifgs   = output.nifgs;
stackData.win.n             = length(polygonId);

clearvars -except stackData output

end