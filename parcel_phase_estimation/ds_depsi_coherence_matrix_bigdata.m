function [stackData,output] = ds_depsi_coherence_matrix_bigdata(stackData)
% Implements parcel-based multilooking by calculating the coherence matrix
% for each parcel and determines the equivalent single master (ESM) and
% equivalent daisy chain (EDC) phases for each parcel.

% Authors: Philip Conroy
% September 2021
% TU Delft Department of Geoscience and remote sensing

slcPath         = stackData.stackinfo.path_slcs;
nlines          = stackData.stackinfo.nlines;
npixels         = stackData.stackinfo.npixels;
nslcs           = stackData.stackinfo.nslcs;
pixelIdx        = stackData.pixelIdx;
polygonId       = stackData.polygonId;
nDs             = stackData.nDs;
nifgs           = nslcs-1;
masterIdx       = stackData.stackinfo.master_idx;
ifgIdx          = stackData.stackinfo.ifg_idx;

% Preallocate output data
esmPhases       = zeros(nDs,nslcs-1);
edcPhases       = zeros(nDs,nslcs-1);
edcCoherence    = zeros(nDs,nslcs-1);
h2ph            = zeros(nDs,nslcs-1);
meanAmp         = zeros(nDs,nslcs);
rmsAmp          = zeros(nDs,nslcs);
meanAmpCal      = zeros(nDs,nslcs);
ampDisp         = zeros(nDs,1);
nshp            = zeros(nDs,1);
z               = zeros(nDs,1);
line            = zeros(nDs,1);
pixel           = zeros(nDs,1);

% Loop over each polygon and calculate the coherence matrix
for i = 1:nDs
    
    if mod(i,5000) == 0
        disp(['Done ' num2str(i) ' polygons (' num2str(i/nDs*100,2) '%)'])
    end
    
    %% Find extent of parcel in pixel indices
    [l,p]       = ind2sub([nlines,npixels],pixelIdx{i});
    
    % Rectangular extent, following the notation of freadbk
    
    %pixelIdx{i}
    
    r0          = min(l);
    rN          = max(l);
    c0          = min(p);
    cN          = max(p);
       
    % Load the SLC stack
    if strcmp(stackData.stackinfo.stack_type,'slc')
        for j = 1:nslcs
            slc             = freadbk_quiet(slcPath{j},nlines,'cpxfloat32',r0,rN,c0,cN);
            slc_stack(:,j)  = slc(:);
            %         calFac(j)       = mean(abs(slc(:)));
        end
        
    elseif strcmp(stackData.stackinfo.stack_type,'cint')
        master_slc          = freadbk_quiet(slcPath{masterIdx},nlines,'cpxfloat32',r0,rN,c0,cN);
        master_slc          = master_slc(:);
        for j = 1:nslcs
            if ismember(j,ifgIdx)
                ifg             = freadbk_quiet(slcPath{j},nlines,'cpxfloat32',r0,rN,c0,cN);
                slc_stack(:,j)  = conj(ifg(:)./master_slc); % Equivalent: S = M.*conj(I)./abs(M).^2
            else
                slc_stack(:,j)  = master_slc;
            end
        end
        
        
        
        clear master_slc ifg
    else
        error('Unrecognized stack data')
    end
    
    %% Select pixel stacks in the current parcel
    subselIdx   = sub2ind([rN-r0+1,cN-c0+1],l-r0+1,p-c0+1);
    selection   = slc_stack(subselIdx,:);
    
    save(strcat(stackData.outputFolder,"selection.mat"),'selection');
    
    clear slc_stack
    
    % Look for any nan pixels, ex. water 
%     badPix = find(any(isnan(selection),2));
%     if ~isempty(badPix)
%         badIds          = pixelIdx{i}(badPix);
%         [badL,badP]     = ind2sub([nlines,npixels],badIds);
%         lon             = freadbk_quiet(stackData.stackinfo.path_coords{1},nlines,'float32',r0,rN,c0,cN);
%         lat             = freadbk_quiet(stackData.stackinfo.path_coords{2},nlines,'float32',r0,rN,c0,cN);
%         subselIdx       = sub2ind([rN-r0+1,cN-c0+1],badL-r0+1,badP-c0+1);
%         
%         disp('Found nan pixels at: ')
%         disp(['(' num2str(lat(subselIdx)) ',' num2str(lon(subselIdx)) ')'])
        
%         selection(badPix,:) = [];
%     end
    
    
    % Code to make sure subselIdx is calculated properly
%     lon             = freadbk_quiet(stackData.stackinfo.path_coords{1},nlines,'float32',r0,rN,c0,cN);
%     lat             = freadbk_quiet(stackData.stackinfo.path_coords{2},nlines,'float32',r0,rN,c0,cN);
%     polygons        = shaperead(stackData.parcelShapefile);
%     ploygonIdx      = find([polygons.int_id] == polygonId(i));
%     [in_poly,~]     = inpolygon(lon(:),lat(:),polygons(ploygonIdx).X,polygons(ploygonIdx).Y);
%     check           = find(in_poly)
    
    %% SHP test within the parcel
    if stackData.shpTest == 1
        ksmat = false(length(selection));
        parfor j = 1:size(selection,1)
            % This makes the lower triangular matrix
            for k = 1:j
                % Zero = they come from the same distribution (null hypothesis)
                ksmat(j,k) = ~kstest2(sort(abs(selection(j,:))),sort(abs(selection(k,:))));
            end
        end
        
        % Fill in the whole thing
        uptri = tril(ksmat)';
        ksmat = bitor(ksmat,uptri);
        
        % for debugging
        %figure
        %imagesc(ksmat)
        
        [~,idx]     = max(sum(ksmat,2));
        selection   = selection(ksmat(:,idx),:);
        
    end
    
    %% Calculate coherence matrix and do multilooking
    % Number of pixels used in averaging
    nshp(i) = size(selection,1);
    
    % calculate full complex coherence
    ifgs    = selection.'*conj(selection); % .' is the non-conjugate transpose
    abs_sum = sum(abs(selection).^2);
    denom   = sqrt(abs_sum'*abs_sum);
    cpxcoh  = ifgs./denom;
    
    if stackData.saveCohMatrices
        coherenceMatDir = [char(stackData.outputFolder) 'coherence_matrices/'];
        if ~isfolder(coherenceMatDir)
            mkdir(coherenceMatDir)
        end
        filepath = [coherenceMatDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '.mat'];
        save(filepath,'cpxcoh');
        stackData.coherenceMatPath(i) = string(filepath);
    end
    
    if stackData.saveCohFigs
        figure
        imagesc(abs(cpxcoh), [0 0.6])
        colorbar
        title(['Coherence matrix: Parcel ID ' num2str(polygonId(i))])
        ticks = 1:10:nifgs;
        labels = datestr(stackData.stackinfo.dates(ticks),'yyyymmdd');
        set(gca, 'XTick', ticks, 'XTickLabel', labels)
        set(gca, 'YTick', ticks, 'YTickLabel', labels)
        xtickangle(90)
        set(gcf,'Color','w')
        
        coherenceMatDir = [char(stackData.outputFolder) 'coherence_matrices/'];
        if ~isfolder(coherenceMatDir)
            mkdir(coherenceMatDir)
        end
        filepath = [coherenceMatDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '.fig'];
        saveas(gcf,filepath);
        close(gcf)
    end
    
    if stackData.saveParcelIfgs
        ifgDir = [char(stackData.outputFolder) 'parcel_ifgs/'];
        if ~isfolder(ifgDir)
            mkdir(ifgDir)
        end
        filepath = [ifgDir char(stackData.stackId) '_Parcel_' num2str(polygonId(i)) '_ifg.mat'];
        save(filepath,'ifgs');
        stackData.parcelIfgPath(i) = string(filepath);
    end
    
    %% Phase-linking (aka equivalent phase estimation)
    edcCoherence(i,:)   = abs(diag(cpxcoh,1));
    
    if stackData.blockwise == 1
        [esmPhases(i,:),edcPhases(i,:)] = emi_mask(cpxcoh,stackData);
        
    elseif stackData.blockwise == 2
        [esmPhases(i,:),edcPhases(i,:)] = emi_blockwise(cpxcoh,stackData);
        
    else
        sigma           = jihg_ph_est_emi(cpxcoh);
        esmPhases(i,:)  = angle(sigma(masterIdx,ifgIdx)); % Gives form: M - S
        edcPhases(i,:)  = angle(diag(sigma,1)); % Gives form: 1-2, 2-3, 3-4, and wrap(diff(esm)) = edc
    end
    
    % Determine the averaged amplitude time series of each DS (normal and RMS)
    meanAmp(i,:)        = mean(abs(selection),1);
    rmsAmp(i,:)         = sqrt(mean(abs(selection).^2,1));
    
    % Mean calibrated amplitude
    meanAmpCal(i,:)     = sqrt(mean((abs(selection)./meanAmp(i,:)).^2));
    ampDisp(i)          = sqrt( var(meanAmp) / mean(meanAmp).^2 ); % Is this correct? Not used anyways...
    
    %% Read other data
    % Height-to-phase values
    for j = 1:nifgs
        readData        = freadbk_quiet(char(stackData.stackinfo.path_h2ph(j)),nlines,'float32',r0,rN,c0,cN);
        h2ph(i,j)       = mean(readData(subselIdx));
    end
    
    % DEM height
    dem                 = freadbk_quiet(char(stackData.stackinfo.path_dem),nlines,'float32',r0,rN,c0,cN);
    z(i)                = mean(dem(subselIdx));
    
    % Median line and pixels numbers
    line(i)             = round(median(l));
    pixel(i)            = round(median(p));
    
end


%% Output
output.edcPhases            = edcPhases;
output.esmPhases            = esmPhases;
output.edcCoherence         = edcCoherence;
output.nifgs                = nifgs;
output.meanAmp              = meanAmp;
output.meanAmpCal           = meanAmpCal;
output.ampDisp              = ampDisp;
output.nshp                 = nshp;
output.h2ph                 = h2ph;
output.z                    = z;
output.line                 = line;
output.pixel                = pixel;

stackData.stackinfo.nifgs   = output.nifgs;
stackData.win.n             = length(polygonId);

clearvars -except stackData output

end