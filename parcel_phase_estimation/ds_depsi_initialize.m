function ds_depsi_initialize(settings)

% Initializes the data structure containing all stack information and runs
% main program for each stack individually.

for i = 1:settings.numTracks
    
    % Stack info
    stackData.processor         = settings.processor;
    stackData.n_lines_res       = settings.n_lines_res(i);
    stackData.n_pixels_res      = settings.n_pixels_res(i);
    stackData.projectName       = settings.projectName;
    stackData.outputFolder      = settings.phaseEstStackDir(i);
    stackData.masterDate        = settings.masterDate(i);
    stackData.stackPath         = settings.stackDir(i);
    stackData.stackId           = settings.stackId(i);
    stackData.startDate         = settings.startDate;
    stackData.endDate           = settings.endDate;
    stackData.masterDir         = settings.masterDir(i);
    stackData.stackIdx          = i;
    
    % Contextual data
    stackData.parcelShapefile   = settings.parcelShapefile;
    stackData.meteoFile         = string(settings.meteoFile);
    stackData.rnn_output_file   = settings.rnn_output_file;
    stackData.confusion_matrix  = settings.confusion_matrix;
    stackData.rnn_model         = settings.rnn_model;
    
    % Settings
    stackData.minShps           = settings.pe.minShps;
    stackData.shpTest           = settings.pe.shpTest;
    stackData.saveCohMatrices   = settings.pe.saveCohMatrices;
    stackData.saveCohFigs       = settings.pe.saveCohFigs;
    stackData.saveParcelIfgs    = settings.pe.saveParcelIfgs;
    stackData.refLoc            = settings.indices.IGRSLoc;
    stackData.minCoherence      = settings.seg.minCoherence;
    stackData.minSegLength      = settings.seg.minSegLength;
    stackData.blockwise         = settings.pe.blockwise;
    
    % Data export settings
    stackData.depsiDir          = settings.depsiStackdir(i);
    stackData.depsiExportFile   = settings.dsExportFilename(i);
    
    stackData.stmDir            = settings.stmDir;
    stackData.dsStmFilename     = settings.dsStmFilename(i);
    
    % Save this struct
    disp(['##### Processing ' char(stackData.outputFolder)]);
    save(strcat(stackData.outputFolder, "stackData"),'-struct','stackData');
    
    % Run "main" function: consistent phase estimation for the given
    % stack
    dsStmFilename = char(settings.dsStmFilename(i));
    if isfile(fullfile(settings.stmDir,string(dsStmFilename))) ...
    || isfile(fullfile(settings.stmDir,string([dsStmFilename(1:end-7) 'depsi_stm.mat']))) ...
    || isfile(fullfile(settings.depsiStackdir(i),settings.dsExportFilename(i)))
        disp('Processed data found, skipping track.')
    else
        ds_depsi_main(stackData);
    end
    
    disp(['### DONE processing ' char(stackData.outputFolder)]);
    disp('____________________________________________________________________________')
end



end