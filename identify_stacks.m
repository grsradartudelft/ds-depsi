function [settings] = identify_stacks(settings)

%% Detect stacks with specified name
% Note: this assumes the stack folder is named '{stackName}_s1_{a/d}sc_tXXX'
% This will filter out things named like 'nl_veenweiden_s1_asc_t088_test'
dirlist             = dir(fullfile(settings.stackRootDir));
idx                 = ~cellfun('isempty',regexp(string({dirlist.name}),[settings.stackName '_?s1_[ad]sc_t[0-9][0-9][0-9]$']));
stackDirs           = fullfile(string({dirlist(idx).folder}),string({dirlist(idx).name}));
numTracksInit       = length(stackDirs);

disp(['Found ' num2str(numTracksInit) ' Doris v5 stacks in the specified location.'])

if numTracksInit == 0
    error('Could not find radar data in specified directory.')
end

%% Check if stack intersects the region of interest
numTracks   = numTracksInit;
keepIdx     = true(1,numTracksInit);

for i = 1:numTracksInit
    
    filelist        = dir(fullfile(stackDirs(i),'**/stackburst_coverage.shp'));
    stackShape      = shapeinfo(fullfile(filelist.folder,filelist.name));
    stackXvertices  = [stackShape.BoundingBox(1,1), stackShape.BoundingBox(1,1), stackShape.BoundingBox(2,1), stackShape.BoundingBox(2,1)];
    stackYvertices  = [stackShape.BoundingBox(1,2), stackShape.BoundingBox(2,2), stackShape.BoundingBox(2,2), stackShape.BoundingBox(1,2)];
    stackPoly       = polyshape(stackXvertices,stackYvertices);
    
    roiShape        = shapeinfo(settings.parcelShapefile);
    roiXvertices    = [roiShape.BoundingBox(1,1), roiShape.BoundingBox(1,1), roiShape.BoundingBox(2,1), roiShape.BoundingBox(2,1)];
    roiYvertices    = [roiShape.BoundingBox(1,2), roiShape.BoundingBox(2,2), roiShape.BoundingBox(2,2), roiShape.BoundingBox(1,2)];
    roiPoly         = polyshape(roiXvertices,roiYvertices);
    
    [~,overlap,~]   = intersect(stackPoly,roiPoly);
    
    if isempty(overlap)
        disp('Discarding stack:')
        disp(stackDirs(i))
        keepIdx(i)  = false;
        numTracks   = numTracks - 1;
    end
end

stackDirs = stackDirs(keepIdx);

if numTracks == 0
    error('No stacks intersect the region of interest! Check shapefile is in WGS84')
end

settings.numTracks = numTracks;

disp('____________________________________________________________________________')
disp(['Using ' num2str(numTracks) ' stacks:'])

%% Extract the location of each master image and date
% Assumes images are named yyyymmdd
for i = 1:settings.numTracks
    
    % Find master directory
    if strcmp(settings.processor,'caroline')
        % Search for the master SLC file
        filelist            = dir(fullfile(stackDirs(i), '**/slave_rsmp_reramped.raw'));
        
        % Filter out any SLCs present in the burst folders
        burstIdx            = contains(string({filelist.folder}),'burst');
        filelist            = filelist(~burstIdx);
        
    elseif strcmp(settings.processor,'flinsar')
        filelist            = dir(fullfile(stackDirs(i), '**/lam*.raw'));
    end
    
    masterDir               = filelist.folder;
    masterDateStr           = masterDir(end-7:end); 
    settings.masterDate(i)  = datetime(masterDateStr,'InputFormat','yyyyMMdd');
    settings.stackDir(i)    = string(masterDir(1:end-8));
    settings.masterDir(i)   = string([masterDir '/']);
    
    if settings.masterDate(i) < settings.startDate || settings.masterDate(i) > settings.endDate
        error('Master image is outside specified date range')
    end
    
    % Extract the name of the stack folder
    tempName                = char(settings.stackDir(i));
    idx                     = strfind(tempName,'s1_');
    settings.stackId(i)     = string(tempName(idx:idx+10)); % Assumes naming convention:s1_(a)(d)sc_t###
    
    disp(strcat("Track: ", settings.stackId(i), " Master: ", string(masterDateStr)))
    
    % Read data from the master res file
    resfile     = strcat(settings.masterDir(i),"master.res");
    opts        = fixedWidthImportOptions('datalines',[32 100],'VariableNames',{'Field','Value'},'VariableWidths',[58,69]);
    readData    = readtable(resfile,opts);
    
    % Radar parameters
    settings.swath(i)               = string(readData.Value(7));
    settings.pass(i)                = string(readData.Value(8));
    settings.r_pixel_spacing(i)     = round(str2double(readData.Value(16)),1);
    settings.az_pixel_spacing(i)    = round(str2double(readData.Value(17)),1);
    settings.n_pixels_res(i)        = str2double(readData.Value(68));
    settings.n_lines_res(i)         = str2double(readData.Value(69));
end

end

