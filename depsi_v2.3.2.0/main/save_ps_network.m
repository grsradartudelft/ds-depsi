function save_ps_network(Nifgs,Npsc,Npsp,Npsc_selections,Nref)

% ps_densification
%
% Input:    - Nifgs               number of interferograms
%           - Npsc                number of psc's
%           - Npsp                number of psp's
%           - Npsc_selections     number of psc selections
%           - Nref                number of reference points
%           - Nslc                number of slc's
%
% Output:   - Npsc                 number of pcs's
%
% ----------------------------------------------------------------------
% File............: ps_densification.m
% Version & Date..: 1.7.2.16, 12-DEC-2009
% Authors.........: Freek van Leijen
%                   Gini Ketelaar
%                   Delft Institute of Earth Observation and Space Systems
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2009 Delft University of Technology, The Netherlands
%
% Change log
% v1.7.2.12, Freek van Leijen
% - filenames_output in cells
% v1.7.2.17, Freek van Leijen
% - mode_fix option for densification check
% - any number of connecting arcs (together with mode_fix)
% v1.7.5.3, Sami Samiei Esfahany
% - bug fixed, line 539: psp_acheck_grid(ps_index(ps_index2),:,:) = NaN([length(ps_index<<<2>>>) Nifgs 3]);
%


% ----------------------------------------------------------------------
% Initialize
% ----------------------------------------------------------------------

ps_set_globals;

% ----------------------------------------------------------------------
% Loop
% ----------------------------------------------------------------------

for z = 1:Npsc_selections
    
    Nslc = Nifgs+1;
    
    % ----------------------------------------------------------------------
    % Read data
    % ----------------------------------------------------------------------
    
    psc_fid = fopen([project_id '_psc_sel' num2str(z) '.raw'],'r');
    psc_data = fread(psc_fid,[2*Nifgs+7 Npsc(z)],'double')';
    fclose(psc_fid);
    
    psc_array       = psc_data(:,1:2);
    psc_az          = psc_data(:,5);
    psc_r           = psc_data(:,6);
    psc_phase       = psc_data(:,7:Nifgs+6);
    psc_h2ph        = psc_data(:,Nifgs+7:2*Nifgs+6);
    psc_amp_disp    = psc_data(:,2*Nifgs+7);
    clear psc_data
    
    psc_validation_fid = fopen([project_id '_psc_validation_sel' num2str(z) '.raw'],'r');
    psc_amp = fread(psc_validation_fid,[Nslc Npsc(z)],'double')';
    fclose(psc_validation_fid);
    
    psc_results_fid = fopen([project_id '_psc_results_sel' num2str(z) '.raw'],'r');
    psc_data = fread(psc_results_fid,[3*Nifgs+Npar_max+2 Npsc(z)],'double')';
    fclose(psc_results_fid);
    
    psc_ens_coh             = psc_data(:,Npar_max+1);
    
    clear psc_data
    
    psc_atmo_fid = fopen([project_id '_psc_atmo_sel' num2str(z) '.raw'],'r');
    psc_atmo = fread(psc_atmo_fid,[Nifgs Npsc(z)],'double')';
    fclose(psc_atmo_fid);
    
    ref_fid = fopen([project_id '_ref_sel' num2str(z) '.raw'],'r');
    ref_array = fread(ref_fid,[3 Nref(z)],'double')';
    fclose(ref_fid);
    
    % ----------------------------------------------------------------------
    % Save PSC Data
    % ----------------------------------------------------------------------
    
    psc                         = find(psc_array(:,2)~=0); % remove isolated psc
    psc_az_new                  = psc_az(psc);
    psc_r_new                   = psc_r(psc);
    psc_h2ph_new                = psc_h2ph(psc,:);
    psc_phase_new               = psc_phase(psc,:);
    psc_amp_new                 = psc_amp(psc,:);
    psc_amp_disp_new            = psc_amp_disp(psc);
    psc_ens_coh_new             = psc_ens_coh(psc);
    psc_atmo_new                = psc_atmo(psc,:);
    
    %psc_sig2hat_new            = psc_sig2hat(psc);
    %psc_phase_res_new          = psc_phase_res(psc,:);
    %psc_acheck_new             = psc_acheck(psc,:);
    %psc_param_new              = psc_param(psc,:);
    
    % Reference point determination
    ref_index                   = ref_array(1,1); % index of general reference point
    [temp_index]                = find(psc<ref_index);
    ref_index                   = length(temp_index)+1; %update ref_index
    
    % Save to output struct (could use STM modules for this)
    ps_data_out.nIfgs           = Nifgs;
    ps_data_out.nPsc            = length(psc_az_new); % check with freek!!
    
    ps_data_out.psc_az          = psc_az_new;
    ps_data_out.psc_r           = psc_r_new;
    ps_data_out.psc_phase       = psc_phase_new;
    ps_data_out.psc_ens_coh     = psc_ens_coh_new;
    ps_data_out.psc_h2ph        = psc_h2ph_new;
    ps_data_out.psc_atmo        = psc_atmo_new;
    ps_data_out.ref_index       = ref_index;
    ps_data_out.psc_amp         = psc_amp_new;
    ps_data_out.psc_amp_disp    = psc_amp_disp_new;
    
    % ----------------------------------------------------------------------
    % PSP Data
    % ----------------------------------------------------------------------
    
    % Save PSP points
    psp_fid = fopen([project_id '_psp_sel' num2str(Npsc_selections) '.raw'],'r');
    psp_data = fread(psp_fid,[2*Nifgs+4 Npsp],'double')';
    fclose(psp_fid);
    
    %psp_grid_az                = psp_data(:,1);
    %psp_grid_r                 = psp_data(:,2);
    psp_az                      = psp_data(1:NpspOrig,3);
    psp_r                       = psp_data(:,4);
    psp_phase                   = psp_data(:,5:Nifgs+4);
    psp_h2ph                    = psp_data(:,Nifgs+5:2*Nifgs+4);
    
    ps_data_out.nPsp            = Npsp;
    ps_data_out.psp_az          = psp_az;
    ps_data_out.psp_r           = psp_r;
    ps_data_out.psp_phase       = psp_phase;
    ps_data_out.psp_h2ph        = psp_h2ph;
    
    save(['ps_data_out_sel' num2str(z) '.mat'],'ps_data_out')
end


