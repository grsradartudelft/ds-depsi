function aoi_mask = ps_read_aoi_shapefile(aoi_filepath,nlines,bufferShape)

startLine   = bufferShape(1);
endLine     = bufferShape(2);
startPix    = bufferShape(3);
endPix      = bufferShape(4);

aoi_shp     = shaperead(aoi_filepath{1});
lon         = freadbk_quiet(char(aoi_filepath{2}),nlines,'float32',startLine,endLine,startPix,endPix);
lat         = freadbk_quiet(char(aoi_filepath{3}),nlines,'float32',startLine,endLine,startPix,endPix);

% Inverted because DePSI has 0 == true
% aoi_mask    = ~inpolygon(lon(startLine:endLine,startPix:endPix),lat(startLine:endLine,startPix:endPix),aoi_shp.X,aoi_shp.Y);
aoi_mask    = inpolygon(lon,lat,aoi_shp.X,aoi_shp.Y);

end

