function save_ds_network(Nifgs,Npsp,Npsc_selections)

% ps_densification
%
% Input:    - Nifgs               number of interferograms
%           - Npsc                number of psc's
%           - Npsp                number of psp's
%           - Npsc_selections     number of psc selections
%
% ----------------------------------------------------------------------
% File............: ps_densification.m
% Version & Date..: 1.7.2.16, 12-DEC-2009
% Authors.........: Freek van Leijen
%                   Gini Ketelaar
%                   Delft Institute of Earth Observation and Space Systems
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2009 Delft University of Technology, The Netherlands
%
% Change log
% v1.7.2.12, Freek van Leijen
% - filenames_output in cells
% v1.7.2.17, Freek van Leijen
% - mode_fix option for densification check
% - any number of connecting arcs (together with mode_fix)
% v1.7.5.3, Sami Samiei Esfahany
% - bug fixed, line 539: psp_acheck_grid(ps_index(ps_index2),:,:) = NaN([length(ps_index<<<2>>>) Nifgs 3]);
%


% ----------------------------------------------------------------------
% Initialize
% ----------------------------------------------------------------------

ps_set_globals;

% ----------------------------------------------------------------------
% Loop
% ----------------------------------------------------------------------

for z = 1:Npsc_selections
    
    
    % ----------------------------------------------------------------------
    % Read data
    % ----------------------------------------------------------------------
    
    psp_fid = fopen([project_id '_psp_sel' num2str(Npsc_selections) '.raw'],'r');
    psp_data = fread(psp_fid,[2*Nifgs+4 Npsp],'double')';
    fclose(psp_fid);
    
    psp_grid_az             = psp_data(:,1);
    psp_grid_r              = psp_data(:,2);
    psp_az                  = psp_data(:,3);
    psp_r                   = psp_data(:,4);
    psp_phase               = psp_data(:,5:Nifgs+4);
    psp_h2ph                = psp_data(:,Nifgs+5:2*Nifgs+4);
   
    % Save PSP (DS) points
    ds_data_out.nIfgs       = Nifgs;
    ds_data_out.nDs         = Npsp;
    ds_data_out.ds_az       = psp_az;
    ds_data_out.ds_r        = psp_r;
    ds_data_out.ds_phase    = psp_phase;
    ds_data_out.ds_h2ph     = psp_h2ph;
    
    save(['ds_data_out_sel' num2str(z) '.mat'],'ds_data_out')
end


