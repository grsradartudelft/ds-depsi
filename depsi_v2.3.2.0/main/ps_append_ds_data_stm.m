function [Ntot,nDs] = ps_append_ds_data_stm(dsFileName,Npsp,Npsc_selections,Nifgs)

% Function to add DS to the PSP selection
%
% Input:  - dsFileName         filename DS data file (.mat)
%         - Npsp               number of PSP
%         - Npsc_selections    number of PSC selections
%
% Output: - Npsp               updated number of PSP
%
% ----------------------------------------------------------------------
% File............: ps_append_ds_data.m
% Version & Date..: 23-SEP-2021
% Author..........: Philip Conroy
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%  
% Copyright (c) 2004-2021 Delft University of Technology, The Netherlands
% 
% Change log
% 27.09.2021 - PC - added imported amplitude data for validation psp file


% ----------------------------------------------------------------------
% Initialize
% ----------------------------------------------------------------------

global project_id

% Load multilooked DS Data (virtual PS) STM object

dsStm = stmread(dsFileName);

nDs = dsStm.numPoints;
Nifgs_ds = dsStm.numEpochs;

% Check if DS import matches Depsi
if Nifgs_ds ~= Nifgs
    error('Num. imported ifgs does not match current Depsi stack.')
end

% Read original PSP file
psp_fid = fopen([project_id '_psp_2orig_sel' num2str(Npsc_selections) '.raw'],'r');
psp_data = fread(psp_fid,[2*Nifgs+4 Npsp],'double')';
fclose(psp_fid);

% Find nearest PSP and add DS to the corresponding buffer
for i = 1:nDs
    [~,idx] = min(hypot(dsStm.pntAttrib.az(i)-psp_data(:,3),dsStm.pntAttrib.az(4)-psp_data(:,4)));
    dsGrid(i,1) = psp_data(idx,1);
    dsGrid(i,2) = psp_data(idx,2);
end

% Append DS data to PSP file
for z = 1:Npsc_selections
    psp_fid = fopen([project_id '_psp_2orig_sel' num2str(z) '.raw'],'a');
    fwrite(psp_fid,dsGrid(:,1:2*Nifgs+4)','double');
    fclose(psp_fid);
    
    % Update number of secondary PSC with the additional DS "points"
    Ntot(z) = Npsp(z) + nDs;
    
    
    % For debugging
%     psp_fid = fopen([project_id '_psp_2orig_sel' num2str(z) '.raw'],'r');
%     psp_new = fread(psp_fid,[2*Nifgs+4 Npsp(z)],'double')';
%     fclose(psp_fid);

%     figure
%     hold on
%     plot(psp_new(1,Nifgs+5:2*Nifgs+4))
%     test=psp_new(end,Nifgs+5:2*Nifgs+4);
% %     test(15)=[];
%     plot(test)
    
end

% Update psp validation file
% Write DS amplitude data to psp validation file
dsAmpData = dsStm.auxData.amplitude;

psp_validation_fid = fopen([project_id '_psp_validation.raw'],'a');
fwrite(psp_validation_fid,dsAmpData','double');
fclose(psp_validation_fid);

disp('DS Data appended to PSP file')

end

