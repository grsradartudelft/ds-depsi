function [Npsp,NpspOrig,nDs] = ps_append_ds_data_igrs(dsFileName,Npsp,Npsc_selections,Nifgs,lam_filepath,phi_filepath,nlines,filenames_slc,filenames_h2ph)

% Function to add DS to the PSP selection 
% Modified to add IGRS pixel
%
% Input:  - dsFileName         filename DS data file (.mat)
%         - Npsp               number of PSP
%         - Npsc_selections    number of PSC selections
%
% Output: - Npsp               updated number of PSP
%
% ----------------------------------------------------------------------
% File............: ps_append_ds_data.m
% Version & Date..: 23-SEP-2021
% Author..........: Philip Conroy
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%  
% Copyright (c) 2004-2021 Delft University of Technology, The Netherlands
% 
% Change log
% 27.09.2021 - PC - added imported amplitude data for validation psp file


% ----------------------------------------------------------------------
% Initialize
% ----------------------------------------------------------------------

global project_id

disp('Adding virtual DS points to Depsi.')

%% Load multilooked DS Data (virtual PS)
exportStruct = load(dsFileName); 
dsData = exportStruct.exportData;
nDs = size(dsData,1);
Nifgs_ds = (size(dsData,2)-7)/3;

% Check if DS import matches Depsi
if Nifgs_ds ~= Nifgs
    error('Num. imported ifgs does not match current Depsi stack.')
end

% Read original PSP file
psp_fid = fopen([project_id '_psp_2orig_sel' num2str(Npsc_selections) '.raw'],'r');
psp_data = fread(psp_fid,[2*Nifgs+4 Npsp],'double')';
fclose(psp_fid);

% Find nearest PSP and add DS to the corresponding buffer
for i = 1:nDs
    [~,idx] = min(hypot(dsData(i,3)-psp_data(:,3),dsData(i,4)-psp_data(:,4)));
    dsData(i,1) = psp_data(idx,1);
    dsData(i,2) = psp_data(idx,2);
end

%% Add IGRS pixel
% IGRS Zegveld coordinates: 52.137794, 4.839186
igrsCoords      = [52.137794, 4.839186];

lat             = freadbk_quiet(phi_filepath,nlines);
slc_size        = size(lat);
lat             = lat(:);

lon             = freadbk_quiet(lam_filepath,nlines);
lon             = lon(:);

[~,igrs_idx]    = min(hypot(lat-igrsCoords(1),lon-igrsCoords(2)));
[igrs_l,igrs_p] = ind2sub(slc_size,igrs_idx);

% Load a 5x5 square around the coordinate, then sample the brightest pixel
r0 = igrs_l-2;
rN = igrs_l+2;
c0 = igrs_p-2;
cN = igrs_p+2;

for j = 1:Nifgs+1
    slc             = freadbk_quiet([filenames_slc{j}(1:end-14) 'slc_srd.raw'],nlines,'cpxfloat32',r0,rN,c0,cN);
    slc_stack(:,j)  = slc(:);
    
    if j < Nifgs+1
        h2ph            = freadbk_quiet([filenames_h2ph{j}(1:end-8) '.raw'],nlines,'float32',r0,rN,c0,cN);
        h2ph_stack(:,j) = h2ph(:);
    end
end

% Take the brightest pixel
[~,idx]             = max(mean(slc_stack,2));
igrsSlc             = slc_stack(idx,:);

% Write SLC pixel value to csv
writematrix(igrsSlc,['igrs_pixel_slc_' dsFileName(1:11) '.csv']); 

% Interferometric values (depsi always has the last date as master)
igrsIfgs            = igrsSlc(end).*conj(igrsSlc(1:end-1));

% Single-difference phases
igrsPhases          = angle(igrsIfgs);

% Amplitude data
igrsAmp             = abs(igrsSlc);
igrsAmpDisp         = std(igrsAmp)/mean(igrsAmp);

% h2ph values
igrsH2ph            = h2ph_stack(idx,:);

% Line and pixel coordinates
[lsel,psel]         = ind2sub([3,3],idx);
lineNum             = r0+lsel-1;
pixNum              = c0+psel-1;

% Find nearest buffer
[~,idx]             = min(hypot(lineNum-psp_data(:,3),pixNum-psp_data(:,4)));

% Fill in the IGRS data array
igrsData                        = zeros(1,2*Nifgs+4);
igrsData(1)                     = psp_data(idx,1);
igrsData(2)                     = psp_data(idx,2);
igrsData(3)                     = lineNum;
igrsData(4)                     = pixNum;
igrsData(5:Nifgs+4)             = igrsPhases;
igrsData(Nifgs+5:2*Nifgs+4)     = igrsH2ph;

%% Append DS and IGRS data to PSP file
% Add IGRS data first so that it doesnt end up in the list of DS points
for z = 1:Npsc_selections
    psp_fid = fopen([project_id '_psp_2orig_sel' num2str(z) '.raw'],'a');
    fwrite(psp_fid,igrsData,'double');
    fwrite(psp_fid,dsData(:,1:2*Nifgs+4)','double');
    fclose(psp_fid);
    
    % Update number of secondary PSC with the additional DS "points"        
    % +1 for IGRS
    NpspOrig(z) = Npsp(z) + 1; %Add a +1 to this one too to include the igrs in the list of psps
    Npsp(z) = Npsp(z) + nDs + 1;
end

% Update psp validation file
% Write DS amplitude data to psp validation file
dsAmpData = dsData(:,2*Nifgs+5:3*Nifgs+6);

igrsAmpData           = zeros(1,size(dsAmpData,2));
igrsAmpData(1)        = igrsAmpDisp; 
igrsAmpData(:,2:end)  = igrsAmp; 

psp_validation_fid = fopen([project_id '_psp_validation.raw'],'a');
fwrite(psp_validation_fid,igrsAmpData','double');
fwrite(psp_validation_fid,dsAmpData','double');
fclose(psp_validation_fid);

disp(['Done. Added ' num2str(nDs) ' to the PSP list.'])

end

