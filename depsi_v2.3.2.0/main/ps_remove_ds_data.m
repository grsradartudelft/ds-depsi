function Npsp = ps_remove_ds_data(Npsp,NpspOrig,nDs,Npsc_selections,Nifgs)

% Function to remove DS to the PSP selection after APS filtering
%
% Input:  - Npsp               number of PSPs + DSs
%         - NpspOrig           original number of PSPs
%         - nDs                number of DSs
%         - Npsc_selections    number of PSC selections
%
% Output: - Npsp               updated number of PSP
%
% ----------------------------------------------------------------------
% File............: ps_remove_ds_data.m
% Version & Date..: 31-JAN-2024
% Author..........: Philip Conroy
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%  
% Copyright (c) 2004-2021 Delft University of Technology, The Netherlands
% 
% Change log
% 27.09.2021 - PC - added imported amplitude data for validation psp file


% ----------------------------------------------------------------------
% Initialize
% ----------------------------------------------------------------------

global project_id
disp('Removing virtual DS points from Depsi.')

%% Read files
% Read PSP file
psp_fid = fopen([project_id '_psp_2orig_sel' num2str(Npsc_selections) '.raw'],'r');
psp_data = fread(psp_fid,[2*Nifgs+4 Npsp],'double')';
fclose(psp_fid);

% Read PSP validation file
psp_validation_fid = fopen([project_id '_psp_validation.raw'],'r');
psp_val_data = fread(psp_validation_fid,[Nifgs+2 Npsp],'double')';
fclose(psp_validation_fid);

%% Remove DS data (last Nds lines)
psp_data(end-nDs+1:end,:)       = [];
psp_val_data(end-nDs+1:end,:)   = [];
Npsp = NpspOrig;

%% Write new files
% Write new psp file
psp_fid = fopen([project_id '_psp_2orig_sel' num2str(Npsc_selections) '.raw'],'w');
fwrite(psp_fid,psp_data','double');
fclose(psp_fid);

% Write new psp validation file
psp_fid = fopen([project_id '_psp_validation.raw'],'w');
fwrite(psp_fid,psp_val_data','double');
fclose(psp_fid);


disp(['Done. Removed ' num2str(nDs) ' DS points from the PSP list.'])

end

