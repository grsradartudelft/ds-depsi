%Input_file template for ps_analysis.m
%(See the function ps_readinput_parameters.m for more information)

% General parameters
% ----------------------------------------------------------------------

max_mem_buffer = 50e7
visible_plots = 'n'
detail_plots = 'n'
processing_groups = []
run_mode = 'normal'


% Project parameters
% ----------------------------------------------------------------------

project_id = 'nobv_zegveld'
input_file = []
processDir = '/Users/pconroy/phd/projects/nobv/zegveld/insar/flinsar/spider_new/s1_asc_t161'
startDate = '20200112'
stopDate = '20220531'
excludeDate = []
ifgsVersion = '_srd'
altimg = ''
master = '20200330'
swath_burst = []
sensor = 's1'
orbit = 'asc'
processor = 'doris_flinsar'
project = 'nl_groenehart'
crop = []
az_spacing = 13.9
r_spacing = 4.0
slc_selection_input = []
ifg_selection_input = []
ref_cn = []
Ncv = 25
ps_method = 'rnn'
psc_model = [2]
ps_model = [5]
final_model = [5]
breakpoint = []
breakpoint2 = []
ens_coh_threshold = 0.5
varfac_threshold = 3
detrend_method = 'yes'
output_format = 1
stc_min_max = [30,100]
do_apriori_sidelobe_mask = 'yes'
do_aposteriori_sidelobe_mask = 'no'


% Geocoding parameters
%----------------------------------------------------------------------

master_res = 'master.res' %'slave.res' 
ref_height = 0
demFile = 'dem_radar_nl_groenehart.raw'

% Psc parameters
%----------------------------------------------------------------------

amplitude_calibration = 'yes'
psc_selection_method = 'threshold'
psc_selection_gridsize = 300
psc_threshold = 0.2
max_arc_length = 5000
network_method = 'spider'
Ncon = 16
Nparts = 8
Npsc_selections = 1
filename_water_mask = []
gamma_threshold = 0.45
psc_distribution = 'uniform'
weighted_unwrap = 'yes'

% threshold is percentage of slc's that has an amplitude peak
livetime_threshold = 0.2;
% include local near maxima 
peak_tolerance = 0.9;


% Ps parameters
% ----------------------------------------------------------------------

psp_selection_method = 'ampdisp'
psp_threshold1 = 0.4
psp_threshold2 = []
ps_eval_method = 'psp'
Namp_disp_bins = 100
Ndens_iterations = 5
densification_flag = 'yes'
ps_area_of_interest = []
dsFileName = 's1_asc_t161_ds_export_data.mat' % Ds data to append to secondary network. Leave empty if not required
dens_method = 'orig'
dens_check = 'nocheck'
Nest = 1;


% Stochastic model parameters
% ----------------------------------------------------------------------
% std_param = [topo master_atmo subpixel linear quadratic cubic periodic]
% [par1 par2 par3 ...], standard deviations of parameters,
% used to set the search space for unwrapping algorithms
std_param = [30,0.005,1,0.001,0.01,0.01,0.01];
defo_range = 5000
weighting = 'vce'
ts_atmo_filter = 'gaussian'
ts_atmo_filter_length = 12/12
ts_noise_filter = 'gaussian'
ts_noise_filter_length = 8/12


% Bowl parameters
%-----------------------------------------------------------------------

defo_method = []
xc0 = []
yc0 = []
zc0 = []
r0 = []
r10 = []
epoch = []
