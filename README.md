# README #

### What is this repository for? ###

* Quick summary: DS-Depsi is a DS InSAR processing suite which uses contextual data to aid the processing steps. 
* Version 1.0

### How do I get set up? ###

* Summary of set up: Download all the code and set up a separate directory where you want to process the data. DS_PROCESSING_MAIN.m runs the program. 

* Configuration: 
    * Set up a sub directory in the root processing folder for contextual data input. The remaining folder structure will be auto-generated. 
    * Modify processing_settings.m as needed

* Dependencies
    * Doris v5 processed SLC stack (with the bursts stitched together)
    * Shapefile of parcel contextual information
    * KNMI climate data file: etmgeg_###.txt (https://www.knmi.nl/nederland-nu/klimatologie/daggegevens) 
    * RNN model: <model name>.hdf5
    * RNN model outputs: rnnOutputDF_<AOI>_<training location>_<model name>.csv, confusion_matrix.csv
    * stmutil MATLAB library

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* p.n.conroy@tudelft.nl