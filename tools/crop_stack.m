clear
clc

% crop_stack.m
% Crops a coregistered doris 5 slc stack with the shp file provided
% Author: Philip Conroy
% 8 Aug 2022

addpath(genpath('/project/caroline/Share/software/Utilities'))

stack_dir       = {'/project/caroline/Share/stitch/nl_veenweiden/nl_veenweiden_s1_asc_t015/nl_veenweiden_cropped_stack';
                   '/project/caroline/Share/stitch/nl_veenweiden/nl_veenweiden_s1_asc_t088/nl_veenweiden_cropped_stack';
                   '/project/caroline/Share/stitch/nl_veenweiden/nl_veenweiden_s1_dsc_t037/nl_veenweiden_cropped_stack'};

output_dir      = {'/project/caroline/Share/projects/nobv/cropped_stacks/rouveen/s1_asc_t015';
                   '/project/caroline/Share/projects/nobv/cropped_stacks/rouveen/s1_asc_t088';
                   '/project/caroline/Share/projects/nobv/cropped_stacks/rouveen/s1_asc_t037'};

aoi_shapefile   = '/project/caroline/Share/projects/nobv/aoi/zegveld/zegveld_aoi_polygon.shp';
project_name    = 'nl_veenweiden';
start_date      = datetime(2016,1,1); % Ignore images before this date
end_date        = datetime(2022,1,1); % Ignore images after this date
nlines          = []; % If empty, i will search for the nlines_crp.txt file (recommended)
npixels         = []; % If empty, i will search for the npixels_crp.txt file (recommended)

%% load shapefile
shp_data        = shaperead(aoi_shapefile);
min_lat         = shp_data.BoundingBox(1,2);
max_lat         = shp_data.BoundingBox(2,2);
min_lon         = shp_data.BoundingBox(1,1);
max_lon         = shp_data.BoundingBox(2,1);

if length(stack_dir) ~= length(output_dir)
    error('Mismatch number of input and output paths')
end

for i = 1:length(stack_dir)
    
    disp(['Cropping stack ' num2str(i) ' of ' num2str(length(stack_dir))])
    
    %% find master image
    filelist        = dir(fullfile(stack_dir{i}, ['**/master.res']));
    master_dir      = filelist.folder;
    master_date     = master_dir(end-7:end); % Assumes images are named yyyymmdd
    master_dt       = datetime(master_date,'InputFormat','yyyyMMdd');
    disp(['Found master image: ' master_dir])
    
    if master_dt < start_date || master_dt > end_date
        error('Master image is outside specifed date range')
    end
    
    %% load geolocation data
    if isempty(nlines)
        fid         = fopen([stack_dir{i} '/nlines_crp.txt']);
        data        = textscan(fid,'%s \n %s \n %s');
        nlines      = str2double(data{1});
        start_line  = str2double(data{2});
        end_line    = str2double(data{3});
        fclose(fid);
    end
    
    if isempty(npixels)
        fid         = fopen([stack_dir{i} '/npixels_crp.txt']);
        data        = textscan(fid,'%s \n %s \n %s');
        npixels     = str2double(data{1});
        start_pixel = str2double(data{2});
        end_pixel   = str2double(data{3});
        fclose(fid);
    end
    
    lon_grid        = freadbk_quiet([master_dir '/lam_' project_name '.raw'],nlines);
    lat_grid        = freadbk_quiet([master_dir '/phi_' project_name '.raw'],nlines);
    
    if size(lon_grid,1) ~= nlines || size(lon_grid,2) ~= npixels
        error('Size of coordinate grid does not match expected grid size')
    end
    
    % This finds the max extent of the polygon in radar coordinates to prevent
    % having to reproject the stack into geocoordinates
    extent = lon_grid >= min(shp_data.X) & lon_grid <= max(shp_data.X) & lat_grid >= min(shp_data.Y) & lat_grid <= max(shp_data.Y);
    [idx(:,1),idx(:,2)] = ind2sub(size(extent),find(extent));
    crop = [min(idx) max(idx)];
    
    %% load stack data, crop and save output
    if ~isfolder(output_dir{i})
        mkdir(output_dir{i})
    end
    
    slc_paths       = dir(fullfile(stack_dir{i}, ['**/slc_srd_' project_name '.raw']));
    nlines_new      = crop(3)-crop(1)+1;
    npixels_new     = crop(4)-crop(2)+1;
    cropped_stack   = zeros(nlines_new,npixels_new,length(slc_paths));
    
    disp(['Found ' num2str(length(slc_paths)) ' images in stack'])
    for j = 1:length(slc_paths)
        
        date    = slc_paths(j).folder(end-7:end);
        date_dt = datetime(date,'InputFormat','yyyyMMdd');
        
        if date_dt >= start_date && date_dt <= end_date
            
            output_path = [output_dir{i} '/' date];
            
            if ~isfolder([output_dir{i} '/' date])
                mkdir([output_dir{i} '/' date])
            end
            
            % SLC radar data
            slc             = freadbk_quiet([slc_paths(j).folder '/' slc_paths(j).name],nlines,'cpxfloat32');
            cropped_slc     = slc(crop(1):crop(3),crop(2):crop(4));
            fwritebk(cropped_slc,[output_path '/slc_srd.raw'],'cpxfloat32');
            
            if strcmp(date,master_date)
                % res file
                copyfile([slc_paths(j).folder '/master.res'],[output_path '/master.res']);
                
                % dem
                dem             = freadbk_quiet([slc_paths(j).folder '/dem_radar_' project_name '.raw'],nlines,'float32');
                cropped_dem     = dem(crop(1):crop(3),crop(2):crop(4));
                fwritebk(cropped_dem,[output_path '/dem_radar.raw'],'float32');
                
                % coordinates
                cropped_lam     = lon_grid(crop(1):crop(3),crop(2):crop(4));
                fwritebk(cropped_lam,[output_path '/lam.raw'],'float32');
                
                cropped_phi     = lat_grid(crop(1):crop(3),crop(2):crop(4));
                fwritebk(cropped_phi,[output_path '/phi.raw'],'float32');
                
            else
                % Interferograms and height to phase factors
                ifg             = freadbk_quiet([slc_paths(j).folder '/cint_srd_' project_name '.raw'],nlines,'cpxfloat32');
                cropped_ifg     = ifg(crop(1):crop(3),crop(2):crop(4));
                fwritebk(cropped_slc,[output_path '/cint_srd.raw'],'cpxfloat32');
                
                h2ph            = freadbk_quiet([slc_paths(j).folder '/h2ph_' project_name '.raw'],nlines,'float32');
                cropped_h2ph    = h2ph(crop(1):crop(3),crop(2):crop(4));
                fwritebk(cropped_h2ph,[output_path '/h2ph.raw'],'float32');
                
                % Res files
                copyfile([slc_paths(j).folder '/slave.res'],[output_path '/slave.res']);
                
            end
            
        end
    end
    
    start_line_new  = start_line  + crop(1) - 1;
    end_line_new    = start_line_new + nlines_new - 1;
    start_pixel_new = start_pixel + crop(2) - 1;
    end_pixel_new   = start_pixel_new + npixels_new - 1;
    
    writematrix([string(num2str(nlines_new));  string(num2str(start_line_new));  string(num2str(end_line_new))], [output_dir{i} '/nlines_crp.txt']);
    writematrix([string(num2str(npixels_new)); string(num2str(start_pixel_new)); string(num2str(end_pixel_new))],[output_dir{i} '/npixels_crp.txt']);
end