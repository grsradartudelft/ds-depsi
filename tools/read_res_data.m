function resData = read_res_data(master_date,dates,res_files)



m_ind = master_date==dates;

opts = fixedWidthImportOptions('datalines',[32 99],'VariableNames',{'Field','Value'},'VariableWidths',[58,69]);
readData = readtable(res_files{m_ind},opts);

% Radar parameters
resData.swath               = string(readData.Value(7));
resData.pass                = string(readData.Value(8));
resData.polarization        = string(readData.Value(10));
resData.frequency           = str2double(readData.Value(20));
resData.wavelength          = str2double(readData.Value(27));
resData.prf                 = str2double(readData.Value(31));
resData.az_bw               = str2double(readData.Value(33));
resData.az_win              = string(readData.Value(34));
resData.r_fs                = str2double(readData.Value(36));
resData.r_bw                = str2double(readData.Value(37));
resData.r_win               = string(readData.Value(38));
resData.nlines              = str2double(readData.Value(51));
resData.pixels              = str2double(readData.Value(52));

% Scene location
ul_lat                      = str2double(readData.Value(53));
ur_lat                      = str2double(readData.Value(54));
lr_lat                      = str2double(readData.Value(55));
ll_lat                      = str2double(readData.Value(56));


ul_lon                      = str2double(readData.Value(57));
ur_lon                      = str2double(readData.Value(58));
lr_lon                      = str2double(readData.Value(59));
ll_lon                      = str2double(readData.Value(60));


resData.nw_corner           = [ul_lat,ul_lon];
resData.ne_corner           = [ur_lat,ur_lon];
resData.se_corner           = [lr_lat,lr_lon];
resData.sw_corner           = [ll_lat,ll_lon];

% Timestamps
resData.master_timestamp    = datetime(string(readData.Value(44)),'InputFormat','yyyy-MMM-dd HH:mm:ss.SSSSSS');

opts = fixedWidthImportOptions('datalines',[75,75],'VariableNames',{'Field','Value'},'VariableWidths',[58,69]);
for i = 1:length(res_files)
   readData = readtable(res_files{i},opts);
   timestamps(i) = datetime(string(readData.Value(1)),'InputFormat','yyyy-MMM-dd HH:mm:ss.SSSSSS');
end
resData.timestamps          = timestamps'; % This gives all image timestamps
