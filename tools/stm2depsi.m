function stm2depsi(stm,filepathOut)
% Converts a DS STM file to depsi formatted input file

nIfgs = stm.numEpochs;
nDs   = stm.numPoints;

% Convert parcel locations (centroids) to line/pixel coords
if ~isfield(stm.pntAttrib,'lineNum') || ~isfield(stm.pntAttrib,'pixelNum')
    
    nlines  = stm.techniqueAttrib.nlines;
    npixels = stm.techniqueAttrib.npixels;
    
    lon     = freadbk_quiet(stm.techniqueAttrib.lon_path,nlines);
    lat     = freadbk_quiet(stm.techniqueAttrib.lat_path,nlines);
    
    idx = nan(nDs,1);
    for i = 1:nDs
        [~,idx(i)] = min(hypot(stm.pntCrd(i,2)-lon(:),stm.pntCrd(i,1)-lat(:)));
    end
    [lineNum,pixelNum] = ind2sub([nlines,npixels],idx);
end

% Read h2ph data from stack
if ~isfield(stm.auxData,'h2ph')
    
    stackPath   = stm.techniqueAttrib.stack_path;
    stackId     = stm.techniqueAttrib.stackId;
    h2phList    = dir(fullfile(stackPath,['**/h2ph_' stackId '.raw']));
    
    h2phFiles   = [];
    h2ph        = nan(length(lineNum),nIfgs);
    
    masterDate  = stm.techniqueAttrib.master_timestamp;
    startDate   = dateshift(stm.epochAttrib.timestamp(1), 'start', 'day');
    endDate     = dateshift(stm.epochAttrib.timestamp(end), 'start', 'day');
    
    for i = 1:length(h2phList)
        fileDate = datetime(h2phList(i).folder(end-7:end),'InputFormat','yyyyMMdd');
        if fileDate ~= masterDate && fileDate >= startDate && fileDate <= endDate
            h2phFiles = [h2phFiles; string([h2phList(i).folder '/' h2phList(i).name])];
        end
    end
    if length(h2phFiles) ~= nIfgs
        error('Mismatch between stack dates and h2ph files')
    end
    
    for i = 1:nIfgs
        readData = freadbk_quiet(char(h2phFiles(i)),nlines,'float32');
        readData = readData(:);
        h2ph(:,i) = readData(idx);
    end
end

exportData = nan(nDs,3*nIfgs+6);

exportData(:,1)                     = ones(size(lineNum));      % Set during the depsi import
exportData(:,2)                     = ones(size(lineNum));      % Set during the depsi import
exportData(:,3)                     = lineNum;                  % Closest radar coords of parcel centroid
exportData(:,4)                     = pixelNum;                 % Closest radar coords of parcel centroid
exportData(:,5        :  nIfgs+4)   = stm.obsData.rawPhase;     % Raw ESM phase estimation
exportData(:,nIfgs+5  :2*nIfgs+4)   = h2ph;                     % Height to phase factors
exportData(:,2*nIfgs+5          )   = ones(nDs,1);              % Amplitude dispersion (placeholder value)
exportData(:,2*nIfgs+6:3*nIfgs+6)   = stm.auxData.amplitude;    % Mean amplitude time series
exportData(:,3*nIfgs+7)             = stm.pntAttrib.parcelId;   % Parcel ID

save(filepathOut,'exportData');

end

