function [la,inc] = la_inc_angles(l,p,h,master_date,dates,res_files)
% This script calculates the look angle, perpendicular baselines and height
% to phase factor (h2ph) using DORIS result files
%
% INPUT:
% l:        array of line coordinates
% p:        array of pixel coordinates
% h:        array of heights of (l,p) above the ellipsoid
% 
%
% OUTPUT:
% la:       array of look angles at location (l,p,h).
% inc:      array of incidence angles at location (l,p,h).
%
% Author: FMG HEUFF - 03/2018, P. Conroy 03/2022

% Modified: 17/02/2020 - Added comments. (FH)

% PC 03/2022: modification of la_bp_inc.m to only do incidence angles


%% Master SLC
m_ind                   = master_date==dates;
% Read resfiles
[m_image,m_orbit]       = metadata(res_files{m_ind});
% Fit orbit
m_orbfit                = orbitfit(m_orbit,4,'verbose',0);
% Convert to XYZ
[m_xyz,m_satvec]        = lph2xyz(l,p,h,m_image,m_orbfit);
m_sat_xyz               = m_satvec(:,1:3);
m_r                     = m_sat_xyz - m_xyz;  

R1                      = sqrt(sum(m_r.^2,2));          % Distance satellite <-> pixels
rho1                    = sqrt(sum(m_sat_xyz.^2,2));    % Distance satellite <-> (x0,y0,z0)
Re                      = sqrt(sum(m_xyz.^2,2));        % Distance pixel <-> (x0,y0,z0)

la                      = acosd((rho1.^2-Re.^2+R1.^2)./(2*rho1.*R1));       % Look-angle 
inc                     = 180-acosd((-rho1.^2+Re.^2+R1.^2)./(2*R1.*Re));    % Incidence angle


