function [stmOut] = stm_add_parcel_attrs(stm,shpPath)

parcelData                          = shaperead(shpPath);
stmOut                              = stm;
stmOut.techniqueAttrib.shp_path     = shpPath;

for i = 1:stm.numPoints
    
    % Get parcel ids - numeric
    if isfield(parcelData(i),'id')
        parcelIdList = [parcelData.id]';
    elseif isfield(parcelData(i),'int_id')
        parcelIdList = [parcelData.int_id]';
    else
        error('Couldnt read parcel IDs')
    end
    idx = find(parcelIdList == stm.pntAttrib.parcelId(i));

    if length(idx) > 1
        error(['Same parcel (' num2str(stm.pntAttrib.id(i)) ') has multiple entries. Check shapefile.'])
    end
    
    % Crop code - numeric
    if isfield(parcelData,'gewascode')
        if isa(parcelData(idx).gewascode,'char')
            stmOut.pntAttrib.cropcode(i)    = str2double(parcelData(idx).gewascode);
        else
            stmOut.pntAttrib.cropcode(i)    = parcelData(idx).gewascode;
        end
    elseif isfield(parcelData,'cropcode')
        stmOut.pntAttrib.cropcode(i)        = parcelData(idx).cropcode;
    end
    
    % Crop name - text
    if isfield(parcelData,'gewas')
        stmOut.pntAttrib.cropname(i)        = string(parcelData(idx).gewas);
    end
    
    % Soilcode - text
    if isfield(parcelData,'soilcode')
        stmOut.pntAttrib.soilcode(i)        = string(parcelData(idx).soilcode);
    elseif isfield(parcelData,'EERSTE_BOD')
        stmOut.pntAttrib.soilcode(i)        = string(parcelData(idx).EERSTE_BOD);
    end
    
    % Landform subgroup - text
    if isfield(parcelData,'landformsu')
        stmOut.pntAttrib.landformsg(i)      = string(parcelData(idx).landformsg);
    end
    
    % AHN3 heights - numeric
    if isfield(parcelData,'ahn3_mean')
        stmOut.pntAttrib.ahn3(i)            = parcelData(idx).ahn3_mean;
    end
    
    % GHG - numeric
    if isfield(parcelData,'ghg_mean')
        stmOut.pntAttrib.ghg(i)             = parcelData(idx).ghg_mean/10; % Raster data provided in cm
    end
    
    % GLG - numeric
    if isfield(parcelData,'glg_mean')
        stmOut.pntAttrib.glg(i)             = parcelData(idx).glg_mean/10; % Raster data provided in cm
    end
    
    % Peilgebied ID - numeric
    if isfield(parcelData,'peilgebied')
        stmOut.pntAttrib.peilgebied(i)      = parcelData(idx).peilgebied;
    end
    
    % KNMI Station ID - numeric
    if isfield(parcelData,'knmi_id')
        stmOut.pntAttrib.knmi_id(i)         = parcelData(idx).knmi_id;
    end
    
    % Archetype - text
    if isfield(parcelData,'archetype')
        stmOut.pntAttrib.archetype(i)       = string(parcelData(idx).archetype);
    end
    
    % Region - text
    if isfield(parcelData,'region')
        stmOut.pntAttrib.region(i)          = string(parcelData(idx).region);
    end
    
end

if isfield(parcelData,'gewascode') || isfield(parcelData,'cropcode')
    stmOut.pntAttrib.cropcode               = stmOut.pntAttrib.cropcode';
else
    disp('Did not add crop codes')
end

if isfield(parcelData,'gewas')
    stmOut.pntAttrib.cropname               = stmOut.pntAttrib.cropname';
else
    disp('Did not add crop names')
end

if isfield(parcelData,'EERSTE_BOD') || isfield(parcelData,'soilcode')
    stmOut.pntAttrib.soilcode               = stmOut.pntAttrib.soilcode';
else
    disp('Did not add soil codes')
end

if isfield(parcelData,'landformsu')
    stmOut.pntAttrib.landformsg             = stmOut.pntAttrib.landformsg';
else
    disp('Did not add landform subgroup')
end

if isfield(parcelData,'ahn3_mean')
    stmOut.pntAttrib.ahn3                   = stmOut.pntAttrib.ahn3';
else
    disp('Did not add AHN')
end

if isfield(parcelData,'ghg_mean')
    stmOut.pntAttrib.ghg                    = stmOut.pntAttrib.ghg';
else
    disp('Did not add GHG')
end

if isfield(parcelData,'glg_mean')
    stmOut.pntAttrib.glg                    = stmOut.pntAttrib.glg';
else
    disp('Did not add GLG')
end

if isfield(parcelData,'peilgebied')
    stmOut.pntAttrib.peilgebied             = stmOut.pntAttrib.peilgebied';
else
    disp('Did not add peilgebied')
end

if isfield(parcelData,'knmi_id')
    stmOut.pntAttrib.knmi_id                = stmOut.pntAttrib.knmi_id';
else
    disp('Did not add knmi id')
end

if isfield(parcelData,'archetype')
    stmOut.pntAttrib.archetype              = stmOut.pntAttrib.archetype';
else
    disp('Did not add archetype')
end

if isfield(parcelData,'region')
    stmOut.pntAttrib.region                 = stmOut.pntAttrib.region';
else
    disp('Did not add region')
end

end

