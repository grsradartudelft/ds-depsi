function [outputTs,usedDates] = combine_tracks_nan(inputTT,filterLen)
% Combine tracks and preserve nan values
% InputTT should only conatin insar data
combinedTT = mergevars(inputTT,string(inputTT.Properties.VariableNames),'NewVariableName','all_tracks');
combinedTs = nan(height(combinedTT),1);

for l=1:length(combinedTs)
    if any(~isnan(combinedTT.all_tracks(l,:)))
        combinedTs(l)       = combinedTT.all_tracks(l,~isnan(combinedTT.all_tracks(l,:)));
        combinedDates(l)    = combinedTT.Time(l);
    end
end

%% Linear interpolation in empty dates
combinedTTInterp = timetable(inputTT.Properties.RowTimes,combinedTs);
combinedTTInterp = retime(combinedTTInterp,'daily','linear');

combinedTsInterp = combinedTTInterp.(1);
dates            = combinedTTInterp.Properties.RowTimes;

nanIdx           = isnan(combinedTs);
nanDates         = combinedTT.Time(nanIdx);

%% Moving average filter (set filterLen to 1 to not filter)
outputTs = nan(size(combinedTsInterp));
for l = (floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2)
    outputTs(l) = mean(combinedTsInterp(l-floor(filterLen/2):l+floor(filterLen/2)));
end

outputTs        = outputTs((floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2));
usedDates       = dates((floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2));

nanIdxLong      = ismember(usedDates,nanDates);
outputTs(nanIdxLong) = nan;

% For debugging
% figure
% hold on
% plot(usedDates,outputTs)
% for i = 1:size(inputTT,2)
%     plot(inputTT.Time,inputTT.(i),'.')
% end

end

