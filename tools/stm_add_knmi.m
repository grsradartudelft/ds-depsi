function [stmOut] = stm_add_knmi(stm,knmiFiles,stackData)
% Add KNMI data

stmOut          = stm;

if isfield(stm.pntAttrib,'knmi_id')
    stations    = unique(stm.pntAttrib.knmi_id);
    nStations   = length(stations);
    
    % If the station ID is provided in the contextual data, then the user
    % only needs to provide the folder will all the knmi files and this
    % will select the correct ones
    if isfolder(knmiFiles)
        filelist    = dir(knmiFiles);
        filenames   = string({filelist.name})';
        folder      = string({filelist.folder})';
        knmiFiles   = strcat(folder, repmat("/",size(folder)), filenames);
    end    
    
else
    % If no station is provided, then there must be only a single knmi file
    % Extract the station number from the file
    
    if isfile(knmiFiles)
        [~,filename]    = fileparts(knmiFiles);
        stations        = str2double(regexp(filename,'\d*','Match'));
    else
        error('Missing station ID field/ No station specified')
    end
    nStations   = 1;
end

startDate       = stackData.startDate - 100;
endDate         = stackData.endDate;

for i = 1:nStations
    
    % Find corresponding meteo file
    if isfield(stm.pntAttrib,'knmi_id')
        fileName            = ['etmgeg_' num2str(stations(i))];
        idx                 = contains(knmiFiles,fileName);
        if sum(idx) == 0
            error(['Station data not found (' num2str(stations(i)) ')'])
        end
        selectedFiles(i)    = knmiFiles(idx);
    else
        % In this case, no station is specified, so it is assumed that
        % there is only one file to read
        selectedFiles(i)    = knmiFiles; 
    end
    
    % Read the meteo file
    opts            = delimitedTextImportOptions;
    opts.DataLines  = 53;
    knmiData        = readtable(selectedFiles(i),opts);
    knmiData        = knmiData(:,[2,12,23,41]);
    
    % Extract the dates
    fileDates       = datetime(table2array(knmiData(:,1)),'InputFormat','yyyyMMdd');
    
    % Temperature
    temperature(i,:)    = str2double(table2array(knmiData(fileDates>=startDate & fileDates<=endDate,2))); % 0.1 deg C
    
    % Precipitation
    precipitation(i,:)  = str2double(table2array(knmiData(fileDates>=startDate & fileDates<=endDate,3))); % 0.1 mm
    precipitation(precipitation==-1) = 0;
    
    % Evapotranspiration
    evapotrans(i,:)     = str2double(table2array(knmiData(fileDates>=startDate & fileDates<=endDate,4))); % 0.1 mm
end

knmiDates = fileDates(fileDates>=startDate & fileDates<=endDate);

stmOut.auxData.knmiDates            = knmiDates';
stmOut.auxData.temperature          = temperature/10; % deg C
stmOut.auxData.precipitation        = precipitation/10; % mm
stmOut.auxData.evapotrans           = evapotrans/10; % mm
stmOut.auxData.knmiStations         = stations;
stmOut.techniqueAttrib.knmi_files   = selectedFiles;

stmOut.auxTypes                     = {stmOut.auxTypes,'weatherTimestamp','temperature','precipitation'};

end

