function dates = get_stack_dates(stackDir)
% Get list of image dates from file list
filelist = dir(stackDir);

% Old
% filelist = filelist([filelist.isdir]);
% filelist(strcmp(string({filelist.name}),'.')) = [];
% filelist(strcmp(string({filelist.name}),'..')) = [];

% Filter out anything that is not an 8-digit number
goodIdx     = ~cellfun('isempty',regexp(string({filelist.name}),'\d{8}'));
filelist    = filelist(goodIdx);

% Extract dates from the directory name
dates       = sort(datetime(string({filelist.name})','InputFormat','yyyyMMdd'),'ascend');
end

