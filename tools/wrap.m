function wrapped = wrap(unwrapped)
% Wraps a (phase) time series on [-pi,pi)

wrapped = mod(unwrapped + pi,2*pi)-pi;

end

