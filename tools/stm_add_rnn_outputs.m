function [stmOut] = stm_add_rnn_outputs(stm,rnnModelPath,rnnOutputPath,conMatPath)

% Adds RNN output to the space-time matrix. 
% Path to the confusion matrix is required for the unwrapping procedure
% Path to the RNN model is required for traceability

stmOut                              = stm;
rnnData                             = readtable(rnnOutputPath);

stmOut.auxData.rnn.timestamp        = rnnData.Date';
stmOut.auxData.rnn.output           = table2array(rnnData(:,2:end))';
stmOut.auxData.rnn.model            = rnnModelPath;
stmOut.auxData.rnn.confusion_mat    = conMatPath;

end

