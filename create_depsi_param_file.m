function settings = create_depsi_param_file(settings)

for i = 1:settings.numTracks
    
    % Get list of image dates from file list
    filelist    = dir(settings.stackDir(i));
    filelist    = filelist([filelist.isdir]);
    filelist(strcmp(string({filelist.name}),'.'))   = [];
    filelist(strcmp(string({filelist.name}),'..'))  = [];
    dates       = datetime(string({filelist.name})','InputFormat','yyyyMMdd');
    
    % Find first image after startDate
    idx         = find(dates >= settings.startDate & dates <= settings.endDate);
    firstImage  = datestr(dates(idx(1))  ,'yyyymmdd');
    
    % Find last image before endDate
    lastImage   = datestr(dates(idx(end)),'yyyymmdd');
   
    % Save the path of the param file
    settings.depsiParamFile(i) = strcat(settings.depsiStackdir(i), settings.stackId(i), "_param_file.m");
    
    % Look for previous progress
    % Very basic for now, can be expanded in the future
    if isfile(fullfile(settings.depsiStackdir(i),"*project.mat"))
        projectFile = load(fullfile(settings.depsiStackdir(i),"*project.mat"));
        if isfield(projectFile,'Nds')
            progress = true;
        else
            progress = false;
        end
    else
        progress = false;
    end
    
    % Here we go...
    fid = fopen(settings.depsiParamFile(i),'w');
    fprintf(fid,'%s\n','%Input_file template for ps_analysis.m');
    fprintf(fid,'%s\n','%(See the function ps_readinput_parameters.m for more information)');
    fprintf(fid,'%s\n','');
    fprintf(fid,'%s\n','% General parameters');
    fprintf(fid,'%s\n','% ----------------------------------------------------------------------');
    fprintf(fid,'%s\n','max_mem_buffer = 50e7');
    fprintf(fid,'%s\n',"visible_plots = 'n'");
    fprintf(fid,'%s\n',"detail_plots = 'n'");
    if progress == true
        fprintf(fid,'%s\n',"processing_groups = 3:8");
    else
        fprintf(fid,'%s\n',"processing_groups = []");
    end
    fprintf(fid,'%s\n',"run_mode = 'normal'");
    fprintf(fid,'%s\n','');
    fprintf(fid,'%s\n',strcat("project_id = '", settings.projectName, "'"));
    fprintf(fid,'%s\n','input_file = []');
    fprintf(fid,'%s\n',strcat("processDir = '", settings.stackDir(i),"'"));
    fprintf(fid,'%s\n',strcat("startDate = '", firstImage, "'"));
    fprintf(fid,'%s\n',strcat("stopDate = '", lastImage,"'"));
    fprintf(fid,'%s\n','excludeDate = []');
    fprintf(fid,'%s\n',"ifgsVersion = '_srd'");
    fprintf(fid,'%s\n',"altimg = ''");
    fprintf(fid,'%s\n',strcat("master = '", string(datestr(settings.masterDate(i),'yyyymmdd')),"'"));
    fprintf(fid,'%s\n',"swath_burst = []");
    fprintf(fid,'%s\n',"sensor = 's1'");
    if strcmp(settings.pass(i),'Ascending')
        fprintf(fid,'%s\n',"orbit = 'asc'");
    elseif strcmp(settings.pass(i),'Descending')
        fprintf(fid,'%s\n',"orbit = 'dsc'");
    else
        error('Could not determine pass geometry from res file')
    end
    if strcmp(settings.processor,'flinsar')
        fprintf(fid,'%s\n',"processor = 'doris_flinsar'");
    elseif strcmp(settings.processor,'caroline')
        fprintf(fid,'%s\n',"processor = 'caroline'");
    end
    if isfield(settings.depsi,'forceIgrs')
        fprintf(fid,'%s\n',strcat("force_igrs = ", num2str(settings.depsi.forceIgrs)));
    else
        fprintf(fid,'%s\n',strcat("force_igrs = 0"));
    end
    fprintf(fid,'%s\n',strcat("project = '", settings.projectName, "'"));
    fprintf(fid,'%s\n',"crop = []");
    fprintf(fid,'%s\n',['az_spacing = ' num2str(settings.az_pixel_spacing(i),'%.1f')]);
    fprintf(fid,'%s\n',['r_spacing = ' num2str(settings.r_pixel_spacing(i),'%.1f')]);
    fprintf(fid,'%s\n',"slc_selection_input = []");
    fprintf(fid,'%s\n',"ifg_selection_input = []");
    fprintf(fid,'%s\n',"ref_cn = []");
    fprintf(fid,'%s\n',"Ncv = 25");
    fprintf(fid,'%s\n',"ps_method = 'perio'");
    fprintf(fid,'%s\n',"psc_model = [1]");
    fprintf(fid,'%s\n',"ps_model = [1]");
    fprintf(fid,'%s\n',"final_model = [2]");
    fprintf(fid,'%s\n',"breakpoint = []");
    fprintf(fid,'%s\n',"breakpoint2 = []");
    fprintf(fid,'%s\n',"ens_coh_threshold = 0.5");
    fprintf(fid,'%s\n',"varfac_threshold = 3");
    fprintf(fid,'%s\n',"detrend_method = 'yes'");
    fprintf(fid,'%s\n',"output_format = 1");
    fprintf(fid,'%s\n',"stc_min_max = [30,100]");
    fprintf(fid,'%s\n',"do_apriori_sidelobe_mask = 'yes'");
    fprintf(fid,'%s\n',"do_aposteriori_sidelobe_mask = 'no'");
    fprintf(fid,'%s\n',strcat("lam_filepath = 'lam.raw'"));
    fprintf(fid,'%s\n',strcat("phi_filepath = 'phi.raw'"));
    if isfield(settings,'aoiShapefile')
        fprintf(fid,'%s\n',strcat("aoi_filepath = '",settings.aoiShapefile,"'"));
    end
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% Geocoding parameters");
    fprintf(fid,'%s\n',"%----------------------------------------------------------------------");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"master_res = 'master.res'");
    fprintf(fid,'%s\n',"ref_height = 43");
    fprintf(fid,'%s\n',strcat("demFile = '", settings.demFilename(i), "'"));
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% Psc parameters");
    fprintf(fid,'%s\n',"%----------------------------------------------------------------------");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"amplitude_calibration = 'yes'");
    fprintf(fid,'%s\n',"psc_selection_method = 'threshold'");
    fprintf(fid,'%s\n',"psc_selection_gridsize = 300"); %!!! larger gridsize=less psc's
    fprintf(fid,'%s\n',strcat("psc_threshold = ", string(num2str(settings.depsi.pscAdThreshold))));
    fprintf(fid,'%s\n',"max_arc_length = 5000"); %!!! 10 km to help overall network connection
    fprintf(fid,'%s\n',"network_method = 'spider'"); % option 2: use delaunay network
    fprintf(fid,'%s\n',"Ncon = 16"); %!!! Less connections per direction ex. 8
    fprintf(fid,'%s\n',"Nparts = 8");
    fprintf(fid,'%s\n',"Npsc_selections = 1");
    fprintf(fid,'%s\n',"filename_water_mask = []");
    fprintf(fid,'%s\n',"gamma_threshold = 0.45");
    fprintf(fid,'%s\n',"psc_distribution = 'uniform'");
    fprintf(fid,'%s\n',"weighted_unwrap = 'yes'");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% threshold is percentage of slc's that has an amplitude peak");
    fprintf(fid,'%s\n',"livetime_threshold = 0.2;");
    fprintf(fid,'%s\n',"% include local near maxima");
    fprintf(fid,'%s\n',"peak_tolerance = 0.9;");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% Ps parameters");
    fprintf(fid,'%s\n',"% ----------------------------------------------------------------------");
    fprintf(fid,'%s\n',"psp_selection_method = 'ampdisp'");
    fprintf(fid,'%s\n',"psp_threshold1 = 0.4");
    fprintf(fid,'%s\n',"psp_threshold2 = []");
    fprintf(fid,'%s\n',"ps_eval_method = 'psp'");
    fprintf(fid,'%s\n',"Namp_disp_bins = 100");
    fprintf(fid,'%s\n',"Ndens_iterations = 5");
    fprintf(fid,'%s\n',"densification_flag = 'yes'");
    fprintf(fid,'%s\n',"ps_area_of_interest = []");
    fprintf(fid,'%s\n',strcat("dsFileName = '", settings.dsExportFilename(i), "'"));
    fprintf(fid,'%s\n',"dens_method = 'orig'");
    fprintf(fid,'%s\n',"dens_check = 'nocheck'");
    fprintf(fid,'%s\n',"Nest = 1;");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% Stochastic model parameters");
    fprintf(fid,'%s\n',"% ----------------------------------------------------------------------");
    fprintf(fid,'%s\n',"% std_param = [topo master_atmo subpixel linear quadratic cubic periodic]");
    fprintf(fid,'%s\n',"% [par1 par2 par3 ...], standard deviations of parameters,");
    fprintf(fid,'%s\n',"% used to set the search space for unwrapping algorithms");
    fprintf(fid,'%s\n',"std_param = [30,0.005,1,0.01,0.01,0.01,0.005];");
    fprintf(fid,'%s\n',"defo_range = 5000");
    fprintf(fid,'%s\n',"weighting = 'vce'");
    fprintf(fid,'%s\n',"ts_atmo_filter = 'gaussian'");
    fprintf(fid,'%s\n',"ts_atmo_filter_length = 2/12");
    fprintf(fid,'%s\n',"ts_noise_filter = 'gaussian'");
    fprintf(fid,'%s\n',"ts_noise_filter_length = 8/12");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"% Bowl parameters");
    fprintf(fid,'%s\n',"%-----------------------------------------------------------------------");
    fprintf(fid,'%s\n',"");
    fprintf(fid,'%s\n',"defo_method = []");
    fprintf(fid,'%s\n',"xc0 = []");
    fprintf(fid,'%s\n',"yc0 = []");
    fprintf(fid,'%s\n',"zc0 = []");
    fprintf(fid,'%s\n',"r0 = []");
    fprintf(fid,'%s\n',"r10 = []");
    fprintf(fid,'%s\n',"epoch = []");
    
    fclose(fid);
    
    
end
end

