function [modelParams,deltaZ] = fit_defo_model_v2(segments,dates,settings)

% Least-squares fit of a specified model type to a segmented time series.
% This is different from Depsi in that the time series is sparse.
% Each segemnt in the full TS starts at zero wrt the 1st epoch
%
% Input:
%         - segments         segmented time series (nSegments x nDates)
%         - dates            interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_seasonal_plus_lin.m
% Version & Date..: 1.0, 19-JAN-2022
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

funTol      = settings.seg.functionTolerance;
maxFunEvals = settings.seg.maxFunEvals;
usePar      = settings.seg.useParallel;
nSegments   = size(segments,1);

disp(['Estimating mean displacement model for ' num2str(nSegments) ' segments (all tracks)'])

% Segment phases are already projected to vertical, minus sign for positive
% upward motion
ph2v        = -0.0556/(4*pi); 
vSegments   = segments .* ph2v;

doy         = day(dates,'dayofyear')/365.25;
yearFrac    = unwrap(doy*2*pi)/(2*pi);

%% Initial estimates
amp         = 0.04; % 0.15*(ghg{m}-glg{m});
lin         = 0;
phase       = -2*pi*1/12;

% deltaZ      = zeros(nSegments,1);
% 
% % Remove epochs which have no segments
% epochIdx = any(~isnan(vSegments),1);

%% Model definition and constraints
model       = @(theta,t) theta(1)*cos( 2*pi*t + theta(2) ) + theta(3)*t;
shifts      = @(theta,t) mean(vSegments-model(theta,t),2,'omitnan');

if settings.seg.cost     == 1
    % Cost based on model fit to shifted segments
    cost        = @(theta) sqrt(mean(mean((vSegments - model(theta,yearFrac) - shifts(theta,yearFrac) ).^2,'omitnan'),'omitnan'));
    
elseif settings.seg.cost == 2
    % Cost based on dispersion of shifted segments
    cost        = @(theta) sqrt(mean(var( vSegments - shifts(theta,yearFrac),0,1,'omitnan'),'omitnan'));
    
end

A           = [];
b           = [];
Aeq         = [];
beq         = [];
lb          = [settings.seg.minAmp, settings.seg.minShift, settings.seg.minLinRate]; % Meters
ub          = [settings.seg.maxAmp, settings.seg.maxShift, settings.seg.maxLinRate]; % Meters

%% Solve

opts = optimoptions('fmincon','Display','off','MaxFunEvals',maxFunEvals,'FunctionTolerance',funTol,'UseParallel',usePar);

% if settings.seg.type     == 1
    [modelParams,costVal] = fmincon(cost,[amp,phase,lin],A,b,Aeq,beq,lb,ub,[],opts);
    
% elseif settings.seg.type == 2
%     modelParams = lsqnonlin(cost,[amp,phase,lin],lb,ub,opts);
% end

modelEval   = modelParams(1)*cos( 2*pi*yearFrac + modelParams(2) ) + modelParams(3)*yearFrac;
deltaZ      = mean(vSegments-modelEval,2,'omitnan');

%% Test solution
% 
% figure
% hold on
% for i=1:nSegments
%     plot(yearFrac,vSegments(i,:)-modelParams(i+3),'-','linewidth',2)
% end
% plot(yearFrac,modelEval,'k','linewidth',3)
% grid on

disp(['A     = ' num2str(modelParams(1),2)])
disp(['phase = ' num2str(modelParams(2),2)])
disp(['lin   = ' num2str(modelParams(3),2)])
end