function [dsStmList,params] = reestimate_group(dsStmList,groupId,parcelIds,settings)

% Segmentation parameters
minCoherence    = settings.seg.minCoherence;
minSegLength    = settings.seg.minSegLength;


m               = 1;
segmentTT       = timetable();
segmentDiffTT   = timetable();
segmentId       = string();

for i = 1:settings.numTracks
    disp(['Unwrapping track ' char(dsStmList(i).datasetId) ' (' num2str(i) ' of ' num2str(settings.numTracks) ')'])
    
    % The points are the same in each track but they have different indices
    pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelIds));
    nPoints  = length(parcelIds);
    
    groupIdx = find(dsStmList(i).auxData.groupIdList == groupId);
    
    %% Divide selected time series into coherent segments
    segVec    = [];
    coherence = dsStmList(i).obsData.dcCoherence(pointIdx,:);
    for j = 1:nPoints
        [nSegments{i}(j),segIdx{i}{j},segLength{i}{j},segVec(j,:)] = segmentation(coherence(j,:),minCoherence,minSegLength);
    end
    
    segIdx{i}{j}      = segIdx{i}{j}';
    segLength{i}{j}   = segLength{i}{j}';
    
    %% Initial loop over all segments and perform initial unwrapping
    unwrappedSegments{i}    = nan(nPoints,dsStmList(i).numEpochs);
    
    % Outer loop: each selected ds point
    for j = 1:nPoints
        
        % Inner loop: all segments at that point
        for k = 1:nSegments{i}(j)
            currentSegment                      = nan(1,dsStmList(i).numEpochs);
            currentDiffSegment                  = nan(1,dsStmList(i).numEpochs);
            epochIdx                            = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
            
            % Convert to vertical phase and positive upward motion
            dphase                              = dsStmList(i).obsData.dphase(pointIdx(j),epochIdx);
            dphase                              = dphase/cosd(dsStmList(i).pntAttrib.incAngle(pointIdx(j)));

            % Unwrap segment
            [unwrappedTs,~,~]                   = rnn_unwrap_stm(dsStmList(i),dphase,epochIdx,pointIdx(j),[],settings);
            unwrappedSegments{i}(j,epochIdx)    = unwrappedTs-unwrappedTs(1);
            diffSegments(j,epochIdx)            = [nan diff(unwrappedTs)];
            
            % Append result to timetable of all segments
            % Segments are labeled "track index, point index, segment index"
            currentSegment(epochIdx)        = unwrappedTs-unwrappedTs(1);
            currentDiffSegment(epochIdx)    = diffSegments(j,epochIdx);
            segmentId(m)                    = string([num2str(i) ',' num2str(pointIdx(j)) ',' num2str(k)]);
            newTT                           = array2timetable(currentSegment','RowTimes',dsStmList(i).epochAttrib.timestamp','VariableNames',segmentId(m));
            segmentTT                       = synchronize(segmentTT,newTT);
            newDiffTT                       = array2timetable(currentDiffSegment','RowTimes',dsStmList(i).epochAttrib.timestamp','VariableNames',segmentId(m));
            segmentDiffTT                   = synchronize(segmentDiffTT,newDiffTT);
            m = m + 1;
        end
    end
end

%% Fit model to each segment
dates = segmentTT.Time;
if     settings.seg.method == 1
    [params,deltaZ] = fit_defo_model_v2(table2array(segmentTT)',dates',settings);
elseif settings.seg.method == 2
    [params,deltaZ] = fit_defo_model_piecewise(table2array(segmentTT)',dates',settings);
elseif settings.seg.method == 3
    [params,deltaZ] = fit_defo_model(dsStmList,table2array(segmentTT)',dates',settings);
elseif settings.seg.method == 4
    [params,deltaZ,cost] = fit_defo_diff_model(dsStmList,table2array(segmentTT)',table2array(segmentDiffTT)',dates',settings);
end

%% Shift each segment according to model
shiftedSegments = unwrappedSegments;
m = 1;

for i = 1:settings.numTracks
    
    % The points are the same in each track but they have different indices
    pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelIds));
    
    for j = 1:nPoints
        
        % Inner loop: all segments at that point
        for k = 1:nSegments{i}(j)
            
            % Check if indices match
            check = split(segmentId(m),",");
            trackCheck = str2double(check(1));
            pointCheck = str2double(check(2));
            segCheck   = str2double(check(3));
            
            if trackCheck ~= i || pointCheck ~= pointIdx(j) || segCheck ~= k
                error('uhoh')
            end
            
            epochIdx = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
            
            v2ph = -4*pi./dsStmList(i).techniqueAttrib.wavelength;
            shiftedSegments{i}(j,epochIdx) = unwrappedSegments{i}(j,epochIdx) - deltaZ(m)*v2ph;
            
            m=m+1;
        end

    end
    
    groupMean    = mean(shiftedSegments{i},1,'omitnan');
    groupMedian  = median(shiftedSegments{i},1,'omitnan');
        
    % Update result
    dsStmList(i).obsData.vertUnwPhase(pointIdx,:)       = shiftedSegments{i};
    dsStmList(i).auxData.groupMean(groupIdx,:)          = groupMean;
    dsStmList(i).auxData.groupMedian(groupIdx,:)        = groupMedian;
    dsStmList(i).auxData.groupModelParams(groupIdx,:)   = params;
    dsStmList(i).auxData.groupModelCost(groupIdx,:)     = cost;
    
end

disp('____________________________________________________________________________')