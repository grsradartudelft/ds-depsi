function [modelParams,deltaZ] = fit_defo_model(dsStmList,segments,dates,settings)

% Least-squares fit of a specified model type to a segmented time series.
% This is different from Depsi in that the time series is sparse.
% Each segemnt in the full TS starts at zero wrt the 1st epoch
%
% Input:
%         - segments         segmented time series (nSegments x nDates)
%         - dates            interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model.m
% Version & Date..: 1.0, 15-FEB-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

funTol      = settings.seg.functionTolerance;
maxFunEvals = settings.seg.maxFunEvals;
usePar      = settings.seg.useParallel;
nSegments   = size(segments,1);

disp(['Estimating mean displacement model for ' num2str(nSegments) ' segments (all tracks)'])

% Segment phases are already projected to vertical, minus sign for positive
% upward motion
ph2v        = -0.0556/(4*pi); 
vSegments   = segments .* ph2v;

doy         = day(dates,'dayofyear')/365.25;
yearFrac    = unwrap(doy*2*pi)/(2*pi);

%% Initial estimates
theta1      = 2.8e-4;
theta2      = 0.52;
theta3      = -1e-5;
tau         = 70;

%% Model inputs 
% KNMI data is the same for every stm/track
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dateshift(dates, 'start', 'day')));
precip      = dsStmList(1).auxData.precipitation;
evap        = dsStmList(1).auxData.evapotrans;

%% Model definition and constraints
shifts      = @(precip,evap,theta) mean(vSegments-p_model_fitting(precip,evap,epochIdx,theta)',2,'omitnan');

if settings.seg.cost     == 1
    % Cost based on model fit to shifted segments
    cost        = @(theta) sqrt(mean(mean((vSegments - p_model_fitting(precip,evap,epochIdx,theta)' - shifts(precip,evap,theta) ).^2,'omitnan'),'omitnan'));
    
elseif settings.seg.cost == 2
    % Cost based on dispersion of shifted segments
    cost        = @(theta) sqrt(mean(var( vSegments - shifts(precip,evap,theta),0,1,'omitnan'),'omitnan'));
    
end

A           = [];
b           = [];
Aeq         = [];
beq         = [];
lb          = [0,       0.3,  -5e-5,  50];
ub          = [1e-3,    1.7,  -1e-5,    80];

%% Solve

opts = optimoptions('fmincon','Display','off','MaxFunEvals',maxFunEvals,'FunctionTolerance',funTol,'UseParallel',usePar);
[modelParams,costVal] = fmincon(cost,[theta1, theta2, theta3, tau],A,b,Aeq,beq,lb,ub,[],opts);
    
modelEval   = p_model_fitting(precip,evap,epochIdx,modelParams);
deltaZ      = mean(vSegments-modelEval',2,'omitnan');

%% Test solution

% figure
% hold on
% for i=1:nSegments
%     plot(dates,vSegments(i,:)-deltaZ(i),'.','linewidth',2)
% end
% plot(dates,modelEval,'k','linewidth',3)
% grid on


end