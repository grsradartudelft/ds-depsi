function dsStmList = fit_defo_model_parcel_final(dsStmList,parcelId,settings)

% Fits a final displacement model to a given parcel. The parcel should be
% time series should be shifted and unwrapped by this point. 
%
% Input:
%         - dsStmList           Array of DS STMs per track
%         - parcelId            ID code of parcel
%         - settings            Settings struct
%
% Output: - dsStmList           Array of DS STMs per track updated with 
%                               fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_parcel_final.m
% Version & Date..: 2.0, 7-DEC-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands


%% Initial estimates from group model
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,parcelId));
groupIdx    = find(ismember(dsStmList(1).auxData.groupIdList,dsStmList(1).pntAttrib.groupId(pointIdx)));
initParams  = dsStmList(1).auxData.groupModelParams(groupIdx,:);
ph2v        = -1*dsStmList(1).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical

%% Shifted segments
% Combine tracks into one timetable
parcelTT = timetable();
for i = 1:length(dsStmList)
    pointIdx        = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelId));
    vertDefo        = dsStmList(i).obsData.finalUnwPhase(pointIdx,:)'*ph2v;
    trackOutputTT   = timetable(dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day'),vertDefo,'VariableNames',{dsStmList(i).datasetId});
    parcelTT        = synchronize(parcelTT,trackOutputTT);
end

% Array of vertical time series per track 
vertTs      = table2array(parcelTT)';
dates       = parcelTT.Time';

%% Check for all nan time series
if all(isnan(vertTs),'all')
    disp(['Discarding parcel ' num2str(parcelId)])
    for i = 1:length(dsStmList)
        pointIdx        = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelId));
        dsStmList(i).pntAttrib.groupId(pointIdx) = nan;
    end
    return
end

%% Model inputs 
% KNMI data is the same for every stm/track
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,parcelId));
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dates));

if isfield(dsStmList(1).pntAttrib,'knmi_id')
    stationIdx      = dsStmList(1).auxData.knmiStations == mode(dsStmList(1).pntAttrib.knmi_id(pointIdx));
    precip          = dsStmList(1).auxData.precipitation(stationIdx,:);
    evap            = dsStmList(1).auxData.evapotrans(stationIdx,:);
else
    precip          = dsStmList(1).auxData.precipitation;
    evap            = dsStmList(1).auxData.evapotrans;
end

%% Cost function
cost        = @(theta) sqrt( mean( mean( (vertTs - p_model_fitting_lin(precip,evap,epochIdx,theta,settings)).^2,'omitnan'),'omitnan'));

%% Solve
[modelParams,residual] = fminsearch(cost,initParams);
    
% Test solution (for debugging)
% [modelEval,~,~] = p_model_lin(precip,evap,modelParams);
% figure
% hold on
% plot(dates,vertTs,'.','markersize',6)
% plot(dates,modelEval(epochIdx),'k','linewidth',3)
% grid on

%% Save final params to stm
for i = 1:length(dsStmList)
    pointIdx  = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelId));
    dsStmList(i).pntAttrib.finalModelParams(pointIdx,:) = modelParams;
end

end