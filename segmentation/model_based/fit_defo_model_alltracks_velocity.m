function [posModelParams] = fit_defo_model_alltracks_velocity(segmentTT,settings)

% Least-squares fit of a specified model type to a segmented time series.
% This is different from Depsi in that the time series is sparse.
% Each segemnt in the full TS starts at zero wrt the 1st epoch
%
% Input:
%         - segments         segmented time series (nSegments x nDates)
%         - dates            interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_seasonal_plus_lin.m
% Version & Date..: 1.0, 19-JAN-2022
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

funTol      = settings.seg.functionTolerance;
maxFunEvals = settings.seg.maxFunEvals;
usePar      = settings.seg.useParallel;
nSegments   = size(segmentTT,2);

disp(['Estimating mean velocity model for ' num2str(nSegments) ' segments (all tracks)'])

ph2v        = 0.0556/(4*pi); % Segment phases are already projected to vertical!

velTT       = timetable;

% close all
% figure
% hold on
for m = 1:nSegments
    idx     = find(~isnan(segmentTT.(m)));
    segment = segmentTT.(m)(idx(2:end)) * ph2v;
    dates   = segmentTT.Time(idx(2:end));
    deltaT  = years(diff(dates));
    segVel  = diff(segment)./deltaT;
    
    segName = string(segmentTT.Properties.VariableNames{m});
    newTT   = array2timetable(segVel,'RowTimes',dates(2:end),'VariableNames',segName);
    velTT   = synchronize(velTT,newTT);
%     plot(dates(2:end),segVel)
end

velocity    = table2array(velTT);
doy         = day(velTT.Time,'dayofyear')/365.25;
yearFrac    = unwrap(doy*2*pi)/(2*pi);

%% Initial estimates
amp         = 0.5; % 0.15*(ghg{m}-glg{m});
lin         = -0.005;
phase       = -2*pi*1/12 + pi/2;

modelTest   = amp*cos( 2*pi*yearFrac + phase) + lin;

%% Model definition and constraints
model       = @(theta,t) theta(1)*cos( 2*pi*t + theta(2)) + theta(3);
cost        = @(theta) mean(mean((velocity - model(theta,yearFrac)).^2,'omitnan'),'omitnan');

A           = [];
b           = [];
Aeq         = [];
beq         = [];
lb          = [0,-2*pi*1/12+pi/2, -0.02]; % Meters
ub          = [1, 2*pi*3/12+pi/2,  0.02]; % Meters

%% Solve
opts                  = optimoptions('fmincon','Display','off','MaxFunEvals',maxFunEvals,'FunctionTolerance',funTol,'UseParallel',usePar);
[modelParams,costVal] = fmincon(cost,[amp,phase,lin],A,b,Aeq,beq,lb,ub,[],opts);

%% Test solution
modelFit = modelParams(1)*cos( 2*pi*yearFrac + modelParams(2) ) + modelParams(3);

% plot(velTT.Time,modelTest,'k--','linewidth',3)
% plot(velTT.Time,modelFit,'k','linewidth',3)
% grid on

disp(['A     = ' num2str(modelParams(1),2)])
disp(['phase = ' num2str(modelParams(2),2)])
disp(['lin   = ' num2str(modelParams(3),2)])

mean(median(velocity,2,'omitnan'))

% len = 3;
% meanVel = nan(size(velocity,1),1);
% for k = 3:size(velocity,1)-2
%     sel = velocity(k-2:k+2,:);
%     if sum(~isnan(sel(:))) > 30
%         meanVel(k) = mean(sel(:),'omitnan');
%     end
% end

% figure
% hold on
% plot(velTT.Time,meanVel)
% plot(velTT.Time,modelTest,'k--','linewidth',3)
% plot(velTT.Time,modelFit,'k','linewidth',3)
%% Position model

Apos        = modelParams(1)/(2*pi);
phasePos    = modelParams(2) - pi/2;
lin         = modelParams(3);
doy         = day(segmentTT.Time,'dayofyear')/365.25;
yearFrac    = unwrap(doy*2*pi)/(2*pi);
posModel    = Apos*cos(2*pi*yearFrac + phasePos) + yearFrac*modelParams(3);

shiftedTT = segmentTT;

% figure
% hold on
for m = 1:nSegments
    idx         = ~isnan(segmentTT.(m));
    segment     = segmentTT.(m)(idx)*ph2v;
    dates       = segmentTT.Time(idx);
    zShift(m)   = mean(segmentTT.(m)*ph2v - posModel,'omitnan');
    shiftedTT.(m) = shiftedTT.(m)*ph2v-zShift(m);
%     plot(dates,segment-zShift(m))
%     plot(dates,segment)
end
medDefo = median(table2array(shiftedTT),2,'omitnan');

% plot(segmentTT.Time,posModel,'k--','linewidth',3)
% plot(segmentTT.Time,medDefo,'b','linewidth',3)

posModelParams = [Apos, phasePos, lin, zShift];

end