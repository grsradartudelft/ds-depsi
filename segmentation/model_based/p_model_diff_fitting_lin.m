function hDiff = p_model_diff_fitting_lin(precip,evap,epochIdx,tBaseline,params,settings)

% epochIdx keeps track of which days there are observations, of any track

A       = params(1);
B       = params(2);
C       = params(3);
intTime = round(params(4));

% Add "constraints" to the model (for fminsearch)
if A < settings.seg.Amin || A > settings.seg.Amax
    hDiff = ones(size(epochIdx))*9999999;  
    return
end
if B < settings.seg.Bmin || B > settings.seg.Bmax
    hDiff = ones(size(epochIdx))*9999999;  
    return
end
if C < settings.seg.Cmin || C > settings.seg.Cmax
    hDiff = ones(size(epochIdx))*9999999;  
    return
end
if intTime < settings.seg.Tmin || intTime > settings.seg.Tmax
    hDiff = ones(size(epochIdx))*9999999;  
    return
end

% The first intTime number of values will be nans, that is okay because this
% keeps the output height the same length as the inputs, which is then
% sampled to match the insar dates, removing the nans in the process
reversible  = nan(size(precip));
for i = intTime+1:length(precip)
    reversible(i) = A*sum(B*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i) = reversible(i) < 0;
end

% Position time series spanning full range of meteo dates
irreversible = cumsum(C*dryFlag);
height = reversible + irreversible;

% The temporal baseline can change within a given time series from 6 to 12
% days, this is an attempt to be flexible with that
for i = 1:length(epochIdx) 
    hDiff(i) = height(epochIdx(i)) - height(epochIdx(i)-tBaseline(i));
end

% -- old solution, assumed 6-day separation ---
% % Downsample to find the daily, 6-day differences
% hDiff   = [];
% idx     = (1:length(height));
% idxDs   = [];
% for i = 1:6
%     hDiffLoop = [nan; diff(height(i:6:end))];
%     hDiff = [hDiff; hDiffLoop];
%     idxDs = [idxDs; idx(i:6:end)];
% end
% 
% [~,sortIdx] = sort(idxDs);
% hDiff       = hDiff(sortIdx);
%     
% % Downsample to match insar dates
% hDiff_ds = hDiff(epochIdx);

end

