function y = piecewise_seasonal_model(t,theta)
%Piecewise linear and seasonal model. Seasonal component is constant
%throughout, and each year has a different linear component
% t is time in fractional years
% theta is the vector of model parameters
nYears  = ceil(max(t)); 


f       = 1; % Annual frequency
a       = theta(1);
phase   = theta(2);
b       = theta(3:end);

for n = 1:nYears
    epochIdx = find(t > (n-1) & t <= n);
    if n ~=1
        c(n) = c(n-1) + b(n-1)*t(min(epochIdx)) - b(n)*t(min(epochIdx));
    else
        c(n) = 0;
    end
    y(epochIdx) = a*cos(2*pi*f*t(epochIdx) + phase) + b(n)*t(epochIdx) + c(n);
end

end

