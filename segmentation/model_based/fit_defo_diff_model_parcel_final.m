function [modelParams,residual] = fit_defo_diff_model_parcel_final(dsStmList,parcelId,settings)

% Least-squares fit of a specified model type to a segmented time series.
% Estimates the model parameters given a final parcel time series
%
% Input:
%         - vertUnwPhase        final vertical time series (1 x nDates)
%         - dates               interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_parcel.m
% Version & Date..: 1.0, 16-AUG-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands


%% Initial estimates from group model
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,parcelId));
groupIdx    = find(ismember(dsStmList(1).auxData.groupIdList,dsStmList(1).pntAttrib.groupId(pointIdx)));
initParams  = dsStmList(1).auxData.groupModelParams(groupIdx,:);
ph2v        = -1*dsStmList(1).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical

%% Shifted segments
parcelTT = timetable();

for i = 1:length(dsStmList)
    pointIdx        = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelId));
    vertDefo{i}     = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)'*ph2v;
    vertDiff{i}     = [nan; diff(vertDefo{i})];
    
    % Combine tracks into one timetable
    trackOutputTT   = timetable(dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day'),vertDiff{i},'VariableNames',{dsStmList(i).datasetId});
    parcelTT        = synchronize(parcelTT,trackOutputTT);
end

vDiffSegments   = table2array(parcelTT)';
dates           = parcelTT.Time';

%% Model inputs 
% KNMI data is the same for every stm/track
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dates));

if isfield(dsStmList(1).pntAttrib,'knmi_id')
    stationIdx  = dsStmList(1).auxData.knmiStations == mode(dsStmList(1).pntAttrib.knmi_id(pointIdx));
    precip      = dsStmList(1).auxData.precipitation(stationIdx,:);
    evap        = dsStmList(1).auxData.evapotrans(stationIdx,:);
else
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
end

tBaseline   = [];
combDates   = []; % Use this to sort the baseline array
for i = 1:length(dsStmList)
    trackDates  = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
    combDates   = [combDates, trackDates];
    tBaseline   = [tBaseline, 6, days(diff(trackDates))]; % For now just assume the 1st epoch is a 6-day interferogram
end

[~,sortIdx] = sort(combDates);
tBaseline   = tBaseline(sortIdx);

%% Cost function
cost        = @(theta) sqrt(mean(mean((vDiffSegments - p_model_diff_fitting_lin(precip,evap,epochIdx,tBaseline,theta,settings)).^2,'omitnan'),'omitnan'));

%% Solve
[modelParams,residual] = fminsearch(cost,initParams);
    
%% Test solution
[modelEval,~,~] = p_model_lin(precip,evap,modelParams);
figure
hold on
% plot(dates,vertTs,'.','markersize',6)
plot(dates,modelEval(epochIdx),'k','linewidth',3)
grid on

disp(num2str(modelParams))

end