function [newGroup,newGroupId,newGroupSize] = grow_regions(dsStmList,groupsInit,parcelsInit,settings)

% This function takes small groups will too few members on their own and sees
% if they can be combined into one group large enough to be used. 

newGroup     = [];
newGroupId   = string();
newGroupSize = [];
parcelList = vertcat(parcelsInit{:});
p = 1;

while length(parcelList) > settings.seg.minMembers
   
    pointIdx                = find(ismember(dsStmList(1).pntAttrib.parcelId,parcelList(1)));
    soilcode                = dsStmList(1).pntAttrib.soilcode(pointIdx(1));
    
    location                = dsStmList(1).pntCrd(pointIdx,:);
    distance                = 3000;
    
    ghg                     = mean(dsStmList(1).pntAttrib.ghg(pointIdx));
    glg                     = mean(dsStmList(1).pntAttrib.glg(pointIdx));
    deltaGW                 = 2.5;
    
    idxOut                  = find_stm_points(dsStmList(1),location,distance,soilcode,[],[],ghg,glg,deltaGW);
    parcelsOut              = dsStmList(1).pntAttrib.parcelId(idxOut);
    similarIdx              = ismember(parcelList,parcelsOut);
    similarParcels          = parcelList(similarIdx);
    parcelList(similarIdx)  = [];
        
    
        % For debugging
    %     parcelIdx = find(ismember(dsStmList(1).pntAttrib.parcelId,similarParcels));
    %     figure
    %     hold on
    %     plot(dsStmList(1).pntCrd(:,2),dsStmList(1).pntCrd(:,1),'k.')
    %     plot(dsStmList(1).pntCrd(parcelIdx,2),dsStmList(1).pntCrd(parcelIdx,1),'r*')
    %     plot(dsStmList(1).pntCrd(pointIdx,2),dsStmList(1).pntCrd(pointIdx,1),'g*')
    %     hold off

    if length(similarParcels) > settings.seg.minMembers
        newGroup{p} = similarParcels;
        newGroupId(p) = string(['N' num2str(p)]);
        newGroupSize(p) = length(similarParcels);
        p=p+1;
    end
end

