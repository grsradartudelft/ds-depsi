function height_ds = p_model_fitting_nonlin(precip,evap,epochIdx,params)

A       = params(1);
B       = params(2);
C       = params(3);
intTime = round(params(4));

if intTime < 2
    intTime = 2;
end

% The first intTime number of values will be nans, that is okay because this
% keeps the output height the same length as the inputs, which is then
% sampled to match the insar dates, removing the nans in the process
reversible  = nan(size(precip));
irreversible  = nan(size(precip));
for i = intTime+1:length(precip)
    reversible(i) = A*sum(B*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i) = reversible(i) < 0;
end

% Position time series spanning full range of meteo dates
irreversible(intTime+1:end) = cumsum(C*reversible(intTime+1:end).*dryFlag(intTime+1:end)');
height = reversible + irreversible;

% Downsample to match insar dates
height_ds = height(epochIdx);
end

