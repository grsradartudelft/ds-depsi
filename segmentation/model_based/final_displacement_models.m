function dsStmList = final_displacement_models(dsStmList,settings)
% Fit a displacement model for each parcel based on the combined
% reconstructed phases of each

disp('Fitting final displacement model for each parcel')

% Initialize final output field
for i = 1:length(dsStmList)
    dsStmList(i).pntAttrib.finalModelParams = nan(dsStmList(i).numPoints,4);
end

% Loop through list of parcels with an estimation
% nPoints is different per track, but the number of points with actual 
% estimations is always the same
goodIdx             = ~isnan(dsStmList(1).pntAttrib.groupId);
parcelIdList        = dsStmList(1).pntAttrib.parcelId(goodIdx);

for j = 1:length(parcelIdList)
    
    if mod(j,1000) == 0
        disp(['Done ' num2str(j) ' of ' num2str(length(parcelIdList))])
    end
    
    parcelId  = parcelIdList(j);
    
    % Do not loop over tracks because it uses all tracks simulataneously
    dsStmList = fit_defo_model_parcel_final(dsStmList,parcelId,settings);
end

disp('Done')