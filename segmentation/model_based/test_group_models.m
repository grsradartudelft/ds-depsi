function dsStmList = test_group_models(dsStmList,settings)
%% Checks groups for badly estimated models, removes outliers and re-estimates
disp('Checking for anomalous groups')
disp('____________________________________________________________________________')

% Settings
maxIter             = settings.omt.maxIter; %3
alpha               = settings.omt.alpha; %0.3

% Initial overall group statistics
groupParams         = dsStmList(1).auxData.groupModelParams;
meanParams          = median(groupParams,1);
stdParams           = abs(std(groupParams,1));
threshold           = 2*stdParams;

%% Perform OMT test and model re-estimation
% For now I just check the 3rd paramter as that one is the most error-prone
badGroupIdx         = find(any( abs(groupParams(:,3) - meanParams(3)) > threshold(3),2));
badGroupsInit       = dsStmList(1).auxData.groupIdList(badGroupIdx);
discarded           = 0;
iter                = 1;

while ~isempty(badGroupIdx)
    
    disp(['Iteration ' num2str(iter) ' (alpha = ' num2str(alpha) '): ' num2str(length(badGroupIdx)) ' anomalous groups identified.'])
    disp(['Median A:  ' num2str(meanParams(1),'%.2e') ', std A: ' num2str(stdParams(1),'%.2e')])
    disp(['Median B:  ' num2str(meanParams(2),'%.2f') ',     std B: ' num2str(stdParams(2),'%.2f')])
    disp(['Median C: '  num2str(meanParams(3),'%.2e') ', std C: ' num2str(stdParams(3),'%.2e')])
    disp('____________________________________________________________________________')
    
    for p = 1:length(badGroupIdx)
        groupId         = dsStmList(1).auxData.groupIdList(badGroupIdx(p));
        pntIdx          = find(ismember(dsStmList(1).pntAttrib.groupId,groupId));
        parcelIds       = dsStmList(1).pntAttrib.parcelId(pntIdx);
       
        % Perform parcel-wise OMT
        accept = false(length(pntIdx),1);
        for j = 1:length(pntIdx)
            [accept(j),T(j)] = omt_parcel(dsStmList,parcelIds(j),alpha,settings);
            
            % Remove rejected parcels from result
            if accept(j) == 0
                for i = 1:length(dsStmList)
                    badPoint = find(dsStmList(i).pntAttrib.parcelId == parcelIds(j));
                    dsStmList(i).pntAttrib.groupId(badPoint)        = nan;
                    dsStmList(i).obsData.vertUnwPhase(badPoint,:)   = nan(1,dsStmList(i).numEpochs);
                    dsStmList(i).obsData.finalUnwPhase(badPoint,:)  = nan(1,dsStmList(i).numEpochs);
                    dsStmList(i).obsData.confidence(badPoint,:)     = nan(1,dsStmList(i).numEpochs);
                end
            end
        end
        
        % Re-estimate model with rejected parcels removed
        nRemoved = sum(~accept);
        if nRemoved > 0
            disp(['Re-estimating group ' num2str(groupId) ' (' num2str(nRemoved) ' of ' num2str(length(pntIdx)) ' points removed)'])
            [dsStmList,newParams] = reestimate_group_spams(dsStmList,groupId,parcelIds(accept),settings);
        else
            disp(['Skipping group ' num2str(groupId) ' (' num2str(nRemoved) ' of ' num2str(length(pntIdx)) ' points removed)'])
            disp('____________________________________________________________________________')
        end
    end
   
    iter = iter + 1;
    
    % Check if reestimation satisfies original conditions
    groupParams = dsStmList(1).auxData.groupModelParams;
    badGroupIdx = find(any( abs(groupParams(:,3) - meanParams(3)) > threshold(3),2));
    
    % Recheck group statistics
    meanParams  = median(groupParams,1);
    stdParams   = abs(std(groupParams,1));
    threshold   = 2*stdParams;
    
    % If there are more anomalous groups than before, break the loop 
    % Most likely the dispersion of the groups is very low in this case
    % If not, comment this out and run again
    if sum(any( abs(groupParams(:,3) - meanParams(3)) > threshold(3),2)) > length(badGroupIdx)
        break;
    end
    
    if iter > maxIter
        break
    end
    
    if ~isempty(badGroupIdx)
        alpha = alpha + 0.05;
    end
       
    % Re-identify anomalous groups for next loop
    badGroupIdx = find(any( abs(groupParams(:,3) - meanParams(3)) > threshold(3),2));

end

badGroups   = dsStmList(1).auxData.groupIdList(badGroupIdx);
goodGroups  = badGroupsInit(~ismember(badGroupsInit,badGroups));

for p = 1:length(badGroups)
    disp(['Discarding group ' num2str(badGroups(p))])
    disp('____________________________________________________________________________')
    
    pntIdx     = find(ismember(dsStmList(1).pntAttrib.groupId,badGroups(p)));
    parcelIds  = dsStmList(1).pntAttrib.parcelId(pntIdx);
    
    for i = 1:length(dsStmList)
        for j = 1:length(parcelIds)
            badPoint = find(dsStmList(i).pntAttrib.parcelId     == parcelIds(j));
            dsStmList(i).pntAttrib.groupId(badPoint)            = nan;
            dsStmList(i).obsData.vertUnwPhase(badPoint,:)       = nan(1,dsStmList(i).numEpochs);
            dsStmList(i).obsData.finalUnwPhase(badPoint,:)      = nan(1,dsStmList(i).numEpochs);
            dsStmList(i).obsData.confidence(badPoint,:)         = nan(1,dsStmList(i).numEpochs);
        end
        groupIdx   = find(ismember(dsStmList(i).auxData.groupIdList,badGroups(p)));
        dsStmList(i).auxData.groupMean(groupIdx,:)              = [];
        dsStmList(i).auxData.groupMedian(groupIdx,:)            = [];
        dsStmList(i).auxData.groupModelParams(groupIdx,:)       = [];
        dsStmList(i).auxData.groupModelCost(groupIdx)           = [];
        dsStmList(i).auxData.groupMeanInc(groupIdx)             = [];
        dsStmList(i).auxData.groupIdList(groupIdx)              = [];
    end
    
    discarded = discarded + 1;
end

reestimated     = length(goodGroups);

disp([num2str(reestimated) ' groups re-estimated.'])
disp([num2str(discarded) ' groups discarded.'])
disp('____________________________________________________________________________')

%% Do spatial unwrapping on the good groups
disp('Redoing spatial unwrapping on reestimated groups')
dsStmList = lambdaAmbiguityEstimationByGroup(dsStmList,goodGroups,settings);


