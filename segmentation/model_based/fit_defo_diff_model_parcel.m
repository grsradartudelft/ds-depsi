function [modelParams,residual] = fit_defo_diff_model_parcel(dsStmList,parcelId,settings)

% Least-squares fit of a specified model type to a segmented time series.
% Estimates the model parameters given a final parcel time series
%
% Input:
%         - vertUnwPhase        final vertical time series (1 x nDates)
%         - dates               interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_parcel.m
% Version & Date..: 1.0, 16-AUG-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands


%% Initial estimates from group model
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,parcelId));
groupIdx    = dsStmList(1).pntAttrib.groupId(pointIdx);
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dateshift(dates, 'start', 'day')));
initParams  = dsStmList(1).auxData.groupModelParams(groupIdx,:);
   
%% Model inputs 
% KNMI data is the same for every stm/track
if isfield(dsStmList(1).pntAttrib,'knmi_id')
    stationIdx  = dsStmList(1).auxData.knmiStations == mode(dsStmList(1).pntAttrib.knmi_id(pointIdx));
    precip      = dsStmList(1).auxData.precipitation(stationIdx,:);
    evap        = dsStmList(1).auxData.evapotrans(stationIdx,:);
else
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
end

tBaseline   = [];
combDates   = []; % Use this to sort the baseline array
for i = 1:length(dsStmList)
    trackDates  = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
    combDates   = [combDates, trackDates];
    tBaseline   = [tBaseline, 6, days(diff(trackDates))]; % For now just assume the 1st epoch is a 6-day interferogram
end

[~,sortIdx] = sort(combDates);
tBaseline   = tBaseline(sortIdx);

%% Diff TS
ph2v        = -1*dsStmList.techniqueAttrib.wavelength/(4*pi);
vertDiffTs  = diff(vertTs*ph2v)';

%% Cost function
cost        = @(theta) sqrt( mean(vertDiffTs - p_model_diff_fitting_lin(precip,evap,epochIdx,theta,settings),'omitnan').^2 );

%% Solve

[modelParams,residual] = fminsearch(cost,initParams);
    
%% Test solution

[modelEval,~,~] = p_model_lin(precip,evap,modelParams);
figure
hold on
plot(dates,vertTs,'linewidth',2)
plot(dates,modelEval,'k','linewidth',3)
grid on


end