function [modelParams,deltaZ,costVal] = fit_defo_diff_model(dsStmList,selectedIds,segments,diffSegments,dates,settings)

% Least-squares fit of a specified model type to a segmented time series.
% This is different from Depsi in that the time series is sparse.
% Each segemnt in the full TS starts at zero wrt the 1st epoch
%
% Input:
%         - segments         segmented time series (nSegments x nDates)
%         - dates            interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_diff_model.m
% Version & Date..: 1.0, 20-FEB-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

nSegments   = size(diffSegments,1);
disp(['Estimating mean displacement model for ' num2str(nSegments) ' segments (all tracks)'])

% Segment phases are already projected to vertical, minus sign for positive
% upward motion
ph2v            = -0.0556/(4*pi); 
vSegments       = segments * ph2v;
vDiffSegments   = diffSegments * ph2v;

%% Parcel selections
pointIdx    = find(ismember(dsStmList(1).pntAttrib.parcelId,selectedIds));


%% Initial estimates
% Take the mode, just in case there are differences within the group
if isfield(dsStmList(1).pntAttrib,'archetype')
    archetype = string(mode(categorical(dsStmList(1).pntAttrib.archetype(pointIdx))));
elseif isfield(dsStmList(1).pntAttrib,'region')
    archetype = string(mode(categorical(dsStmList(1).pntAttrib.region(pointIdx))));
else
    archetype = 'unknown';
end

if strcmpi(archetype,'aldeboarn')
    initParams = [1.3e-4, 0.8, -1e-4,   80];
    
elseif strcmpi(archetype,'assendelft')
    initParams = [9.2e-5, 1.4, -1.4e-4, 70];
    
elseif strcmpi(archetype,'rouveen')
    initParams = [9.5e-5, 0.52,-2.3e-5, 50];
    
elseif strcmpi(archetype,'vlist')
    initParams = [9e-5,   1.4, -2.2e-5, 60];
    
elseif strcmpi(archetype,'zegveld')
    initParams = [2.8e-4, 0.52,-1e-5,   70];
    
else % Ballpark initial guess
    initParams = [1e-4, 0.52,-1e-5,     60];
end


%% Model inputs 
% KNMI data is the same for every stm/track
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dateshift(dates, 'start', 'day')));

if isfield(dsStmList(1).pntAttrib,'knmi_id')
    stationIdx  = dsStmList(1).auxData.knmiStations == mode(dsStmList(1).pntAttrib.knmi_id(pointIdx));
    precip      = dsStmList(1).auxData.precipitation(stationIdx,:);
    evap        = dsStmList(1).auxData.evapotrans(stationIdx,:);
else
    precip      = dsStmList(1).auxData.precipitation;
    evap        = dsStmList(1).auxData.evapotrans;
end

tBaseline   = [];
combDates   = []; % Use this to sort the baseline array
for i = 1:length(dsStmList)
    trackDates  = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
    combDates   = [combDates, trackDates];
    tBaseline   = [tBaseline, 6, days(diff(trackDates))]; % For now just assume the 1st epoch is a 6-day interferogram
end

[~,sortIdx] = sort(combDates);
tBaseline   = tBaseline(sortIdx);

%% Model definition and constraints
% Cost based on model fit to differential segments
cost        = @(theta) sqrt(mean(mean((vDiffSegments - p_model_diff_fitting_lin(precip,evap,epochIdx,tBaseline,theta,settings)).^2,'omitnan'),'omitnan'));

%% Solve
[modelParams,costVal] = fminsearch(cost,initParams);

%% Evaluate segment shifts
modelEval   = p_model_fitting_lin(precip,evap,epochIdx,modelParams,settings);
deltaZ      = mean(vSegments-modelEval,2,'omitnan');

disp(['A   = ' num2str(modelParams(1),2)])
disp(['B   = ' num2str(modelParams(2),2)])
disp(['C   = ' num2str(modelParams(3),2)])
disp(['tau = ' num2str(round(modelParams(4)),2)])

if isnan(costVal)
    costVal
end

%% Plot solution

% figure
% hold on
% for i=1:nSegments
%     plot(dates,vSegments(i,:)-deltaZ(i),'.','linewidth',2)
% end
% plot(dates,modelEval,'k','linewidth',3)
% grid on


end