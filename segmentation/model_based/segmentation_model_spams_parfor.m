function dsStmList = segmentation_model_spams_parfor(dsStmList,settings)
% Function for the estimation of the ambiguities and parameters of
% interest of DS points using the space-time matrix (STM) data format.
%
% DS points (parcels) are only temporarlily coherent. This function
% segments the time series based on the coherence and individually unwraps
% them. A deformation model of a group of parcels is estimated based on the 
% unwrapped segements of all available tracks. Grouping is done based on
% the contextual information in the STM dataset. 
%
% Input:
%         - dsStmList           array of space-time matrix (STM) objects (1
%                               STM per track)
%         - settings            processing settings
%
% Output: - dsStmList           updated STMs with unwrapped phases and
%                               model parameters
%         - Mean displacements (might delete later)
%
%
% ----------------------------------------------------------------------
% File............: segmentation_model_full.m
% Version & Date..: 1.0, 10-AUG-2022
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

% Loop variables
% i - iterates over track
% j - iterates over points per track
% k - iterates over segments per point
% l - iterates over epochs
% m - iterates over points (all tracks)
% p - iterates over groups

% Segmentation parameters
minCoherence = settings.seg.minCoherence;
minSegLength = settings.seg.minSegLength;

% Initialize output variables
for i = 1:settings.numTracks
    dsStmList(i).pntAttrib.groupId      = nan(size(dsStmList(i).pntAttrib.peilgebied));
    dsStmList(i).obsData.vertUnwPhase   = nan(size(dsStmList(i).obsData.dphase));
    dsStmList(i).obsData.confidence     = nan(size(dsStmList(i).obsData.dphase));
end

%% Find groups based on all relevant attributes
% These are the same for each track, so just use first stm in list
disp('Identifying parcels common to all tracks')

% Extract only the capital letter of the soilcode (ie. the main
% classification)
% for m = 1:length(dsStmList(1).pntAttrib.soilcode)
%     soilcodeLong    = char(dsStmList(1).pntAttrib.soilcode(m));
%     idx             = isstrprop(soilcodeLong,'upper');
%     soilcodeList(m) = string(soilcodeLong(idx));
% end
% soilcodeList    = soilcodeList';

cropcodeList    = dsStmList(1).pntAttrib.cropcode;
soilcodeList    = dsStmList(1).pntAttrib.soilcode;
peilgebiedList  = dsStmList(1).pntAttrib.peilgebied;

groupList       = findgroups(cropcodeList,soilcodeList,peilgebiedList);
groups          = unique(groupList);

% Inital loop over all groups to get Parcel Ids
for p = 1:length(groups)
    groupIdx        = find(ismember(groupList,groups(p)));
    selectedIds{p}  = sort(dsStmList(1).pntAttrib.parcelId(groupIdx));
    nPoints(p)      = length(groupIdx);
    groupCropCode(p) = mode(dsStmList(1).pntAttrib.cropcode(groupIdx)); 
end

% Remove groups that are not grasslands
goodIdx     = groupCropCode == 265;
groups      = groups(goodIdx);
selectedIds = selectedIds(goodIdx);
nPoints     = nPoints(goodIdx);

% Remove groups with too few members
goodIdx     = nPoints >= settings.seg.minMembers;
groups      = groups(goodIdx);
selectedIds = selectedIds(goodIdx);
nPoints     = nPoints(goodIdx);

% Loop over remaining tracks to ensure the same parcels are in each track
for p = 1:length(groups)
    for i = 1:settings.numTracks
        groupIdx    = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        parcelGroup = dsStmList(i).pntAttrib.parcelId(groupIdx);
        
        % Test "brotherhood" of selected parcels
        if settings.seg.ksTest == 1
            amp         = dsStmList(i).auxData.amplitude(groupIdx,:);
            nPointsInit = length(groupIdx);
            ksmat       = false(nPointsInit);
            
            for j = 1:nPointsInit
                for k = 1:nPointsInit
                    % Zero = they come from the same distribution (null hypothesis)
                    ksmat(j,k) = ~kstest2(sort(amp(j,:)),sort(amp(k,:)));
                end
            end
            
            % for debugging
            %     figure
            %    imagesc(ksmat)
            
            [~,idx]      = max(sum(ksmat,2));
            parcelGroup  = parcelGroup(ksmat(:,idx(1)),:);
        end
        
        parcelGroup     = sort(parcelGroup);
        selectedIds{p}  = sort(intersect(selectedIds{p},parcelGroup));
        nPoints(p)      = length(selectedIds{p});
    end
end

% Remove groups with too few members
goodIdx     = nPoints >= settings.seg.minMembers;
groups      = groups(goodIdx);
selectedIds = selectedIds(goodIdx);
nPoints     = nPoints(goodIdx);

% Split groups with too many members
splitIdx = 0;
while ~isempty(splitIdx)
    splitIdx    = find(nPoints > settings.seg.maxMembers);
    for p = 1:length(splitIdx)
        
        % First, determine number of new groups to make
        nPointsInit     = nPoints(splitIdx(p));
        nSmallGroups    = ceil(nPointsInit/settings.seg.maxMembers);
        
        % Create new groups by clustering by location
        pointIdx        = find(ismember(dsStmList(1).pntAttrib.parcelId,selectedIds{splitIdx(p)}));
        coords          = dsStmList(1).pntCrd(pointIdx,1:2);
        newIdx          = kmeans(coords,nSmallGroups);
        
        % Update group lists
        for q = 1:nSmallGroups
            nNewPoints  = sum(newIdx==q);
            nPoints     = [nPoints nNewPoints];
            
            newIds      = dsStmList(1).pntAttrib.parcelId(pointIdx(newIdx==q));
            selectedIds = [selectedIds newIds];
            
            newGroup    = max(groups) + q;
            groups      = [groups; newGroup];
        end
        
        % Remove old group
        groups(splitIdx(p))         = [];
        selectedIds(splitIdx(p))    = [];
        nPoints(splitIdx(p))        = [];
        
    end
end

parfor p = 1:length(groups)
    
    % Initialize internal variables for parfoor loop
    groupMean           = cell(4,1);
    groupMedian         = cell(4,1);
    nSegments           = cell(4,1);
    segIdx              = cell(4,nPoints(p));
    segLength           = cell(4,nPoints(p));
    segmentTT           = timetable();
    segmentDiffTT       = timetable();
    combOutputTT        = timetable();
    unwrappedSegments   = cell(4,1);
    
    %% Initial processing of each track
    for i = 1:settings.numTracks
        
        
        % The points are the same in each track but they have different indices
        pointIdx  = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));

        coherence = dsStmList(i).obsData.dcCoherence(pointIdx,:);
        nPixels   = dsStmList(i).pntAttrib.nPixels(pointIdx);
        
        %% Divide selected time series into coherent segments
        segVec    = [];
        for j = 1:nPoints(p)
            [nSegments{i}(j),segIdx{i}{j},segLength{i}{j},segVec(j,:)] = segmentation(coherence(j,:),minCoherence,minSegLength);
        end
        
        segIdx{i}{j}      = segIdx{i}{j}';
        segLength{i}{j}   = segLength{i}{j}';
        
        %% Initial loop over all segments and perform initial unwrapping
        unwrappedSegments{i}    = nan(nPoints(p),dsStmList(i).numEpochs);
        
        % Outer loop: each selected ds point
        for j = 1:nPoints(p)
            parcelId = dsStmList(i).pntAttrib.parcelId(pointIdx(j));
            [unwrappedSegments{i}(j,:),~] = spams_unwrap_stm(dsStmList(i),parcelId,[],settings);
            
            % Inner loop: all segments at that point
            for k = 1:nSegments{i}(j)
                
                % Append result to timetable of all segments
                % Segments are labeled "track index, point index, segment index"
                epochIdx                        = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
                currentSegment                  = nan(1,dsStmList(i).numEpochs);
                currentDiffSegment              = nan(1,dsStmList(i).numEpochs);
                
                currentSegment(epochIdx)        = unwrappedSegments{i}(j,epochIdx);
                currentDiffSegment(epochIdx)    = [nan diff(unwrappedSegments{i}(j,epochIdx))];
                segmentId                       = string([num2str(i) ',' num2str(pointIdx(j)) ',' num2str(k)]);
                newTT                           = array2timetable(currentSegment','RowTimes',dsStmList(i).epochAttrib.timestamp','VariableNames',segmentId);
                segmentTT                       = synchronize(segmentTT,newTT);
                newDiffTT                       = array2timetable(currentDiffSegment','RowTimes',dsStmList(i).epochAttrib.timestamp','VariableNames',segmentId);
                segmentDiffTT                   = synchronize(segmentDiffTT,newDiffTT);
            end
        end
    end
    
    %% Fit model to each segment   
    dates                   = segmentTT.Time;
    [params,deltaZ,cost]    = fit_defo_diff_model(dsStmList,selectedIds{p},table2array(segmentTT)',table2array(segmentDiffTT)',dates',settings);

    %% Shift each segment according to model
    shiftedSegments = unwrappedSegments;
    m = 1;
    
    for i = 1:settings.numTracks
        
        % The points are the same in each track but they have different indices
        pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        
        for j = 1:nPoints(p)
            
            % Inner loop: all segments at that point
            for k = 1:nSegments{i}(j)
                epochIdx                        = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
                v2ph                            = -4*pi./dsStmList(i).techniqueAttrib.wavelength;
                shiftedSegments{i}(j,epochIdx)  = unwrappedSegments{i}(j,epochIdx) - deltaZ(m)*v2ph;                
                m=m+1;
            end           
        end
        
        groupMean{i}    = mean(shiftedSegments{i},1,'omitnan');
        groupMedian{i}  = median(shiftedSegments{i},1,'omitnan');
        
        trackOutputTT   = timetable(dsStmList(i).epochAttrib.timestamp',groupMedian{i}');
        combOutputTT    = synchronize(combOutputTT,trackOutputTT);

        %% Convert phase to vertical deformation
        incAngle                = dsStmList(i).pntAttrib.incAngle(pointIdx);
        meanInc{p}{i}           = mean(incAngle);

        %% Save output to STM
        vertUnwPhase{p}{i}      = shiftedSegments{i};
        groupMeanOut{p}{i}      = groupMean{i};
        groupMedianOut{p}{i}    = groupMedian{i};
        groupModelParams(p,:)   = params;
        groupModelCost(p,:)     = cost;
        meanInc{p}{i}           = mean(incAngle);
        groupIdList(p)          = groups(p);
        
    end

    disp(['Done group ' num2str(p) ' of ' num2str(length(groups)) ' (' datestr(now, 'yyyy-mm-dd HH:MM') ')'])
    
end

%% Save output to STM
for i = 1:settings.numTracks
    % The points are the same in each track but they have different indices
    pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
    
    for j = 1:nPoints(p)
        dsStmList(i).obsData.vertUnwPhase(pointIdx,:)  = vertUnwPhase{p}{i};
        dsStmList(i).pntAttrib.groupId(pointIdx)       = groups(p);
        dsStmList(i).auxData.groupMean(p,:)            = groupMeanOut{p}{i};
        dsStmList(i).auxData.groupMedian(p,:)          = groupMedianOut{p}{i};
        dsStmList(i).auxData.groupModelParams(p,:)     = groupModelParams(p,:);
        dsStmList(i).auxData.groupModelCost(p,:)       = groupModelCost(p,:);
        dsStmList(i).auxData.groupMeanInc(p,:)         = meanInc{p}{i};
        dsStmList(i).auxData.groupIdList(p)            = groupIdList(p);
%         dsStmList(i).auxData.dates                     = datesOut{i};
    end
end

end

