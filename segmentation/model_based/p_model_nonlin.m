function [height, reversible, irreversible] = p_model_nonlin(precip,evap,params)

A       = params(1);
B       = params(2);
C       = params(3);
intTime = round(params(4));

reversible  = nan(size(precip));
irreversible  = nan(size(precip));
for i = intTime+1:length(precip)
    reversible(i) = A*sum(B*precip(i-intTime:i) - evap(i-intTime:i));
    dryFlag(i) = reversible(i) < 0;
end

irreversible(intTime+1:end) = cumsum(C*reversible(intTime+1:end).*dryFlag(intTime+1:end)');

height = reversible + irreversible;

end

