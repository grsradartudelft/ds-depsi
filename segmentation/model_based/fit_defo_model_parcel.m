function [modelParams,residual] = fit_defo_model_parcel(dsStmList,groupId,vertTs,dates,settings)

% Least-squares fit of a specified model type to a segmented time series.
% Estimates the model parameters given a final parcel time series
%
% Input:
%         - vertUnwPhase        final vertical time series (1 x nDates)
%         - dates               interferogram dates
%
% Output: - modelParams         struct of fitted model parameters
%
%
% ----------------------------------------------------------------------
% File............: fit_defo_model_parcel.m
% Version & Date..: 1.0, 16-AUG-2023
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands


%% Initial estimates from group model
groupIdx    = find(dsStmList(1).auxData.groupIdList == groupId);
initParams  = dsStmList(1).auxData.groupModelParams(groupIdx,:);

%% Model inputs 
% KNMI data is the same for every stm/track
epochIdx    = find(ismember(dsStmList(1).auxData.knmiDates,dateshift(dates, 'start', 'day')));
precip      = dsStmList(1).auxData.precipitation;
evap        = dsStmList(1).auxData.evapotrans;

%% Cost function
if size(vertTs,2) == 1
    cost        = @(theta) sqrt( mean(vertTs - p_model_fitting_lin(precip,evap,epochIdx,theta,settings),'omitnan').^2 );
else
    cost        = @(theta) sqrt( mean(mean(vertTs - p_model_fitting_lin(precip,evap,epochIdx,theta,settings),'omitnan'),'omitnan').^2 );
end
%% Solve

[modelParams,residual] = fminsearch(cost,initParams);
    
%% Test solution

% figure
% hold on
% for i=1:nSegments
%     plot(dates,vSegments(i,:)-deltaZ(i),'.','linewidth',2)
% end
% plot(dates,modelEval,'k','linewidth',3)
% grid on


end