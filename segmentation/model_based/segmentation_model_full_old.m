function [dsStmList,vertGroupMean,vertGroupMedian,meanInc] = segmentation_model_full_old(dsStmList,settings)
% Function for the estimation of the ambiguities and parameters of
% interest of DS points using the space-time matrix (STM) data format.
%
% DS points (parcels) are only temporarily coherent. This function
% segments the time series based on the coherence and individually unwraps
% them. A deformation model of a group of parcels is estimated based on the 
% unwrapped segements of all available tracks. Grouping is done based on
% the contextual information in the STM dataset. 
%
% Input:
%         - dsStmList           array of space-time matrix (STM) objects (1
%                               STM per track)
%         - settings            processing settings
%
% Output: - dsStmList           updated STMs with unwrapped phases and
%                               model parameters
%         - Mean displacements (might delete later)
%
%
% ----------------------------------------------------------------------
% File............: segmentation_model_full.m
% Version & Date..: 1.0, 10-AUG-2022
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------
%
% This software is developed by Delft University of Technology and is
% intended for scientific use only. Applications for commercial use are
% prohibited.
%
% Copyright (c) 2004-2022 Delft University of Technology, The Netherlands

% Loop variables
% i - iterates over track
% j - iterates over points per track
% k - iterates over segments per point
% l - iterates over epochs
% m - iterates over points (all tracks)
% p - iterates over groups

% Segmentation parameters
minCoherence = settings.seg.minCoherence;
minSegLength = settings.seg.minSegLength;

% Initialize output variables
for i = 1:settings.numTracks
    dsStmList(i).pntAttrib.groupId      = nan(size(dsStmList(i).pntAttrib.peilgebied));
    dsStmList(i).obsData.vertUnwPhase   = nan(size(dsStmList(i).obsData.dphase));
    dsStmList(i).obsData.confidence     = nan(size(dsStmList(i).obsData.dphase));
    dsStmList(i).pntAttrib.initParams   = nan(size(dsStmList(i).pntAttrib.peilgebied,1),4);
    dsStmList(i).pntAttrib.initResidual = nan(size(dsStmList(i).pntAttrib.peilgebied));
end

%% Find groups based on all relevant attributes
% These are the same for each track, so just use first stm in list
disp('Identifying parcels common to all tracks')

% Extract only the capital letter of the soilcode (ie. the main
% classification)
for m = 1:length(dsStmList(1).pntAttrib.soilcode)
    soilcodeLong    = char(dsStmList(1).pntAttrib.soilcode(m));
    idx             = isstrprop(soilcodeLong,'upper');
    soilcodeList(m) = string(soilcodeLong(idx));
end
soilcodeList    = soilcodeList';
cropcodeList    = dsStmList(1).pntAttrib.cropcode;
% soilcodeList    = dsStmList(1).pntAttrib.soilcode;
peilgebiedList  = dsStmList(1).pntAttrib.peilgebied;
groupList       = findgroups(cropcodeList,soilcodeList,peilgebiedList);
groups          = unique(groupList);

% Inital loop over all groups to get Parcel Ids
for p = 1:length(groups)
    groupIdx        = find(ismember(groupList,groups(p)));
    selectedIds{p}  = sort(dsStmList(1).pntAttrib.parcelId(groupIdx));
    nPoints(p)      = length(groupIdx);
end

% Remove groups with too few members
goodIdx     = nPoints >= settings.seg.minMembers;
groups      = groups(goodIdx);
selectedIds = selectedIds(goodIdx);
nPoints     = nPoints(goodIdx);

% Loop over remaining tracks to ensure the same parcels are in each track
for p = 1:length(groups)
    for i = 1:settings.numTracks
        groupIdx    = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        parcelGroup = dsStmList(i).pntAttrib.parcelId(groupIdx);
        
        % Test "brotherhood" of selected parcels
        if settings.seg.ksTest == 1
            amp         = dsStmList(i).auxData.amplitude(groupIdx,:);
            nPointsInit = length(groupIdx);
            ksmat       = false(nPointsInit);
            
            for j = 1:nPointsInit
                for k = 1:nPointsInit
                    % Zero = they come from the same distribution (null hypothesis)
                    ksmat(j,k) = ~kstest2(sort(amp(j,:)),sort(amp(k,:)));
                end
            end
            
            % for debugging
            %     figure
            %    imagesc(ksmat)
            
            [~,idx]      = max(sum(ksmat,2));
            parcelGroup  = parcelGroup(ksmat(:,idx(1)),:);
        end
        
        parcelGroup     = sort(parcelGroup);
        selectedIds{p}  = sort(intersect(selectedIds{p},parcelGroup));
        nPoints(p)      = length(selectedIds{p});
    end
end

% Remove groups with too few members
goodIdx     = nPoints >= settings.seg.minMembers;
groups      = groups(goodIdx);
selectedIds = selectedIds(goodIdx);
nPoints     = nPoints(goodIdx);

% Split groups with too many members
splitIdx = 0;
while ~isempty(splitIdx)
    splitIdx    = find(nPoints > settings.seg.maxMembers);
    for p = 1:length(splitIdx)
        
        % First, determine number of new groups to make
        nPointsInit     = nPoints(splitIdx(p));
        nSmallGroups    = ceil(nPointsInit/settings.seg.maxMembers);
        
        % Create new groups by clustering by location
        pointIdx        = find(ismember(dsStmList(1).pntAttrib.parcelId,selectedIds{splitIdx(p)}));
        coords          = dsStmList(1).pntCrd(pointIdx,1:2);
        newIdx          = kmeans(coords,nSmallGroups);
        
        % for debugging
        %     figure
        %     hold on
        %     plot(coords(newIdx==1,1),coords(newIdx==1,2),'.')
        %     plot(coords(newIdx==2,1),coords(newIdx==2,2),'.')
        
        % Update group lists
        for q = 1:nSmallGroups
            nNewPoints  = sum(newIdx==q);
            nPoints     = [nPoints nNewPoints];
            
            newIds      = dsStmList(1).pntAttrib.parcelId(pointIdx(newIdx==q));
            selectedIds = [selectedIds newIds];
            
            newGroup    = max(groups) + q;
            groups      = [groups; newGroup];
        end
        
        % Remove old group
        groups(splitIdx(p))         = [];
        selectedIds(splitIdx(p))    = [];
        nPoints(splitIdx(p))        = [];
        
    end
end

for p = 1:length(groups)
    %% Initial processing of each track
    disp(['Processing group ' num2str(p) ' of ' num2str(length(groups)) ' (' datestr(now, 'yyyy-mm-dd HH:MM') ')'])
    
    for i = 1:settings.numTracks
        disp(['Unwrapping track ' char(dsStmList(i).datasetId) ' (' num2str(i) ' of ' num2str(settings.numTracks) ')'])
        
        % The points are the same in each track but they have different indices
        pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        
        % for debugging
%         if settings.plotPointSel == 1
%             figure
%             hold on
%             plot(dsStmList(i).pntCrd(:,2), dsStmList(i).pntCrd(:,1),'k.')
%             plot(dsStmList(i).pntCrd(pointIdx,2),dsStmList(i).pntCrd(pointIdx,1),'r*')
%             hold off
%         end
        
        coherence = dsStmList(i).obsData.dcCoherence(pointIdx,:);
        nPixels   = dsStmList(i).pntAttrib.nPixels(pointIdx);
        
        %% Divide selected time series into coherent segments
        segVec    = [];
        for j = 1:nPoints(p)
            [nSegments{i}(j),segIdx{i}{j},segLength{i}{j},segVec(j,:)] = segmentation(coherence(j,:),minCoherence,minSegLength);
        end
        
        segIdx{i}{j}      = segIdx{i}{j}';
        segLength{i}{j}   = segLength{i}{j}';
        tempCoverage      = sum(~isnan(segVec),2)/size(segVec,2);
        [~,coverageRank]  = sort(tempCoverage,'descend');
        rankedPoints      = pointIdx(coverageRank);
        segVecRanked      = segVec(coverageRank,:);
        
        % Determine nPixels used
        for l = 1:dsStmList(i).numEpochs
            nCohPoints(l)       = sum(~isnan(segVec(:,l)));
            idx                 = pointIdx(~isnan(segVec(:,l)));
            pixelsPerEpoch(l)   = sum(dsStmList(i).pntAttrib.nPixels(idx));
        end
        
        if settings.plotSegmentDiagram == 1
            plot_segment_diagram_pointwise(dsStmList(i),segVecRanked,rankedPoints)
        end
        
        
        %% Initial loop over all segments and perform initial unwrapping
        unwrappedSegments{i}    = nan(nPoints(p),dsStmList(i).numEpochs);
        confidence{i}           = nan(nPoints(p),dsStmList(i).numEpochs);
        
        % Outer loop: each selected ds point
        for j = 1:nPoints(p)
            
            % Inner loop: all segments at that point
            for k = 1:nSegments{i}(j)
                epochIdx                            = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
                
                % Convert to vertical phase and positive upward motion
                dphase                              = dsStmList(i).obsData.dphase(pointIdx(j),epochIdx);
                dphase                              = dphase/cosd(dsStmList(i).pntAttrib.incAngle(pointIdx(j)));
                
                % Unwrap segment
                [unwrappedTs,~,confidenceOut]       = rnn_unwrap_stm(dsStmList(i),dphase,epochIdx,pointIdx(j),[],settings);
                unwrappedSegments{i}(j,epochIdx)    = unwrappedTs-unwrappedTs(1);
                confidence{i}(j,epochIdx)           = confidenceOut;
            end
            
            % Fit an initial model to each parcel
            [initParams,initResidual] = fit_defo_diff_model_parcel(dsStmList(i),unwrappedSegments{i}(j,:),settings);
            dsStmList(i).pntAttrib.initParams(pointIdx(j),:)       = initParams;
            dsStmList(i).pntAttrib.initResidual(pointIdx(j))       = initResidual;
            
        end
    end
    
    %% Find mean group model
    params = [];
    for i=1:length(dsStmList)
        pointIdx        = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        params          = [params; dsStmList(i).pntAttrib.initParams(pointIdx,:)];
    end
    
    groupParams         = mean(params,1,'omitnan');
    groupParams(4)      = round(groupParams(4));
    
    % Filter bad results
    params(any(abs(params-groupParams)./abs(groupParams) > 0.5,2),:) = [];
    
    groupParams         = mean(params,1,'omitnan');
    groupParams(4)      = round(groupParams(4));
    
%     [count1,edges1]     = histcounts(groupParams(1));
%     [count2,edges2]     = histcounts(groupParams(2));
%     [count3,edges3]     = histcounts(groupParams(3));
%     [count4,edges4]     = histcounts(groupParams(4));
%     
%     mode1               = mode(count1);
%     mode2               = mode(count2);
%     mode3               = mode(count3);
%     mode4               = mode(count4);
%     
%     groupParams(1)      = mean(edges1(mode1));
%     groupParams(2)      = mean(edges1(mode1));
%     groupParams(3)      = mean(edges1(mode1));
%     groupParams(4)      = mean(edges1(mode1));

    % Model evaluation
    precip              = dsStmList(i).auxData.precipitation;
    evap                = dsStmList(i).auxData.evapotrans;
    [groupModel,~,~]    = p_model_lin(precip,evap,groupParams);
    
    disp('Group model parameters:')
    disp(['A   = ' num2str(groupParams(1))])
    disp(['B   = ' num2str(groupParams(2))])
    disp(['C   = ' num2str(groupParams(3))])
    disp(['tau = ' num2str(groupParams(4))])
%     disp('____________________________________')
%     disp(['x_P = ' num2str(params(1)*params(2))])
%     disp(['x_E = ' num2str(params(1))])
%     disp(['x_I = ' num2str(params(3))])
%     disp(['tau = ' num2str(params(4))])
    
%     figure; histogram(paramsGroup(:,1)); hold on; xline(params(1),'r'); 
%     figure; histogram(paramsGroup(:,2)); hold on; xline(params(2),'r');
%     figure; histogram(paramsGroup(:,3)); hold on; xline(params(3),'r'); 

    %% Shift each segment according to model
    shiftedSegments = unwrappedSegments;
    for i = 1:settings.numTracks
        
        % The points are the same in each track but they have different indices
        modelEpochIdx = ismember(dsStmList(i).auxData.knmiDates,dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day'));
        groupModelTrack = groupModel(modelEpochIdx);
        
        for j = 1:nPoints(p)
            
            % Inner loop: all segments at that point
            for k = 1:nSegments{i}(j)
                epochIdx                        = segIdx{i}{j}(k):segIdx{i}{j}(k)+segLength{i}{j}(k)-1;
                v2ph                            = -4*pi./dsStmList(i).techniqueAttrib.wavelength;
                deltaZ                          = mean(unwrappedSegments{i}(j,epochIdx) - groupModelTrack(epochIdx)'*v2ph);
                shiftedSegments{i}(j,epochIdx)  = unwrappedSegments{i}(j,epochIdx) - deltaZ;
            end
            
%             figure
%             hold on
%             plot(dsStmList(i).epochAttrib.timestamp,unwrappedSegments{i}(j,:))
%             plot(dsStmList(i).epochAttrib.timestamp,shiftedSegments{i}(j,:))
%             plot(dsStmList(1).auxData.knmiDates,groupModel*v2ph)
            
        end
        
        groupMean{i}    = mean(shiftedSegments{i},1,'omitnan');
        groupMedian{i}  = median(shiftedSegments{i},1,'omitnan');
    end

    for i = 1:settings.numTracks    
    pointIdx = find(ismember(dsStmList(i).pntAttrib.parcelId,selectedIds{p}));
        
        %% Convert phase to vertical deformation
        incAngle               = dsStmList(i).pntAttrib.incAngle(pointIdx);
        ph2v                   = -dsStmList(i).techniqueAttrib.wavelength/(4*pi);
        meanInc{p}{i}          = mean(incAngle);
        vertGroupMean{p}{i}    = groupMean{i}   * ph2v;
        vertGroupMedian{p}{i}  = groupMedian{i} * ph2v;
        
        %% Save output to STM
        dsStmList(i).obsData.vertUnwPhase(pointIdx,:)  = shiftedSegments{i};
        dsStmList(i).obsData.confidence(pointIdx,:)    = confidence{i};
        dsStmList(i).pntAttrib.groupId(pointIdx)       = groups(p);
        dsStmList(i).auxData.groupMean(p,:)            = groupMean{i};
        dsStmList(i).auxData.groupMedian(p,:)          = groupMedian{i};
        dsStmList(i).auxData.groupModelParams(p,:)     = groupParams;
        dsStmList(i).auxData.groupMeanInc(p,:)         = meanInc{p}{i};
        dsStmList(i).auxData.groupIdList(p)            = groups(p);
    end
           
    clearvars segIdx segLength
    
    disp(['Done group ' num2str(p) ' of ' num2str(length(groups)) ' (' num2str(p/length(groups)*100,2) '%)'])
    disp('____________________________________________________________________________')
    
end

end

