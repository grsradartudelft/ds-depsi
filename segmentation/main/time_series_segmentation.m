function [dsStmList,psStmList] = time_series_segmentation(settings)

% Reads PS and DS data and finds a common reference point between all tracks.
% Creates arcs between all DS points and the common PS reference point.

disp('____________________________________________________________________________')

if ~isempty(settings.indices.IGRSLoc)
    IGRSLoc = settings.indices.IGRSLoc;
end

%% Combine Depsi and STM data
for i = 1:settings.numTracks
    dsStmName                                                       = [settings.stmDir char(settings.dsStmFilename(i))];
    psStmName                                                       = [settings.stmDir char(settings.psStmFilename(i))];
    [dsStmList(i),dsStmNameList(i),psStmList(i),psStmNameList(i)]   = depsi_to_stm(dsStmName,psStmName,settings);
end
disp('____________________________________________________________________________')


%% Change reference point
if strcmp(settings.indices.changeRef, 'igrs')
    
    for i = 1:length(dsStmList)
        [~,refIdx(i)] = min(hypot( psStmList(i).pntCrd(:,1) - IGRSLoc(1),  psStmList(i).pntCrd(:,2) - IGRSLoc(2)  ));
        
        dsStmList(i).obsData.dphase = dsStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
        psStmList(i).obsData.dphase = psStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
        
        dsStmList(i).obsData.dphase = mod(dsStmList(i).obsData.dphase+pi,2*pi)-pi; 
        psStmList(i).obsData.dphase = mod(psStmList(i).obsData.dphase+pi,2*pi)-pi; 
        
    end
    settings.indices.psIdx = refIdx;
    
elseif strcmp(settings.indices.changeRef, 'best')
    
    % Find PSs near the location of interest
    for i = 1:length(dsStmList)
        pscIdx{i}           = find(strcmp(psStmList(i).pntAttrib.type,"psc"));
        locations{i}        = psStmList(i).pntCrd(pscIdx{i},:);
    end
    
    % Now find a PS point that is common to all tracks
    commonLocs = locations{1};
    for i = 1:length(dsStmList)
        mask                = ismembertol(commonLocs,locations{i},3e-4,'ByRows',1);
        commonLocs          = commonLocs(mask,:);
        if isempty(commonLocs)
            error('Cannot find common PS among all tracks. Adjust tolerance?')
        end
    end
    numPoints = size(commonLocs,1);
    
   
    %figure; hold on;
    for i = 1:length(dsStmList)
        for j = 1:numPoints
            dist            = hypot(locations{i}(:,1)-commonLocs(j,1),locations{i}(:,2)-commonLocs(j,2));
            [err(i,j),idx]  = min(dist);
            pointIdx(i,j)   = pscIdx{i}(idx);
        end
        %plot(psStmList(i).pntCrd(idx(i,:),2),psStmList(i).pntCrd(idx(i,:),1),'.')
    end
    
    meanErr = mean(err,1);
    
    % Delete points that are too far apart
    pointIdx(:,meanErr > settings.indices.tolerance)  = [];
    err(:,meanErr > settings.indices.tolerance)       = [];
    
    numPoints = size(pointIdx,2);
    
    if numPoints == 0
        error('Cannot find common PS among all tracks. Adjust tolerance?')
    end
    
    % Plot of final selections (for debugging)
%     figure; hold on;
%     for i = 1:length(dsStmList)
%         plot(psStmList(i).pntCrd(pointIdx(i,:),2),psStmList(i).pntCrd(pointIdx(i,:),1),'.')
%     end
    
    % Now select the best point based on amplitude dispersion
    for j = 1:numPoints
        for i = 1:length(dsStmList)
            ampDisp(i)  = psStmList(i).pntAttrib.ampDisp(pointIdx(i,j));
        end
        ampDispSum(j)   = sum(ampDisp);
    end
    
    [~,bestPoint]   = min(ampDispSum);
    refIdx          = pointIdx(:,bestPoint);

    % Finally, create arcs with selected point as reference
    for i = 1:length(dsStmList)
        refLoc(i,:)                 = psStmList(i).pntCrd(refIdx(i),:);
        dsStmList(i).obsData.dphase = dsStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
        psStmList(i).obsData.dphase = psStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
    end
    settings.indices.psIdx  = refIdx;
    settings.indices.refLoc = refLoc;
    
else
    % I dont think this works because the ref is not explicitly included in
    % depsi points
    for i = 1:length(psStmList)
        refIdx(i) = psStmList(i).techniqueAttrib.depsiRefIdx;
        dsStmList(i).obsData.dphase = dsStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
        psStmList(i).obsData.dphase = psStmList(i).obsData.apsPhase - psStmList(i).obsData.apsPhase(refIdx(i),:);
    end
    settings.indices.psIdx = refIdx;
end

% Save location of the reference point
for i = 1:length(dsStmList)
    settings.indices.refLoc(i,:) = psStmList(i).pntCrd(refIdx(i),:);
    disp(['Track ' psStmList(i).datasetId ' ref PS Lat: ' num2str(psStmList(i).pntCrd(refIdx(i),1),'%.4f') ' Lon ' num2str(psStmList(i).pntCrd(refIdx(i),2),'%.4f')])
end

%% RPN Correction
if settings.indices.rpnCorrection == 1
    for i = 1:settings.numTracks
        rpnEst{i} = correct_rpn_stm(psStmList(i));
        
        % Save a version without the RPN removal
        psStmList(i).obsData.dphaseNoRpnRem = psStmList(i).obsData.dphase;
        dsStmList(i).obsData.dphaseNoRpnRem = dsStmList(i).obsData.dphase;
        
        psStmList(i).obsData.dphase = psStmList(i).obsData.dphase - rpnEst{i};
        dsStmList(i).obsData.dphase = dsStmList(i).obsData.dphase - rpnEst{i};
        
        
    end
    
    if settings.plotRpn == 1
        figure
        hold on
        ii = 1;
        for i = 1:settings.numTracks
            plot(dsStmList(i).epochAttrib.timestamp,rpnEst{i})
            legendStr{ii} = string(dsStmList(i).datasetId);
            ii=ii+1;
        end
        grid on
        ylabel('Estimated Reference Point Noise (rad)')
        legend(legendStr,'Interpreter','none')
    end
end


%% Reference point information
% Display reference point location
if settings.plotRefLoc == 1
    for i = 1:settings.numTracks
        display_master_image(dsStmList(i));
        hold on
        plot(psStmList(i).pntAttrib.r,psStmList(i).pntAttrib.az,'b*')
        plot(psStmList(i).pntAttrib.r(refIdx(i)),psStmList(i).pntAttrib.az(refIdx(i)),'r*')
        disp(['Ref PS Lat: ' num2str(psStmList(i).pntCrd(refIdx(i),1),'%.4f') ' Lon ' num2str(psStmList(i).pntCrd(refIdx(i),2),'%.4f')])
    end
end

% Display reference point phase
if settings.plotRefPhase == 1
    figure
    hold on
    plot(dsStmList(1).epochAttrib.timestamp, zeros(size(dsStmList(1).epochAttrib.timestamp)),'HandleVisibility','off')
    ii=1;
    for i = 1:settings.numTracks
        plot(dsStmList(i).epochAttrib.timestamp, psStmList(i).obsData.apsPhase(refIdx(i),:))
        legendStr{ii} = string([dsStmList(i).datasetId ' Type: ' char(psStmList(i).pntAttrib.type(refIdx(i)))]);
        ii=ii+1;
    end
    legend(legendStr,'Interpreter','none')
    title('Reference point phases')
    ylim([-pi pi])
    grid on
end

%% PS location plot
if settings.plotNearbyPs == 1
    figure
    hold on
    for i = 1:length(psStmList)
        c = ['b','r','m','k'];
        plot(psStmList(i).pntCrd(:,2),psStmList(i).pntCrd(:,1),'*','color',c(i))
        grid on
    end
    legend(psStmList.datasetId,'Interpreter', 'none')
end
disp('____________________________________________________________________________')

%% Segmented phase estimation
% Analyse surrounding parcels to bridge incoherent epochs and unwrap phase
% All necessary data is returned in dsStmList.
dsStmList = segmentation_model_spams(dsStmList,settings);

%% Save intermediate product
for i = 1:settings.numTracks
    stmwrite(dsStmList(i),[settings.segDir 'segmentation_' char(settings.dsStmFilename(i))]);
    stmwrite(psStmList(i),[settings.segDir 'segmentation_' char(settings.psStmFilename(i))]);
end

end
