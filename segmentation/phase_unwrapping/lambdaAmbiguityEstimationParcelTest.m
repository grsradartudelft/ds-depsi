function unwrapped = lambdaAmbiguityEstimationParcelTest(dsStm,parcelId,segments,params,settings)

% disp('Adapting ambiguities using parcel model')


pointIdx        = find(ismember(dsStm.pntAttrib.parcelId,parcelId));
dates           = dateshift(dsStm.epochAttrib.timestamp, 'start', 'day');
modelDateIdx    = ismember(dsStm.auxData.knmiDates,dates);
v2ph            = -4*pi/dsStm.techniqueAttrib.wavelength;

if settings.seg.method == 1 || settings.seg.method == 2
    doy         = day(dsStm.epochAttrib.timestamp,'dayofyear')/365.25;
    t           = unwrap(doy*2*pi)/(2*pi);
    modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
else
    precip      = dsStm(1).auxData.precipitation;
    evap        = dsStm(1).auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params);
    
end

% Refine ambiguities on per-segment basis

coherence                       = dsStm.obsData.dcCoherence(pointIdx,:);
[nSegments,segIdx,segLength,~]  = segmentation(coherence,settings.seg.minCoherence,settings.seg.minSegLength);
unwrapped                       = nan(size(coherence))';
for k = 1:nSegments
    segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
    
    % Initial phases
    y       = segments(segEpochIdx)';
    y       = y - y(1);
    
    % Parcel model
    model   = modelFit(modelDateIdx) * v2ph;
    bHat    = model(segEpochIdx);
    bHat    = bHat - bHat(1);
    
    %     figure
    %     hold on
    %     plot(y)
    %     plot(bHat)
    
    % Weight observations by CRB of daisy-chain coherence
    gamma   = double(dsStm.obsData.dcCoherence(pointIdx,segEpochIdx));
    OSR     = dsStm.techniqueAttrib.prf/dsStm.techniqueAttrib.az_bw * dsStm.techniqueAttrib.r_fs/dsStm.techniqueAttrib.r_bw;
    L       = floor(dsStm.pntAttrib.nPixels(pointIdx)/OSR);
    
    CRB     = (1-gamma.^2)./(2*L*gamma.^2);
    Qy      = diag(CRB);
    
    % Fill in the 1st off-diagonal of the variance-covariance matrix
    % If we dont have this, the bootstrap solution is equivalent to
    % integer rounding
    
    % Need full coherence matrix to get the full variance-covariance matrix
    % If you have the full coherence matrix, use that to get the
    % CRB matrix instead of doing this
    CRB_1 = zeros(1,length(CRB)-1);
    for n = 1:length(CRB)-1
        gamma_ij = gamma(n);
        gamma_jk = gamma(n+1);
        gamma_ik = gamma(n)/exp(1); % Just assume a value because we dont have it
        CRB_1(n) = (gamma_ij*gamma_jk - gamma_ik)/(2*L*gamma_ij*gamma_jk);
    end
    %         CRB_1(CRB_1<0) = 0;
    Qy      = Qy + diag(CRB_1,1);
    Qy      = Qy + diag(CRB_1,-1);
    
    % check if matrix is positive definite, otherwise regularize
    [~,z] = chol(abs(Qy));
    e=1e-6;
    while z > 0
        Qy = Qy + e*eye(length(CRB));
        [~,z] = chol(abs(Qy));
        e = e*2;
    end
    
    A       = 2*pi*eye(length(segEpochIdx));
    
    % Float solution
    QaHat   = inv(A'* inv(Qy) * A);
    aHatFl  = QaHat * A' * inv(Qy) * (y-bHat);
    
    % Integer bootstrapping
    aHat    = LAMBDA(aHatFl,QaHat,4);
    
    % Unwrapped phase, yHat
    yHat                = y - A*aHat;
    
    % Re-evaluate vertical shifts
    
    shift                   = mean(yHat - model(segEpochIdx),'omitnan');
    shifted                 = yHat - shift;
    unwrapped(segEpochIdx)  = shifted;
    
end

unwrapped = unwrapped';

% For debugging
%     figure
%     hold on
%     plot(dates,dsStm.obsData.vertUnwPhase(pointIdx,:),'.-')
%     plot(dates,unwrapped,'.-')
%     %             plot(dates,unwrapped + 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
%     %             plot(dates,unwrapped - 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
%     plot(dates,modelFit(modelDateIdx) * v2ph)
%     legend('Initial','Unwrapped, $\hat{y}$','Estimated model, $\hat{b}$','interpreter','latex')
%     set(gca,'fontsize',14)
%     grid on

