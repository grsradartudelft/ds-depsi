function unwrapped = lambdaAmbiguityEstimationArchetype(dsStm,parcelId,settings)

% Archetype = 'zegveld'
% params          = [2.8e-4,0.4,-2.8e-5,70];

% Archetype = 'rouveen'
params          = [9e-5,0.52,-2.3e-5,50];

pointIdx        = find(ismember(dsStm.pntAttrib.parcelId,parcelId));
dates           = dateshift(dsStm.epochAttrib.timestamp, 'start', 'day');
modelDateIdx    = ismember(dsStm.auxData.knmiDates,dates);
los2v           = 1/cosd(dsStm.pntAttrib.incAngle(pointIdx));
v2ph            = -4*pi/dsStm.techniqueAttrib.wavelength;

if settings.seg.method == 1 || settings.seg.method == 2
    doy         = day(dsStm.epochAttrib.timestamp,'dayofyear')/365.25;
    t           = unwrap(doy*2*pi)/(2*pi);
    modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
else
    precip      = dsStm(1).auxData.precipitation;
    evap        = dsStm(1).auxData.evapotrans;
    modelFit    = p_model_lin(precip,evap,params);
end
model           = modelFit(modelDateIdx) * v2ph;

% Estimate ambiguities on per-segment basis
coherence                       = dsStm.obsData.dcCoherence(pointIdx,:);
[nSegments,segIdx,segLength,~]  = segmentation(coherence,settings.seg.minCoherence,settings.seg.minSegLength);
unwrapped                       = nan(size(coherence))';

for k = 1:nSegments
    segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
    
    % Initial phases
%     y       = diff([0 dsStm.obsData.dphase(pointIdx,segEpochIdx)*los2v]');
    y       = dsStm.obsData.dphase(pointIdx,segEpochIdx)'*los2v;
    y       = y-y(1);
%     y       = mod(y+pi,2*pi);
    
    % Parcel model
    
    modelSeg = model(segEpochIdx);
%     bHat    = diff([0; modelSeg]);
    bHat    = modelSeg;
    
    %     figure
    %     hold on
    %     plot(y)
    %     plot(bHat)
    
    % Weight observations by CRB of daisy-chain coherence
    gamma   = double(dsStm.obsData.dcCoherence(pointIdx,segEpochIdx));
    OSR     = dsStm.techniqueAttrib.prf/dsStm.techniqueAttrib.az_bw * dsStm.techniqueAttrib.r_fs/dsStm.techniqueAttrib.r_bw;
    L       = floor(dsStm.pntAttrib.nPixels(pointIdx)/OSR);
    
    CRB     = (1-gamma.^2)./(2*L*gamma.^2);
    Qy      = diag(CRB);
    
    % Fill in the 1st off-diagonal of the variance-covariance matrix
    % If we dont have this, the bootstrap solution is equivalent to
    % integer rounding
    
    % Need full coherence matrix to get the full variance-covariance matrix
    % If you have the full coherence matrix, use that to get the
    % CRB matrix instead of doing this
    CRB_1 = zeros(1,length(CRB)-1);
    for n = 1:length(CRB)-1
        gamma_ij = gamma(n);
        gamma_jk = gamma(n+1);
        gamma_ik = gamma(n)/exp(1); % Just assume a value because we dont have it
        CRB_1(n) = (gamma_ij*gamma_jk - gamma_ik)/(2*L*gamma_ij*gamma_jk);
    end
    %         CRB_1(CRB_1<0) = 0;
    Qy      = Qy + diag(CRB_1,1);
    Qy      = Qy + diag(CRB_1,-1);
    
    % check if matrix is positive definite, otherwise regularize
    [~,z] = chol(abs(Qy));
    e=1e-6;
    while z > 0
        Qy = Qy + e*eye(length(CRB));
        [~,z] = chol(abs(Qy));
        e = e*2;
    end
    
    A       = 2*pi*eye(length(segEpochIdx));
    
    % Float solution
    QaHat   = inv(A'* inv(Qy) * A);
    aHatFl  = QaHat * A' * inv(Qy) * (y-bHat);
    
%     QaHat   = inv(A'* Qy \ A);
%     aHatFl  = QaHat * A' * Qy \ (y-bHat);
    
    % Integer bootstrapping
    aHat    = LAMBDA(aHatFl,QaHat,4);
    
    % Unwrapped phase, yHat
    yHat    = y - A*aHat;
    
    % Move back to ESM form
%     yHat    = yHat - yHat(1);
%     unwrapped(segEpochIdx) = cumsum(yHat);
%     unwrapped(segEpochIdx) = unwrapped(segEpochIdx) - unwrapped(segEpochIdx(1));
    unwrapped(segEpochIdx)    = yHat - yHat(1);
    
%     figure
%     hold on
%     plot(y,'.')
%     plot(yHat)
%     plot(bHat)
    
end

figure
hold on
plot(dates,unwrapped)
plot(dates,model)

end