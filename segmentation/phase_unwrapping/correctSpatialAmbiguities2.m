function dsStmList = correctSpatialAmbiguities2(dsStmList)


% Combine group means of each track
for p = 1:length(dsStmList(1).auxData.groupIdList)
    
    groupId = dsStmList(1).auxData.groupIdList(p);
    
    for i = 1:length(dsStmList)
        groupMean{i}    = dsStmList(i).auxData.groupMedian(p,:);
        trackDates{i}   = dsStmList(i).epochAttrib.timestamp;
    end
    [meanDefoAll{p}, dates{p}] = combine_tracks_v2(trackDates,groupMean,7);
    
    % Loop over all points in the group
    for i = 1:length(dsStmList)
        pointIdx = find(ismember(dsStmList(i).pntAttrib.groupId,groupId));
        
        
        
        for j = 1:length(pointIdx)
            
            meanDefo        = groupMean{i};
            phiNew          = dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:);
            
            for l = 1:length(phiNew)
                
                if ~isnan(phiNew(l))
                    difference0     = abs(phiNew(l) - meanDefo(l));
                    differencePlus  = abs(phiNew(l) + 2*pi - meanDefo(l));
                    differenceMinus = abs(phiNew(l) - 2*pi - meanDefo(l));
                    
                    [~,state]       = min([difference0 differencePlus differenceMinus]);
                    
                    if     state == 2
                        phiNew(l:end) = phiNew(l:end) + 2*pi;
                    elseif state == 3
                        phiNew(l:end) = phiNew(l:end) - 2*pi;
                    end
                end
            end
            
           
            % Re-evaluate vertical shifts
%             [nSegments,segIdx,segLength,~]  = segmentation(goodEpochs,0.1,1);
%             params                          = dsStmList(1).auxData.groupModelParams(p,:);
%             doy                             = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
%             t                               = unwrap(doy*2*pi)/(2*pi);
%             modelFit                        = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
%             v2ph                            = -4*pi/dsStmList(i).techniqueAttrib.wavelength;
%             for k = 1:nSegments
%                 epochIdx            = segIdx(k):segIdx(k)+segLength(k)-1;
% %                 shift               = mean(phiNew(epochIdx) - meanDefo(epochIdx),'omitnan');
%                 shift               = mean(phiNew(epochIdx) - modelFit(epochIdx)*v2ph,'omitnan');
%                 phiNew(epochIdx)    = phiNew(epochIdx) - shift;
%             end
            
            
            
            % for debugging
%             precip      = dsStmList(1).auxData.precipitation;
%             evap        = dsStmList(1).auxData.evapotrans;
%             modelEval   = p_model(precip,evap,dsStmList(1).auxData.groupModelParams(p,:));
%             
%             close all
%             figure
%             hold on
%            
%             plot(dsStmList(i).auxData.knmiDates,modelEval*-4*pi/0.0556)
%             plot(dsStmList(i).epochAttrib.timestamp,meanDefo,'linewidth',1)
%             plot(dsStmList(i).epochAttrib.timestamp,dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:),'linewidth',3)
%             plot(dsStmList(i).epochAttrib.timestamp,phiNew,'linewidth',3)
%             legend('Model','Mean','Old','New')
            
            dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = phiNew;
            
%             rmse = sqrt(mean((phiNew-meanDefo).^2,'omitnan'));
%             disp(['RMSE = ' num2str(rmse)])
            

                

            
            
        end
        
        % Recompute group means
        groupIdx                                        = find(dsStmList(i).auxData.groupIdList == groupId);
        dsStmList(i).auxData.groupMean(groupIdx,:)      = mean(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
        dsStmList(i).auxData.groupMedian(groupIdx,:)    = median(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
    end
    
end