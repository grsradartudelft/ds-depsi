function [unwrappedSegments,params] = spams_unwrap_stm(stm,parcelId,initParams,settings)

% Unwraps one time series of one DS-PS arc, based on the SPAMS model. 
% Does an intial fitting of the model by trying to reproduce the observed
% phases. The cost function is the distance in the phasor space.

% Once the inital model is fit, the observed phases are unwrapped
% accordingly.

% Initial fit is likely to be suboptimal because of low SNR and
% loss-of-lock

% If initParams is empty, an initial guess based on a supplied soil
% archetype is made. If no archetype is supplied, a generic set of initial
% params is used.

% Call this function multiple times to do multiple points

dates       = dateshift(stm.epochAttrib.timestamp, 'start', 'day');
pointIdx    = find(stm.pntAttrib.parcelId == parcelId);


%% Get segments
% Load coherence data
coherence   = stm.obsData.dcCoherence(pointIdx,:);

% Identify segments
minCoherence = settings.seg.minCoherence;
minSegLength = settings.seg.minSegLength;
[nSegments,segIdx,segLength,~] = segmentation(coherence,minCoherence,minSegLength);

% Wrapped Time Series
dphase      = stm.obsData.dphase(pointIdx,:);
segments    = nan(size(dphase));

% Each segments phases start at zero
for k = 1:nSegments
    segments(segIdx(k):segIdx(k)+segLength(k)-1) = wrap(dphase(segIdx(k):segIdx(k)+segLength(k)-1) - dphase(segIdx(k)));
end

%% Set up model
% Initial model parameters - archetypes

if isfield(settings,'archetype')
    
    % Archetype based on definition in contextual data
    if strcmp(settings.archetype,'predefined')
        archetype = stm.pntAttrib.archetype(pointIdx);
        
    % Base inital guess off of soil archetype
    elseif strcmp(settings.archetype,'soil type')
        soilcodeLong    = char(dsStmList(1).pntAttrib.soilcode(m));
        idx             = isstrprop(soilcodeLong,'upper');
        archetype       = string(soilcodeLong(find(idx,1)));
        
    % Base inital guess off of region archetype
    elseif isfield(stm.pntAttrib,'region')
        archetype = stm.pntAttrib.region(pointIdx);
        
    % If nothing is available use a basic initial guess
    else
        archetype = 'unknown';
    end

% If no archetype selection is defined    
else
    archetype = 'unknown';
end

% Only use the archetype if no initial params are specified
if isempty(initParams)
    
    % Archetype based on primary soil code, based on median params from
    % intial run. Does not take into account location or water conditions 
    % so it may be inaccurate
    
    if strcmpi(archetype,'A')     % defined associations (?)
        initParams = [7.5e-5, 0.42,-1.3e-4, 60];
        
    elseif strcmpi(archetype,'E') % thick moorland soils (?)
        initParams = [1e-4,   0.52,-1e-5,   60];
        
    elseif strcmpi(archetype,'H') % podzolic soils
        initParams = [9.4e-5, 0.25,-1.4e-5, 62]; 
        
    elseif strcmpi(archetype,'M') % marine clay soils
        initParams = [8.3e-5, 0.35,-1.2e-5, 61];
        
    elseif strcmpi(archetype,'R') % river clay soils
        initParams = [8.5e-5, 0.42,-1.3e-5, 63];
        
    elseif strcmpi(archetype,'V') % peat soils
        initParams = [8.6e-5, 0.44,-1.3e-5, 62];
    
    elseif strcmpi(archetype,'W') % marshy soils
        initParams = [8.9e-5, 0.35,-1.2e-5, 62];
        
    elseif strcmpi(archetype,'Z') % calcareous sands
        initParams = [7.4e-5, 0.38,-1.3e-5, 59];
        
    % ----------------------------------------------------------
    
    elseif strcmpi(archetype,'aldeboarn')
        initParams = [1.3e-4, 0.8, -1e-4,   80];
        
    elseif strcmpi(archetype,'assendelft')
        initParams = [9.2e-5, 1.4, -1.4e-4, 70]; 
        
    elseif strcmpi(archetype,'rouveen')
        initParams = [9.5e-5, 0.52,-2.3e-5, 50]; 
        
    elseif strcmpi(archetype,'vlist')
        initParams = [9e-5,   1.4, -2.2e-5, 60];
        
    elseif strcmpi(archetype,'zegveld')
        initParams = [2.8e-4, 0.52,-1e-5,   70];
   
    else % Ballpark initial guess
        initParams = [1e-4, 0.52,-1e-5,     60];
    end
end

% Meteo data
epochIdx    = find(ismember(stm.auxData.knmiDates,dates));

if size(stm.auxData.precipitation,1) > 1
    stationIdx  = stm.auxData.knmiStations == stm.pntAttrib.knmi_id(pointIdx);
    precip      = stm.auxData.precipitation(stationIdx,:);
    evap        = stm.auxData.evapotrans(stationIdx,:);
else
    precip      = stm.auxData.precipitation;
    evap        = stm.auxData.evapotrans;
end

% Convert displacement to phase
z2ph        = -4*pi/stm.techniqueAttrib.wavelength * cosd(stm.pntAttrib.incAngle(pointIdx));

% Cost function
estSegs     = @(theta) p_model_fitting_segments(precip,evap,epochIdx,segIdx,segLength,z2ph,theta,settings);
cost        = @(theta) mean(abs(exp(1i*segments)-exp(1i*estSegs(theta))).^2,'omitnan');

%% Solve
opts        = optimset('Display','off');
% opts        = optimset('MaxIter',1e6,'MaxFunEvals',1e6);
[params,C]  = fminsearch(cost,initParams,opts);

%% Unwrap

% Fitted model
modelDispFull       = p_model_lin(precip,evap,params);
modelDisp           = modelDispFull(epochIdx);

QyFull              = estimate_Qy(stm,parcelId);
shiftedSegments     = nan(size(segments));
unwrappedSegments   = nan(size(segments));

for k = 1:nSegments
    idx                     = segIdx(k):segIdx(k)+segLength(k)-1;
    y                       = wrap(segments(idx)' - segments(idx(1))');
    bHat                    = (modelDisp(idx) - modelDisp(idx(1)))'*z2ph;
    
    % Estimate mean bias 
    bHatw                   = wrap(bHat);
    bias                    = mean(y-bHatw);
    
%     y                       = [0; diff(segments(idx)')];
%     bHat                    = [0; diff(modelDisp(idx))*z2ph];
    
    Qy                      = QyFull(idx,idx);
    A                       = 2*pi*eye(length(y));

    % Float solution
    QaHat                   = inv(A'* inv(Qy) * A);
    aHatFl                  = QaHat * A' * inv(Qy) * (y-bHat-bias);
    
    % Integer bootstrapping
    aHat                    = LAMBDA(aHatFl,QaHat,4); 
    
    % Unwrapped phase, yHat
    yHat                    = y - A*aHat;
    shift                   = mean(yHat - modelDisp(idx)*z2ph,'omitnan');
    
    % Apply shift to check against model, for debugging. 
    % In full process, we reestimate the model with the full contextual
    % group
    
    % I also return th model params for funsies
    unwrappedSegments(idx)  = yHat';
    shiftedSegments(idx)    = yHat' - shift;
    
%     figure
%     hold on
%     plot(dates(idx),bHatw,'.')
%     plot(dates(idx),y-bias,'.')
%     grid on
%     
%     figure
%     hold on
%     plot(dates(idx),bHat)
%     plot(dates(idx),yHat)
%     grid on
end

% close all
% figure
% hold on
% % plot(dates,modelPhase)
% plot(dates,segments)
% plot(dates,modelSegs)
% grid on
% 
% figure
% hold on
% plot(exp(1i*segments),'.')
% plot(exp(1i*modelSegs),'.')
% 
% figure
% hold on
% plot(dates,abs(exp(1i*segments)-exp(1i*modelSegs)))
% grid on

% figure
% hold on
% plot(dates,shiftedSegments)
% plot(dates,modelDisp*z2ph)
% 
% err = mean(abs(exp(1i*segments)-exp(1i*modelSegs)),'omitnan')
% params
