function Qy = estimate_Qy(dsStm,pointSel)

pointIdx = find(dsStm.pntAttrib.parcelId == pointSel);

gamma   = double(dsStm.obsData.dcCoherence(pointIdx,:));
OSR     = dsStm.techniqueAttrib.prf/dsStm.techniqueAttrib.az_bw * dsStm.techniqueAttrib.r_fs/dsStm.techniqueAttrib.r_bw;
L       = floor(dsStm.pntAttrib.nPixels(pointIdx)/OSR);

CRB     = (1-gamma.^2)./(2*L*gamma.^2);
Qy      = diag(CRB);

% Fill in the 1st off-diagonal of the variance-covariance matrix
% If we dont have this, the bootstrap solution is equivalent to
% integer rounding

% Need full coherence matrix to get the full variance-covariance matrix
% If you have the full coherence matrix, use that to get the
% CRB matrix instead of doing this
CRB_1 = zeros(1,length(CRB)-1);
for n = 1:length(CRB)-1
    gamma_ij = gamma(n);
    gamma_jk = gamma(n+1);
    gamma_ik = gamma(n)/exp(1); % Just assume a value because we dont have it
    CRB_1(n) = (gamma_ij*gamma_jk - gamma_ik)/(2*L*gamma_ij*gamma_jk);
end
%         CRB_1(CRB_1<0) = 0;
Qy      = Qy + diag(CRB_1,1);
Qy      = Qy + diag(CRB_1,-1);

% check if matrix is positive definite, otherwise regularize
[~,z] = chol(abs(Qy));
e=1e-6;
iter = 1;
while z > 0
    Qy = Qy + e*eye(length(CRB));
    [~,z] = chol(abs(Qy));
    e = e*2;
    iter=iter+1;
    
    if iter > 1000
        Qy = diag(CRB); % Default back to uncorrelated case
        break;
    end
end

end

