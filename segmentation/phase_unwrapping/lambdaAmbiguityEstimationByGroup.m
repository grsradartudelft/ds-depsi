function dsStmList = lambdaAmbiguityEstimationByGroup(dsStmList,groupSel,settings)

% Same function as lambdaAmbiguityEstimation but only for specified groups

disp('Re-estimating ambiguities using LAMBDA')

% Combine group means of each track
for p = 1:length(groupSel)
    
    groupId     = groupSel(p);
    groupIdx    = find(ismember(dsStmList(1).auxData.groupIdList,groupId));
    params      = dsStmList(1).auxData.groupModelParams(groupIdx,:);
    
    combOutputTT = timetable();
    for i = 1:length(dsStmList)
        groupIdx        = find(ismember(dsStmList(i).auxData.groupIdList,groupId));
        ph2v            = -1*dsStmList(i).techniqueAttrib.wavelength/(4*pi); % Phases are already projected to vertical
        medianDefo      = dsStmList(i).auxData.groupMedian(groupIdx,:)'*ph2v;
        trackOutputTT   = timetable(dateshift(dsStmList(i).epochAttrib.timestamp', 'start', 'day'),medianDefo);
        combOutputTT    = synchronize(combOutputTT,trackOutputTT);
    end
    
    % Loop over all tracks in the group
    for i = 1:length(dsStmList)
        if settings.seg.method == 1 || settings.seg.method == 2
            doy         = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
            t           = unwrap(doy*2*pi)/(2*pi);
            modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
        else
            precip      = dsStmList(1).auxData.precipitation;
            evap        = dsStmList(1).auxData.evapotrans;
            modelFit    = p_model_lin(precip,evap,params);
            
        end
        groupIdx        = find(ismember(dsStmList(i).auxData.groupIdList,groupId));
        pointIdx        = find(ismember(dsStmList(i).pntAttrib.groupId,groupId));
        dates           = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        knmiTrackIdx    = ismember(dsStmList(1).auxData.knmiDates,dates);
        v2ph            = -4*pi/dsStmList(i).techniqueAttrib.wavelength;
        
        % Loop over all points in the group
        for j = 1:length(pointIdx)
            
            % Wrapped phases
            y       = dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:)';
            y(isnan(y)) = 0;

            
            % Mean "model"
            bHat    = dsStmList(i).auxData.groupMedian(groupIdx,:)';
            bHat    = bHat - bHat(1);

            % Estimate mean bias 
            bHatw   = wrap(bHat);
            bias    = mean(y-bHatw);
            
            % Estimated model            
            % Get Qy from CRB of daisy-chain coherence
            pntSel  = dsStmList(i).pntAttrib.parcelId(pointIdx(j));
            Qy      = estimate_Qy(dsStmList(i),pntSel);
            
            A       = 2*pi*eye(dsStmList(i).numEpochs);
                        
            % Float solution
            QaHat   = inv(A'* inv(Qy) * A);
            aHatFl  = QaHat * A' * inv(Qy) * (y-bHat-bias);
            
            % Integer bootstrapping
            aHat    = LAMBDA(aHatFl,QaHat,4); 
            
            % Unwrapped phase, yHat
            yHat                = y - A*aHat;
            goodIdx             = ~isnan(dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:));
            unwrapped           = nan(size(yHat))';
            unwrapped(goodIdx)  = yHat(goodIdx);
            shifted             = unwrapped;
            
            % Re-evaluate vertical shifts
            [nSegments,segIdx,segLength,~]  = segmentation(goodIdx,0.1,1);
            for k = 1:nSegments
                segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
                shift                       = mean(unwrapped(segEpochIdx) - bHat(segEpochIdx)','omitnan');
                shifted(segEpochIdx)        = unwrapped(segEpochIdx) - shift;
            end
            
            
            % For debugging
%             figure
%             hold on
%             plot(dates,dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:))
%             plot(dates,unwrapped)
%             plot(dates,shifted)
% %             plot(dates,unwrapped + 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
% %             plot(dates,unwrapped - 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
%             plot(dates,bHat)
%             legend('RNN segments','Unwrapped, $\hat{y}$','Reshifted','Estimated model, $\hat{b}$','interpreter','latex')
%             set(gca,'fontsize',14)
%             grid on
            
            % Update values 
            %dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = shifted;
            dsStmList(i).obsData.finalUnwPhase(pointIdx(j),:) = shifted;
            
            % Perform overall model test
            alpha = 0.1;
            [accept,T] = omt_parcel(dsStmList,pntSel,alpha,settings);
            dsStmList(i).pntAttrib.T(pointIdx(j)) = T;
            
            % Do not use this parcel if the OMT is rejected
            if accept == 0
                %dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = nan(size(shifted));
                dsStmList(i).obsData.finalUnwPhase(pointIdx(j),:) = nan(size(shifted));
            end
            
        end
        
        
        % For debugging
%         figure
%         hold on
%         plot(dates,dsStmList(i).auxData.groupMedian(p,:))
%         plot(dates,bHat)
%         plot(dates,median(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan'))
%         legend('old','model','new')
        
        % Update group mean and median
        dsStmList(i).auxData.groupMean(groupIdx,:)   = mean(dsStmList(i).obsData.finalUnwPhase(pointIdx,:),1,'omitnan');
        dsStmList(i).auxData.groupMedian(groupIdx,:) = median(dsStmList(i).obsData.finalUnwPhase(pointIdx,:),1,'omitnan');
        
    end
    
    disp(['Done ' num2str(p) ' of ' num2str(length(groupSel)) ' groups'])
    
end