function dsStmList = correctSpatialAmbiguities(dsStmList,settings)


% Combine group means of each track
for p = 1:length(dsStmList(1).auxData.groupIdList)
    
    groupId = dsStmList(1).auxData.groupIdList(p);
    
    for i = 1:length(dsStmList)
        groupMean{i}    = dsStmList(i).auxData.groupMedian(p,:);
        trackDates{i}   = dsStmList(i).epochAttrib.timestamp;
    end
    [meanDefoAll{p}, combDates{p}] = combine_tracks_v2(trackDates,groupMean,7);
    
    % Loop over all points in the group
    for i = 1:length(dsStmList)
        pointIdx = find(ismember(dsStmList(i).pntAttrib.groupId,groupId));
        
        
        
        for j = 1:length(pointIdx)
            
%             if dsStmList(i).pntAttrib.parcelId(pointIdx(j)) ==  591099
%                 disp('hi')
%             end
            
            epochIdx        = ismember(combDates{p},dsStmList(i).epochAttrib.timestamp);
            meanDefo        = meanDefoAll{p}(epochIdx);
%             meanDefo        = groupMean{i};
            deltaMean       = [0 diff(meanDefo)];
            deltaPhi        = [0 diff(dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:))];
            phiNew          = dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:);
            goodEpochs      = ~isnan(phiNew);
            nanEpochs       = isnan(deltaPhi);
            
            for l = 1:length(deltaPhi)
                
                difference0     = abs(deltaPhi(l) - deltaMean(l));
                differencePlus  = abs(deltaPhi(l) + 2*pi - deltaMean(l));
                differenceMinus = abs(deltaPhi(l) - 2*pi - deltaMean(l));
                
                [~,state]       = min([difference0 differencePlus differenceMinus]);
                
                if     state == 2
                    deltaPhi(l) = deltaPhi(l) + 2*pi;
                    phiNew      = cumsum(deltaPhi,'omitnan');
                    deltaPhi    = [0 diff(phiNew)];
                    deltaPhi(nanEpochs) = nan;
                    
                elseif state == 3
                    deltaPhi(l) = deltaPhi(l) - 2*pi;
                    phiNew      = cumsum(deltaPhi,'omitnan');
                    deltaPhi    = [0 diff(phiNew)];
                    deltaPhi(nanEpochs) = nan;
                end
                
            end
            
            phiNew(~goodEpochs) = nan;
            
            % Re-evaluate vertical shifts
            [nSegments,segIdx,segLength,~]  = segmentation(goodEpochs,0.1,1);
            params                          = dsStmList(1).auxData.groupModelParams(p,:);
            dates                           = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
            
            if settings.seg.method == 1 || settings.seg.method == 2
                doy                         = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
                t                           = unwrap(doy*2*pi)/(2*pi);
                modelFit                    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
            else
                precip                      = dsStmList(1).auxData.precipitation;
                evap                        = dsStmList(1).auxData.evapotrans;
                modelFit                    = p_model_lin(precip,evap,params);
            end
            v2ph                            = -4*pi/dsStmList(i).techniqueAttrib.wavelength;
            
            for k = 1:nSegments
                segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
                knmiEpochIdx                = find(ismember(dsStmList(1).auxData.knmiDates,dates(segEpochIdx)));
%                 shift                     = mean(phiNew(epochIdx) - meanDefo(epochIdx),'omitnan');
                shift                       = mean(phiNew(segEpochIdx) - modelFit(knmiEpochIdx)'*v2ph,'omitnan');
                phiNew(segEpochIdx)         = phiNew(segEpochIdx) - shift;
            end
            
            
            
            % for debugging
%             precip      = dsStmList(1).auxData.precipitation;
%             evap        = dsStmList(1).auxData.evapotrans;
%             modelEval   = p_model_lin(precip,evap,dsStmList(1).auxData.groupModelParams(p,:));
            
%             close all
%             figure
%             hold on
%            
%             plot(dsStmList(i).auxData.knmiDates,modelFit*v2ph)
%             plot(dsStmList(i).epochAttrib.timestamp,meanDefo,'linewidth',2)
%             plot(dsStmList(i).epochAttrib.timestamp,dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:),'linewidth',2)
%             plot(dsStmList(i).epochAttrib.timestamp,phiNew,'linewidth',2)
%             legend('Model','Mean','Old','New')
            
            dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = phiNew;
            
%             rmse = sqrt(mean((phiNew-meanDefo).^2,'omitnan'));
%             disp(['RMSE = ' num2str(rmse)])
            

                

            
            
        end
        
        % Recompute group means
        groupIdx                                        = find(dsStmList(i).auxData.groupIdList == groupId);
        dsStmList(i).auxData.groupMean(groupIdx,:)      = mean(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
        dsStmList(i).auxData.groupMedian(groupIdx,:)    = median(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
    end
    
end