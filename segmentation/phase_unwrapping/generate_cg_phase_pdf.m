function vari = generate_cg_phase_pdf(phi0,coherence,L)
% V1.0
% Philip Conroy
% TU Delft Dept. Geoscience and Remote Sensing
% 18-10-2021
%
% Generates circular gaussian noise PDF (Barber 1993, Hanssen 2001), 
% according to the coherence provided. Coherence must be between 0 and 1
%
% Inputs:   phi0 = mean of distribution (usually 0),
%           coherence (gamma), [0 1]
%           L (equivalent number of looks) [1 100]
%
% Outputs:  vari = the variance of the c.g. distribution from which n was sampled, with len(n) = len(coherence)

phi = linspace(-pi,pi,5000);
vari = zeros(size(coherence));

for t = 1:length(coherence)
    
    % define the pdf and cdf
    % hypergeom([a,b],c,z) is the Gauss hypergeometric function 2F1(a,b;c;z).
    beta = abs(coherence(t))*cos(phi-phi0);
    
    pdf = ( gamma(L+0.5)*(1-coherence(t)^2)^L * beta ) ./ ...
          ( 2*sqrt(pi)*gamma(L)*(1-beta.^2).^(L+0.5) ) + ...
          (1-coherence(t)^2)^L /(2*pi) * hypergeom([1,L],0.5,beta.^2) ;
    
    % For debugging
%     figure
%     plot(phi,pdf)
%     grid on
    
    % Calculate the variance
    vari(t) = trapz(phi, (phi-phi0).^2 .*pdf);
end

end

