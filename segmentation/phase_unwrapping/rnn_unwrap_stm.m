function [unwrappedTs,ambTs,confidence] = rnn_unwrap_stm(stm,dphase,epochIdx,pntIdx,meanCoherence,settings)

% Unwraps one time series or segment of one DS-PS arc. Call this function
% multiple times to do multiple points

%%
% Select the dates corresponding to dphase out of the RNN data
% This might be buggy if RNN dates don't completely overlap dates
% Alternative- create a timetable and directly index using dates
dates = dateshift(stm.epochAttrib.timestamp(epochIdx), 'start', 'day');

rnnEpochIdx = find(ismember(stm.auxData.rnn.timestamp,dates));
usedDates = stm.auxData.rnn.timestamp(rnnEpochIdx);

%Create observation vector
[~,OBS] = max(stm.auxData.rnn.output(:,rnnEpochIdx),[],1);

%Create state vector
STATE = [1,2,3];

%Initial state: no movement yet
INIT = [1,0,0];

% confusionMatrix = readmatrix(stm.auxData.rnn.confusion_mat);
% E = zeros(3);
% for k = 1:3
%     E(:,k) = confusionMatrix(:,k)/sum(confusionMatrix(:,k));
% end
% % % %Manually change 3rd row so its not zero
% E(3,1) = E(3,1) - 0.025;
% E(3,2) = E(3,2) + 0.05;
% E(3,3) = E(3,3) - 0.025;

% Old confusion matrix
% E = [0.615,0.118,0.222;
%      0.143,0.882,0.019;
%      0.218,0.050,0.734];

% New confusion matrix
E = [0.776,0.272,0.379;
     0.080,0.728,0.001;
     0.144,0.001,0.621];

%% Estimate noise levels
% Load coherence data
if isempty(meanCoherence)
    coherence = stm.obsData.dcCoherence(pntIdx,epochIdx);
else
    coherence = meanCoherence;
end

% Estimate noise standard deviation
OSR = stm.techniqueAttrib.prf/stm.techniqueAttrib.az_bw * stm.techniqueAttrib.r_fs/stm.techniqueAttrib.r_bw;
L = floor(stm.pntAttrib.nPixels(pntIdx)/OSR);

% Approximation of eq. 3.63 Esfahni thesis
noiseStd = zeros(size(coherence));
noiseStd(coherence>=0.1) = pi^2/3 ./(coherence(coherence>=0.1) * L/2);
noiseStd(coherence<0.1) = (max(noiseStd)-2*pi/sqrt(12))/0.1 .* coherence(coherence<0.1) + 2*pi/sqrt(12);

% Debugging
if settings.plotMeanCoherence == 1 && ~isempty(meanCoherence)
    figure
%     plot(dates,coherence)
    plot(coherence,noiseStd,'.')
%     ylabel('Mean Coherence')
    grid on
end
%% Set up Hidden Markov Model
% ----------------------------------------------------------------------
% Wrapped Time Series
% ----------------------------------------------------------------------

% Depsi arc phase differences may exceed +/- pi
dphase_rewrap   = mod(dphase+pi,2*pi)-pi; 
wrappedTs       = dphase_rewrap;
wrappedDiff     = zeros(size(wrappedTs));
wrappedDiff(1)  = wrappedTs(1);

% ----------------------------------------------------------------------
% Create Hidden Markov Model
% ----------------------------------------------------------------------

T = zeros(length(usedDates),3);


for t = 1:length(usedDates)
    
    %Observed phase differences
%     wrappedDiff(t) = wrappedTs(t) - wrappedTs(t-1);
    if t > 1
        wrappedDiff(t) = wrappedTs(t) - wrappedTs(t-1);
%         wrappedDiff(t) = mod( wrappedTs(t) - wrappedTs(t-1) +pi,2*pi)-pi;
    end
        
    % Define the two possible branches (closest ambiguity levels)
    branch1 = wrappedDiff(t);
    branch2 = wrappedDiff(t) - sign(wrappedDiff(t))*2*pi;
    b = [branch1,branch2];
    
    % Estimate probability of each branch
%     if strcmp(settings.branchTransitions,'erf')
    p1 = 1-0.5*(erf((abs(branch1)-pi))+1);
%     elseif strcmp(settings.branchTransitions,'exponential')
%         p1 = 1-0.5*exp(3.22*(abs(branch1)-pi));
%     end
    
    p2 = 1-p1;    
    p  = [p1,p2];
    
    %Estimate the probability that the movement is greater than noise
    pSig = erf( abs(wrappedDiff(t))/(1.5*noiseStd(t)*sqrt(2)) );
    
    % Estimate up, down stay probabilities by conditioning branch probabilities
    % on movement probability
    [~,idx] = min(b); % Up = negaitive change in phase
    pUp   = p(idx) * pSig;
    
    [~,idx] = max(b); % Down = positive change in phase
    pDown = p(idx) * pSig;
    
    pStay = 1-pSig;
    
    % Create transition matrix
    % State 1: stay
    % State 2: up
    % State 3: down
    % Transitions are independent of current state
    T(t,1) = pStay;
    T(t,2) = pUp;
    T(t,3) = pDown;
end

[states, trellis] = viterbi(OBS,STATE,INIT,T,E);

confidence = max(trellis,[],2)';

%%
% ----------------------------------------------------------------------
% Unwrap Time Series In Segments
% ----------------------------------------------------------------------

ambTs = int32(zeros(size(wrappedTs)));
unwrappedTs = wrappedTs;

for t = 1:length(dates)
    
    % Define the two possible branches (closest ambiguity levels)
    branch1 = wrappedDiff(t);
    branch2 = wrappedDiff(t) - sign(wrappedDiff(t))*2*pi;
    
    upper = min(branch1,branch2);
    lower = max(branch1,branch2);
    
    ambiguityUp   = upper - wrappedDiff(t);
    ambiguityDown = lower - wrappedDiff(t);
    
    if states(t) == 1
        % Take the shorter branch (defualt)
        ambiguity = 0;
        ambInt = 0;
        
    elseif states(t) == 2
        
        ambiguity = ambiguityUp;
        if abs(abs(ambiguity)-2*pi)<1e-4
            ambInt = 1;
        else
            ambInt = 0;
        end
        
        
    elseif states(t) == 3
        
        ambiguity = ambiguityDown;
        if abs(abs(ambiguity)-2*pi)<1e-4
            ambInt = -1;
        else
            ambInt = 0;
        end
        
    end
    
    % Update the time series. ambiguity will be either 0 or +/- 2pi depending on the logic above
    unwrappedTs(t:end) = unwrappedTs(t:end) + ambiguity;
    ambTs(t:end) = ambTs(t:end) + ambInt;
    
end

% figure
% plot(dates,unwrappedTs)
% grid on
% disp('hi')
