function dsStmList = correctSpatialAmbiguities3(dsStmList,settings)


% Combine group means of each track
for p = 1:length(dsStmList(1).auxData.groupIdList)
    
    groupId         = dsStmList(1).auxData.groupIdList(p);
    params          = dsStmList(1).auxData.groupModelParams(p,:);
    
    % Loop over all points in the group
    for i = 1:length(dsStmList)
        
        if settings.seg.method == 1 || settings.seg.method == 2
            doy         = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
            t           = unwrap(doy*2*pi)/(2*pi);
            modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
        else
            precip      = dsStmList(1).auxData.precipitation;
            evap        = dsStmList(1).auxData.evapotrans;
            modelFit    = p_model_lin(precip,evap,params);
            
        end
        
        pointIdx        = find(ismember(dsStmList(i).pntAttrib.groupId,groupId));
        dates           = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
        knmiTrackIdx    = ismember(dsStmList(1).auxData.knmiDates,dates);
        v2ph            = -4*pi/dsStmList(i).techniqueAttrib.wavelength;
        
        for j = 1:length(pointIdx)
           
            if settings.seg.method == 3 || settings.seg.method == 4
                meanDefo    = modelFit(knmiTrackIdx)' * v2ph;
            else
                meanDefo    = modelFit' * v2ph;
            end
            
            deltaMean       = [0 diff(meanDefo)];
            deltaPhi        = [0 diff(dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:))];
            phiNew          = dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:);
            goodEpochs      = ~isnan(phiNew);
            nanEpochs       = isnan(deltaPhi);
            
            for l = 1:length(deltaPhi)
                
                difference0     = abs(deltaPhi(l) - deltaMean(l));
                differencePlus  = abs(deltaPhi(l) + 2*pi - deltaMean(l));
                differenceMinus = abs(deltaPhi(l) - 2*pi - deltaMean(l));
                
                [~,state]       = min([difference0 differencePlus differenceMinus]);
                
                if     state == 2
                    deltaPhi(l) = deltaPhi(l) + 2*pi;
                    phiNew      = cumsum(deltaPhi,'omitnan');
                    deltaPhi    = [0 diff(phiNew)];
                    deltaPhi(nanEpochs) = nan;
                    
                elseif state == 3
                    deltaPhi(l) = deltaPhi(l) - 2*pi;
                    phiNew      = cumsum(deltaPhi,'omitnan');
                    deltaPhi    = [0 diff(phiNew)];
                    deltaPhi(nanEpochs) = nan;
                end
                
            end
            
            phiNew(~goodEpochs) = nan;
            
            % Re-evaluate vertical shifts
            [nSegments,segIdx,segLength,~]  = segmentation(goodEpochs,0.1,1);
            
            for k = 1:nSegments
                segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
                knmiSegIdx                  = ismember(dsStmList(1).auxData.knmiDates,dates(segEpochIdx));
                shift                       = mean(phiNew(segEpochIdx) - modelFit(knmiSegIdx)'*v2ph,'omitnan');
                phiNew(segEpochIdx)         = phiNew(segEpochIdx) - shift;
            end
            
            if dsStmList(i).pntAttrib.parcelId(pointIdx(j)) ==  591099
                disp('hi')
            end
            
            % for debugging
            
            close all
            figure
            hold on
           
            plot(dsStmList(i).auxData.knmiDates,modelFit*v2ph)
            plot(dsStmList(i).epochAttrib.timestamp,meanDefo,'linewidth',2)
            plot(dsStmList(i).epochAttrib.timestamp,dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:),'linewidth',2)
            plot(dsStmList(i).epochAttrib.timestamp,phiNew,'linewidth',2)
            legend('Model','Mean','Old','New')
            
            dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = phiNew;
            
%             rmse = sqrt(mean((phiNew-meanDefo).^2,'omitnan'));
%             disp(['RMSE = ' num2str(rmse)])
            

                

            
            
        end
        
        % Recompute group means
        groupIdx                                        = find(dsStmList(i).auxData.groupIdList == groupId);
        dsStmList(i).auxData.groupMean(groupIdx,:)      = mean(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
        dsStmList(i).auxData.groupMedian(groupIdx,:)    = median(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan');
    end
    
end