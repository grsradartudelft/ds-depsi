function dsStmList = inter_track_amb_check(dsStmList)


allDates = [];
for i = 1:length(dsStmList)
    allDates = [allDates, dsStmList(i).epochAttrib.timestamp];
end
allDates = sort(allDates,'ascend');

for j = 1:dsStmList(1).numPoints
    if dsStmList(1).pntAttrib.groupId(j) ~= 0
        
       
        for i = 1:length(dsStmList)
            groupIdx            = find(dsStmList(i).auxData.groupIdList == dsStmList(1).pntAttrib.groupId(j));
            groupMean{i}        = dsStmList(i).auxData.groupMedian(groupIdx,:);
            pointIdx            = find(dsStmList(i).pntAttrib.parcelId == dsStmList(1).pntAttrib.parcelId(j));
            
            phase{i}            = dsStmList(i).obsData.vertUnwPhase(pointIdx,:);
            phaseInterp(i,:)    = interp1(dsStmList(i).epochAttrib.timestamp,phase{i},allDates,'linear');
            
            idCheck(i)          = dsStmList(i).pntAttrib.parcelId(pointIdx);
        end
        
        % Difference in delta phi between tracks (approximate)
        deltaPhaseInterp        = diff(phaseInterp,1,2);
        diffDeltaPhaseInterp    = deltaPhaseInterp(1,:) - deltaPhaseInterp(2,:);
        
        % Identify epochs where the difference in delta phi > 2pi
        flag                    = find(abs(diffDeltaPhaseInterp) > 2*pi)+1;
        
        figure
        hold on
        plot(allDates(2:end),abs(diffDeltaPhaseInterp))
        yline(2*pi,'k--','linewidth',2)
        yline(-2*pi,'k--','linewidth',2)
        
        figure
        hold on
        for i = 1:length(dsStmList)
            plot(dsStmList(i).epochAttrib.timestamp,groupMean{i})
            plot(dsStmList(i).epochAttrib.timestamp,phase{i},'*')
            plot(allDates,phaseInterp(i,:),'.--')
        end
        for n = 1:length(flag)
            xline(allDates(flag(n)),'r--')
        end
        grid on
        title(['Parcel ' num2str(dsStmList(i).pntAttrib.parcelId(pointIdx))])
        
        if ~all(idCheck == idCheck(1))
            disp('we have a problem')
        end
        
    end
end
