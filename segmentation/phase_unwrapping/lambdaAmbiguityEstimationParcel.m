function [dsStmList,unwrapped] = lambdaAmbiguityEstimationParcel(dsStmList,parcelId,settings)

% disp('Adapting ambiguities using parcel model')



% Loop over all tracks
for i = 1:length(dsStmList)
    
    pointIdx        = find(ismember(dsStmList(i).pntAttrib.parcelId,parcelId));
    groupId         = dsStmList(i).pntAttrib.groupId(pointIdx);
    groupIdx        = find(dsStmList(i).auxData.groupIdList == groupId);
    dates           = dateshift(dsStmList(i).epochAttrib.timestamp, 'start', 'day');
    modelDateIdx    = ismember(dsStmList(1).auxData.knmiDates,dates);
%     params          = dsStmList(i).pntAttrib.params(pointIdx,:);
    params          = dsStmList(i).auxData.groupModelParams(groupIdx,:);
    v2ph            = -4*pi/dsStmList(i).techniqueAttrib.wavelength;
    
    if settings.seg.method == 1 || settings.seg.method == 2
        doy         = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
        t           = unwrap(doy*2*pi)/(2*pi);
        modelFit    = params(1)*cos( 2*pi*t + params(2) ) + params(3)*t;
    else
        precip      = dsStmList(1).auxData.precipitation;
        evap        = dsStmList(1).auxData.evapotrans;
        modelFit    = p_model_lin(precip,evap,params);
        
    end
    
    % Refine ambiguities on per-segment basis
    
    coherence                       = dsStmList(i).obsData.dcCoherence(pointIdx,:);
    [nSegments,segIdx,segLength,~]  = segmentation(coherence,settings.seg.minCoherence,settings.seg.minSegLength);
    unwrapped                       = nan(size(coherence))';
    for k = 1:nSegments
        segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
        
        % Initial phases
        y       = dsStmList(i).obsData.vertUnwPhase(pointIdx,segEpochIdx)';    
        y       = y - y(1);
        
        % Parcel model
        model   = modelFit(modelDateIdx) * v2ph;
        bHat    = model(segEpochIdx);
        bHat    = bHat - bHat(1);
        
        %     figure
        %     hold on
        %     plot(y)
        %     plot(bHat)
        
        % Weight observations by CRB of daisy-chain coherence
        gamma   = double(dsStmList(i).obsData.dcCoherence(pointIdx,segEpochIdx));
        OSR     = dsStmList(i).techniqueAttrib.prf/dsStmList(i).techniqueAttrib.az_bw * dsStmList(i).techniqueAttrib.r_fs/dsStmList(i).techniqueAttrib.r_bw;
        L       = floor(dsStmList(i).pntAttrib.nPixels(pointIdx)/OSR);
        
        CRB     = (1-gamma.^2)./(2*L*gamma.^2);
        Qy      = diag(CRB);
        
        % Fill in the 1st off-diagonal of the variance-covariance matrix
        % If we dont have this, the bootstrap solution is equivalent to
        % integer rounding
        
        % Need full coherence matrix to get the full variance-covariance matrix
        % If you have the full coherence matrix, use that to get the
        % CRB matrix instead of doing this
        CRB_1 = zeros(1,length(CRB)-1);
        for n = 1:length(CRB)-1
            gamma_ij = gamma(n);
            gamma_jk = gamma(n+1);
            gamma_ik = gamma(n)/exp(1); % Just assume a value because we dont have it
            CRB_1(n) = (gamma_ij*gamma_jk - gamma_ik)/(2*L*gamma_ij*gamma_jk);
        end
%         CRB_1(CRB_1<0) = 0;
        Qy      = Qy + diag(CRB_1,1);
        Qy      = Qy + diag(CRB_1,-1);
        
        % check if matrix is positive definite, otherwise regularize
        [~,z] = chol(abs(Qy));
        e=1e-6;
        while z > 0
            Qy = Qy + e*eye(length(CRB));
            [~,z] = chol(abs(Qy));
            e = e*2;
        end
        
        A       = 2*pi*eye(length(segEpochIdx));
        
        % Float solution
        QaHat   = inv(A'* inv(Qy) * A);
        aHatFl  = QaHat * A' * inv(Qy) * (y-bHat);
        
        % Integer bootstrapping
        aHat    = LAMBDA(aHatFl,QaHat,4);
        
        % Unwrapped phase, yHat
        yHat                = y - A*aHat;
        
        % Re-evaluate vertical shifts
        
        shift                   = mean(yHat - model(segEpochIdx),'omitnan');
        shifted                 = yHat - shift;
        unwrapped(segEpochIdx)  = shifted;
        
    end
    
    
    % For debugging
%     figure
%     hold on
%     plot(dates,dsStmList(i).obsData.vertUnwPhase(pointIdx,:),'.-')
%     plot(dates,unwrapped,'.-')
%     %             plot(dates,unwrapped + 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
%     %             plot(dates,unwrapped - 2*pi,'Color',[0.7,0.7,0.7],'linewidth',1,'handlevisibility','off')
%     plot(dates,modelFit(modelDateIdx) * v2ph)
%     legend('Initial','Unwrapped, $\hat{y}$','Estimated model, $\hat{b}$','interpreter','latex')
%     set(gca,'fontsize',14)
%     grid on
    
    % Update values
    dsStmList(i).obsData.vertUnwPhase(pointIdx,:) = unwrapped;
    
end