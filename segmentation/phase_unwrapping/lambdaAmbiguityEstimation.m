function dsStmList = lambdaAmbiguityEstimation(dsStmList,settings)

%% Check if this already exists
fileFlag = false(length(dsStmList),1);
for i = 1:length(dsStmList)
    filename(i) = string(fullfile(settings.segDir, ['lambda_' char(settings.dsStmFilename(i))]));
    fileFlag(i) = logical(isfile(filename(i)));
end

if all(fileFlag == 1)
    disp('Lambda results found, skipping step.')
    
    for i = 1:length(dsStmList)
        dsStmList(i) = stmread(filename(i));
    end
    return
else
    %% Run LAMBDA spatial ambiguity correction
    disp('Re-estimating ambiguities using LAMBDA')
    
    % Initialize output variables
    for i = 1:length(dsStmList)
        dsStmList(i).obsData.finalUnwPhase  = nan(size(dsStmList(i).obsData.dphase));
    end
    
    % Combine group means of each track
    for p = 1:length(dsStmList(1).auxData.groupIdList)
        
        groupId = dsStmList(1).auxData.groupIdList(p);
        
        % Loop over all tracks in the group
        for i = 1:length(dsStmList)
            
            pointIdx = find(ismember(dsStmList(i).pntAttrib.groupId,groupId));
            
            % Loop over all points in the group
            for j = 1:length(pointIdx)
                
                % Wrapped phases
                y = wrap(dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:)');
                
                % Check for bad points
                if all(isnan(y),'all')
                    badId = dsStmList(i).pntAttrib.parcelId(pointIdx(j));
                    disp(['Discarding polygon ' num2str(badId)]);
                    
                    for l = 1:length(dsStmList)
                        badIdx  = find(ismember(dsStmList(i).pntAttrib.parcelId,badId));
                        dsStmList(i).pntAttrib.groupId(badIdx) = nan;
                    end
                else
                    % Mean "model"
                    bHat    = dsStmList(i).auxData.groupMedian(p,:)';
                    first   = find(~isnan(bHat),1);
                    bHat    = bHat - bHat(first);
                    
                    % Estimate mean bias
                    bHatw   = wrap(bHat);
                    bias    = mean(y-bHatw,'omitnan');
                    
                    % Remove nans from data
                    y(isnan(y))         = 0;
                    bHat(isnan(bHat))   = 0;
                    
                    % Get Qy from CRB of daisy-chain coherence
                    pntSel  = dsStmList(i).pntAttrib.parcelId(pointIdx(j));
                    Qy      = estimate_Qy(dsStmList(i),pntSel);
                    
                    A       = 2*pi*eye(dsStmList(i).numEpochs);
                    
                    % Float solution
                    QaHat   = inv(A'* inv(Qy) * A);
                    aHatFl  = QaHat * A' * inv(Qy) * (y-bHat-bias);
                    
                    % Integer bootstrapping
                    aHat    = LAMBDA(aHatFl,QaHat,4);
                    
                    % ILS
                    %[aHat,sqnorm]                  = LAMBDA(aHatFl,QaHat,1);
                    
                    % "PAR" ??
                    %[aHat,sqnorm,Ps,Qz,Z,nfix]     = LAMBDA(aHatFl,QaHat,5,'P0',0.95);
                    
                    % Rounding
                    %aHat                           = round(aHatFl);
                    
                    
                    % Unwrapped phase, yHat
                    yHat                = y - A*aHat;
                    goodIdx             = ~isnan(dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:));
                    unwrapped           = nan(size(yHat))';
                    unwrapped(goodIdx)  = yHat(goodIdx);
                    shifted             = unwrapped;
                    
                    % Re-evaluate vertical shifts
                    [nSegments,segIdx,segLength,~]  = segmentation(goodIdx,0.1,1);
                    for k = 1:nSegments
                        segEpochIdx                 = segIdx(k):segIdx(k)+segLength(k)-1;
                        shift                       = mean(unwrapped(segEpochIdx) - bHat(segEpochIdx)','omitnan');
                        shifted(segEpochIdx)        = unwrapped(segEpochIdx) - shift;
                    end
                    
                    
                    % For debugging
%                     figure
%                     hold on
%                     dates = dsStmList(i).epochAttrib.timestamp;
%                     plot(dates,dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:))
%                     plot(dates,shifted)
%                     plot(dates,bHat)
%                     legend('Original','Reshifted,$\hat{y}$','Estimated model, $\hat{b}$','interpreter','latex')
%                     set(gca,'fontsize',14)
%                     grid on
                    
                    % Update values
                    %dsStmList(i).obsData.vertUnwPhase(pointIdx(j),:) = shifted;
                    dsStmList(i).obsData.finalUnwPhase(pointIdx(j),:) = shifted;
                   
                end
            end
            
            
            % For debugging
            %         figure
            %         hold on
            %         plot(dates,dsStmList(i).auxData.groupMedian(p,:))
            %         plot(dates,bHat)
            %         plot(dates,median(dsStmList(i).obsData.vertUnwPhase(pointIdx,:),1,'omitnan'))
            %         legend('old','model','new')
            
            % Update group mean and median
            dsStmList(i).auxData.groupMean(p,:)   = mean(dsStmList(i).obsData.finalUnwPhase(pointIdx,:),1,'omitnan');
            dsStmList(i).auxData.groupMedian(p,:) = median(dsStmList(i).obsData.finalUnwPhase(pointIdx,:),1,'omitnan');
        end
        if mod(p,10) == 0
            disp(['Done ' num2str(p) ' of ' num2str(length(dsStmList(1).auxData.groupIdList)) ' groups'])
        end
    end
    
    %% Save intermediate product
    for i = 1:settings.numTracks
        stmwrite(dsStmList(i),[settings.segDir 'lambda_' char(settings.dsStmFilename(i))]);
    end
    
end