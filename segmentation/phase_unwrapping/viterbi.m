function [k,trellis] =  viterbi(OBS,STATE,INIT,T,E)
    
    %Coherence factor
    % C = coherence/0.5
    
    % Number of observations
    nObs = length(OBS);
    
    % Holds the probability of each state at each obs. epoch t
    trellis = zeros(length(OBS),length(STATE));
    
    % Probability of each state at t=0
%     trellis(1,:) = INIT .* E(:,OBS(1))';
    
    % Determine probability of each state assuming most likely prior state, k
    k = zeros(nObs,1);
%     [~,k(1)] = max(trellis(:,1));
    
    for t = 1:nObs 
        % Probability of each state at time t
        trellis(t,:) = T(t,:) .* E(OBS(t),:);
        
        % Renormalize
        trellis(t,:) = trellis(t,:)/sum(trellis(t,:));
        
        % State vector
        [~,k(t)] = max(trellis(t,:));
    end

end

