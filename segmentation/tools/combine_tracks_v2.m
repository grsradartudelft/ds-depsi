function [outputTs,allDates] = combine_tracks_v2(dates,trackMean,filterLen)
% Combine tracks and fill nan dates with interpolated value
% InputTT should only conatin insar data

allDates = [];
for i = 1:length(dates)
    allDates = [allDates, dates{i}];
end
allDates = sort(allDates,'ascend');

%% Linear interpolation in empty dates
for i = 1:length(dates)
    trackMeanInterp(i,:)    = interp1(dates{i}, trackMean{i}, allDates);
end

%% Realign vertically
for i = 1:length(dates)
    shift                   = mean(trackMeanInterp(i,:) - trackMeanInterp(i,:),'omitnan');
    trackMeanInterp(i,:)    = trackMeanInterp(i,:) - shift;
end

%% Take the mean across tracks
combinedMean = mean(trackMeanInterp,1,'omitnan');

%% Moving average filter (set filterLen to 1 to not filter)
outputTs = nan(size(combinedMean));
for l = (floor(filterLen/2)+1):length(combinedMean)-floor(filterLen/2)
    outputTs(l) = mean(combinedMean(l-floor(filterLen/2):l+floor(filterLen/2)));
end

end

