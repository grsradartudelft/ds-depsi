function stmdates2csv(stmList,outputFolder)

nTracks = length(stmList);
for i = 1:nTracks
    dateTable = table(stmList(i).epochAttrib.timestamp');
    dateTable.Properties.VariableNames = string(stmList(i).datasetId);
    
    writetable(dateTable,[outputFolder '/' stmList(i).datasetId '.csv'])
    
end

end

