function plot_segment_diagram_pointwise(stm,segVec,pointIdx)

figure
hold on
for i = 1:size(segVec,1)
    plot(stm.epochAttrib.timestamp,i*segVec(i,:),'LineWidth',2)
    ticklabels{i} = num2str(stm.pntAttrib.parcelId(pointIdx(i)));
end

yticks(1:length(segVec));
yticklabels(ticklabels);
title(['Pointwise Segmentation Diagram (' stm.datasetId ')'],'Interpreter', 'none')
set(gca,'FontSize',14)
set(gcf,'color','w')
grid on

end




