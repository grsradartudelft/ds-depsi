function dsStmList = detrend_seasonal(dsStmList,psStmList,groupId)


x = [];
y = [];
w = [];
for i = 1:length(dsStmList)
    ph2l            = dsStmList(i).techniqueAttrib.wavelength/(4*pi);
    groupIdx        = find(dsStmList(i).auxData.groupIdList == groupId);
    pointIdx        = find(dsStmList(i).pntAttrib.groupId == groupId);
    vertSegments    = dsStmList(i).obsData.vertUnwPhase(pointIdx,:)*ph2l; % Convert to m
    vertGroupMedian = dsStmList(i).auxData.groupMedian(groupIdx,:)*ph2l;  % Convert to m
    amplitude       = dsStmList(i).auxData.amplitude(pointIdx,2:end);
    coherence       = dsStmList(i).obsData.dcCoherence(pointIdx,:);
    rawPhase        = dsStmList(i).obsData.rawPhase(pointIdx,:);
    los2vert        = dsStmList(i).pntAttrib.los2vert(pointIdx,:);
    
    doy             = day(dsStmList(i).epochAttrib.timestamp,'dayofyear')/365.25;
    fracYear        = unwrap(doy*2*pi)/(2*pi);
    dphase          = dsStmList(i).obsData.apsPhase(pointIdx,:) - dsStmList(i).obsData.apsPhase(pointIdx,1);
    dphase          = mod(dphase+pi,2*pi)-pi;
    
    meanSeasonal    = vertGroupMedian - dsStmList(i).auxData.groupModelParams(groupIdx,3)*fracYear;
    if any(isnan(meanSeasonal))
        badIdx = find(isnan(meanSeasonal));
        for n = 1:length(badIdx)
            if badIdx(n) == 1
                meanSeasonal(badIdx(n)) = meanSeasonal(badIdx(n)+1);
            elseif badIdx(n) == length(meanSeasonal)
                meanSeasonal(badIdx(n)) = meanSeasonal(badIdx(n)-1);
            else
                meanSeasonal(badIdx(n)) = mean([meanSeasonal(badIdx(n)-1),meanSeasonal(badIdx(n)+1)],'omitnan');
            end
        end
    end
    
    nPoints         = length(pointIdx);
    opts            = optimset('display','off');
    detrendedPhase  = [];
    
    for m = 1:nPoints
        
        % Remove the linear component of the solution
        segment         = vertSegments(m,:) - dsStmList(i).auxData.groupModelParams(groupIdx,3)*fracYear;
        
        % Scale the median seasonal trend to the given segment
        cost            = @(A) sqrt(mean((segment - A*meanSeasonal).^2,'omitnan'));
        scale           = fminunc(cost,1,opts); % Initial estimate of scale parameter is 1
        
        seasonalTrend   = scale*meanSeasonal;
        seasonalTrendPh = seasonalTrend/ph2l./los2vert;
        seasonalTrendPh = seasonalTrendPh - seasonalTrendPh(1);
        seasonalTrendWr = mod(seasonalTrendPh+pi,2*pi)-pi;
    
%         % Fix unwrapping errors
% TO DO: fix on per-segment basis, then reshift, then evaluate residuals
        segmentNew = segment;
        for l = 1:length(segmentNew)
            if      abs( segmentNew(l)+2*pi*ph2l*los2vert(m) - seasonalTrend(l) ) < abs( segmentNew(l) - seasonalTrend(l) )
                segmentNew(l) = segmentNew(l) + 2*pi*ph2l*los2vert(m);
            
            elseif  abs( segmentNew(l)-2*pi*ph2l*los2vert(m) - seasonalTrend(l) ) < abs( segmentNew(l) - seasonalTrend(l) )
                segmentNew(l) = segmentNew(l) - 2*pi*ph2l*los2vert(m);
            end
        end
        
        figure
        hold on
        plot(dsStmList(i).epochAttrib.timestamp,segment)
        plot(dsStmList(i).epochAttrib.timestamp,segmentNew)
        plot(dsStmList(i).epochAttrib.timestamp,seasonalTrend)
        
        figure
        plot(dsStmList(i).epochAttrib.timestamp,(segment-seasonalTrend)/(ph2l*los2vert(m)))
        
        detrendedPhase(m,:)  = dphase(m,:) - seasonalTrendWr;
        detrendedSegs(m,:) = vertSegments(m,:) - seasonalTrend;
        
        if any(isnan(detrendedPhase(m,:)))
           disp('hi')
        end
%       
        

        

%         phasor      = amplitude(m,:) .* exp(1i*detrendedPhase(m,:));
%         phasor      = phasor/max(abs(phasor(:)));
%     
%         phasorLp    = lowpass(phasor,0.05);
%         phasorLp    = phasorLp/max(abs(phasorLp(:)));
%         I           = real(phasorLp);
%         Q           = imag(phasorLp);
    end
    
    figure
    plot(dsStmList(i).epochAttrib.timestamp,detrendedSegs)
    
%     figure
%     hold on
%     plot(dsStmList(i).epochAttrib.timestamp,I,'b')
%     plot(dsStmList(i).epochAttrib.timestamp,Q,'r')
%     grid on
   

    detrendedDelta = [zeros(size(detrendedPhase,1),1),diff(detrendedPhase,[],2)];
    unwrappedPhase = unwrap(detrendedPhase,[],2);
%     y = [y; reshape(unwrappedPhase',[],1)];
%     x = [x; repmat(fracYear',size(unwrappedPhase,1),1)];
%     w = [w; reshape(coherence',[],1)];

    y = reshape(unwrappedPhase'*ph2l*los2vert,[],1);
    x = repmat(fracYear',size(unwrappedPhase,1),1);
%     w = reshape(coherence',[],1);
    
end

% Linear regression
A       = [ones(length(x),1) x]; %321*53x2
% W       = diag(w);
xHat    = (A'*A)\A'*y; % 2x1
yHat    = [ones(length(fracYear'),1) fracYear']*xHat; % Remake A so that time vector does not repeat

linRate = xHat(2);

figure('color','w')
hold on
plot(dsStmList(i).epochAttrib.timestamp,unwrappedPhase*ph2l,'.','handlevisibility','off')
plot(dsStmList(i).epochAttrib.timestamp,yHat*ph2l,'--k','linewidth',4)
grid on
legend(['Estimated linear rate: ' num2str(linRate*1000,2) ' mm/y'] )
set(gca,'fontsize',14)
title('Detrended Unwrapped Phases')

for i = 1:length(dsStmList)
    groupIdx = find(dsStmList(i).auxData.groupIdList == groupId);
    
    newRate = linRate;
    oldRate = dsStmList(i).auxData.groupModelParams(groupIdx,3);
    
    if i == 1
        disp('_____________________________')
        disp(['Group ' num2str(groupId)])
        disp(['Old rate: ' num2str(oldRate)])
        disp(['New rate: ' num2str(newRate)])
    end
    dsStmList(i).auxData.groupModelParams(groupIdx,3) = newRate;
    
end
