function [idx] = find_stm_points_bytype(stm,soil_code,crop_code,ahn,ahn_diff,gw,gw_diff,peilgebied)
% find_stm_points_bytype.m
% (c) Delft University of Technology, 2022.
%
% Created by Philip Conroy
%
% Takes an stm object and returns all point indices which match a given criteria,
% ex. All points with a certain soil or crop code.
% To not use a certain criteria, pass an empty array to that parameter.
%
% INPUTS
% - stm: a space-time matrix object defined according to stmutil
% - refIdx: index of reference point (int)
% - distance: search for points within this radius [m] (double)
% - soil_code: search for points with matching soil code (string)
% - crop_code: search for points with matching crop code (int)
%
% OUTPUT
% - idx: logical array of stm indices which match given conditions

distIdx = true(stm.numPoints,1);
soilIdx = true(stm.numPoints,1);
cropIdx = true(stm.numPoints,1);
ahnIdx  = true(stm.numPoints,1);
gwIdx   = true(stm.numPoints,1);
peilIdx = true(stm.numPoints,1);

if ~isempty(soil_code)
    soilIdx = strcmp(stm.pntAttrib.soilcode,soil_code);
end

if ~isempty(crop_code)
    cropIdx = stm.pntAttrib.cropcode == crop_code;
end

if ~isempty(ahn)
    ahnIdx = (abs(ahn - stm.pntAttrib.ahn3) < ahn_diff);
end

if ~isempty(gw)
    deltaGW = stm.pntAttrib.ghg - stm.pntAttrib.glg;
    gwIdx = (abs(gw - deltaGW) < gw_diff);
end

if ~isempty(peilgebied)
    peilIdx = any(stm.pntAttrib.peilgebied == peilgebied,2);
end

idx = distIdx & soilIdx & cropIdx & ahnIdx & gwIdx & peilIdx;

% For debugging
% figure
% hold on
% plot(stm.pntCrd(:,2),stm.pntCrd(:,1),'k.')
% plot(stm.pntCrd(idx,2),stm.pntCrd(idx,1),'r*')
% plot(stm.pntCrd(refIdx,2),stm.pntCrd(refIdx,1),'g*')
% hold off

end

