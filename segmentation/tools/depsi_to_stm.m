function [dsStm,dsStmName,psStm,psStmName] = depsi_to_stm(dsStmName,psStmName,settings)

%% Check if this already exists
if isfile([dsStmName(1:end-7) 'depsi_stm.mat']) && isfile([psStmName(1:end-7) 'depsi_stm.mat'])
    disp('Found STM files with Depsi results.')
    dsStmName   = string([dsStmName(1:end-7) 'depsi_stm.mat']);
    psStmName   = string([psStmName(1:end-7) 'depsi_stm.mat']);
    dsStm       = stmread(dsStmName);
    psStm       = stmread(psStmName);
    
else
    disp('Converting Depsi results to STM format.')
    
    %% DS STM
    dsStm                               = stmread(dsStmName);
    
    % Select epochs to use in analysis
    if isfield(settings,'startDate') && isfield(settings,'endDate')
        epochIdx                        = dsStm.epochAttrib.timestamp >= settings.startDate & dsStm.epochAttrib.timestamp <= settings.endDate;
        dsStm.epochAttrib.timestamp     = dsStm.epochAttrib.timestamp(epochIdx);
        dsStm.epochDyear                = dsStm.epochDyear(epochIdx);
        dsStm.numEpochs                 = sum(epochIdx);
    end
    
    trackId                             = dsStm.techniqueAttrib.track;
    nIfgs                               = dsStm.numEpochs;
    
    % Read Depsi processed data
    data_out                            = load([settings.depsiDir char(trackId) '/psds_data_out_sel1.mat']); %
    data_out                            = data_out.data_out;
    
    ds_az                               = data_out.ds_az;
    ds_r                                = data_out.ds_r;
    ds_phase                            = data_out.ds_phase;
    ds_h2ph                             = data_out.ds_h2ph;
    
    % Update STMs with detrended and APS removed phases from Depsi
    dsStm.obsData.rawPhase              = dsStm.obsData.rawPhase(:,epochIdx);
    dsStm.obsData.dcCoherence           = dsStm.obsData.dcCoherence(:,epochIdx);
    dsStm.obsData.apsPhase              = ds_phase(:,epochIdx);
    dsStm.obsData.h2ph                  = ds_h2ph(:,epochIdx);
    
    % Az and range coordinates (line and pixel nums)
    dsStm.pntAttrib.az                  = ds_az;
    dsStm.pntAttrib.r                   = ds_r;
    
    % Projection to vertical
    dsStm.pntAttrib.los2vert            = 1./cosd(dsStm.pntAttrib.incAngle);
    dsStm.pntAttrib.ph2v                = dsStm.techniqueAttrib.wavelength/(4*pi) .* dsStm.pntAttrib.los2vert;
    
    % Update filename in stm list
    dsfilename                          = [dsStmName(1:end-7) 'depsi_stm.mat'];
    dsStmName                           = string(dsfilename);
    
    %% PS STM
    % Load coordinate files for PS STM
    % Check if stacks were loaded via a temp/scratch directory
    lonPath                             = char(dsStm.techniqueAttrib.lon_path);
    latPath                             = char(dsStm.techniqueAttrib.lat_path);
    if strcmp(lonPath(1:5),'/tmp/')
        lonPath                         = [settings.stackRootDir lonPath(6:end)];
        latPath                         = [settings.stackRootDir latPath(6:end)];
    end
    lat_grid                            = freadbk_quiet(latPath,dsStm.techniqueAttrib.nlines);
    lon_grid                            = freadbk_quiet(lonPath,dsStm.techniqueAttrib.nlines);
    
    % Create and fill PSC STM list
    psStm                               = stm(settings.projectName,char(trackId),'insar','DATA',psStmName);
    psStmName                           = [psStmName(1:end-7) 'depsi_stm.mat'];
    psStmName                           = string(psStmName);
    
    % Timestamp
    psStm.epochAttrib.timestamp         = dsStm.epochAttrib.timestamp;
    
    % Read Depsi processed PS data
    nPsc                                = data_out.nPsc;
    depsiRefIdx                         = data_out.ref_index;
    
    psc_az                              = data_out.psc_az;
    psc_r                               = data_out.psc_r;
    psc_phase                           = data_out.psc_phase;
    % psc_h2ph                            = data_out.psc_h2ph;
    
    % Update STMs with detrended and APS removed phases from Depsi
    psStm.obsData.apsPhase              = psc_phase(:,epochIdx);
    % psStm.obsData.h2ph                  = psc_h2ph(:,epochIdx);
    
    % Save original reference point used by depsi
    psStm.techniqueAttrib.depsiRefIdx   = depsiRefIdx;
    
    % Attribute each PSC to a coordinate pair. Height unknown so set to 0.
    for j = 1:nPsc
        psStm.pntCrd(j,:)               = [lat_grid(psc_az(j),psc_r(j)),lon_grid(psc_az(j),psc_r(j)),0];
    end
    
    % Fill in remaining attributes
    psStm.numPoints                     = nPsc;
    psStm.numEpochs                     = nIfgs;
    
    psStm.pntAttrib.az                  = psc_az;
    psStm.pntAttrib.r                   = psc_r;
    psStm.pntAttrib.type                = repmat("psc",nPsc,1);
    
    % psStm.pntAttrib.ensCoh              = data_out.psc_ens_coh;
    psStm.pntAttrib.ampDisp             = data_out.psc_amp_disp;
    
    % Read Depsi processed PSP wrapped phases
    nPsp                                = data_out.nPsp;
    
    psp_az                              = data_out.psp_az;
    psp_r                               = data_out.psp_r;
    psp_phase                           = data_out.psp_phase;
    % psp_h2ph                            = data_out.psp_h2ph;
    psp_amp_disp                        = nan([nPsp 1]);
    
    % Update PS list with PSP points
    psStm.numPoints                     = psStm.numPoints + nPsp;
    
    % Update obs data
    psStm.obsData.apsPhase              = [psStm.obsData.apsPhase; psp_phase(:,epochIdx)];
    % psStm.obsData.h2ph                  = [psStm.obsData.h2ph; psp_h2ph(:,epochIdx)];
    
    % Update point attributes
    psStm.pntAttrib.az                  = [psStm.pntAttrib.az; psp_az];
    psStm.pntAttrib.r                   = [psStm.pntAttrib.r; psp_r];
    psStm.pntAttrib.ampDisp             = [psStm.pntAttrib.ampDisp; psp_amp_disp];
    psStm.pntAttrib.type                = [psStm.pntAttrib.type; repmat("psp",nPsp,1)];
    
    for j = 1:nPsp
        psStm.pntCrd(j+nPsc,:)          = [lat_grid(psp_az(j),psp_r(j)),lon_grid(psp_az(j),psp_r(j)),0];
    end
    
    %% Save STMs
    stmwrite(dsStm,char(dsStmName))
    stmwrite(psStm,char(psStmName))
end
end

