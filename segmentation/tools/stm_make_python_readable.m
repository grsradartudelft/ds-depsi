function [pyStm] = stm_make_python_readable(stmFileName,saveFlag)

%% Read the data
pyStm                           = stmread(stmFileName);

%% Convert datetime objects to string
pyStm.epochAttrib.datenum       = datenum(pyStm.epochAttrib.timestamp);

if isfield(pyStm.auxData,'knmiDates')
    pyStm.auxData.knmiDatenum   = datenum(pyStm.auxData.knmiDates);
end
if isfield(pyStm.auxData.rnn,'timestamp')
    pyStm.auxData.rnn.datenum   = datenum(pyStm.auxData.rnn.timestamp);
end
if isfield(pyStm.auxData,'dates')
    pyStm.auxData.datenum       = datenum(pyStm.auxData.dates);
end

%% Save python-friendly version
if saveFlag == 1
    pyStmFileName = char(stmFileName);
    pyStmFileName = [pyStmFileName(1:end-4) '_py.mat'];
    stmwrite(pyStm,pyStmFileName);
end

end

