function [outputTs,usedDates] = combine_tracks(inputTT,filterLen)
% Combine tracks and fill nan dates with interpolated value
% InputTT should only conatin insar data
combinedTT = mergevars(inputTT,string(inputTT.Properties.VariableNames),'NewVariableName','all_tracks');
combinedTs = nan(height(combinedTT),1);

for l=1:length(combinedTs)
    if any(~isnan(combinedTT.all_tracks(l,:)))
        combinedTs(l) = combinedTT.all_tracks(l,~isnan(combinedTT.all_tracks(l,:)));
    end
end

%% Linear interpolation in empty dates
combinedTTInterp = timetable(inputTT.Properties.RowTimes,combinedTs);
combinedTTInterp = retime(combinedTTInterp,'daily','linear');

combinedTsInterp = combinedTTInterp.(1);
dates            = combinedTTInterp.Properties.RowTimes;

% figure
% hold on
% plot(inputTT.Properties.RowTimes,combinedTs,'linewidth',3)
% plot(combinedTTResamp.Properties.RowTimes,combinedTTResamp.(1))


% combinedTsInterp      = combinedTs;
% idx                   = isnan(combinedTs);
% t                     = 1:numel(combinedTs);
% combinedTsInterp(idx) = interp1(t(~idx), combinedTs(~idx), t(idx));


%% Moving average filter (set filterLen to 1 to not filter)
outputTs = nan(size(combinedTsInterp));
for l = (floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2)
    outputTs(l) = mean(combinedTsInterp(l-floor(filterLen/2):l+floor(filterLen/2)));
end

outputTs  = outputTs((floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2));
usedDates = dates((floor(filterLen/2)+1):length(combinedTsInterp)-floor(filterLen/2));

end

