function [idx] = find_stm_points(stm,location,distance,soil_code,crop_code,ahn_diff,ghg,glg,deltaGW)
% find_stm_points.m
% (c) Delft University of Technology, 2022.
%
% Created by Philip Conroy
%
% Takes an stm object and returns all point indices which match a given criteria,
% ex. All points within given distance, with a certain soil or crop code.
% The reference point is also returned for convenience.
% To not use a certain criteria, pass an empty array to that parameter.
%
% INPUTS
% - stm: a space-time matrix object defined according to stmutil
% - location: location center of search radius [lat,lon] (1x3 double)
% - distance: search for points within this radius [m] (double)
% - soil_code: search for points with matching soil code (string)
% - crop_code: search for points with matching crop code (int)
%
% OUTPUT
% - idx: logical array of stm indices which match given conditions

distIdx = true(stm.numPoints,1);
soilIdx = true(stm.numPoints,1);
cropIdx = true(stm.numPoints,1);
ahnIdx  = true(stm.numPoints,1);
ghgIdx  = true(stm.numPoints,1);
glgIdx  = true(stm.numPoints,1);

if ~isempty(distance)
    lat     = stm.pntCrd(:,1);
    lon     = stm.pntCrd(:,2);
    height  = zeros(size(stm.pntCrd(:,1)));
    
    [xRef,yRef,zRef]    = ell2xyz([6378137.000, 6356752.314],location(1),location(2),location(3));
    [x,y,z]             = ell2xyz([6378137.000, 6356752.314],lat,lon,height);

    % This ignores the curvature of the Earth but things are close enough 
    % that it doesnt really matter
    pntDistance = sqrt((x-xRef).^2 + (y-yRef).^2 + (z-zRef).^2 );
    distIdx = pntDistance <= distance;
end

if ~isempty(soil_code)
    soilIdx = strcmp(stm.pntAttrib.soilcode,soil_code);
end

if ~isempty(crop_code)
    cropIdx = stm.pntAttrib.cropcode == crop_code;
end

if ~isempty(ahn_diff)
    ahnIdx = (abs(stm.pntAttrib.ahn3(refIdx) - stm.pntAttrib.ahn3) < ahn_diff);
end

if ~isempty(ghg)
    ghgIdx = abs(ghg - stm.pntAttrib.ghg) < deltaGW;
end

if ~isempty(glg)
    glgIdx = abs(glg - stm.pntAttrib.glg) < deltaGW;
end

idx = distIdx & soilIdx & cropIdx & ahnIdx & ghgIdx & glgIdx;

% For debugging
% figure
% hold on
% plot(stm.pntCrd(:,2),stm.pntCrd(:,1),'k.')
% plot(stm.pntCrd(idx,2),stm.pntCrd(idx,1),'r*')
% plot(stm.pntCrd(refIdx,2),stm.pntCrd(refIdx,1),'g*')
% hold off

end

