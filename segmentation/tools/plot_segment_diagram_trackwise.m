function plot_segment_diagram_trackwise(stmList,segVec)

figure
hold on
for i = 1:length(stmList)
    plot(stmList(i).epochAttrib.timestamp,i*segVec{i},'LineWidth',2)
    ticklabels{i} = stmList(i).datasetId;
end
yticks(1:length(stmList));
yticklabels(ticklabels);
set(gca,'TickLabelInterpreter', 'none');
ylim([-length(stmList) 2*length(stmList)])
title('Trackwise Segmentation Diagram')
set(gca,'FontSize',14)
set(gcf,'color','w')
grid on

end

