function display_interferogram(stm,date)

if isfield(stm.techniqueAttrib, 'stack_path') && isfield(stm.techniqueAttrib, 'stackId') && isfield(stm.techniqueAttrib, 'master_timestamp')
    
    masterImg = datestr(stm.techniqueAttrib.master_timestamp, 'yyyymmdd');
    filename  = ['cint_srd_' stm.techniqueAttrib.stackId '.raw'];
    filepath  = [stm.techniqueAttrib.stack_path '/' date '/' filename];
    nlines    = stm.techniqueAttrib.nlines;
    imgData   = freadbk_quiet(filepath,nlines,'cpxfloat32');
    
    if strcmp(stm.techniqueAttrib.pass,'Ascending')
        imgData   = flipud(freadbk_quiet(filepath,nlines,'cpxfloat32'));
    end
        
    figure
    imagesc(-angle(imgData))
    colormap(jet)
    colorbar
    title(['Interferogram (' masterImg '-' date ') Track: ' stm.datasetId],'Interpreter','none')
    set(gcf,'color','w')
else
    disp('No stack path, project ID or master timestamp specified in STM')
end


end

