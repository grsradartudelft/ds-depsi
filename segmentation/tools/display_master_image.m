function display_master_image(stm)

if isfield(stm.techniqueAttrib, 'stack_path') && isfield(stm.techniqueAttrib, 'stackId') && isfield(stm.techniqueAttrib, 'master_timestamp')
    
    masterImg = datestr(stm.techniqueAttrib.master_timestamp, 'yyyymmdd');
    filename  = ['slc_srd_' char(stm.techniqueAttrib.stackId) '.raw'];
    filepath  = [char(stm.techniqueAttrib.stack_path) '/' masterImg '/' filename];
    nlines    = stm.techniqueAttrib.nlines;
    imgData   = freadbk_quiet(filepath,nlines,'cpxfloat32');
    
%     if strcmp(stm.techniqueAttrib.pass,'Ascending')
%         imgData   = flipud(freadbk_quiet(filepath,nlines,'cpxfloat32'));
%     end
%     if strcmp(stm.techniqueAttrib.pass,'Descending')
%         imgData   = fliplr(freadbk_quiet(filepath,nlines,'cpxfloat32'));
%     end
        
    figure
    imagesc(20*log10(abs(imgData)))
    colormap(grey)
    colorbar
    title(['Master SLC Power Track: ' stm.datasetId],'Interpreter','none')
    set(gcf,'color','w')
    
%     figure
%     imagesc(flipud(angle(imgData)))
%     colormap(jet)
%     colorbar
%     title(['Master SLC Phase Track: ' stm.datasetId],'Interpreter','none')
%     set(gcf,'color','w')
else
    disp('No stack path, project ID or master timestamp specified in STM')
end


end

