function [nSegments,segIdx,segLength,segVec] = segmentation(coherence,minCoherence,minSegLength)

% ----------------------------------------------------------------------
% File............: segmentation.m
% Version & Date..: 1.0, 09-FEB-2022
% Author..........: Philip Conroy
%                   Dept. Geoscience and Remote Sensing
%                   Delft University of Technology
% ----------------------------------------------------------------------

nSegments = 0;
%while nSegments == 0
    cohVec = coherence > minCoherence;
    
    % the variable 'segments' contains the lengths of each segement at the starting index
    % of each segement
    segments = zeros(size(cohVec));
    aa       = [0,cohVec,0];
    ii       = strfind(aa, [0 1]);
    segments(ii) = strfind(aa, [1 0]) - ii;
    segments(segments<minSegLength) = 0;
    
    segIdx = find(segments>=minSegLength);
    segLength = segments(segments>=minSegLength);
    nSegments = length(segIdx);
    
%     if nSegments == 0
%         warning('No coherent segments could be made!')
%         minCoherence = minCoherence - 0.01;
%     end
%end

segVec = nan(size(coherence));
for ii = 1:nSegments
    segVec(segIdx(ii):segIdx(ii)+segLength(ii)-1) = 1;
end

end

