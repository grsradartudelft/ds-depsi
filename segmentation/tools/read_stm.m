function [dsStm,dsStmName] = read_stm(dsStmName,settings)

%% DS STM
dsStm                               = stmread(dsStmName);

% Select epochs to use in analysis
if isfield(settings,'startDate') && isfield(settings,'endDate')
    epochIdx                        = dateshift(dsStm.epochAttrib.timestamp,'start','day') >= settings.startDate & dateshift(dsStm.epochAttrib.timestamp,'start','day') <= settings.endDate;
    dsStm.epochAttrib.timestamp     = dsStm.epochAttrib.timestamp(epochIdx);
    dsStm.epochDyear                = dsStm.epochDyear(epochIdx);
    dsStm.numEpochs                 = sum(epochIdx);
end

dsStm.obsData.rawPhase              = dsStm.obsData.rawPhase(:,epochIdx);
dsStm.obsData.dcCoherence           = dsStm.obsData.dcCoherence(:,epochIdx);

% Projection to vertical
dsStm.pntAttrib.los2vert            = 1./cosd(dsStm.pntAttrib.incAngle);
dsStm.pntAttrib.ph2v                = -1*dsStm.techniqueAttrib.wavelength/(4*pi) .* dsStm.pntAttrib.los2vert;

% Update filename in stm list
dsfilename                          = [dsStmName(1:end-7) 'depsi_stm.mat'];
dsStmName                           = string(dsfilename);

% stackData.startDate = settings.startDate;
% stackData.endDate = settings.endDate;
% dsStm = stm_add_knmi(dsStm,dsStm.techniqueAttrib.knmi_file,stackData);
% stmwrite(dsStm, dsfilename)
end

