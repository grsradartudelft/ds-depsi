function [peilgbdSel] = select_peil_clickable_plot_stm(dsStmList)
%% Test clickable plot

dsStm = dsStmList(end);
usrIn=[];
peilgbdSel=[];

groups = unique(dsStm.pntAttrib.peilgebied);

while isempty(usrIn)
    
    % Base plot of all points coloured by peilgebied
    %close all
    figure
    hold on
    plotColours = jet(length(groups));
    for m = 1:length(groups)
        pntIdx = dsStm.pntAttrib.peilgebied == groups(m);
        plot(dsStm.pntCrd(:,2), dsStm.pntCrd(:,1),'k.','HandleVisibility','off')
        plot(dsStm.pntCrd(pntIdx,2),dsStm.pntCrd(pntIdx,1),'*','color',plotColours(m,:));
        legendstr{m} = num2str(groups(m));
    end
    legend(legendstr,'location','northeastoutside')
    set(gcf,'color','w')
    
    % Get user input to select a peilgebied
    [x,y]=ginput(1);
    [~,nearestPnt]  = min(hypot(dsStm.pntCrd(:,2)-x,dsStm.pntCrd(:,1)-y));
    peilgbdSel      = [peilgbdSel dsStm.pntAttrib.peilgebied(nearestPnt)];
    
    usrIn = input('Click a peilgebied (enter to continue, any key+enter to quit): ',"s");
    
end

peilgbdSel = unique(peilgbdSel); % Remove accidental double selections
disp('Peilgebied selections: ')
disp(num2str(peilgbdSel'))

end

