function [rpnEst] = correct_rpn_stm(psStm)
% V1.0
% Philip Conroy
% TU Delft Dept. Geoscience and Remote Sensing
% 11-02-2021
%
% Checks for RPN correction and applies it using "Shenzhen" algorithm
% https://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=7208887 eqn. 15

% NOTE: Has temporal deramping been performed?
%
% "To estimate and eliminate the error of the reference point, we first 
% reduce the deformation time series of every single point by subtracting a 
% linear trend, which is an operation known as temporal deramping. Then, we 
% compute the average value of all points per epoch that is considered to 
% reflect the error of the reference point at that epoch."

rpnEst = mean(psStm.obsData.dphase,1,'omitnan');
   

