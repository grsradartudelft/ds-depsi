# V1.0
# Philip Conroy
# TU Delft Dept. Geoscience and Remote Sensing
# 12-04-2021

import numpy as np
import pandas as pd
import collections
import random
import os.path
import copy
import math

def save_rnn_prediction_output(startDate,endDate,meteoPath,dataSel,seqLen,modelPath,outputPath):
    
    import tensorflow as tf
    
    #%% Load RNN model
    modelData = read_rnn_inputs_predict(meteoPath,startDate,endDate)
    rnnInputDf = prepare_input(modelData)
    xIn = preprocess_df(rnnInputDf, dataSel, seqLen)

    # Load a previously trained model
    model = tf.keras.models.load_model(modelPath)

    # Compute probability of up/down motion
    probabilities = np.squeeze(model.predict(xIn))
    rnnOutputDf = pd.DataFrame()
    rnnOutputDf['Date'] = rnnInputDf['time'].loc[seqLen-1:]
    rnnOutputDf['Probability stay'] = probabilities[:,0]
    rnnOutputDf['Probability up'] = probabilities[:,1]
    rnnOutputDf['Probability down'] = probabilities[:,2]
    rnnOutputDf.set_index('Date', inplace=True)
    
    rnnOutputDf.to_csv(outputPath)

def run_rnn_predict_depsi(rnn_param_file):
    
    import tensorflow as tf
    import rnn_param_file as params
    
    #%% Load RNN model
    modelData = read_rnn_inputs(params.extPath,params.meteoPath)
    rnnInputDf = prepare_input(modelData)
    xIn = preprocess_df(rnnInputDf, params.dataSel, params.seqLen)

    # Load a previously trained model
    model = tf.keras.models.load_model(params.modelPath)

    # Compute probability of up/down motion
    probabilities = np.squeeze(model.predict(xIn))
    rnnOutputDf = pd.DataFrame()
    rnnOutputDf['Date'] = rnnInputDf['time'].loc[params.seqLen-1:]
    rnnOutputDf['Probability stay'] = probabilities[:,0]
    rnnOutputDf['Probability up'] = probabilities[:,1]
    rnnOutputDf['Probability down'] = probabilities[:,2]
    rnnOutputDf.set_index('Date', inplace=True)

    rnnInputDf.set_index('time', inplace=True)
    extDates = rnnOutputDf.index
    
    return rnnOutputDf

def read_rnn_inputs_predict(meteoPath, startDate, endDate):    
        
    # Read meteo data
    meteoDf = pd.read_csv(meteoPath,header=50,usecols=[1,11,22],names=['date','Temp','Rainfall'])
    meteoDf['date'] = pd.to_datetime(meteoDf['date'],format='%Y%m%d')
    meteoDf = meteoDf.loc[(meteoDf.date>=startDate) & (meteoDf.date<=endDate)]
    meteoDf['Temp'] = meteoDf['Temp'].astype(float)/10
    meteoDf['Rainfall'] = meteoDf['Rainfall'].astype(float)/10
    meteoDf.set_index('date', inplace=True)
    
    return meteoDf

def read_rnn_inputs(extPath,meteoPath):    
    
    outPath = extPath.replace('.xlsm','_combined.csv')
    
    # Check if parsed output file already exists
    if os.path.isfile(outPath):
        extDf = pd.read_csv(outPath)
        extDf['Time'] = pd.to_datetime(extDf['Time'])
        extDf.set_index('Time', inplace=True)
        return extDf
    
    
    # Read top bit of spreadsheet to find the MV -0.05 column
    # The sheets are not all formatted the same way, this mess is to try and sort it out
    extHeader = pd.read_excel(extPath,sheet_name='bewerking',header=None,skiprows=5,nrows=1)
    extHeader2 = pd.read_excel(extPath,sheet_name='bewerking',header=None,skiprows=6,nrows=1)
    colNum = np.where((extHeader=='MV -0.05m') | (extHeader=='MV -0.06 m') & ((extHeader2=='tov NAP') | (extHeader2=='hoogte tov NAP')) )[1][0]
    
    # Extract times and MV -0.05m wrt NAP column
    extData = pd.read_excel(extPath,sheet_name='bewerking',header=4,names=['Time','Extensometer'],usecols=[0,colNum],skiprows=4)
    extData['Time'] = pd.to_datetime(extData['Time'])
    extData.set_index('Time', inplace=True)
    extDf = extData.groupby(pd.Grouper(freq='1D')).mean()
    
    # Extract start and end dates to take from meteo data
    dates = extDf.index
    startDate = dates[0]
    endDate = dates[-1]
    
    # Read meteo data
    meteoDf = pd.read_csv(meteoPath,header=100,usecols=[1,11,22],names=['date','Temp','Rainfall'])
    meteoDf['date'] = pd.to_datetime(meteoDf['date'],format='%Y%m%d')
    meteoDf = meteoDf.loc[(meteoDf.date>=startDate) & (meteoDf.date<=endDate)]
    meteoDf['Temp'] = meteoDf['Temp'].astype(float)/10
    meteoDf['Rainfall'] = meteoDf['Rainfall'].astype(float)/10
    meteoDf.set_index('date', inplace=True)
    
    # Combine into one df
    extDf['Temp'] = meteoDf['Temp']
    extDf['Rainfall'] = meteoDf['Rainfall']
    
    # Remove dates with missing measurements
    extDf = extDf.dropna()
    
    # Save a csv file of the data to make subsequent runs faster
    extDf.to_csv(outPath)
    
    return extDf

def prepare_training_input(data_df, samplePeriod):
    # V1.0
    # Philip Conroy
    # TU Delft Dept. Geoscience and Remote Sensing
    # 12-04-2021
    # 
    # Reads training inputs from a csv file, normalizes the data and puts 
    # everything into a DF. Extensometer data is differenced to create an
    # "up-or-down" training target. Use for training inputs for a new model.
    
    

    # Time data
    time = data_df.index
    timestamp = np.arange(0,len(time)) # day number
    doy = time.dayofyear # day of year
    doy = doy/max(doy)
    
    # Extensometer data for training
    ext = np.array(data_df['Extensometer'])
    ext = ext[~np.isnan(ext)]
    ext = ext - ext[0]
    extIdx = range(0,len(ext))

    # Downsample to every n days, shift and repeat so all days are used
    extDS = ext[::samplePeriod]
    extIdxDS = extIdx[::samplePeriod]
    
    for i in range(1,samplePeriod):
        extDS = np.append(extDS,ext[i::samplePeriod])
        extIdxDS = np.append(extIdxDS,extIdx[i::samplePeriod])
        
    # We will try to guess 'up or down'. 1st find diffferences between epochs
    extDiff=np.zeros(extDS.shape)
    extDiffIdx=np.zeros(extDS.shape,dtype=int)
    for j in range(1,len(extDS)):
        if extIdxDS[j] - extIdxDS[j-1] == samplePeriod:
            extDiff[j] = extDS[j] - extDS[j-1]
            extDiffIdx[j] = extIdxDS[j]
        else:
            extDiff[j] = np.nan
            extDiffIdx[j] = extIdxDS[j]
    
    # Remove nan rows. (should be samplePeriod-1)
    mask = extDiffIdx >= samplePeriod
    extDiffIdx = extDiffIdx[mask]
    extDiff = extDiff[mask]
    
    # Make sure there's no errors
    if np.any(np.isnan(extDiff)):
        print('Error: downsampling error or corrupt input data')
        exit(1)
    
    #
    # extDiff = np.roll(ext,5) - ext
    # extDiff = extDiff[5:]
    extDiffSign = np.sign(extDiff)
    extDiffSign[(extDiffSign < 0)] = 0
    target = extDiffSign
    
    # Model inputs go into this df
    input_df = pd.DataFrame()
    input_df['time'] = time[extDiffIdx]
    input_df['timestamp'] = timestamp[extDiffIdx]
    input_df['doy'] = doy[extDiffIdx]
        
    if 'Rainfall' in data_df:
        rainfall = np.array(data_df['Rainfall'])
        rainfallNorm = rainfall/max(rainfall)
        rainfallNorm[rainfallNorm<0]=0
        input_df['rainfall'] = rainfallNorm[extDiffIdx]
        
    if 'Snowfall' in data_df:
        snowfall = np.array(data_df['Snowfall'])
        snowfallNorm = snowfall/max(snowfall)
        input_df['snowfall'] = snowfallNorm[extDiffIdx]
    
    if 'Temp' in data_df:
        temp = np.array(data_df['Temp'])
        tempNorm = temp/max(abs(temp))
        input_df['temp'] = tempNorm[extDiffIdx]
        
    if 'Evapotranspiration' in data_df:
        evo24 = np.array(data_df['Evapotranspiration'])
        evo24Norm = evo24/max(abs(evo24))
        input_df['evo24'] = evo24Norm[extDiffIdx]

    # Final entry is the training target
    input_df['target'] = target
    
    # Clean the final input df
    input_df = input_df.dropna()
    input_df = input_df.sort_values('time')
    input_df = input_df.reset_index(drop=True)
    return input_df

def prepare_training_input_sigdef(data_df, sigDef, samplePeriod):
    # V1.0
    # Philip Conroy
    # TU Delft Dept. Geoscience and Remote Sensing
    # 12-04-2021
    # 
    # Reads training inputs from a csv file, normalizes the data and puts 
    # everything into a DF. Extensometer data is differenced to create an
    # "up-or-down" training target. Use for training inputs for a new model.
    
    # Time data
    time = data_df.index
    timestamp = np.arange(0,len(time)) # day number
    doy = time.dayofyear # day of year
    doy = doy/366
    
    # Extensometer data for training
    ext = np.array(data_df['Extensometer'])
    #ext = ext[~np.isnan(ext)]
    offset = ext[0] # save this number to retrieve original data
    ext = ext - offset
    extIdx = range(0,len(ext))

    # Downsample to every n days, shift and repeat so all days are used
    extDS = ext[::samplePeriod]
    extIdxDS = extIdx[::samplePeriod]
    
    for i in range(1,samplePeriod):
        extDS = np.append(extDS,ext[i::samplePeriod])
        extIdxDS = np.append(extIdxDS,extIdx[i::samplePeriod])
        
    # We will try to guess 'up or down'. 1st find diffferences between epochs
    extDiff=np.zeros(extDS.shape)
    extDiffIdx=np.zeros(extDS.shape,dtype=int)
    for j in range(1,len(extDS)):
        if extIdxDS[j] - extIdxDS[j-1] == samplePeriod:
            extDiff[j] = extDS[j] - extDS[j-1]
            extDiffIdx[j] = extIdxDS[j]
        else:
            extDiff[j] = np.nan
            extDiffIdx[j] = extIdxDS[j]
    
    # Remove nan rows. (should be samplePeriod-1)
    mask = extDiffIdx >= samplePeriod
    extDiffIdx = extDiffIdx[mask]
    extDiff = extDiff[mask]
       
    # Make sure there's no errors
    if np.any(np.isnan(extDiff)):
        print('Error: downsampling error or corrupt input data')
        exit(1)
    
    # Determine sign of motion
    extDiffSign = np.zeros(extDiff.shape)
    extDiffSign[(extDiff > sigDef)] = 1
    extDiffSign[(extDiff < -sigDef)] = 2
    target = extDiffSign

    # Old way of getting differences, do not use
    # extDiff2 = np.roll(ext,6) - ext
    # extDiff2 = extDiff2[6:]
    # extDiffSign2 = np.zeros(extDiff2.shape)
    # extDiffSign2[(extDiff2 > sigDef)] = 1
    # extDiffSign2[(extDiff2 < -sigDef)] = 2
    # target2 = extDiffSign2
    
    # Model inputs go into this df
    input_df = pd.DataFrame()
    input_df['time'] = time[extDiffIdx]
    input_df['timestamp'] = timestamp[extDiffIdx]
    input_df['doy'] = doy[extDiffIdx]
    
    # Ext. data for plotting
    input_df['ext'] = ext[extDiffIdx] + offset
    input_df['ext_diff'] = extDiff
    
    if 'Rainfall' in data_df:
        rainfall = np.array(data_df['Rainfall'])
        rainfallNorm = rainfall/max(rainfall)
        rainfallNorm[rainfallNorm<0]=0
        input_df['rainfall'] = rainfallNorm[extDiffIdx]
        
    if 'Snowfall' in data_df:
        snowfall = np.array(data_df['Snowfall'])
        snowfallNorm = snowfall/max(snowfall)
        input_df['snowfall'] = snowfallNorm[extDiffIdx]
    
    if 'Temp' in data_df:
        temp = np.array(data_df['Temp'])
        tempNorm = temp/max(abs(temp))
        input_df['temp'] = tempNorm[extDiffIdx]
        
    if 'Evapotranspiration' in data_df:
        evo24 = np.array(data_df['Evapotranspiration'])
        evo24Norm = evo24/max(abs(evo24))
        input_df['evo24'] = evo24Norm[extDiffIdx]
     
    # Final entry is the training target
    input_df['target'] = target
    # input_df['target2'] = target2 # do not use: for debugging
    
    
    # Clean final df
    input_df = input_df.dropna()
    input_df = input_df.sort_values('time') 
    input_df = input_df.reset_index(drop=True)
    
    # Remove gradients calculated over gaps in data
    delta = input_df['time']-input_df['time'].shift(samplePeriod)
    idx = (input_df[delta > pd.Timedelta(samplePeriod,unit='days')]).index
    input_df = input_df.drop(idx)
    
    
    return input_df

def prepare_input(data_df):
    # V1.0
    # Philip Conroy
    # TU Delft Dept. Geoscience and Remote Sensing
    # 12-04-2021
    # 
    # Reads training inputs from a csv file, normalizes the data and puts 
    # everything into a DF. Use for inputs to a previously trained model

    # Time data
    time = data_df.index
    timestamp = np.arange(0,len(time)) # day number
    doy = time.dayofyear # day of year
    doy = doy/366
    
    # Model inputs go into this df
    input_df = pd.DataFrame()
    input_df['time'] = time[5:]
    input_df['timestamp'] = timestamp[5:]
    input_df['doy'] = doy[5:]
        
    if 'Rainfall' in data_df:
        rainfall = np.array(data_df['Rainfall'])
        rainfallNorm = rainfall/max(rainfall)
        rainfallNorm[rainfallNorm<0]=0
        input_df['rainfall'] = rainfallNorm[5:]
        
    if 'Snowfall' in data_df:
        snowfall = np.array(data_df['Snowfall'])
        snowfallNorm = snowfall/max(snowfall)
        input_df['snowfall'] = snowfallNorm[5:]
    
    if 'Temp' in data_df:
        temp = np.array(data_df['Temp'])
        tempNorm = temp/max(abs(temp))
        input_df['temp'] = tempNorm[5:]
    
    if 'Extensometer' in data_df:
        ext = np.array(data_df['Extensometer'])
        input_df['extensometer'] = ext[5:]
    
    input_df = input_df.dropna()
    input_df = input_df.reset_index()
    return input_df

def preprocess_training_df(input_df,data_sel,seq_len,pct_train,balancing):
    # V1.0
    # Philip Conroy
    # TU Delft Dept. Geoscience and Remote Sensing
    # 12-04-2021
    #
    # Takes input DF and filters for selected data types. Create an 'array' of 
    # seq_len-sized input units coupled with one training target. Shuffles and 
    # divides data into training and validation sets. 
    
    model_input = pd.DataFrame()
    
    # Select model inputs to include
    if 'doy' in data_sel:
        model_input['doy'] = input_df['doy']
        
    if 'rainfall' in data_sel:
        model_input['rainfall'] = input_df['rainfall']
        
    if 'snowfall' in data_sel:
        model_input['snowfall'] = input_df['snowfall']
        
    if 'temp' in data_sel:
        model_input['temp'] = input_df['temp']
    
    model_input['target'] = input_df['target']
    
    sequential_data = []
    prev_days = collections.deque(maxlen=seq_len)

    for i in model_input.values:
        prev_days.append([n for n in i[0:-1]])
        if len(prev_days) == seq_len:
            sequential_data.append([np.array(prev_days), i[-1]])
            
    random.shuffle(sequential_data)
    
    # Balance the data (equal number pos and neg training examples)
    if balancing:
        ups = []
        downs = []
        stays = []
        
        for seq, target in sequential_data:
            if target == 0:
                stays.append([seq,target])
            elif target == 1:
                ups.append([seq,target])
            else:
                downs.append([seq,target])
    
        count = [len(downs),len(ups),len(stays)]
    
        # Remove excess training example from larger group
        ups = ups[:np.min(count)]
        downs = downs[:np.min(count)]
        stays = stays[:np.min(count)]
    
        sequential_data = ups+downs+stays
        random.shuffle(sequential_data)
    
    # Split into train and val segments
    cutoff = int(pct_train*len(sequential_data))
    train_seq = sequential_data[:cutoff]
    val_seq = sequential_data[cutoff:]
    
    x_train = []
    y_train = []
    for seq, target in train_seq:
        x_train.append(seq)
        y_train.append(target)
    
    x_val = []
    y_val = []
    for seq, target in val_seq:
        x_val.append(seq)
        y_val.append(target)
    
    return np.array(x_train), np.array(y_train), np.array(x_val), np.array(y_val)

def preprocess_df(input_df,data_sel,seq_len):
    # V1.0
    # Philip Conroy
    # TU Delft Dept. Geoscience and Remote Sensing
    # 12-04-2021
    #
    # Takes input DF and filters for selected data types. Create an 'array' of 
    # seq_len-sized input units for a previously trained RNN
    
    model_input = pd.DataFrame()
    
    # Select model inputs to include
    if 'doy' in data_sel:
        model_input['doy'] = input_df['doy']
        
    if 'rainfall' in data_sel:
        model_input['rainfall'] = input_df['rainfall']
        
    if 'snowfall' in data_sel:
        model_input['snowfall'] = input_df['snowfall']
        
    if 'temp' in data_sel:
        model_input['temp'] = input_df['temp']
        
    #model_input['time']=input_df['time'] #for debugging, delete this
    sequential_data = []
    prev_days = collections.deque(maxlen=seq_len)

    for i in model_input.values:
        prev_days.append([n for n in i])
        if len(prev_days) == seq_len:
            sequential_data.append(np.array(prev_days))
    
    return np.array(sequential_data)

def create_hmm(wrappedTimeSeries,rnnOutputDf,usedDates,noiseStd,confusionMatrixPath):
    #%% Create hidden Markov model
        
    # Create emission matrix
    # Describes the probability of being in a given movement state based on the 
    # "observed" RNN state. Can be estimated by confusion matrix which basically 
    # describes the same thing. Constant from epoch to epoch
    confusionMatrix = pd.read_csv(confusionMatrixPath,index_col=0)
    E = np.zeros((3,3))
    for k in range(0,3):
        E[k,:] = confusionMatrix.iloc[k,:]/np.sum(confusionMatrix.iloc[k,:])
    
    # Manually change 3rd row so its not zero    
    E[2,0] = E[2,0] - 0.025
    E[2,1] = E[2,1] + 0.05
    E[2,2] = E[2,2] - 0.025
    
    # Create transition matrix
    # Current approach: probability of transition to next state (uplift, 
    # subsidence, no sig. movement) is independent of current state. Probability 
    # is based on comparing euclidean distance between upper and lower branches.
    # Values change every epoch
    
    # Initialize loop variables
    wrappedDiff = np.zeros(wrappedTimeSeries.shape[0])
    T = np.zeros((len(usedDates),3))
    
    for t in range(1,len(usedDates)):
        
        # Observed phase differences
        wrappedDiff[t] = wrappedTimeSeries[t] - wrappedTimeSeries[t-1]
        
        # Define the two possible branches (closest ambiguity levels)
        branch1 = wrappedDiff[t]
        branch2 = wrappedDiff[t] - np.sign(wrappedDiff[t])*2*np.pi
        b = (branch1, branch2)
        
        # Estimate probability of each branch
        #p1 = 1-np.abs(branch1)/(2*np.pi)
        p1 = 1-0.5*(math.erf(np.abs(branch1)-np.pi)+1)
        p2 = 1-p1
        #p2 = 1-np.abs(branch2)/(2*np.pi)
        p = (p1, p2)
        
        # Estimate the probability that the movement is greater than sigDef
        pSig = np.abs( math.erf(wrappedDiff[t]/(3*noiseStd[t]/math.sqrt(2))) )
        
        # Estimate up, down stay probabilities by conditioning branch probabilities 
        # on movement probability
        pUp = p[np.argmax(b)] * pSig
        pDown = p[np.argmin(b)] * pSig
        pStay = 1-pSig
        
        # Create transition matrix
        # State 0: stay
        # State 1: up
        # State 2: down
        # Transitions are independent of current state
        T[t,0] = pStay
        T[t,1] = pUp
        T[t,2] = pDown

    return T,E

def viterbi(OBS,STATE,INIT,T,E):
    
    #Coherence factor
    # C = coherence/0.5
    
    # Number of observations
    nObs = len(OBS)
    
    # Holds the probability of each state at each obs. epoch t
    trellis = np.zeros( (len(OBS),len(STATE)) )
    
    # Probability of each state at t=0
    trellis[0,:] = INIT * E[:,OBS[0]]
    
    # Determine probability of each state assuming most likely prior state, k
    k = np.zeros(nObs).astype(int)
    k[0] = np.argmax(trellis[:,0])
    for t in range(1, nObs):
        # k[t] = np.argmax( np.multiply(trellis[t-1,:], np.multiply( T[t,:], E[OBS[t],:] )) )
        # k[t] = np.argmax( np.multiply( T[t,:], E[OBS[t],:] ) )
        
        # Probability of each state at time t
        # trellis[t,:] = trellis[k[t],t-1] * np.multiply( T[t,:], E[OBS[t],:] )
        # trellis[t,:] = np.multiply( C[t]*T[t,:], (1-C[t])*E[OBS[t],:] )
        trellis[t,:] = np.multiply( T[t,:], E[OBS[t],:] )
        
        # Renormalize
        # trellis[t,:] = trellis[t,:]/np.sum(trellis[t,:])
        
        k[t] = np.argmax(trellis[t,:])
        
    return k, trellis 
            
def viterbi_levelwise(OBS,STATE,INIT,T,E):
    
    # Number of observations
    nObs = len(OBS)
    
    # Holds the probability of each state at each obs. epoch t
    trellis = np.zeros( (len(OBS),len(STATE)) )
    
    # Probability of each state at t=0
    trellis[0,:] = INIT * E[:,OBS[0]]
    
    # Determine probability of each state assuming most likely prior state, k
    k = np.zeros(nObs).astype(int)
    k[0] = np.argmax(trellis[:,0])
    for t in range(1, nObs):
        # k[t] = np.argmax( np.multiply(trellis[t-1,:], np.multiply( T[t,:], E[OBS[t],:] )) )
        # k[t] = np.argmax( np.multiply( T[t,:], E[OBS[t],:] ) )
        
        # Probability of each state at time t
        # trellis[t,:] = trellis[k[t],t-1] * np.multiply( T[t,:], E[OBS[t],:] )
        trellis[t,:] = np.multiply( T[t,:], E[OBS[t],:] )
        
        # Renormalize
        trellis[t,:] = trellis[t,:]/np.sum(trellis[t,:])
        
        k[t] = np.argmax(trellis[t,:])
        
    return k, trellis 
                        

def viterbi_unwrap(states,trellis,wrappedTimeSeries,usedDates):
    #%% Unwrap time series according to Viterbi path
    wrappedDiff = np.zeros(wrappedTimeSeries.shape[0])
    newTimeSeries = pd.DataFrame()
    newTimeSeries['Branch 0'] = copy.deepcopy(wrappedTimeSeries)
    
    for t in range(1,len(usedDates)):
        # Observed phase differences
        wrappedDiff[t] = wrappedTimeSeries[t] - wrappedTimeSeries[t-1]
        
        # Define the two possible branches (closest ambiguity levels)
        branch1 = wrappedDiff[t]
        branch2 = wrappedDiff[t] - np.sign(wrappedDiff[t])*2*np.pi
        
        upper = np.max((branch1,branch2))
        lower = np.min((branch1,branch2))
        
        ambiguityUp = upper - wrappedDiff[t]
        ambiguityDown = lower - wrappedDiff[t]
        
        if states[t] == 0:
            #newTimeSeries[t:] = newTimeSeries[t:] # Take the shorter branch (defualt)
            ambiguity = 0.0
            
        elif states[t] == 1:
            #newTimeSeries[t:] = newTimeSeries[t:] 
            ambiguity = ambiguityUp
            
        elif states[t] == 2:
            #newTimeSeries[t:] = newTimeSeries[t:] - wrappedDiff[t] + lower
            ambiguity = ambiguityDown
        
        # Update the time series. ambiguity will be either 0 or +/- 2pi depending on the logic above
        newTimeSeries.iloc[t:,:] = newTimeSeries.iloc[t:,:] + ambiguity
        
    return newTimeSeries