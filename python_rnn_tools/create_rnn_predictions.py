import numpy as np
import tensorflow as tf
import pandas as pd

# My libraries
from soil_rnn_tools import prepare_input
from soil_rnn_tools import preprocess_df
from soil_rnn_tools import read_rnn_inputs_predict
#from soil_rnn_tools import read_rnn_inputs

#%% Options
# Output path
outputPath = "/Users/pconroy/phd/projects/delfland/rnn/output/rnnOutputDF_delfland_zegveld_1660053007_spider.csv"

# File names
savedModel = "/Users/pconroy/phd/projects/delfland/rnn/checkpoints/zegveld_nobv_1660053007.hdf5" # Newest Zegveld
meteoPath = "/Users/pconroy/phd/projects/delfland/input/etmgeg_344_rotterdam.txt"
extPath = "/Users/pconroy/phd/projects/extensometer/feb2022/ZEG_RF_feb2022.xlsm"

# RNN settings
dataSel = ['doy','rainfall','temp']
seqLen = 12
sigDef = 3e-3

#%% Load RNN model
startDate = pd.to_datetime('20191216')
endDate = pd.to_datetime('20220531')

modelData = read_rnn_inputs_predict(meteoPath, startDate, endDate)
rnnInputDf = prepare_input(modelData)

# For debugging
# modelData2 = read_rnn_inputs(extPath,meteoPath)
# rnnInputDf2 = prepare_input(modelData2)


xIn = preprocess_df(rnnInputDf, dataSel, seqLen)
 

# Load a previously trained model
model = tf.keras.models.load_model(savedModel)

# Compute probability of up/down motion
probabilities = np.squeeze(model.predict(xIn))
rnnOutputDf = pd.DataFrame()
rnnOutputDf['Date'] = rnnInputDf['time'].loc[seqLen-1:]
rnnOutputDf['Probability stay'] = probabilities[:,0]
rnnOutputDf['Probability up'] = probabilities[:,1]
rnnOutputDf['Probability down'] = probabilities[:,2]
rnnOutputDf.set_index('Date', inplace=True)

rnnOutputDf.to_csv(outputPath)

