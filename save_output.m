function  save_output(dsStmList,psStmList,settings)

disp(['Saving output to ' char(settings.outputDir)])

for i = 1:settings.numTracks

    %% Save processed data in STM format
    stmwrite(dsStmList(i),[settings.outputDir char(settings.dsStmFilename(i))]);
    stmwrite(psStmList(i),[settings.outputDir char(settings.psStmFilename(i))]);
    
    %% Save settings file
    save([settings.outputDir 'settings.mat'],'settings');
    
%     %% Create attached json file with fields the STAC system looks for
%     jsonData = [];
% 
%     % Top-level fields
%     jsonData.type                       = "Processed dataset"; % Placeholder name for what should go in here
%     jsonData.id                         = dsStmList.datasetId;
%     jsonData.processor                  = "ds-depsi";
% 
%     % Settings
%     jsonData.settings                   = settings;
%     
%     % Properties
%     jsonData.properties.start_datetime  = dsStmList(i).epochAttrib.timestamp(1);
%     jsonData.properties.end_datetime    = dsStmList(i).epochAttrib.timestamp(end);
%     
%     % Geometry
%     jsonData.geometry                   = dsStmList(i).techniqueAttrib.aoiData.geometry;
%     jsonData.bbox                       = dsStmList(i).techniqueAttrib.aoiData.bbox;
% 
%     % Links to data
%     jsonData.links(1)                   = strcat(string(settings.outputDir), settings.dsStmFilename(i));
%     jsonData.links(2)                   = strcat(string(settings.outputDir), settings.psStmFilename(i));
% 
%     % Assets
%     jsonData.assets(1)                  = string(dsStmList(i).techniqueAttrib.shp_path);
%     jsonData.assets(2)                  = string(dsStmList(i).techniqueAttrib.knmi_file);
% 
%     
%     
%     %% Generate output file
%     if verLessThan('matlab','10.0')
%         jsonText        = jsonencode(jsonData);
%     else
%         jsonText        = jsonencode(jsonData,"PrettyPrint",true);
%     end
%     jsonFilename    = [dsStmList(i).datasetId '.json'];
%     fid             = fopen([settings.outputDir jsonFilename],'w');
%     fprintf(fid,'%s',jsonText);
%     fclose(fid);
end
end

