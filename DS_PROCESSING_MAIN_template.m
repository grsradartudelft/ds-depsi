clear
clc
close all
fclose('all');

warning('off','MATLAB:polyshape:repairedBySimplify')

addpath(genpath('/project/caroline/Share/software/ds-depsi'))
addpath(genpath('/project/caroline/Share/software/stmutil'))
addpath(genpath('/project/caroline/Share/software/geocoding_v0.9'))
addpath(genpath('/project/caroline/Share/software/LAMBDA'))

%% Read master settings file
settings = processing_settings;
if isfile([char(settings.runDir) 'settings.mat'])
    load([char(settings.runDir) 'settings.mat']);
    disp('Settings file found.')
end

%% Check for overwrite flag
if settings.overwrite == 1
    usrIn = input('Warning! Overwrite is on. All previous progress will be lost. Continue? (y/n) ','s');
    if ~(strcmp(usrIn,'y') || strcmp(usrIn,'yes'))
        disp('Quitting')
        return
    end
end

%% Identify SLC stack data
if ~isfile([char(settings.runDir) 'settings.mat'])
    settings = identify_stacks(settings);
end

%% Set up folder structure
if ~isfile([char(settings.runDir) 'settings.mat'])
    settings = create_processing_folders(settings);
    save([char(settings.runDir) 'settings.mat'],'settings');
end

%% Check for progress
if isfile([char(settings.runDir) 'progress.mat']) && settings.overwrite == 0
    load([char(settings.runDir) 'progress.mat']);
    disp('Progress file found. Skipping previously completed steps.')
    disp('If you want to rerun all steps, turn overwrite setting on or start a new run.')
else
    progress.phaseEstimation    = 0;
    progress.depsi              = 0;
    progress.segmentation       = 0;
end

%% Skip to segmentation
% Many runs use the same parcel phase estimation and depsi outputs. To 
% save time, these steps can be skipped by using the results from a
% previous run.

% Make sure the settings are the same! Otherwise you will not have data
% processed with the settings you want. 

% For now, this just copies the previous results into this run's folder!
% Not very efficient for space, so be mindful of too many runs.

if ~isempty(settings.skip.previousRunPath) && progress.depsi == 0
    
    disp('Copying initial data from previous run:')
    disp(settings.skip.previousRunPath)
    
    % Copy parcel STMs
    copyfile([settings.skip.previousRunPath 'stm/'],settings.stmDir)
    
    % Copy depsi result files
    for i = 1:length(settings.depsiStackdir)
        sourceFile = [settings.skip.previousRunPath 'depsi/' char(settings.stackId(i)) '/psds_data_out_sel1.mat'];
        destFile   = [char(settings.depsiStackdir(i)) 'psds_data_out_sel1.mat'];
        copyfile(sourceFile,destFile);
    end
    progress.phaseEstimation    = 1;
    progress.depsi              = 1;
end

%% Parcel phase estimation and contextual data linking
% This part allocates all SLC image pixels to the parcels in the shapefile
% provided. Each parcel is then multilooked and the full coherence matrix
% is formed. Equivalent phase estimation is performed and the ESM phases are 
% returned. Each set of parcel phases is linked with a single coordinate
% and the contextual data in the provided shapefile. 

if progress.phaseEstimation == 0 || settings.overwrite == 1
    ds_depsi_initialize(settings)
    progress.phaseEstimation = 1;
    save([char(settings.runDir) 'progress.mat'],'progress')
end

%% DS Depsi
% A modified version of Depsi is used to estimate the APS and orbital phase
% corrections for the DS points. 
if (progress.depsi == 0 || settings.overwrite == 1) && settings.pe.skipDepsi == 0
    
    % Create depsi param files
    settings = create_depsi_param_file(settings);
    save([char(settings.runDir) 'settings.mat'],'settings');
    
    % Run depsi once for each track
    for i = 1:settings.numTracks
        dsStmFilename = char(settings.dsStmFilename(i));
        psStmFilename = char(settings.psStmFilename(i));
        if isfile(strcat(settings.depsiStackdir(i),"psds_data_out_sel1.mat")) ...
        || (isfile(fullfile(settings.stmDir,[dsStmFilename(1:end-7) 'depsi_stm.mat'])) ...
        &&  isfile(fullfile(settings.stmDir,[psStmFilename(1:end-7) 'depsi_stm.mat'])))
            disp('Depsi results found. Skipping track.')
            
        else
            cd(settings.depsiStackdir(i)) % I hate this. Is there a better way?
            depsi_ds(char(settings.depsiParamFile(i)));
        end
    end
    
    progress.depsi = 1;
    save([char(settings.runDir) 'progress.mat'],'progress')
    cd(settings.runDir)
end

%% Time series estimation by segmentation
if progress.segmentation == 0 || settings.overwrite == 1
    [dsStmList,psStmList] = time_series_segmentation(settings);
    progress.segmentation = 1; 
    save([char(settings.runDir) 'progress.mat'],'progress');
else
    for i = 1:settings.numTracks
        dsStmList(i) = stmread([settings.segDir 'segmentation_' char(settings.dsStmFilename(i))]);
        psStmList(i) = stmread([settings.segDir 'segmentation_' char(settings.psStmFilename(i))]);
    end
end

%% Spatial ambiguity correction
dsStmList = lambdaAmbiguityEstimation(dsStmList,settings);
disp('____________________________________________________________________________')

%% Overall model test
dsStmList = test_group_models(dsStmList,settings);
disp('____________________________________________________________________________')

%% Final displacement model per parcel
dsStmList = final_displacement_models(dsStmList);
disp('____________________________________________________________________________')

%% Save output
save_output(dsStmList,psStmList,settings);

disp('____________________________________________________________________________')
disp('____________________________________________________________________________')
disp('DONE ALL :)')